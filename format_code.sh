#!/bin/bash

cargo fmt --manifest-path ./Steigen3000/Cargo.toml
cargo fmt --manifest-path ./native/paraguidecore/Cargo.toml
cargo fmt --manifest-path ./native/api/Cargo.toml
