use chrono::{Datelike, NaiveDate, Utc};
use nmea::{
    sentences::{
        gga::GgaData,
        rmc::{RmcData, RmcStatusOfFix},
    },
    ParseResult,
};
use paraguidecore::igc;
use paraguidecore::paraguide::Paraguide;
use rand::prelude::*;
use std::fs;
use std::path::Path;
mod common;
use hmac::{Hmac, Mac};
use sha2::Sha256;

type HmacSha256 = Hmac<Sha256>;

#[tokio::test]
async fn logging() {
    // Create a random root path, so we have always a clean environment
    let mut rng = rand::thread_rng();
    let mut v: f64 = rng.gen();
    v *= 10000.;
    let root = format!("/tmp/LoggingTest_{}", v);
    const IGC_SECRET: &str = "DUMMY igc secret";

    let mut pg: Paraguide<'_, _, _, common::TimeKeeper, 0> = Paraguide::new(
        common::FileHandler::new(&root),
        &common::DUMMY_DEVICE_INFO,
        IGC_SECRET,
    );

    pg.init(0.).await.unwrap();

    pg.set_pressure_temperature(0., 0.).await;
    let date = NaiveDate::from_ymd_opt(2015, 6, 30);
    let time = date
        .unwrap()
        .and_hms_nano_opt(23, 59, 59, 1_000_000_000)
        .unwrap()
        .and_local_timezone(Utc)
        .unwrap()
        .time();
    let gga_data = GgaData {
        altitude: Some(2000.),
        geoid_separation: None,
        hdop: None,
        fix_satellites: Some(5),
        fix_time: Some(time),
        fix_type: Some(nmea::sentences::FixType::Gps),
        latitude: Some(45.3),
        longitude: Some(10.),
    };
    pg.add_nmea_message(&ParseResult::GGA(gga_data))
        .await
        .unwrap();

    let rmc_data = RmcData {
        fix_date: date,
        fix_time: Some(time),
        faa_mode: None,
        lat: None,
        lon: None,
        magnetic_variation: None,
        speed_over_ground: None,
        true_course: None,
        nav_status: None,
        status_of_fix: RmcStatusOfFix::Autonomous,
    };
    pg.add_nmea_message(&ParseResult::RMC(rmc_data))
        .await
        .unwrap();

    // Start first flight
    pg.start_flight().await.unwrap();
    pg.add_battery_state(2.5).await.unwrap();
    pg.landing().await.unwrap();
    pg.step(0.1).await.unwrap(); // Processing landing

    let git_hash = env!("GIT_HASH");

    let date = date.unwrap();
    let path = Path::new(&root)
        .join("LOG")
        .join(format!("{}", date.year()))
        .join(format!("{:02}-{:02}", date.month(), date.day()));

    let y = igc::year_to_file_name_format(date.year() as usize);
    let m = igc::month_to_file_name_format(date.month() as usize);
    let d = igc::day_to_file_name_format(date.day() as usize);

    // First log file
    {
        let file = Path::new(&path).join(format!("{y}{m}{d}XPG01.igc"));
        let file = file.to_str().unwrap();
        let s = fs::read_to_string(file).unwrap();
        let p_max_alt = common::DUMMY_DEVICE_INFO.pressure_sensor_max_alt;
        let exp = format!(
            "AXPGParagd\r\n\
                    HFDTEDATE:300615,01\r\n\
                    HFPLTPILOTINCHARGE:\r\n\
                    HFGTYGLIDERTYPE:\r\n\
                    HFGIDGLIDERID:\r\n\
                    HFDTMGPSDATUM:WGS84\r\n\
                    HFRFWFIRMWAREVERSION:0.1.0\r\n\
                    HFRHWHARDWAREVERSION:3.3\r\n\
                    HFFTYFRTYPE:Martin Marmsoler,Paraguide\r\n\
                    HFGPSRECEIVER:Quectel,GNSS L76-L,5,80000\r\n\
                    HFPRSPRESSALTSENSOR:ST,LPS22HDTR,{p_max_alt}\r\n\
                    HFFRSSECURITYOK\r\n\
                    LXPGGIT{git_hash}\r\n\
                    LXPGBAT2.500\r\n"
        );

        let mut hasher = HmacSha256::new_from_slice(IGC_SECRET.as_bytes()).unwrap();
        hasher.update(exp.as_bytes());
        let result = hasher.finalize().into_bytes();
        let hex_values_upper: String = result.iter().map(|x| format!("{:02X}", x)).collect();

        let mut signature = String::new();
        for chunk in hex_values_upper.chars().collect::<Vec<_>>().chunks(29) {
            // 29 is the number of max characters per line for the g record
            let s: String = chunk.iter().collect();
            signature.push_str(format!("G{}\r\n", s).as_str());
        }
        assert_eq!(s, format!("{exp}{signature}"));
    }

    let config = "ParaguideConfig:1\n\
                       PilotName:Martin Marmsoler\n\
                       GliderType:Niviuk Artik 5\n\
                       GliderId:SomeGliderId";
    std::fs::write(Path::new(&root).join("Config.pg"), config).unwrap();

    // Start second flight
    pg.init(0.).await.unwrap(); // Reread config file
    pg.start_flight().await.unwrap();
    pg.landing().await.unwrap();
    pg.step(0.1).await.unwrap(); // Processing landing

    // Second log file
    {
        let file = Path::new(&path).join(format!("{y}{m}{d}XPG02.igc"));
        let file = file.to_str().unwrap();
        let s = fs::read_to_string(file).unwrap();
        let p_max_alt = common::DUMMY_DEVICE_INFO.pressure_sensor_max_alt;
        let exp = format!(
            "AXPGParagd\r\n\
                    HFDTEDATE:300615,02\r\n\
                    HFPLTPILOTINCHARGE:Martin Marmsoler\r\n\
                    HFGTYGLIDERTYPE:Niviuk Artik 5\r\n\
                    HFGIDGLIDERID:SomeGliderId\r\n\
                    HFDTMGPSDATUM:WGS84\r\n\
                    HFRFWFIRMWAREVERSION:0.1.0\r\n\
                    HFRHWHARDWAREVERSION:3.3\r\n\
                    HFFTYFRTYPE:Martin Marmsoler,Paraguide\r\n\
                    HFGPSRECEIVER:Quectel,GNSS L76-L,5,80000\r\n\
                    HFPRSPRESSALTSENSOR:ST,LPS22HDTR,{p_max_alt}\r\n\
                    HFFRSSECURITYOK\r\n\
                    LXPGGIT{git_hash}\r\n"
        );

        let mut hasher = HmacSha256::new_from_slice(IGC_SECRET.as_bytes()).unwrap();
        hasher.update(exp.as_bytes());
        let result = hasher.finalize().into_bytes();
        let hex_values_upper: String = result.iter().map(|x| format!("{:02X}", x)).collect();

        let mut signature = String::new();
        for chunk in hex_values_upper.chars().collect::<Vec<_>>().chunks(29) {
            // 29 is the number of max characters per line for the g record
            let s: String = chunk.iter().collect();
            signature.push_str(format!("G{}\r\n", s).as_str());
        }
        assert_eq!(s, format!("{exp}{signature}"));
    }

    if fs::exists(root.clone()).unwrap() {
        fs::remove_dir_all(root).unwrap();
    }
}
