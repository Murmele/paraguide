use paraguidecore::paraguide::Paraguide;
use rand::prelude::*;
use std::fs;
use std::path::Path;
use std::string::String;
use std::vec::Vec;
mod common;
use std::collections::HashMap;

#[tokio::test]
async fn logging() {
    // Create a random root path, so we have always a clean environment
    let mut rng = rand::thread_rng();
    let mut v: f64 = rng.gen();
    v *= 10000.;
    let root = format!("/tmp/LoggingTest_{}", v);
    const IGC_SECRET: &str = "DUMMY igc secret";

    let config = "ParaguideConfig:1\n\
                       PilotName:Martin Marmsoler\n\
                       GliderType:Niviuk Artik 5\n\
                       GliderId:SomeGliderId";
    std::fs::create_dir_all(root.clone()).unwrap();
    std::fs::write(Path::new(&root).join("Config.pg"), config).unwrap();

    let mut pg: Paraguide<'_, _, _, common::TimeKeeper, 0> = Paraguide::new(
        common::FileHandler::new(&root),
        &common::DUMMY_DEVICE_INFO,
        IGC_SECRET,
    );

    pg.init(0.).await.unwrap();
    pg.toggle_mute(); // Make config file dirty
    pg.shutdown().await.unwrap();

    let mut map = HashMap::new();
    let s: Vec<String> = fs::read_to_string(Path::new(&root).join("Config.pg"))
        .unwrap()
        .lines()
        .map(|line| line.to_string())
        .collect();
    print!("{:?}", s);
    for s in s {
        let res: Vec<String> = s.split(":").map(|line| line.to_string()).collect();
        assert!(res.len() == 2);
        map.insert(res[0].clone(), res[1].clone());
    }

    assert!(map.contains_key("Mute"));
    assert_eq!(map.get("Mute").unwrap(), "On");

    if fs::exists(root.clone()).unwrap() {
        fs::remove_dir_all(root).unwrap();
    }
}

#[tokio::test]
async fn config_read_mute_off() {
    // Create a random root path, so we have always a clean environment
    let mut rng = rand::thread_rng();
    let mut v: f64 = rng.gen();
    v *= 10000.;
    let root = format!("/tmp/LoggingTest_{}", v);
    const IGC_SECRET: &str = "DUMMY igc secret";

    let config = "ParaguideConfig:1\n\
                       PilotName:Martin Marmsoler\n\
                       GliderType:Niviuk Artik 5\n\
                       GliderId:SomeGliderId\n\
                       Mute:Off";
    std::fs::create_dir_all(root.clone()).unwrap();
    std::fs::write(Path::new(&root).join("Config.pg"), config).unwrap();

    let mut pg: Paraguide<'_, _, _, common::TimeKeeper, 0> = Paraguide::new(
        common::FileHandler::new(&root),
        &common::DUMMY_DEVICE_INFO,
        IGC_SECRET,
    );

    pg.init(0.).await.unwrap();
    assert_eq!(pg.is_mute(), false);
}

#[tokio::test]
async fn config_read_mute_on() {
    // Create a random root path, so we have always a clean environment
    let mut rng = rand::thread_rng();
    let mut v: f64 = rng.gen();
    v *= 10000.;
    let root = format!("/tmp/LoggingTest_{}", v);
    const IGC_SECRET: &str = "DUMMY igc secret";

    let config = "ParaguideConfig:1\n\
                       PilotName:Martin Marmsoler\n\
                       GliderType:Niviuk Artik 5\n\
                       GliderId:SomeGliderId\n\
                       Mute:On";
    std::fs::create_dir_all(root.clone()).unwrap();
    std::fs::write(Path::new(&root).join("Config.pg"), config).unwrap();

    let mut pg: Paraguide<'_, _, _, common::TimeKeeper, 0> = Paraguide::new(
        common::FileHandler::new(&root),
        &common::DUMMY_DEVICE_INFO,
        IGC_SECRET,
    );

    pg.init(0.).await.unwrap();
    assert_eq!(pg.is_mute(), true);
}
