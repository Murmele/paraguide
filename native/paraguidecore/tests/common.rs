use paraguidecore::file_handling::{
    self, File as FileTrait, FileHandler as FileHandlerTrait, Mode,
};
use paraguidecore::igc;
use paraguidecore::logging;
use paraguidecore::paraguide::DeviceInfo;
use std::fs;
use std::io::Write;
use std::path::{Path, PathBuf};
use std::string::String;
use std::time::{Duration, SystemTime, UNIX_EPOCH};
use tokio::time;

pub const DUMMY_DEVICE_INFO: DeviceInfo<0> = DeviceInfo {
    manufacturer: "Martin Marmsoler",
    model: "Paraguide",
    firmware_version: "0.1.0",
    hw_revision: "3.3",
    igc_fr: "Paragd",           // less than equal 6 characters!
    igc_manufacturer_id: "XPG", // X + 2 Characters

    gnss_receiver_manufacturer: "Quectel",
    gnss_receiver_model: "GNSS L76-L",
    gnss_receiver_max_alt: "80000",
    gnss_receiver_channels: "5",
    gnss_receiver_systems: &[],
    pressure_sensor_manufacturer: "ST",
    pressure_sensor_model: "LPS22HDTR",
    pressure_sensor_max_alt: "11000",
};

#[derive(Clone)]
pub struct FileHandler<'a> {
    virtual_root: &'a str,
}

impl<'a> FileHandler<'a> {
    pub fn new(virtual_root: &'a str) -> Self {
        Self { virtual_root }
    }
}

pub struct File {
    file_path: PathBuf,
    content: Vec<String>,
    next_index: usize,
    mode: Mode,
    open: bool,
}

impl File {
    fn new(file_path: PathBuf, mode: Mode) -> Result<Self, Error> {
        let create = match mode {
            Mode::WriteCreate | Mode::WriteCreateOrAppend | Mode::WriteCreateOrTruncate => true,
            _ => false,
        };

        if !create && !fs::exists(file_path.clone()).map_err(|_| Error::Unknown)? {
            return Err(Error::FileNotFound);
        }

        let l: Vec<String>;
        if let Mode::Read = mode {
            let s = fs::read_to_string(file_path.clone()).map_err(|_| Error::FileNotFound)?;
            l = s
                .lines()
                .map(|s| {
                    let mut st = s.to_string();
                    st.push_str("\r\n");
                    st
                })
                .collect();
        } else {
            l = Vec::new();
        }
        Ok(Self {
            file_path,
            content: l,
            next_index: 0,
            mode,
            open: true,
        })
    }
}

pub enum Error {
    Unknown,
    FileNotFound,
    EndOfFile,
    LineBufferTooSmall,
    FileNotOpen,
}

impl<'a> FileHandlerTrait<File> for FileHandler<'a> {
    type Error = Error;

    async fn file_exists(&self, file_path: &str) -> Result<bool, Self::Error> {
        Ok(Path::new(self.virtual_root).join(file_path).exists())
    }

    async fn open(&mut self, file_path: &str, mode: Mode) -> Result<File, Self::Error> {
        File::new(Path::new(self.virtual_root).join(file_path), mode)
    }

    async fn create_folders(&self, _path: &str) -> Result<(), Self::Error> {
        Ok(())
    }
}

impl igc::Writer for File {
    async fn push(&mut self, c: char) -> Result<(), ()> {
        self.write(&c.to_string()).await.map_err(|_| ())
    }

    async fn push_str(&mut self, content: &str) -> Result<(), ()> {
        self.write(content).await.map_err(|_| ())
    }
}

impl FileTrait for File {
    type Error = Error;
    async fn close(&mut self) -> () {
        if self.mode != Mode::Read {
            fs::create_dir_all(self.file_path.clone().parent().unwrap()).unwrap();
            let mut file;
            let mut append = false;
            let mut truncate = false;
            let mut create = false;
            match self.mode {
                Mode::Read => panic!("Unreachable"),
                Mode::WriteAppend => append = true,
                Mode::WriteCreate => create = true,
                Mode::WriteCreateOrAppend => {
                    create = true;
                    append = true;
                }
                Mode::WriteCreateOrTruncate => {
                    create = true;
                    truncate = true;
                }
                Mode::WriteTruncate => {
                    truncate = true;
                }
            }

            if fs::exists(self.file_path.clone()).unwrap() {
                let mut oo = fs::OpenOptions::new();
                if append {
                    oo.append(true);
                }
                if truncate {
                    oo.write(true).truncate(truncate);
                }
                file = oo.open(self.file_path.clone()).unwrap();
            } else if create {
                file = fs::File::create(self.file_path.clone()).unwrap();
            } else {
                panic!("File does not exist");
            }
            println!("Write content to file: {:?}", self.content);
            for line in &self.content {
                file.write(line.as_bytes()).unwrap();
            }
        }
        self.open = false;
    }

    async fn read_line(
        &mut self,
        out: &mut heapless::String<{ file_handling::LINE_LENGHT }>,
    ) -> Result<(), Self::Error> {
        out.clear();
        if !self.open {
            return Err(Error::FileNotOpen);
        }

        if self.next_index >= self.content.len() {
            Err(Error::EndOfFile)
        } else {
            let s = &self.content[self.next_index];
            out.push_str(&s).map_err(|_| Error::LineBufferTooSmall)?;
            self.next_index += 1;
            Ok(())
        }
    }

    async fn write(&mut self, text: &str) -> Result<(), Self::Error> {
        if self.open {
            self.content.push(text.to_string());
            Ok(())
        } else {
            Err(Error::FileNotOpen)
        }
    }
}

pub struct TimeKeeper {}

impl logging::Timekeeper for TimeKeeper {
    fn get_time_us() -> u64 {
        return SystemTime::now()
            .duration_since(UNIX_EPOCH)
            .unwrap()
            .as_micros() as u64;
    }
}

pub async fn sleep_us(w: u64) {
    time::sleep(Duration::from_micros(w)).await;
}
