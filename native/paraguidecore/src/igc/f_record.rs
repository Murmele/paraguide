use super::util::*;
use super::Writer;

use chrono::NaiveTime;
use heapless::String;

pub async fn create_record<const IDENTIFIERS: usize, T: Writer>(
    time_utc: NaiveTime,
    identifiers: &[u32; IDENTIFIERS],
    writer: &mut T,
) -> Result<(), ()> {
    let mut line: String<LINE_STRING_SIZE> = String::new();
    line.push('F')?;
    push_utc_time(time_utc, &mut line)?;

    for i in identifiers {
        line.push(number_to_character(*i / 10))?;
        line.push(number_to_character(*i % 10))?;
    }
    line.push_str(NEW_LINE)?;

    writer.push_str(&line).await
}

#[cfg(test)]
mod tests {
    use super::*;
    use async_std::task;

    #[test]
    fn create_f_record() {
        let mut record = Buffer::new();
        let time_utc = NaiveTime::from_hms_opt(13, 09, 22).unwrap();
        let identifiers = [99, 1, 9, 12, 37, 80];

        assert_eq!(
            task::block_on(create_record(time_utc, &identifiers, &mut record)),
            Ok(())
        );
        assert_eq!(record.to_str(), "F130922990109123780\r\n");
    }
}
