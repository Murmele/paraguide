use crate::common::Float;
#[cfg(test)]
use crate::igc::Writer;
use chrono::{NaiveTime, Timelike};
use heapless::String;

pub const NEW_LINE: &str = "\r\n";
pub const MAX_CHAR_PER_LINE: usize = 99; // According to IGC excluding new line
pub const LINE_STRING_SIZE: usize = MAX_CHAR_PER_LINE + NEW_LINE.len();

pub fn year_to_file_name_format(year: usize) -> char {
    let year = (year % 10) as u8;
    ('0' as u8 + year) as char
}

pub fn month_to_file_name_format(month: usize) -> char {
    match month {
        month if month > 0 && month <= 9 => ('0' as u8 + month as u8) as char,
        month if month > 9 && month <= 12 => ('A' as u8 + (month - 10) as u8) as char,
        _ => '0',
    }
}

pub fn day_to_file_name_format(day: usize) -> char {
    match day {
        day if day > 0 && day <= 9 => ('0' as u8 + day as u8) as char,
        day if day > 9 && day <= 31 => ('A' as u8 + (day - 10) as u8) as char,
        _ => '0',
    }
}

pub fn number_to_character(number: u32) -> char {
    if number > 9 {
        return '?';
    }
    return ('0' as u8 + number as u8) as char;
}

pub struct DMmmm {
    pub positive: bool,
    pub degree: u32,
    pub minute: u32,
    pub decimal_minutes: u32,
}

impl From<Float> for DMmmm {
    fn from(v: Float) -> Self {
        let positive = v >= 0.;
        let abs = num_traits::Float::abs(v);
        let mut degree = num_traits::Float::floor(abs) as u32;
        let minute = (abs % degree as Float) * 60.;
        let t = minute % (minute as u32) as Float;
        let mut decimal_minutes = num_traits::Float::round(t * 1000.) as u32;

        let mut minute = minute as u32;
        if decimal_minutes == 1000 {
            decimal_minutes = 0;
            minute += 1;
            if minute == 60 {
                minute = 0;
                degree += 1;
            }
        }

        DMmmm {
            positive,
            degree,
            minute,
            decimal_minutes,
        }
    }
}

pub fn push_utc_time(time_utc: NaiveTime, buffer: &mut String<LINE_STRING_SIZE>) -> Result<(), ()> {
    let h = time_utc.hour();
    let min = time_utc.minute();
    let s = time_utc.second();

    if h > 23 {
        return Err(());
    }

    buffer.push(number_to_character(h / 10))?;
    buffer.push(number_to_character(h % 10))?;
    buffer.push(number_to_character(min / 10))?;
    buffer.push(number_to_character(min % 10))?;
    buffer.push(number_to_character(s / 10))?;
    buffer.push(number_to_character(s % 10))
}

/// Helper function to fill buffer
pub fn conditional_push(
    t: &str,
    max_size: usize,
    buffer: &mut String<LINE_STRING_SIZE>,
) -> Result<(), ()> {
    if t.len() > max_size {
        let characters = max_size;
        for i in 0..characters {
            buffer.push(t.as_bytes()[i] as char)?
        }
    } else {
        buffer.push_str(t)?;
    }
    Ok(())
}

#[cfg(test)]
/// Testbuffer used in the tests
pub struct Buffer {
    b: String<LINE_STRING_SIZE>,
}

#[cfg(test)]
impl<'a> Buffer {
    pub const fn new() -> Self {
        Buffer { b: String::new() }
    }

    pub fn to_str(&'a mut self) -> &'a str {
        self.b.as_str()
    }
}

#[cfg(test)]
impl Writer for Buffer {
    async fn push(&mut self, c: char) -> Result<(), ()> {
        self.b.push(c)
    }
    async fn push_str(&mut self, content: &str) -> Result<(), ()> {
        self.b.push_str(content)
    }
}

#[cfg(test)]
mod tests {
    #[test]
    fn year_to_file_name_format() {
        assert_eq!(super::year_to_file_name_format(2015), '5');
        assert_eq!(super::year_to_file_name_format(2021), '1');
        assert_eq!(super::year_to_file_name_format(1999), '9');
    }
    #[test]
    fn month_to_file_name_format() {
        assert_eq!(super::month_to_file_name_format(1), '1');
        assert_eq!(super::month_to_file_name_format(2), '2');
        assert_eq!(super::month_to_file_name_format(3), '3');
        assert_eq!(super::month_to_file_name_format(4), '4');
        assert_eq!(super::month_to_file_name_format(5), '5');
        assert_eq!(super::month_to_file_name_format(6), '6');
        assert_eq!(super::month_to_file_name_format(7), '7');
        assert_eq!(super::month_to_file_name_format(8), '8');
        assert_eq!(super::month_to_file_name_format(9), '9');
        assert_eq!(super::month_to_file_name_format(10), 'A');
        assert_eq!(super::month_to_file_name_format(11), 'B');
        assert_eq!(super::month_to_file_name_format(12), 'C');
        assert_eq!(super::month_to_file_name_format(25), '0'); // Invalid
    }

    #[test]
    fn day_to_file_name_format() {
        assert_eq!(super::day_to_file_name_format(1), '1');
        assert_eq!(super::day_to_file_name_format(2), '2');
        assert_eq!(super::day_to_file_name_format(3), '3');
        assert_eq!(super::day_to_file_name_format(4), '4');
        assert_eq!(super::day_to_file_name_format(5), '5');
        assert_eq!(super::day_to_file_name_format(6), '6');
        assert_eq!(super::day_to_file_name_format(7), '7');
        assert_eq!(super::day_to_file_name_format(8), '8');
        assert_eq!(super::day_to_file_name_format(9), '9');
        assert_eq!(super::day_to_file_name_format(10), 'A');
        assert_eq!(super::day_to_file_name_format(11), 'B');
        assert_eq!(super::day_to_file_name_format(12), 'C');
        assert_eq!(super::day_to_file_name_format(13), 'D');
        assert_eq!(super::day_to_file_name_format(14), 'E');
        assert_eq!(super::day_to_file_name_format(15), 'F');
        assert_eq!(super::day_to_file_name_format(16), 'G');
        assert_eq!(super::day_to_file_name_format(17), 'H');
        assert_eq!(super::day_to_file_name_format(18), 'I');
        assert_eq!(super::day_to_file_name_format(19), 'J');
        assert_eq!(super::day_to_file_name_format(20), 'K');
        assert_eq!(super::day_to_file_name_format(21), 'L');
        assert_eq!(super::day_to_file_name_format(22), 'M');
        assert_eq!(super::day_to_file_name_format(23), 'N');
        assert_eq!(super::day_to_file_name_format(24), 'O');
        assert_eq!(super::day_to_file_name_format(25), 'P');
        assert_eq!(super::day_to_file_name_format(26), 'Q');
        assert_eq!(super::day_to_file_name_format(27), 'R');
        assert_eq!(super::day_to_file_name_format(28), 'S');
        assert_eq!(super::day_to_file_name_format(29), 'T');
        assert_eq!(super::day_to_file_name_format(30), 'U');
        assert_eq!(super::day_to_file_name_format(31), 'V');
        assert_eq!(super::day_to_file_name_format(105), '0'); // Invalid
    }
}
