use super::util::*;
use super::Writer;

use crate::debug;
use heapless::String;

const RECORD_DATE_PREFIX: &str = "HFDTEDATE:";
const RECORD_PILOT_PREFIX: &str = "HFPLTPILOTINCHARGE:";
const RECORD_PILOT2_PREFIX: &str = "HFCM2CREW2:";
const RECORD_GLIDER_TYPE_PREFIX: &str = "HFGTYGLIDERTYPE:";
const RECORD_GLIDER_ID_PREFIX: &str = "HFGIDGLIDERID:";
const RECORD_GPS_DATUM_PREFIX: &str = "HFDTMGPSDATUM:";
const RECORD_RFW_FIRMWARE_VERSION_PREFIX: &str = "HFRFWFIRMWAREVERSION:";
const RECORD_RHW_HARDWARE_VERSION_PREFIX: &str = "HFRHWHARDWAREVERSION:";
const RECORD_FTY_FR_TYPE_PREFIX: &str = "HFFTYFRTYPE:";
const RECORD_GPS_RECEIVER_PREFIX: &str = "HFGPSRECEIVER:";
const RECORD_PRESSURE_SENSOR_PREFIX: &str = "HFPRSPRESSALTSENSOR:";

pub const PILOT_NAME_MAX_CHARACTERS: usize = MAX_CHAR_PER_LINE - RECORD_PILOT_PREFIX.len();
pub const GLIDER_TYPE_MAX_CHARACTERS: usize = MAX_CHAR_PER_LINE - RECORD_GLIDER_TYPE_PREFIX.len();
pub const GLIDER_ID_MAX_CHARACTERS: usize = MAX_CHAR_PER_LINE - RECORD_GLIDER_ID_PREFIX.len();
pub const RFW_FIRMWARE_VERSION_MAX_LENGTH: usize =
    MAX_CHAR_PER_LINE - RECORD_RFW_FIRMWARE_VERSION_PREFIX.len();
pub const RFW_HARDWARE_VERSION_MAX_LENGTH: usize =
    MAX_CHAR_PER_LINE - RECORD_RHW_HARDWARE_VERSION_PREFIX.len();
pub const FTY_FR_TYPE_MAX_LENGTH: usize = MAX_CHAR_PER_LINE - RECORD_FTY_FR_TYPE_PREFIX.len();

pub const MAX_FLIGHT_NUMBER: usize = 99;

pub async fn create_record_date<T: Writer>(
    day: u32,
    month: u32,
    year: u32,
    mut flight_number: usize,
    writer: &mut T,
) -> Result<(), ()> {
    if day > 31 || day == 0 {
        debug!("Invalid day");
        return Err(());
    } else if month == 0 || month > 12 {
        debug!("Invalid month");
        return Err(());
    } else if flight_number > MAX_FLIGHT_NUMBER {
        flight_number = MAX_FLIGHT_NUMBER; // Limit to 2 digits. Unlikely that it happens. No problem if it happens
    }
    let mut line: String<LINE_STRING_SIZE> = String::new();
    line.push_str(RECORD_DATE_PREFIX)?;
    line.push(number_to_character(day / 10))?;
    line.push(number_to_character(day % 10))?;
    line.push(number_to_character(month / 10))?;
    line.push(number_to_character(month % 10))?;
    let y = year % 100;
    line.push(number_to_character(y / 10))?;
    line.push(number_to_character(y % 10))?;
    line.push(',')?;
    line.push(number_to_character(flight_number as u32 / 10))?;
    line.push(number_to_character(flight_number as u32 % 10))?;
    line.push_str(NEW_LINE)?;

    writer.push_str(&line).await
}

pub async fn create_record_pilot<T: Writer>(pilot_name: &str, writer: &mut T) -> Result<(), ()> {
    let mut line: String<LINE_STRING_SIZE> = String::new();
    line.push_str(RECORD_PILOT_PREFIX)?;
    const MAX_LENGTH: usize = MAX_CHAR_PER_LINE - RECORD_PILOT_PREFIX.len();
    conditional_push(pilot_name, MAX_LENGTH, &mut line)?;
    line.push_str(NEW_LINE)?;

    writer.push_str(&line).await
}

pub async fn create_record_cm2<T: Writer>(pilot2_name: &str, writer: &mut T) -> Result<(), ()> {
    let mut line: String<LINE_STRING_SIZE> = String::new();
    line.push_str(RECORD_PILOT2_PREFIX)?;
    const MAX_LENGTH: usize = MAX_CHAR_PER_LINE - RECORD_PILOT2_PREFIX.len();
    conditional_push(pilot2_name, MAX_LENGTH, &mut line)?;
    line.push_str(NEW_LINE)?;

    writer.push_str(&line).await
}

pub async fn create_record_glider_type<T: Writer>(
    glider_type: &str,
    writer: &mut T,
) -> Result<(), ()> {
    let mut line: String<LINE_STRING_SIZE> = String::new();
    line.push_str(RECORD_GLIDER_TYPE_PREFIX)?;
    const MAX_LENGTH: usize = MAX_CHAR_PER_LINE - RECORD_GLIDER_TYPE_PREFIX.len();
    conditional_push(glider_type, MAX_LENGTH, &mut line)?;
    line.push_str(NEW_LINE)?;

    writer.push_str(&line).await
}

pub async fn create_record_glider_id<T: Writer>(glider_id: &str, writer: &mut T) -> Result<(), ()> {
    let mut line: String<LINE_STRING_SIZE> = String::new();
    line.push_str(RECORD_GLIDER_ID_PREFIX)?;
    const MAX_NAME_LENGTH: usize = MAX_CHAR_PER_LINE - RECORD_GLIDER_ID_PREFIX.len();
    conditional_push(glider_id, MAX_NAME_LENGTH, &mut line)?;
    line.push_str(NEW_LINE)?;

    writer.push_str(&line).await
}

pub enum GPSDatum {
    WGS84,
}

impl Into<&'static str> for GPSDatum {
    fn into(self) -> &'static str {
        match self {
            Self::WGS84 => "WGS84",
        }
    }
}

pub async fn create_record_pos_datum<T: Writer>(datum: GPSDatum, writer: &mut T) -> Result<(), ()> {
    let mut line: String<LINE_STRING_SIZE> = String::new();
    line.push_str(RECORD_GPS_DATUM_PREFIX)?;
    line.push_str(datum.into())?;
    line.push_str(NEW_LINE)?;

    writer.push_str(&line).await
}

pub async fn create_record_rfw<T: Writer>(
    firmware_version: &str,
    writer: &mut T,
) -> Result<(), ()> {
    let mut line: String<LINE_STRING_SIZE> = String::new();
    line.push_str(RECORD_RFW_FIRMWARE_VERSION_PREFIX)?;
    const MAX_LENGTH: usize = RFW_FIRMWARE_VERSION_MAX_LENGTH;
    conditional_push(firmware_version, MAX_LENGTH, &mut line)?;
    line.push_str(NEW_LINE)?;

    writer.push_str(&line).await
}

pub async fn create_record_rhw<T: Writer>(
    hardware_revision: &str,
    writer: &mut T,
) -> Result<(), ()> {
    let mut line: String<LINE_STRING_SIZE> = String::new();
    line.push_str(RECORD_RHW_HARDWARE_VERSION_PREFIX)?;
    const MAX_LENGTH: usize = RFW_HARDWARE_VERSION_MAX_LENGTH;
    conditional_push(hardware_revision, MAX_LENGTH, &mut line)?;
    line.push_str(NEW_LINE)?;

    writer.push_str(&line).await
}

pub async fn create_record_fty<T: Writer>(
    manufacturer: &str,
    model: &str,
    writer: &mut T,
) -> Result<(), ()> {
    let mut line: String<LINE_STRING_SIZE> = String::new();
    line.push_str(RECORD_FTY_FR_TYPE_PREFIX)?;
    conditional_push(manufacturer, FTY_FR_TYPE_MAX_LENGTH, &mut line)?;
    line.push(',')?;
    conditional_push(
        model,
        FTY_FR_TYPE_MAX_LENGTH - manufacturer.len(),
        &mut line,
    )?;
    line.push_str(NEW_LINE)?;

    writer.push_str(&line).await
}

#[derive(Clone, Copy)]
pub enum GNSSSystems {
    GPS,     // US
    GALILEO, // EU
    GLONASS, // Russia
    BEIDOU,  // China
             // QZSS // Japan (not supported by IGC?)
}

impl Into<&'static str> for GNSSSystems {
    fn into(self) -> &'static str {
        match self {
            Self::GPS => "GPS",
            Self::GALILEO => "GAL",
            Self::GLONASS => "GLO",
            Self::BEIDOU => "BEI",
        }
    }
}

pub async fn create_record_gnss_receiver<const N: usize, T: Writer>(
    manufacturer: &str,
    model: &str,
    channels: &str,
    max_alt: &str,
    gnss_systems: &[GNSSSystems; N],
    writer: &mut T,
) -> Result<(), ()> {
    let mut line: String<LINE_STRING_SIZE> = String::new();
    line.push_str(RECORD_GPS_RECEIVER_PREFIX)?;
    let mut max_length = MAX_CHAR_PER_LINE - RECORD_GPS_RECEIVER_PREFIX.len();
    conditional_push(manufacturer, max_length, &mut line)?;
    line.push(',')?;
    max_length -= manufacturer.len() + 1;
    conditional_push(model, max_length, &mut line)?;
    line.push(',')?;
    max_length -= model.len() + 1;
    conditional_push(channels, max_length, &mut line)?;
    line.push(',')?;
    max_length -= channels.len() + 1;
    conditional_push(max_alt, max_length, &mut line)?;
    max_length -= max_alt.len();
    for i in 0..N {
        line.push(',')?;
        max_length -= 1;
        conditional_push(gnss_systems[i].into(), max_length, &mut line)?;
        max_length -= <GNSSSystems as Into<&'static str>>::into(gnss_systems[i]).len();
    }
    line.push_str(NEW_LINE)?;

    writer.push_str(&line).await
}

pub async fn create_record_pressure_sensor<T: Writer>(
    manufacturer: &str,
    model: &str,
    max_alt: &str,
    writer: &mut T,
) -> Result<(), ()> {
    let mut line: String<LINE_STRING_SIZE> = String::new();
    line.push_str(RECORD_PRESSURE_SENSOR_PREFIX)?;
    let mut max_length = MAX_CHAR_PER_LINE - RECORD_PRESSURE_SENSOR_PREFIX.len();
    conditional_push(manufacturer, max_length, &mut line)?;
    line.push(',')?;
    max_length -= manufacturer.len() + 1;
    conditional_push(model, max_length, &mut line)?;
    line.push(',')?;
    max_length -= model.len() + 1;
    conditional_push(max_alt, max_length, &mut line)?;
    line.push_str(NEW_LINE)?;

    writer.push_str(&line).await
}

#[derive(Clone, Copy)]
pub enum HRecordSecurity<'a> {
    OK,
    SUSPECT(&'a str),
    MSOPERATED,
}

impl<'a> HRecordSecurity<'a> {
    fn len(self) -> usize {
        Into::<&str>::into(self).len()
    }
}

impl<'a> Into<&'static str> for HRecordSecurity<'a> {
    fn into(self) -> &'static str {
        match self {
            Self::OK => "HFFRSSECURITYOK",
            Self::SUSPECT(_) => "HFFRSSECURITYSUSPECT",
            Self::MSOPERATED => "HFFRSSECURITYMSOPERATED",
        }
    }
}

pub async fn create_record_security<'a, T: Writer>(
    s: HRecordSecurity<'a>,
    writer: &mut T,
) -> Result<(), ()> {
    let mut line: String<LINE_STRING_SIZE> = String::new();
    line.push_str(Into::<&str>::into(s))?;
    if let HRecordSecurity::SUSPECT(t) = s {
        line.push(':')?;
        conditional_push(t, MAX_CHAR_PER_LINE - s.len() - 1, &mut line)?;
    }
    line.push_str(NEW_LINE)?;

    writer.push_str(&line).await
}

#[cfg(test)]
mod tests {
    use super::*;
    use async_std::task;

    #[test]
    fn h_record_date() {
        let mut buffer = Buffer::new();
        assert_eq!(
            task::block_on(create_record_date(21, 12, 23, 1, &mut buffer)),
            Ok(())
        );
        assert_eq!(buffer.to_str(), "HFDTEDATE:211223,01\r\n");
    }

    #[test]
    fn h_record_date2() {
        let mut buffer = Buffer::new();
        assert_eq!(
            task::block_on(create_record_date(1, 03, 09, 102, &mut buffer)),
            Ok(())
        );
        assert_eq!(buffer.to_str(), "HFDTEDATE:010309,99\r\n");
    }

    #[test]
    fn h_record_pilot() {
        let mut buffer = Buffer::new();
        assert_eq!(
            task::block_on(create_record_pilot("Max Mustermann", &mut buffer)),
            Ok(())
        );
        assert_eq!(buffer.to_str(), "HFPLTPILOTINCHARGE:Max Mustermann\r\n");
    }

    #[test]
    fn h_record_glider_type() {
        let mut buffer = Buffer::new();
        assert_eq!(
            task::block_on(create_record_glider_type("Test Glider 123", &mut buffer)),
            Ok(())
        );
        assert_eq!(buffer.to_str(), "HFGTYGLIDERTYPE:Test Glider 123\r\n");
    }
}
