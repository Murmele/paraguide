use super::util::*;
use super::Writer;
use crate::debug;
use heapless::String;

/// Creates an "A record" and stores the result in the `record` string
/// # Arguments
///
/// * `manufacturer_id` - The manufacturer id. If the id is not approved by IGC the first letter must be a X. Paraguide uses the value of MANUFACTURER_ID
/// * `unique_fr` - A 6 digit unique FR number for every device
/// * `additional_data` - additional data can be stored in the A record.
pub async fn create_record<T: Writer>(
    manufacturer_id: &str,
    unique_fr: &str,
    additional_data: &str,
    writer: &mut T,
) -> Result<(), ()> {
    let mut line: String<LINE_STRING_SIZE> = String::new();
    if manufacturer_id.len() != 3 {
        debug!("Invalid manufacturer ID. The manufacturer id must be 3 characters long");
        return Err(());
    } else if unique_fr.len() > 6 {
        debug!("Invalid unique fr. The unique fr must have a maximum of 6 characters");
        return Err(());
    } else if additional_data.len() > MAX_CHAR_PER_LINE - 3 - 6 - 1 - 1 {
        // -1 because of the hyphen, -1 because of the A character
        debug!("Additional data contains to many characters!");
        return Err(());
    }
    line.push('A')?;
    line.push_str(manufacturer_id)?;
    for _ in 0..6 - unique_fr.len() {
        line.push('0')?; // leading zeroes
    }
    line.push_str(unique_fr)?;
    if !additional_data.is_empty() {
        line.push('-')?;
        line.push_str(additional_data)?;
    }
    line.push_str(NEW_LINE)?;

    writer.push_str(&line).await
}

#[cfg(test)]
mod tests {
    use super::*;
    use async_std::task;

    #[test]
    fn a_record() {
        let mut record = Buffer::new();
        assert_eq!(
            task::block_on(create_record("XPG", "ABCD", "lskdjf", &mut record)),
            Ok(())
        );
        assert_eq!(record.to_str(), "AXPG00ABCD-lskdjf\r\n");
    }
}
