use super::util::*;
use super::Writer;

use crate::common::Float;
use chrono::NaiveTime;
use heapless::String;

pub struct Record {
    pub time_utc: Time,
    pub latitude: Float,
    pub longitude: Float,
    pub fix_validity: FixValidity,
    pub press_alt: i32,
    pub gnss_alt: i32,
}

#[derive(PartialEq, Debug)]
pub enum FixValidity {
    A, // 3D Fix
    V, // 2D Fix or no Fix
}

impl Into<char> for FixValidity {
    fn into(self) -> char {
        match self {
            Self::A => 'A',
            Self::V => 'V',
        }
    }
}

pub struct Time {
    hours: usize,
    minutes: usize,
    seconds: usize,
}

impl Time {
    pub const fn to_seconds(&self) -> u32 {
        self.hours as u32 * 3600 + self.minutes as u32 * 60 + self.seconds as u32
    }
}

pub async fn create_record<T: Writer>(
    time_utc: NaiveTime,
    latitude: Float,
    longitude: Float,
    fix_validity: FixValidity,
    pressure_alt: Float,
    gnss_alt: Float,
    writer: &mut T,
) -> Result<(), ()> {
    let mut line: String<LINE_STRING_SIZE> = String::new();
    line.push('B')?;

    push_utc_time(time_utc, &mut line)?;

    let latitude = DMmmm::from(latitude);
    let degree = latitude.degree % 90; // +-90 degree
    line.push(number_to_character(degree / 10))?;
    line.push(number_to_character(degree % 10))?;
    line.push(number_to_character(latitude.minute / 10))?;
    line.push(number_to_character(latitude.minute % 10))?;
    let mut decimal_minutes = latitude.decimal_minutes;
    line.push(number_to_character(decimal_minutes / 100))?;
    decimal_minutes %= 100;
    line.push(number_to_character(decimal_minutes / 10))?;
    decimal_minutes %= 10;
    line.push(number_to_character(decimal_minutes))?;
    if latitude.positive {
        line.push('N')?;
    } else {
        line.push('S')?;
    }

    let longitude = DMmmm::from(longitude);
    let degree = longitude.degree % 180; // +-180 degree
    line.push(number_to_character(degree / 100))?;
    line.push(number_to_character((degree % 100) / 10))?;
    line.push(number_to_character(degree % 10))?;
    line.push(number_to_character(longitude.minute / 10))?;
    line.push(number_to_character(longitude.minute % 10))?;
    let mut decimal_minutes = longitude.decimal_minutes;
    line.push(number_to_character(decimal_minutes / 100))?;
    decimal_minutes %= 100;
    line.push(number_to_character(decimal_minutes / 10))?;
    decimal_minutes %= 10;
    line.push(number_to_character(decimal_minutes))?;
    if longitude.positive {
        line.push('E')?;
    } else {
        line.push('W')?;
    }

    line.push(Into::<char>::into(fix_validity))?;

    let negative = pressure_alt < 0.;
    if negative {
        line.push('-')?;
    }
    let mut pressure_alt = num_traits::Float::abs(pressure_alt) as u32;
    if !negative {
        line.push(number_to_character(pressure_alt / 10000))?;
        pressure_alt = pressure_alt % 10000;
    }
    line.push(number_to_character(pressure_alt / 1000))?;
    pressure_alt = pressure_alt % 1000;
    line.push(number_to_character(pressure_alt / 100))?;
    pressure_alt = pressure_alt % 100;
    line.push(number_to_character(pressure_alt / 10))?;
    pressure_alt = pressure_alt % 10;
    line.push(number_to_character(pressure_alt))?;

    let negative = gnss_alt < 0.;
    if negative {
        line.push('-')?;
    }
    let mut gnss_alt = num_traits::Float::abs(gnss_alt) as u32;
    if !negative {
        line.push(number_to_character(gnss_alt / 10000))?;
        gnss_alt = gnss_alt % 10000;
    }
    line.push(number_to_character(gnss_alt / 1000))?;
    gnss_alt = gnss_alt % 1000;
    line.push(number_to_character(gnss_alt / 100))?;
    gnss_alt = gnss_alt % 100;
    line.push(number_to_character(gnss_alt / 10))?;
    gnss_alt = gnss_alt % 10;
    line.push(number_to_character(gnss_alt))?;
    line.push_str(NEW_LINE)?;

    writer.push_str(&line).await
}

pub fn parse_record(record: &str) -> super::Record {
    const B_RECORD_CHARACTER: usize = 1;
    const TIME_UTC_START_POS: usize = B_RECORD_CHARACTER;
    const TIME_UTC_BYTES: usize = 6;
    const LATITUDE_START_POS: usize = TIME_UTC_START_POS + TIME_UTC_BYTES;
    const LATITUDE_BYTES: usize = 8;
    const LONGITUDE_START_POS: usize = LATITUDE_START_POS + LATITUDE_BYTES;
    const LONGITUDE_BYTES: usize = 9;
    const FIX_VALIDITY_START_POS: usize = LONGITUDE_START_POS + LONGITUDE_BYTES;
    const FIX_VALIDITY_BYTES: usize = 1;
    const PRESS_ALT_START_POS: usize = FIX_VALIDITY_START_POS + FIX_VALIDITY_BYTES;
    const PRESS_ALT_BYTES: usize = 5;
    const GNSS_ALT_START_POS: usize = PRESS_ALT_START_POS + PRESS_ALT_BYTES;
    const GNSS_ALT_BYTES: usize = 5;

    if record.len()
        != B_RECORD_CHARACTER
            + TIME_UTC_BYTES
            + LATITUDE_BYTES
            + LONGITUDE_BYTES
            + FIX_VALIDITY_BYTES
            + PRESS_ALT_BYTES
            + GNSS_ALT_BYTES
    {
        return super::Record::None;
    }

    let hours = match record[TIME_UTC_START_POS..TIME_UTC_START_POS + 2].parse::<usize>() {
        Ok(v) => v,
        Err(_) => return super::Record::None,
    };

    let minutes = match record[TIME_UTC_START_POS + 2..TIME_UTC_START_POS + 2 + 2].parse::<usize>()
    {
        Ok(v) => v,
        Err(_) => return super::Record::None,
    };

    let seconds =
        match record[TIME_UTC_START_POS + 2 + 2..TIME_UTC_START_POS + 2 + 2 + 2].parse::<usize>() {
            Ok(v) => v,
            Err(_) => return super::Record::None,
        };

    let mut latitude: Float = 0.0;
    match record[LATITUDE_START_POS..LATITUDE_START_POS + 2].parse::<i32>() {
        Ok(v) => latitude += v as Float,
        Err(_) => return super::Record::None,
    };

    match record[LATITUDE_START_POS + 2..LATITUDE_START_POS + 2 + 2].parse::<i32>() {
        Ok(v) => latitude += v as Float / 60.0,
        Err(_) => return super::Record::None,
    };

    match record[LATITUDE_START_POS + 2 + 2..LATITUDE_START_POS + 2 + 2 + 3].parse::<i32>() {
        Ok(v) => latitude += v as Float / 3600.0,
        Err(_) => return super::Record::None,
    };

    match record[LATITUDE_START_POS + 2 + 2 + 3..LATITUDE_START_POS + 2 + 2 + 3 + 1]
        .chars()
        .next()
    {
        Some(c) => {
            match c {
                'N' => {} // Nothing to do
                'S' => latitude *= -1.0,
                _ => return super::Record::None,
            }
        }
        None => return super::Record::None,
    }

    let mut longitude: Float = 0.0;
    match record[LONGITUDE_START_POS..LONGITUDE_START_POS + 3].parse::<i32>() {
        Ok(v) => longitude += v as Float,
        Err(_) => return super::Record::None,
    };

    match record[LONGITUDE_START_POS + 3..LONGITUDE_START_POS + 3 + 2].parse::<i32>() {
        Ok(v) => longitude += v as Float / 60.0,
        Err(_) => return super::Record::None,
    };

    match record[LONGITUDE_START_POS + 3 + 2..LONGITUDE_START_POS + 3 + 2 + 3].parse::<i32>() {
        Ok(v) => longitude += v as Float / 3600.0,
        Err(_) => return super::Record::None,
    };

    match record[LONGITUDE_START_POS + 3 + 2 + 3..LONGITUDE_START_POS + 3 + 2 + 3 + 1]
        .chars()
        .next()
    {
        Some(c) => {
            match c {
                'E' => {} // Nothing to do
                'W' => longitude *= -1.0,
                _ => return super::Record::None,
            }
        }
        None => return super::Record::None,
    }

    let r = &record[FIX_VALIDITY_START_POS..FIX_VALIDITY_START_POS + FIX_VALIDITY_BYTES];
    let fix_validity = match r {
        "A" => FixValidity::A,
        "V" => FixValidity::V,
        _ => return super::Record::None,
    };

    let press_alt =
        match record[PRESS_ALT_START_POS..PRESS_ALT_START_POS + PRESS_ALT_BYTES].parse::<i32>() {
            Ok(v) => v,
            Err(_) => return super::Record::None,
        };

    let gnss_alt =
        match record[GNSS_ALT_START_POS..GNSS_ALT_START_POS + GNSS_ALT_BYTES].parse::<i32>() {
            Ok(v) => v,
            Err(_) => return super::Record::None,
        };

    super::Record::B(Record {
        time_utc: Time {
            hours,
            minutes,
            seconds,
        },
        latitude,
        longitude,
        fix_validity,
        press_alt,
        gnss_alt,
    })
}

#[cfg(test)]
mod tests {
    use super::*;

    use async_std::task;

    #[test]
    fn parse_b_record() {
        let res = parse_record("B1650174548846N00614790EA0116801269");
        match res {
            super::super::Record::B(b) => {
                assert_eq!(b.time_utc.hours, 16);
                assert_eq!(b.time_utc.minutes, 50);
                assert_eq!(b.time_utc.seconds, 17);
                assert_eq!(b.latitude, 45. + 48. / 60. + 846. / 3600.);
                assert_eq!(b.longitude, 6. + 14. / 60. + 790. / 3600.);
                assert_eq!(b.fix_validity, FixValidity::A);
                assert_eq!(b.press_alt, 1168);
                assert_eq!(b.gnss_alt, 1269);
            }
            _ => assert!(false),
        }
    }

    #[test]
    fn parse_b_record_south_west() {
        let res = parse_record("B1650170128009S07849001WV0626806263");
        match res {
            super::super::Record::B(b) => {
                assert_eq!(b.time_utc.hours, 16);
                assert_eq!(b.time_utc.minutes, 50);
                assert_eq!(b.time_utc.seconds, 17);
                assert_eq!(
                    (b.latitude * 1e6 as Float).round() / 1e6 as Float,
                    -1.469167
                );
                assert_eq!(
                    (b.longitude * 1e6 as Float).round() / 1e6 as Float,
                    -78.816944
                );
                assert_eq!(b.fix_validity, FixValidity::V);
                assert_eq!(b.press_alt, 6268);
                assert_eq!(b.gnss_alt, 6263);
            }
            _ => assert!(false),
        }
    }

    #[test]
    fn create_b_record_north_west() {
        let mut record = Buffer::new();
        // 72.289, -45.220
        // 72°17'20.4"N 45°13'12.0"W
        assert_eq!(
            task::block_on(create_record(
                NaiveTime::from_hms_opt(12, 03, 55).unwrap(),
                72.289,
                -45.220,
                FixValidity::A,
                2_892.23,
                30_123.348,
                &mut record,
            )),
            Ok(())
        );
        assert_eq!(record.to_str(), "B1203557217340N04513200WA0289230123\r\n");
        assert_eq!(record.to_str().len(), 37);
    }

    #[test]
    fn create_b_record_south_east() {
        let mut record = Buffer::new();
        // -30.316/139.021
        // 30°18'57.6"S 139°01'15.6"E
        assert_eq!(
            task::block_on(create_record(
                NaiveTime::from_hms_opt(06, 15, 09).unwrap(),
                -30.316,
                139.021,
                FixValidity::V,
                10.123523,
                900.234,
                &mut record,
            )),
            Ok(())
        );
        assert_eq!(record.to_str(), "B0615093018960S13901260EV0001000900\r\n");
        assert_eq!(record.to_str().len(), 37);
    }

    #[test]
    fn create_b_record_small_values() {
        let mut record = Buffer::new();
        // -4.570000, -5.800000
        // 4°34'12.0"S 5°48'00.0"W
        assert_eq!(
            task::block_on(create_record(
                NaiveTime::from_hms_opt(0, 0, 0).unwrap(),
                -4.57,
                -5.8,
                FixValidity::V,
                0.123523,
                1.234,
                &mut record,
            )),
            Ok(())
        );
        //let s = record.as_str(); // To debug
        assert_eq!(record.to_str(), "B0000000434200S00548000WV0000000001\r\n");
        assert_eq!(record.to_str().len(), 37);
    }

    #[test]
    fn create_b_record_overflow_seconds_minutes() {
        let mut record = Buffer::new();
        // -4.999806, -41.999889
        // 4°59'59.3"S 41°59'59.6"W
        assert_eq!(
            task::block_on(create_record(
                NaiveTime::from_hms_opt(03, 59, 23).unwrap(),
                -4.999806,
                -41.999889,
                FixValidity::V,
                -23.,
                -230.,
                &mut record,
            )),
            Ok(())
        );
        assert_eq!(record.to_str(), "B0359230459988S04159993WV-0023-0230\r\n");
        assert_eq!(record.to_str().len(), 37);
    }
}
