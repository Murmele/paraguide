use super::Writer;
use super::{util, NEW_LINE};
use heapless::String;
// use sha2::{Digest, Sha256};

const SECURITY_LENGTH_LINE: usize = 29; // Number of symbols in one line
const G_RECORD_LINE_LENGTH: usize = 1 + SECURITY_LENGTH_LINE + util::NEW_LINE.len();

async fn check_length<const L: usize, T: Writer>(
    line: &mut String<L>,
    writer: &mut T,
    count: &mut usize,
) -> Result<(), ()> {
    if (*count % (SECURITY_LENGTH_LINE + 1)) == 0 {
        if line.len() > 0 {
            line.push_str(NEW_LINE)?;
            writer.push_str(&line).await?;
        }
        line.clear();
        line.push('G')?;
        *count += 1;
    }
    Ok(())
}

/// Creating the g record from the security string. The security string is a encrypted hash
/// of the data in the IGC file.
/// As example sha256 can be used for hashing and rsa for encrypting the data
pub async fn create_record<T: Writer>(security: &[u8], writer: &mut T) -> Result<(), ()> {
    let mut line: String<G_RECORD_LINE_LENGTH> = String::new();

    let mut count = 0;
    for s in security.into_iter() {
        let upper = s >> 4;
        let lower = s & 0xF;

        check_length(&mut line, writer, &mut count).await?;
        line.push(hex2char(upper))?;
        count += 1;
        check_length(&mut line, writer, &mut count).await?;
        line.push(hex2char(lower))?;
        count += 1;
    }
    // Write the rest
    if line.len() > 1 {
        // First could be a single 'G'
        line.push_str(NEW_LINE)?;
        writer.push_str(&line).await?;
    }
    Ok(())
}

fn hex2char(value: u8) -> char {
    match value {
        0 => '0',
        1 => '1',
        2 => '2',
        3 => '3',
        4 => '4',
        5 => '5',
        6 => '6',
        7 => '7',
        8 => '8',
        9 => '9',
        10 => 'A',
        11 => 'B',
        12 => 'C',
        13 => 'D',
        14 => 'E',
        15 => 'F',
        _ => 'F', // Invalid
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[tokio::test]
    async fn create() {
        let mut record = util::Buffer::new();

        let code = [
            0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23,
            24, 25, 26, 27, 28, 29, 30, 31, 32, 33, 34, 35, 36, 37, 38, 39, 40, 41, 42, 43,
        ];
        assert!(code.len() > SECURITY_LENGTH_LINE);
        assert!((code.len() % SECURITY_LENGTH_LINE) != 0); // Not a complete record
        assert_eq!(create_record(&code, &mut record).await, Ok(()));
        assert_eq!(
            record.to_str(),
            "G000102030405060708090A0B0C0D0\r\n\
                                     GE0F101112131415161718191A1B1C\r\n\
                                     G1D1E1F202122232425262728292A2\r\n\
                                     GB\r\n"
        );
    }
}
