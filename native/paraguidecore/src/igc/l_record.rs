use super::util::*;
use heapless::Vec;

pub enum InputSource<'a> {
    Manufacturer(&'a str),
    Pilot,
    OO,
    AfterFlightPilot,
}

impl<'a> Into<&'a [u8]> for InputSource<'a> {
    fn into(self) -> &'a [u8] {
        match self {
            Self::Manufacturer(t) => t.as_bytes(),
            Self::Pilot => "PLT".as_bytes(),
            Self::OO => "OOI".as_bytes(),
            Self::AfterFlightPilot => "PFC".as_bytes(),
        }
    }
}

pub fn create_record<const N: usize>(
    source: InputSource,
    text: &str,
    record: &mut Vec<u8, N>,
) -> Result<(), ()> {
    record.extend_from_slice(source.into())?;
    record.extend_from_slice(text.as_bytes())?;
    record.extend_from_slice(NEW_LINE.as_bytes())
}

#[cfg(test)]
mod tests {
    use heapless::Vec;

    use super::create_record;

    #[test]
    fn test_create_record() {
        let mut record: Vec<u8, 16> = Vec::new();
        create_record(super::InputSource::Pilot, "TestText123", &mut record).unwrap();

        assert_eq!(
            record.as_slice(),
            [
                'P' as u8, 'L' as u8, 'T' as u8, 'T' as u8, 'e' as u8, 's' as u8, 't' as u8,
                'T' as u8, 'e' as u8, 'x' as u8, 't' as u8, '1' as u8, '2' as u8, '3' as u8,
                '\r' as u8, '\n' as u8
            ]
        );
    }
}
