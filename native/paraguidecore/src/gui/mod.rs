use crate::common;
use widgets;
use page;

struct Point {
    x: common::Float,
    y: common::Float,
}

trait Renderer {
    fn draw_line(p1: Point, p2: Point);
    fn draw_rectangle(p1: Point, p2: Point);
    fn draw_circle(pos: Point, radius: common::Float);
    fn draw_text(pos: Point, text: &str);
}
