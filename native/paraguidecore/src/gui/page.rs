use heapless;

const MAX_WIDGETS_PER_PAGE: usize = 10;

struct Page {
    widgets: heapless::Vec<PointerToWidget, MAX_WIDGETS_PER_PAGE>
}

