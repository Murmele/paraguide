use crate::gui;
mod digital_vario;

pub trait Widget {
    fn paint(painter: &gui::GuiInterface);
    fn update_state(state: &mut State);
    fn write_settings(writer: &mut ConfigWriter);
    fn read_settings(reader: &mut ConfigReader);
}