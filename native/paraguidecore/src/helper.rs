use crate::common::Float;
use chrono::Datelike;
use chrono::NaiveDate;
use heapless::String;
use num_traits::Float as FloatTrait;

pub fn number_to_character(number: u32) -> Result<char, ()> {
    if number > 9 {
        return Err(());
    }
    return Ok((0x30 + number as u8) as char);
}

fn calculate_factor(digits: usize) -> u32 {
    let mut num = 1;
    for _ in 0..digits {
        num *= 10;
    }
    num
}

pub fn float_to_string<const N: usize>(
    num: Float,
    decimal_digits: usize,
    string: &mut String<N>,
) -> Result<(), ()> {
    if num < 0. {
        string.push('-')?;
    }
    let mut factor = calculate_factor(decimal_digits) as i32;
    let num = (num * factor as Float).round().abs() as i32;
    unum_to_string((num / factor) as u32, 0, string)?;
    string.push('.')?;

    let mut decimal_part = (num % factor).abs();
    factor /= 10;
    while factor >= 1 {
        string.push(number_to_character((decimal_part / factor) as u32)?)?;
        decimal_part = decimal_part % factor;
        factor /= 10;
    }
    Ok(())
}

pub fn float_to_string_2_char<const N: usize>(
    num: Float,
    decimal_digits: usize,
    string: &mut String<N>,
) -> Result<(), ()> {
    if num.abs() < 10. {
        string.push(' ')?;
    }
    if num < 0. {
        string.push('-')?;
    } else {
        string.push(' ')?;
    }

    let mut factor = calculate_factor(decimal_digits) as i32;
    let num = (num * factor as Float).round().abs() as i32;
    unum_to_string((num / factor) as u32, 0, string)?;
    string.push('.')?;

    let mut decimal_part = num % factor;
    factor /= 10;
    while factor >= 1 {
        string.push(number_to_character((decimal_part / factor) as u32)?)?;
        decimal_part = decimal_part % factor;
        factor /= 10;
    }
    Ok(())
}

pub fn num_to_string<const N: usize>(
    num: i32,
    mut min_digits: usize,
    string: &mut String<N>,
) -> Result<(), ()> {
    if num < 0 {
        string.push('-')?;
        if min_digits > 0 {
            min_digits -= 1
        }
        unum_to_string(num.abs() as u32, min_digits, string)
    } else {
        unum_to_string(num as u32, min_digits, string)
    }
}

/// Pasing a number to string `string` with at least `min_digits`. If there are less,
/// leading digits are filles up with `fill_character`
///
/// **Important:** The maximum number is 999_999 (6 digits)
pub fn unum_to_string_fill_character<const N: usize>(
    mut num: u32,
    min_digits: usize,
    fill_character: char,
    string: &mut String<N>,
) -> Result<(), ()> {
    const MAX_DIGITS: usize = 9; // 4 billion is the maximum of u32
    let mut factor: u32 = 1_000_000_000;
    let mut digit = MAX_DIGITS;
    let mut non_zero = false;
    while factor > 1 {
        let n = num / factor;
        if n != 0 {
            non_zero = true;
        }
        if non_zero {
            string.push(number_to_character(n)?)?;
        } else if digit < min_digits {
            string.push(fill_character)?;
        }
        num = num % factor;

        if factor == 10 {
            string.push(number_to_character(num)?)?;
        }
        digit -= 1;
        factor /= 10;
    }
    Ok(())
}

/// Pasing a number to string `string` with at least `min_digits`. If there are less,
/// leading zeros are used to fill
///
/// **Important:** The maximum number is 999_999 (6 digits)
pub fn unum_to_string<const N: usize>(
    mut num: u32,
    min_digits: usize,
    string: &mut String<N>,
) -> Result<(), ()> {
    unum_to_string_fill_character(num, min_digits, '0', string)
}

pub fn date_to_string<const N: usize>(
    date: NaiveDate,
    separator: char,
    string: &mut String<N>,
) -> Result<(), ()> {
    unum_to_string(date.year() as u32, 0, string)?;
    string.push(separator)?;
    unum_to_string(date.month() as u32, 2, string)?;
    string.push(separator)?;
    unum_to_string(date.day() as u32, 2, string)
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_unum_to_string() {
        let mut string: String<30> = String::new();

        unum_to_string(9, 2, &mut string).unwrap();
        assert_eq!(string.as_str(), "09");

        unum_to_string(103, 5, &mut string).unwrap();
        assert_eq!(string.as_str(), "0900103"); // Appended

        unum_to_string(10000, 3, &mut string).unwrap();
        assert_eq!(string.as_str(), "090010310000"); // Appended

        unum_to_string(0, 4, &mut string).unwrap();
        assert_eq!(string.as_str(), "0900103100000000"); // Appended

        unum_to_string(u32::MAX, 0, &mut string).unwrap();
        assert_eq!(string.as_str(), "09001031000000004294967295"); // Appended
    }

    #[test]
    fn test_date_to_string() {
        let mut string: String<20> = String::new();
        let date = NaiveDate::from_ymd_opt(2024, 3, 9).unwrap();

        date_to_string(date, '-', &mut string).unwrap();
        assert_eq!(string, "2024-03-09");
    }

    #[test]
    fn test_num_to_string() {
        let mut string: String<20> = String::new();

        num_to_string(9, 2, &mut string).unwrap();
        assert_eq!(string.as_str(), "09");

        num_to_string(-103, 5, &mut string).unwrap();
        assert_eq!(string.as_str(), "09-0103"); // Appended

        num_to_string(-1, 5, &mut string).unwrap();
        assert_eq!(string.as_str(), "09-0103-0001");
    }

    #[test]
    fn test_float_to_string() {
        let mut string: String<25> = String::new();

        float_to_string(0., 1, &mut string).unwrap();
        assert_eq!(string.as_str(), "0.0");

        string.clear();
        float_to_string(-0.9, 1, &mut string).unwrap();
        assert_eq!(string.as_str(), "-0.9");

        string.clear();
        float_to_string(9., 2, &mut string).unwrap();
        assert_eq!(string.as_str(), "9.00");

        float_to_string(-3.1416, 3, &mut string).unwrap();
        assert_eq!(string.as_str(), "9.00-3.142"); // Appended

        float_to_string(50.066544, 3, &mut string).unwrap();
        assert_eq!(string.as_str(), "9.00-3.14250.067"); // Appended

        float_to_string(-0.02276, 3, &mut string).unwrap();
        assert_eq!(string.as_str(), "9.00-3.14250.067-0.023"); // Appended

        float_to_string(5.6, 1, &mut string).unwrap();
        assert_eq!(string.as_str(), "9.00-3.14250.067-0.0235.6"); // Appended
    }

    #[test]
    fn test_float_to_string_2char() {
        let mut string: String<25> = String::new();

        float_to_string_2_char(-99.0, 1, &mut string).unwrap();
        assert_eq!(string.as_str(), "-99.0");

        float_to_string_2_char(-10.12, 1, &mut string).unwrap();
        assert_eq!(string.as_str(), "-99.0-10.1");

        float_to_string_2_char(-9.85, 1, &mut string).unwrap();
        assert_eq!(string.as_str(), "-99.0-10.1 -9.9");

        float_to_string_2_char(5.6, 1, &mut string).unwrap();
        assert_eq!(string.as_str(), "-99.0-10.1 -9.9  5.6");

        float_to_string_2_char(83.3, 1, &mut string).unwrap();
        assert_eq!(string.as_str(), "-99.0-10.1 -9.9  5.6 83.3");

        string.clear();
        float_to_string_2_char(-0.9964202, 1, &mut string).unwrap();
        assert_eq!(string.as_str(), " -1.0");
    }
}
