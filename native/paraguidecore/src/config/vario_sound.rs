use crate::common::Float;
use crate::error;
use crate::file_handling;
use core::fmt;
use heapless::Vec;

// Interpolate on/off time and frequency
const INTERPOLATION: bool = true;

#[cfg_attr(test, derive(PartialEq, Debug))]
#[derive(Clone)]
pub struct VarioSoundProperties {
    pub(super) vertical_velocity_mps: Float,
    pub(super) frequency: u16,
    pub(super) on_time_ms: u16,
    pub(super) off_time_ms: u16,
}

impl VarioSoundProperties {
    pub fn new(
        vertical_velocity_mps: Float,
        frequency: u16,
        on_time_ms: u16,
        off_time_ms: u16,
    ) -> Self {
        VarioSoundProperties {
            vertical_velocity_mps,
            frequency,
            on_time_ms,
            off_time_ms,
        }
    }
}

#[cfg_attr(test, derive(PartialEq))]
pub struct VarioSound {
    pub climb_tone_on_threshold: Float,
    pub climb_tone_off_threshold: Float,
    pub sink_tone_on_threshold: Float,
    pub sink_tone_off_threshold: Float,
    pub damping_factor: usize,
    pub mute: bool,
    pub(crate) vario_sound_properties: Vec<VarioSoundProperties, { Self::MAX_VARIOSOUND_ELEMENTS }>,

    on: bool,
}

#[derive(Clone)]
pub struct VarioSoundSettings {
    pub frequency: u16,
    pub duty: u16, // normaly 500 (50%)
    pub on_time_ms: u16,
    pub off_time_ms: u16,
}

impl VarioSoundSettings {
    pub const fn new_muted() -> Self {
        Self {
            frequency: 500,
            duty: 500,
            on_time_ms: 0,
            off_time_ms: 0,
        }
    }

    pub fn turn_off(&mut self) {
        self.on_time_ms = 0;
        self.off_time_ms = 0;
    }

    fn assign(&mut self, vario_sound_properties: &VarioSoundProperties) {
        self.frequency = vario_sound_properties.frequency;
        self.on_time_ms = vario_sound_properties.on_time_ms;
        self.off_time_ms = vario_sound_properties.off_time_ms;
    }
}

impl fmt::Debug for VarioSoundSettings {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        f.debug_struct("VarioSoundSettings")
            .field("Frequency", &self.frequency)
            .field("Duty", &self.duty)
            .field("On Time ms", &self.on_time_ms)
            .field("Off Time ms", &self.off_time_ms)
            .finish()
    }
}

impl VarioSound {
    pub const MAX_VARIOSOUND_ELEMENTS: usize = 12;

    pub fn new() -> Self {
        VarioSound {
            damping_factor: 0,
            vario_sound_properties: Vec::from_slice(&[
                VarioSoundProperties::new(-10., 200, 200, 0),
                VarioSoundProperties::new(-3., 293, 200, 0),
                VarioSoundProperties::new(-2., 369, 200, 0),
                VarioSoundProperties::new(-0.8, 440, 300, 300),
                VarioSoundProperties::new(-0.5, 475, 300, 300),
                VarioSoundProperties::new(0.0, 524, 300, 300),
                VarioSoundProperties::new(0.6, 567, 225, 225),
                VarioSoundProperties::new(1.0, 602, 250, 250),
                VarioSoundProperties::new(3.0, 760, 200, 200),
                VarioSoundProperties::new(4.0, 830, 155, 155),
                VarioSoundProperties::new(7.0, 938, 125, 125),
                VarioSoundProperties::new(10.0, 973, 100, 100),
            ])
            .unwrap(),
            climb_tone_on_threshold: -0.8,
            climb_tone_off_threshold: -1.2,
            sink_tone_on_threshold: -2.5,
            sink_tone_off_threshold: -2.0,
            mute: false,
            on: false,
        }
    }

    pub fn get(&mut self, vertical_velocity_mps: Float) -> VarioSoundSettings {
        let mut vario_sound_settings = VarioSoundSettings::new_muted();

        if self.mute {
            return vario_sound_settings;
        }

        // Turning off the vario if between those ranges!
        if self.on {
            if vertical_velocity_mps <= self.climb_tone_off_threshold
                && vertical_velocity_mps >= self.sink_tone_off_threshold
            {
                self.on = false;
                return vario_sound_settings;
            }
        } else {
            if vertical_velocity_mps >= self.climb_tone_on_threshold
                || vertical_velocity_mps <= self.sink_tone_on_threshold
            {
                self.on = true;
            } else {
                return vario_sound_settings;
            }
        }

        // Find first index where the velocity is higher than the required
        let mut index_found = 0usize;
        for p in &self.vario_sound_properties {
            if vertical_velocity_mps >= p.vertical_velocity_mps {
                index_found += 1;
            } else {
                break;
            }
        }

        if index_found == 0 {
            if let Some(p) = self.vario_sound_properties.first() {
                vario_sound_settings.assign(p);
            } else {
                // No elements in list
                error!("VarioSoundSettingsNo elements in list");
                // vario_sound_settings.turn_off();
            }
        } else if index_found >= self.vario_sound_properties.len() {
            let p = self.vario_sound_properties.last().unwrap(); // We know there is at least one element, because index_found is not zero!
            vario_sound_settings.assign(p);
        } else {
            if INTERPOLATION {
                let p_low = &self.vario_sound_properties[index_found - 1];
                let p_high = &self.vario_sound_properties[index_found];

                let vario_diff = p_high.vertical_velocity_mps - p_low.vertical_velocity_mps;
                if vario_diff != 0. {
                    // TODO: * 10.) as usize) as Float/ 10.; // Round to 0.1 steps ??
                    let factor =
                        1. / vario_diff * (vertical_velocity_mps - p_low.vertical_velocity_mps);

                    let frequency = num_traits::Float::round(
                        (p_high.frequency as Float - p_low.frequency as Float) * factor
                            + p_low.frequency as Float,
                    );
                    let on_time = num_traits::Float::round(
                        (p_high.on_time_ms as Float - p_low.on_time_ms as Float) * factor
                            + p_low.on_time_ms as Float,
                    );
                    let off_time = num_traits::Float::round(
                        (p_high.off_time_ms as Float - p_low.off_time_ms as Float) * factor
                            + p_low.off_time_ms as Float,
                    );

                    vario_sound_settings.frequency = frequency as u16;
                    vario_sound_settings.on_time_ms = on_time as u16;
                    vario_sound_settings.off_time_ms = off_time as u16;
                } else {
                    error!("Vario sound settings: Invalid");
                    // vario_sound_settings.turn_off();
                }
            } else {
                vario_sound_settings.assign(&self.vario_sound_properties[index_found - 1]);
            }
        }

        vario_sound_settings
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn get_vario_settings() {
        let mut v_sound = VarioSound {
            damping_factor: 0,
            vario_sound_properties: Vec::from_slice(&[
                VarioSoundProperties::new(-10., 200, 200, 0),
                VarioSoundProperties::new(-3., 293, 200, 0),
                VarioSoundProperties::new(-2., 369, 200, 0),
                VarioSoundProperties::new(-0.8, 440, 300, 300),
                VarioSoundProperties::new(-0.5, 475, 300, 300),
                VarioSoundProperties::new(0.0, 524, 300, 300),
                VarioSoundProperties::new(0.6, 567, 225, 225),
                VarioSoundProperties::new(1.0, 602, 250, 250),
                VarioSoundProperties::new(3.0, 760, 200, 300),
                VarioSoundProperties::new(4.0, 830, 155, 155),
                VarioSoundProperties::new(7.0, 938, 125, 125),
                VarioSoundProperties::new(10.0, 973, 100, 100),
            ])
            .unwrap(),
            climb_tone_on_threshold: -0.8,
            climb_tone_off_threshold: -1.2,
            sink_tone_on_threshold: -2.5,
            sink_tone_off_threshold: -2.0,
            on: false,
            mute: false,
        };

        let c = v_sound.get(-15.);
        assert_eq!(c.frequency, 200);
        assert_eq!(c.on_time_ms, 200);
        assert_eq!(c.off_time_ms, 0);

        let c = v_sound.get(10.0);
        assert_eq!(c.frequency, 973);
        assert_eq!(c.on_time_ms, 100);
        assert_eq!(c.off_time_ms, 100);

        let c = v_sound.get(0.0);
        assert_eq!(c.frequency, 524);
        assert_eq!(c.on_time_ms, 300);
        assert_eq!(c.off_time_ms, 300);

        // Between
        // VarioSoundProperties::new(1.0, 602, 250, 250),
        // VarioSoundProperties::new(3.0, 760, 200, 300),
        let c = v_sound.get(1.25);
        if INTERPOLATION {
            assert_eq!(c.frequency, 622);
            assert_eq!(c.on_time_ms, 244);
            assert_eq!(c.off_time_ms, 256);
        } else {
            assert_eq!(c.frequency, 602);
            assert_eq!(c.on_time_ms, 250);
            assert_eq!(c.off_time_ms, 250);
        }
    }
}
