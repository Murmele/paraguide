//! Generic Filehandler

use core::future::Future;
use heapless;

pub const LINE_LENGHT: usize = 100;

pub trait File {
    type Error;
    /// Reading a complete line. This line includes the line feed (\n) and chariage return (\r) characters!
    fn read_line(
        &mut self,
        out: &mut heapless::String<LINE_LENGHT>,
    ) -> impl Future<Output = Result<(), Self::Error>> + Send;
    /// Write data to file. If the file already exists, the text will be appended
    fn write(&mut self, text: &str) -> impl Future<Output = Result<(), Self::Error>> + Send;
    fn close(&mut self) -> impl Future<Output = ()> + Send;
}

#[derive(PartialEq)]
pub enum Mode {
    /// Open a file for reading, if it exists.
    Read,
    /// Open a file for appending (writing to the end of the existing file), if it exists.
    WriteAppend,
    /// Open a file and remove all contents, before writing to the start of the existing file, if it exists.
    WriteTruncate,
    /// Create a new empty file. Fail if it exists.
    WriteCreate,
    /// Create a new empty file, or truncate an existing file.
    WriteCreateOrTruncate,
    /// Create a new empty file, or append to an existing file.
    WriteCreateOrAppend,
}

pub trait FileHandler<F: File> {
    type Error;
    /// Opening a new file for reading or writing
    /// writing: if true the file will be opened for writing. It will not be checked if already exists because it gets overwritten
    ///          if false an error will be returned if the file does not exist
    fn open(
        &mut self,
        filename: &str,
        mode: Mode,
    ) -> impl Future<Output = Result<F, Self::Error>> + Send;

    fn file_exists(
        &self,
        file_name: &str,
    ) -> impl Future<Output = Result<bool, Self::Error>> + Send;

    fn create_folders(&self, path: &str) -> impl Future<Output = Result<(), Self::Error>> + Send;
}
