// Everything to handle settings

use crate::file_handling;
use crate::helper::{float_to_string, unum_to_string};
use crate::igc;
use crate::igc::b_record::Time;
use crate::paraguide;
use heapless::String;
use vario_sound::VarioSoundProperties;
mod vario_sound;
use crate::common::Float;
use crate::debug;
use paraguide::Error;
pub use vario_sound::VarioSound;
pub use vario_sound::VarioSoundSettings;

const CONFIG_FILENAME: &str = "Config.pg";

const CONFIG_FILE_HEADER: &str = "ParaguideConfig:";
#[allow(unused)]
const VERSION: usize = 1; // Current config version

const CONFIG_PILOT_NAME: &str = "PilotName:";
const CONFIG_GLIDER_TYPE: &str = "GliderType:";
const CONFIG_GLIDER_ID: &str = "GliderId:"; // Id of the glider or the pilot. License number
const CONFIG_AUTO_TAKEOFF: &str = "AutoTakeoff:";
const CONFIG_AUTO_LANDING: &str = "AutoLanding:";
const CONFIG_AUTO_TAKEOFF_HEIGHT_DIFF: &str = "AutoTakeoffHeightThreshold:";
const CONFIG_AUTO_TAKEOFF_TIMEOUT: &str = "AutoTakeoffTimeout:";
const CONFIG_AUTO_LANDING_HEIGHT_DIFF: &str = "AutoLandingThreshold:";
const CONFIG_AUTO_LANDING_TIMEOUT: &str = "AutoLandingTimeout:";

const CONFIG_TONE: &str = "Tone:";
const CONFIG_DAMPING_FACTOR: &str = "Damping:";
const CONFIG_AUDIO_MUTE: &str = "Mute:";
const CONFIG_AUDIO_CLIMB_TONE_ON_THRESHOLD: &str = "ClimbToneOnThreshold:";
const CONFIG_AUDIO_CLIMB_TONE_OFF_THRESHOLD: &str = "ClimbToneOffThreshold:";
const CONFIG_AUDIO_SINK_TONE_ON_THRESHOLD: &str = "SinkToneOnThreshold:";
const CONFIG_AUDIO_SINK_TONE_OFF_THRESHOLD: &str = "SinkToneOffThreshold:";

const CONFIG_TIME_REPRESENTATION: &str = "TimeRepresentation:";

#[derive(Clone, Copy)]
pub enum TimeRepresentation {
    Hour24,
    Hour12,
}

pub struct Config {
    pilot_name: String<{ igc::PILOT_NAME_MAX_CHARACTERS }>,
    glider_type: String<{ igc::GLIDER_TYPE_MAX_CHARACTERS }>,
    glider_id: String<{ igc::GLIDER_ID_MAX_CHARACTERS }>,

    auto_power_off_min: usize,

    vario_sound: VarioSound,

    auto_takeoff: bool,
    auto_landing: bool,
    auto_takeoff_height_threshold: usize,
    auto_takeoff_timeout: usize,
    auto_landing_height_threshold: usize,
    auto_landing_timeout: usize,

    time_representation: TimeRepresentation,

    dirty: bool,
}

impl Config {
    pub fn get_pilot_name<'a>(&'a self) -> &'a str {
        &self.pilot_name
    }

    pub fn get_glider_type<'a>(&'a self) -> &'a str {
        &self.glider_type
    }

    pub fn get_glider_id<'a>(&'a self) -> &'a str {
        &self.glider_id
    }

    pub fn get_vario_sound(&mut self, vario_value: Float) -> VarioSoundSettings {
        self.vario_sound.get(vario_value)
    }

    pub fn is_mute(&self) -> bool {
        self.vario_sound.mute
    }

    pub fn set_mute(&mut self, mute: bool) {
        self.vario_sound.mute = mute;
    }

    pub fn toggle_mute(&mut self) {
        self.vario_sound.mute = !self.vario_sound.mute;
        self.dirty = true;
    }

    pub fn auto_takeoff(&self) -> bool {
        self.auto_takeoff
    }

    pub fn set_auto_takeoff(&mut self, auto: bool) {
        self.auto_takeoff = auto;
        self.dirty = true;
    }

    pub fn auto_landing(&self) -> bool {
        self.auto_landing
    }

    pub fn set_auto_landing(&mut self, auto: bool) {
        self.auto_landing = auto;
        self.dirty = true;
    }

    pub fn get_vario_damping_factor(&self) -> usize {
        self.vario_sound.damping_factor
    }

    pub fn auto_takeoff_height_threshold(&self) -> usize {
        self.auto_takeoff_height_threshold
    }

    pub fn set_auto_takeoff_height_threshold(&mut self, v: usize) {
        self.auto_takeoff_height_threshold = v;
        self.dirty = true;
    }

    pub fn auto_takeoff_timeout(&self) -> usize {
        self.auto_takeoff_timeout
    }

    pub fn set_auto_takeoff_timeout(&mut self, v: usize) {
        self.auto_takeoff_timeout = v;
        self.dirty = true;
    }

    pub fn auto_landing_height_threshold(&self) -> usize {
        self.auto_landing_height_threshold
    }

    pub fn set_auto_landing_height_threshold(&mut self, v: usize) {
        self.auto_landing_height_threshold = v;
        self.dirty = true;
    }

    pub fn auto_landing_timeout(&self) -> usize {
        self.auto_landing_timeout
    }

    pub fn set_auto_landing_timeout(&mut self, v: usize) {
        self.auto_landing_timeout = v;
        self.dirty = true;
    }

    async fn write_config<File, T>(&mut self, mut file_handler: T, filename: &str) -> Result<(), ()>
    where
        File: file_handling::File,
        T: file_handling::FileHandler<File> + Clone,
    {
        if !self.dirty {
            return Ok(());
        }

        let mut file = file_handler
            .open(filename, file_handling::Mode::WriteCreateOrTruncate)
            .await
            .map_err(|_| ())?;
        let res = self.write_config_inner(&mut file).await;
        file.close().await;
        res
    }

    async fn write_config_inner<File>(&mut self, file: &mut File) -> Result<(), ()>
    where
        File: file_handling::File,
    {
        let mut string: String<100> = String::new();
        string.push_str(CONFIG_FILE_HEADER)?;
        unum_to_string(VERSION as u32, 0, &mut string)?;
        string.push('\n')?;
        string.push_str(CONFIG_PILOT_NAME)?;
        string.push_str(&self.pilot_name)?;
        string.push('\n')?;
        file.write(&string).await.map_err(|_| ())?;

        string.clear();
        string.push_str(CONFIG_GLIDER_ID)?;
        string.push_str(&self.glider_id)?;
        string.push('\n')?;
        file.write(&string).await.map_err(|_| ())?;

        string.clear();
        string.push_str(CONFIG_GLIDER_TYPE)?;
        string.push_str(&self.glider_type)?;
        string.push('\n')?;
        file.write(&string).await.map_err(|_| ())?;

        string.clear();
        string.push_str(CONFIG_AUTO_TAKEOFF)?;
        unum_to_string(self.auto_takeoff as u32, 0, &mut string)?;
        string.push('\n')?;
        file.write(&string).await.map_err(|_| ())?;

        string.clear();
        string.push_str(CONFIG_AUTO_LANDING)?;
        unum_to_string(self.auto_landing as u32, 0, &mut string)?;
        string.push('\n')?;
        file.write(&string).await.map_err(|_| ())?;

        string.clear();
        string.push_str(CONFIG_AUTO_TAKEOFF_HEIGHT_DIFF)?;
        unum_to_string(self.auto_takeoff_height_threshold as u32, 0, &mut string)?;
        string.push('\n')?;
        file.write(&string).await.map_err(|_| ())?;

        string.clear();
        string.push_str(CONFIG_AUTO_TAKEOFF_TIMEOUT)?;
        unum_to_string(self.auto_takeoff_timeout as u32, 0, &mut string)?;
        string.push('\n')?;
        file.write(&string).await.map_err(|_| ())?;

        string.clear();
        string.push_str(CONFIG_AUTO_LANDING_HEIGHT_DIFF)?;
        unum_to_string(self.auto_landing_height_threshold as u32, 0, &mut string)?;
        string.push('\n')?;
        file.write(&string).await.map_err(|_| ())?;

        string.clear();
        string.push_str(CONFIG_AUTO_LANDING_TIMEOUT)?;
        unum_to_string(self.auto_landing_timeout as u32, 0, &mut string)?;
        string.push('\n')?;
        file.write(&string).await.map_err(|_| ())?;

        string.clear();
        string.push_str(CONFIG_TIME_REPRESENTATION)?;
        match self.time_representation {
            TimeRepresentation::Hour12 => string.push_str("12")?,
            TimeRepresentation::Hour24 => string.push_str("24")?,
        }
        string.push('\n')?;
        file.write(&string).await.map_err(|_| ())?;

        string.clear();
        string.push_str(CONFIG_AUDIO_MUTE)?;
        if self.vario_sound.mute {
            string.push_str("On")?;
        } else {
            string.push_str("Off")?;
        }
        string.push('\n')?;
        file.write(&string).await.map_err(|_| ())?;

        for properties in &self.vario_sound.vario_sound_properties {
            string.clear();
            string.push_str(CONFIG_TONE)?;
            float_to_string(properties.vertical_velocity_mps, 1, &mut string)?;
            unum_to_string(properties.frequency as u32, 0, &mut string)?;
            string.push_str(", ")?;
            unum_to_string(properties.on_time_ms as u32, 0, &mut string)?;
            string.push_str(", ")?;
            unum_to_string(properties.off_time_ms as u32, 0, &mut string)?;
            string.push('\n')?;
            file.write(&string).await.map_err(|_| ())?
        }

        string.clear();
        string.push_str(CONFIG_DAMPING_FACTOR)?;
        unum_to_string(self.vario_sound.damping_factor as u32, 0, &mut string)?;
        string.push('\n')?;
        file.write(&string).await.map_err(|_| ())?;

        string.clear();
        string.push_str(CONFIG_AUDIO_CLIMB_TONE_ON_THRESHOLD)?;
        float_to_string(self.vario_sound.climb_tone_on_threshold, 1, &mut string)?;
        string.push('\n')?;
        file.write(&string).await.map_err(|_| ())?;

        string.clear();
        string.push_str(CONFIG_AUDIO_CLIMB_TONE_OFF_THRESHOLD)?;
        float_to_string(self.vario_sound.climb_tone_off_threshold, 1, &mut string)?;
        string.push('\n')?;
        file.write(&string).await.map_err(|_| ())?;

        string.clear();
        string.push_str(CONFIG_AUDIO_SINK_TONE_ON_THRESHOLD)?;
        float_to_string(self.vario_sound.sink_tone_on_threshold, 1, &mut string)?;
        string.push('\n')?;
        file.write(&string).await.map_err(|_| ())?;

        string.clear();
        string.push_str(CONFIG_AUDIO_SINK_TONE_OFF_THRESHOLD)?;
        float_to_string(self.vario_sound.sink_tone_off_threshold, 1, &mut string)?;
        string.push('\n')?;
        let res = file.write(&string).await.map_err(|_| ());

        self.dirty = false;
        res
    }
}

impl<'a, T, File, TIME, const N: usize> paraguide::Paraguide<'a, T, File, TIME, N>
where
    File: file_handling::File,
    T: file_handling::FileHandler<File> + Clone,
{
    pub async fn read_config(&mut self) -> Result<(), Error> {
        self.read_config_file(CONFIG_FILENAME).await
    }

    pub async fn write_config(&mut self) -> Result<(), Error> {
        self.config
            .write_config(self.file_handler.clone(), CONFIG_FILENAME)
            .await
            .map_err(|_| Error::UnableToWriteConfig)
    }

    async fn read_config_file(&mut self, filename: &str) -> Result<(), Error> {
        // If file does not exist, use default values
        let mut config_reader = ConfigReader::new(self.file_handler.clone(), filename)
            .await
            .map_err(|_| Error::NoConfigFile)?;

        let result = async {
            config_reader.next().await?;

            let line = config_reader.curr_line();
            let line_length = line.len();

            if !line.starts_with(CONFIG_FILE_HEADER) {
                debug!("Invalid config file");
                return Err(Error::InvalidConfigFile);
            } else if line_length <= CONFIG_FILE_HEADER.len() {
                debug!("No version available");
                return Err(Error::NoVersionInConfig);
            }

            let version_str = line
                .get(CONFIG_FILE_HEADER.len()..line.len())
                .map_or("", |s| s);

            #[allow(unused_variables)]
            let version = usize::from_str_radix(version_str, 10).map_err(|_| Error::NotANumber)?;

            Self::read_elements(&mut config_reader, &mut self.config).await;
            Ok::<(), Error>(())
        }
        .await;
        config_reader.close_file().await;
        result
    }

    // This function intentionally has no Result, because reading the config is always possible
    // If any elements are not available, or parameters are not correct we just ignore it
    async fn read_elements(config_reader: &mut ConfigReader<File>, config: &mut Config) {
        let mut valid_config_tone_found = false;
        while config_reader.next().await.map_or(false, |_| true) {
            let l = config_reader.curr_line();

            match config_reader.element_type() {
                ElementType::Attribute => {
                    if l.starts_with(CONFIG_PILOT_NAME) {
                        let _ = config
                            .pilot_name
                            .push_str(l.get(CONFIG_PILOT_NAME.len()..l.len()).map_or("", |s| s));
                    } else if l.starts_with(CONFIG_GLIDER_TYPE) {
                        let _ = config
                            .glider_type
                            .push_str(l.get(CONFIG_GLIDER_TYPE.len()..l.len()).map_or("", |s| s));
                    } else if l.starts_with(CONFIG_GLIDER_ID) {
                        let _ = config
                            .glider_id
                            .push_str(l.get(CONFIG_GLIDER_ID.len()..l.len()).unwrap_or(""));
                    } else if l.starts_with(CONFIG_AUTO_TAKEOFF) {
                        let l = l.get(CONFIG_AUTO_TAKEOFF.len()..l.len()).unwrap_or("");
                        if l == "true" || l == "1" {
                            config.auto_takeoff = true;
                        } else {
                            config.auto_takeoff = false;
                        }
                    } else if l.starts_with(CONFIG_AUTO_LANDING) {
                        let l = l.get(CONFIG_AUTO_LANDING.len()..l.len()).unwrap_or("");
                        if l == "true" || l == "1" {
                            config.auto_landing = true;
                        } else {
                            config.auto_landing = false;
                        }
                    } else if l.starts_with(CONFIG_AUTO_TAKEOFF_HEIGHT_DIFF) {
                        let l = l
                            .get(CONFIG_AUTO_TAKEOFF_HEIGHT_DIFF.len()..l.len())
                            .unwrap_or("");
                        config.auto_takeoff_height_threshold = usize::from_str_radix(l, 10)
                            .unwrap_or(config.auto_takeoff_height_threshold);
                    } else if l.starts_with(CONFIG_AUTO_TAKEOFF_TIMEOUT) {
                        let l = l
                            .get(CONFIG_AUTO_TAKEOFF_TIMEOUT.len()..l.len())
                            .unwrap_or("");
                        config.auto_takeoff_timeout =
                            usize::from_str_radix(l, 10).unwrap_or(config.auto_takeoff_timeout);
                    } else if l.starts_with(CONFIG_AUTO_LANDING_HEIGHT_DIFF) {
                        let l = l
                            .get(CONFIG_AUTO_LANDING_HEIGHT_DIFF.len()..l.len())
                            .unwrap_or("");
                        config.auto_landing_height_threshold = usize::from_str_radix(l, 10)
                            .unwrap_or(config.auto_landing_height_threshold);
                    } else if l.starts_with(CONFIG_AUTO_LANDING_TIMEOUT) {
                        let l = l
                            .get(CONFIG_AUTO_LANDING_TIMEOUT.len()..l.len())
                            .unwrap_or("");
                        config.auto_landing_timeout =
                            usize::from_str_radix(l, 10).unwrap_or(config.auto_landing_timeout);
                    } else if l.starts_with(CONFIG_AUDIO_MUTE) {
                        let l = l.get(CONFIG_AUDIO_MUTE.len()..l.len()).unwrap_or("");
                        if l.starts_with("On")
                            || l.starts_with("ON")
                            || l.starts_with("on")
                            || l.starts_with("1")
                        {
                            config.vario_sound.mute = true;
                        } else {
                            config.vario_sound.mute = false;
                        }
                    } else if l.starts_with(CONFIG_DAMPING_FACTOR) {
                        let l = l.get(CONFIG_DAMPING_FACTOR.len()..l.len()).unwrap_or("");
                        config.vario_sound.damping_factor = usize::from_str_radix(l, 10)
                            .unwrap_or(config.vario_sound.damping_factor);
                    } else if l.starts_with(CONFIG_AUDIO_CLIMB_TONE_ON_THRESHOLD) {
                        let l = l
                            .get(CONFIG_AUDIO_CLIMB_TONE_ON_THRESHOLD.len()..l.len())
                            .unwrap_or("");
                        if let Ok(v) = l.parse::<Float>() {
                            config.vario_sound.climb_tone_on_threshold = v;
                        }
                    } else if l.starts_with(CONFIG_AUDIO_CLIMB_TONE_OFF_THRESHOLD) {
                        let l = l
                            .get(CONFIG_AUDIO_CLIMB_TONE_OFF_THRESHOLD.len()..l.len())
                            .unwrap_or("");
                        if let Ok(v) = l.parse::<Float>() {
                            config.vario_sound.climb_tone_off_threshold = v;
                        }
                    } else if l.starts_with(CONFIG_AUDIO_SINK_TONE_ON_THRESHOLD) {
                        let l = l
                            .get(CONFIG_AUDIO_SINK_TONE_ON_THRESHOLD.len()..l.len())
                            .unwrap_or("");
                        if let Ok(v) = l.parse::<Float>() {
                            config.vario_sound.sink_tone_on_threshold = v;
                        }
                    } else if l.starts_with(CONFIG_AUDIO_SINK_TONE_OFF_THRESHOLD) {
                        let l = l
                            .get(CONFIG_AUDIO_SINK_TONE_OFF_THRESHOLD.len()..l.len())
                            .unwrap_or("");
                        if let Ok(v) = l.parse::<Float>() {
                            config.vario_sound.sink_tone_off_threshold = v;
                        }
                    } else if l.starts_with(CONFIG_TONE) {
                        if let Ok(p) =
                            &Self::parse_tone(l.get(CONFIG_TONE.len()..l.len()).unwrap_or(""))
                        {
                            if !valid_config_tone_found {
                                config.vario_sound.vario_sound_properties.clear();
                            }
                            valid_config_tone_found = true;
                            let _ = config.vario_sound.vario_sound_properties.push(p.clone());
                        }
                    } else if l.starts_with(CONFIG_TIME_REPRESENTATION) {
                        let l = l.get(CONFIG_TONE.len()..l.len()).unwrap_or("");
                        if l.starts_with("12") {
                            config.time_representation = TimeRepresentation::Hour12;
                        } else {
                            config.time_representation = TimeRepresentation::Hour24;
                        }
                    }
                }
                ElementType::StartElement => {
                    // Add new sections here
                    debug!("Ignoring start element");
                }
                ElementType::EndElement => {
                    debug!("Ignoring end element");
                }
                ElementType::Invalid => {
                    // Can happen if for example no colon is in the line
                    // or if after the colon no text is
                }
            }
        }
    }

    fn parse_tone(line: &str) -> Result<VarioSoundProperties, ()> {
        // let vario_sound_properties;
        let mut last_index = 0;
        let mut parsed_numbers = 0;
        let mut word = false;

        let mut vertical_velocity_mps = 0.;
        let mut frequency = 0;
        let mut on_time_ms = 0;
        let mut off_time_ms = 0;

        for (index, c) in line.chars().into_iter().enumerate() {
            if c == ' ' || c == '\t' {
                if !word {
                    last_index += 1;
                    continue;
                } else {
                    // A number cannot contain whitespaces!
                    return Err(());
                }
            } else {
                word = true;
            }
            if c == ',' {
                match parsed_numbers {
                    0 => {
                        vertical_velocity_mps =
                            line[last_index..index].parse::<Float>().map_err(|_| ())?;
                        parsed_numbers += 1;
                        last_index = index + 1;
                        word = false;
                    }
                    1 => {
                        frequency = line[last_index..index].parse::<u16>().map_err(|_| ())?;
                        parsed_numbers += 1;
                        last_index = index + 1;
                        word = false;
                    }
                    2 => {
                        on_time_ms = line[last_index..index].parse::<u16>().map_err(|_| ())?;
                        parsed_numbers += 1;
                        last_index = index + 1;
                        word = false;
                    }
                    3 => {
                        off_time_ms = line[last_index..index].parse::<u16>().map_err(|_| ())?;
                        parsed_numbers += 1;
                        last_index = index + 1;
                        word = false;
                    }
                    _ => return Err(()),
                }
            }
        }
        if parsed_numbers < 4 && last_index < line.len() {
            off_time_ms = line[last_index..line.len()]
                .parse::<u16>()
                .map_err(|_| ())?;
            parsed_numbers += 1;
        }
        if parsed_numbers >= 4 {
            Ok(VarioSoundProperties::new(
                vertical_velocity_mps,
                frequency,
                on_time_ms,
                off_time_ms,
            ))
        } else {
            Err(())
        }
    }
}

/// Config data for paraguide
impl Config {
    pub fn new() -> Self {
        Config {
            pilot_name: String::new(),
            glider_type: String::new(),
            glider_id: String::new(),

            auto_power_off_min: 0,

            vario_sound: VarioSound::new(),

            auto_takeoff: true,
            auto_landing: true,
            auto_takeoff_height_threshold: 6,
            auto_takeoff_timeout: 60,
            auto_landing_height_threshold: 1,
            auto_landing_timeout: 60,

            time_representation: TimeRepresentation::Hour24,

            dirty: false,
        }
    }

    #[cfg(test)]
    fn equal(&self, rhs: &Self) -> bool {
        self.pilot_name == rhs.pilot_name
            && self.glider_type == rhs.glider_type
            && self.auto_power_off_min == rhs.auto_power_off_min
            && self.vario_sound == rhs.vario_sound
    }
}

#[derive(Clone, Copy, PartialEq)]
enum ElementType {
    StartElement,
    EndElement,
    Attribute,
    Invalid,
}

struct ConfigReader<F>
where
    F: file_handling::File,
{
    curr_line: String<{ file_handling::LINE_LENGHT }>,
    element_type: ElementType,
    file: F,
    open: bool,
}

impl<F> ConfigReader<F>
where
    F: file_handling::File,
{
    pub async fn new<T: file_handling::FileHandler<F>>(
        mut file_handler: T,
        filename: &str,
    ) -> Result<Self, ()> {
        Ok(Self {
            curr_line: String::new(),
            element_type: ElementType::Invalid,
            file: file_handler
                .open(filename, file_handling::Mode::Read)
                .await
                .map_err(|_| ())?,
            open: true,
        })
    }

    pub async fn next(&mut self) -> Result<(), Error> {
        self.element_type = ElementType::Invalid;
        self.file
            .read_line(&mut self.curr_line)
            .await
            .map_err(|_| Error::NoNewLinesAvailable)?;
        if self.curr_line.ends_with("\n") {
            self.curr_line.pop();
        }
        if self.curr_line.ends_with("\r") {
            self.curr_line.pop();
        }
        if self.is_start_element() {
            self.element_type = ElementType::StartElement;
        } else if self.is_end_element() {
            self.element_type = ElementType::EndElement;
        } else {
            if let Some(pos) = self.curr_line.find(':') {
                if pos < self.curr_line.len() - 1 {
                    // After the colon at least one character is available
                    self.element_type = ElementType::Attribute;
                }
            }
        }
        Ok(())
    }

    pub fn curr_line(&self) -> &String<{ file_handling::LINE_LENGHT }> {
        return &self.curr_line;
    }

    pub fn element_type(&self) -> ElementType {
        self.element_type
    }

    fn is_start_element(&self) -> bool {
        return self.curr_line.starts_with('[');
    }

    fn is_end_element(&self) -> bool {
        return self.curr_line.starts_with("[/");
    }

    async fn close_file(&mut self) {
        self.file.close().await;
        self.open = false;
    }
}

impl<F> Drop for ConfigReader<F>
where
    F: file_handling::File,
{
    fn drop(&mut self) {
        if self.open {
            debug!("Config file was not closed properly!!!");
        }
    }
}

#[cfg(test)]
mod tests {
    use crate::{
        file_handling::{self, File as file_handling_file},
        igc::GNSSSystems,
        paraguide::{DeviceInfo, Paraguide},
    };

    use super::*;

    #[derive(Debug)]
    enum Error {
        Unknown,
    }

    #[derive(Clone)]
    pub struct FileHandler {
        file: File,
    }

    const NUMBER_LINES: usize = 100;
    impl FileHandler {
        fn new(file: File) -> Self {
            Self { file }
        }
    }

    impl file_handling::FileHandler<File> for FileHandler {
        type Error = Error;
        fn file_exists(
            &self,
            _file_name: &str,
        ) -> impl async_std::prelude::Future<Output = Result<bool, Self::Error>> + Send {
            async { Err(Error::Unknown) }
        }
        fn open(
            &mut self,
            _filename: &str,
            _mode: file_handling::Mode,
        ) -> impl async_std::prelude::Future<Output = Result<File, Self::Error>> + Send {
            async { Ok(self.file.clone()) }
        }

        async fn create_folders(&self, _path: &str) -> Result<(), Self::Error> {
            Ok(())
        }
    }

    #[derive(Clone)]
    struct File {
        virtual_content: heapless::Vec<String<{ NUMBER_LINES }>, 100>,
        next_write_line: usize,
        next_read_line: isize,
    }

    impl File {
        const fn new() -> Self {
            Self {
                virtual_content: heapless::Vec::new(),
                next_write_line: 0,
                next_read_line: -1,
            }
        }

        fn push_str(&mut self, text: &str) -> Result<(), ()> {
            self.next_read_line = 0; // set read valid
            if self.next_write_line < NUMBER_LINES {
                let mut string: String<{ NUMBER_LINES }> = String::new();
                string.push_str(text).unwrap();
                self.virtual_content.push(string).unwrap();
                self.next_write_line += 1;
                return Ok(());
            }
            return Err(());
        }
    }

    impl file_handling_file for File {
        type Error = Error;

        fn read_line(
            &mut self,
            out: &mut heapless::String<{ file_handling::LINE_LENGHT }>,
        ) -> impl async_std::prelude::Future<Output = Result<(), Self::Error>> + Send {
            async {
                if self.next_read_line >= 0
                    && self.next_read_line <= (self.next_write_line) as isize - 1
                {
                    out.clear();
                    let s = self.virtual_content[self.next_read_line as usize].as_str();
                    let _ = out.push_str(s);
                    self.next_read_line += 1;
                    return Ok(());
                }
                return Err(Error::Unknown);
            }
        }

        fn write(
            &mut self,
            _text: &str,
        ) -> impl async_std::prelude::Future<Output = Result<(), Self::Error>> + Send {
            async { Err(Error::Unknown) }
        }

        fn close(&mut self) -> impl async_std::prelude::Future<Output = ()> + Send {
            async {}
        }
    }

    impl igc::Writer for File {
        async fn push(&mut self, _c: char) -> Result<(), ()> {
            Err(())
        }

        async fn push_str(&mut self, _content: &str) -> Result<(), ()> {
            Err(())
        }
    }

    struct TimeKeeper {}
    impl crate::logging::Timekeeper for TimeKeeper {
        fn get_time_us() -> u64 {
            0
        }
    }

    const DUMMY_DEVICE_INFO: DeviceInfo<2> = DeviceInfo {
        manufacturer: "PG",
        model: "PG",
        firmware_version: "1.0",
        hw_revision: "7.0",
        igc_manufacturer_id: "XPG",
        igc_fr: "324lk",
        gnss_receiver_manufacturer: "GNSS MANUFACTURER",
        gnss_receiver_model: "GNSS Model",
        gnss_receiver_max_alt: "18000",
        gnss_receiver_systems: &[GNSSSystems::GPS, GNSSSystems::GALILEO],
        gnss_receiver_channels: "30",
        pressure_sensor_manufacturer: "BARO Manufacturer",
        pressure_sensor_model: "Baro Model",
        pressure_sensor_max_alt: "11000",
    };

    /// Importing invalid config file. In this case the default config shall be used
    #[tokio::test]
    async fn import_invalid_file_content() {
        let mut file = File::new();
        file.push_str("SomeText").unwrap();
        file.push_str("Some Other text").unwrap();
        let fh = FileHandler::new(file);
        let mut pg: Paraguide<'_, _, _, TimeKeeper, 2> =
            Paraguide::new(fh, &DUMMY_DEVICE_INFO, "Not relevant");
        pg.init(0.0).await.unwrap();

        assert!(pg.config.equal(&Config::new())); // config is still the same as the default
    }

    #[tokio::test]
    async fn import_glider_values() {
        let mut file = File::new();
        file.push_str("ParaguideConfig:0").unwrap();
        file.push_str("PilotName:Max Mustermann").unwrap();
        file.push_str("GliderType:Icaro Gravis 2 S").unwrap();
        file.push_str("Tone: -10, 493, 729, 2").unwrap(); // Contains spaces!
        file.push_str("Tone:0.2,2389,3,29").unwrap();
        file.push_str("Tone:   +5, 2192, 32, 1245").unwrap(); // Contains spaces!
        let fh = FileHandler::new(file);
        let mut pg: Paraguide<'_, _, _, TimeKeeper, 2> =
            Paraguide::new(fh, &DUMMY_DEVICE_INFO, "Not relevant");
        pg.init(0.0).await.unwrap();

        assert_eq!(pg.config.pilot_name, "Max Mustermann");
        assert_eq!(pg.config.glider_type, "Icaro Gravis 2 S");
        assert_eq!(
            pg.config.vario_sound.vario_sound_properties,
            &[
                VarioSoundProperties::new(-10., 493, 729, 2),
                VarioSoundProperties::new(0.2, 2389, 3, 29),
                VarioSoundProperties::new(5.0, 2192, 32, 1245),
            ]
        );
    }

    #[tokio::test]
    async fn import_glider_values_no_tone() {
        let mut file = File::new();
        file.push_str("ParaguideConfig:0").unwrap();
        file.push_str("PilotName:Max Mustermann").unwrap();
        file.push_str("GliderType:Icaro Gravis 2 S").unwrap();
        let fh = FileHandler::new(file);
        let mut pg: Paraguide<'_, _, _, TimeKeeper, 2> =
            Paraguide::new(fh, &DUMMY_DEVICE_INFO, "Not relevant");
        pg.init(0.0).await.unwrap();

        assert_eq!(pg.config.pilot_name, "Max Mustermann");
        assert_eq!(pg.config.glider_type, "Icaro Gravis 2 S");
        assert!(pg.config.vario_sound.vario_sound_properties.len() != 0);
    }

    #[tokio::test]
    async fn invalid_entry() {
        let mut file = File::new();
        file.push_str("ParaguideConfig:0").unwrap();
        file.push_str("PilotName:Max Mustermann").unwrap();
        file.push_str("GliderType:").unwrap(); // No type provided!
        let fh = FileHandler::new(file);
        let mut pg: Paraguide<'_, _, _, TimeKeeper, 2> =
            Paraguide::new(fh, &DUMMY_DEVICE_INFO, "Not relevant");
        pg.init(0.0).await.unwrap();

        assert_eq!(pg.config.pilot_name, "Max Mustermann");
        assert_eq!(pg.config.glider_type, "");
    }

    #[tokio::test]
    async fn no_colon() {
        let mut file = File::new();
        file.push_str("ParaguideConfig:0").unwrap();
        file.push_str("PilotName:Max Mustermann").unwrap();
        file.push_str("GliderType Icaro Gravis").unwrap(); // No colon
        let fh = FileHandler::new(file);

        let mut pg: Paraguide<'_, _, _, TimeKeeper, 2> =
            Paraguide::new(fh, &DUMMY_DEVICE_INFO, "Not relevant");
        pg.init(0.0).await.unwrap();

        assert_eq!(pg.config.pilot_name, "Max Mustermann");
        assert_eq!(pg.config.glider_type, "");
    }
}
