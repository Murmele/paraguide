use crate::common::Float;
use num_traits;

// https://de.wikipedia.org/w/index.php?title=Luftdruck&oldid=240267554
pub const STANDARD_ATMOSPHERE: Float = 101_325.0; // [Pa]
const T0: Float = 288.15; // [K]
pub const TEMPERATURE_GRADIENT: Float = -0.0065; // [K/m]

const G0: Float = 9.80665; // [m/s^2]
const M: Float = 28.9644/*   1000.0*/; // [kg/mol]
const R: Float = 8.31432; // [J/(mol*K)]

// This formula calculates the height using the U.S. standard atmosphere
// https://de.wikipedia.org/wiki/Barometrische_H%C3%B6henformel
// https://ntrs.nasa.gov/citations/19770009539 , formula 33a
pub fn barometric_formula(pressure_pa: Float) -> Float {
    const GMRT_INV: Float = 1.0 / (G0 * M / (R * TEMPERATURE_GRADIENT) / 1000.0); // divide by 1000, because g/mol and J uses kg
    (T0 / num_traits::Float::powf(pressure_pa / STANDARD_ATMOSPHERE, GMRT_INV) - T0)
        / TEMPERATURE_GRADIENT
        + 0.0
}

// This formula calculates the pressure at a height of `h_m`. This formula is only valid between 0m and 11000m
// Source: https://ntrs.nasa.gov/citations/19770009539
#[allow(dead_code)]
pub fn calculate_pressure(h_m: Float) -> Float {
    // formula 33a (LM_B = TEMPERATURE_GRADIENT)
    const GMRT: Float = G0 * M / (R * TEMPERATURE_GRADIENT) / 1000.0; // divide by 1000, because g/mol and J uses kg
    let temperature = T0 + TEMPERATURE_GRADIENT * (h_m - 0.0); // Temperature at height h_m
    let p = STANDARD_ATMOSPHERE * num_traits::Float::powf(T0 / temperature, GMRT);
    p
}

#[cfg(test)]
mod tests {

    #[test]
    fn barometric_formula() {
        // https://de.wikipedia.org/w/index.php?title=Normatmosph%C3%A4re&oldid=226823435
        assert_eq!(super::barometric_formula(101_325.0), 0.0);
        assert!((super::barometric_formula(22_632.1) - 11_000.0).abs() < 0.1);
    }

    #[test]
    fn barometric_formula2() {
        // Value rounded to 1 digit after comma
        assert_eq!(
            (super::calculate_pressure(11_000.0) * 10.0).round() / 10.0,
            22_632.1
        );
    }
}
