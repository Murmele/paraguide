//! Kalman filter to predict height and vertical velocity using a Barometer only
//! See KalmanFilterBaro.wxm for more information
//! Sources:
//! - See KalmanFilterBaro.wxm in the calculations folder
//! - [1] https://www.kalmanfilter.net/kalman1d.html

#[cfg(all(test, feature = "std"))]
use super::super::igc;
use super::pressure;
use crate::common::Float;

/// # Examples
///
/// Doctest is not yet available for private modules! (https://github.com/rust-lang/rust/issues/60820)
///
/// use paraguidecore::state_estimation::baro_kalmanfilter;
///
/// // Time in s
/// let dt = 1.0/100.;
/// // Pressure in Pa
/// let pressure_pa = vec![101_325.0, 101_325.0, 101_325.0, 101_325.0];
/// let mut kalman_filter = baro_kalmanfilter::KalmanFilter::new();
///
/// for p in pressure_pa {
///     kalman_filter.update(p, dt);
///     println!("Height: {}m", kalman_filter.height());
///     println!("Velocity: {}m/s", kalman_filter.velocity());
/// }
pub struct KalmanFilter {
    // height
    x1: Float,
    // velocity
    x2: Float,

    x1_measured: Float, // Calculated value from the measurement

    c11: Float,
    c12: Float,
    c21: Float,
    c22: Float,

    c_w: Float,

    kn1: Float,
    kn2: Float,

    // Variance of the acceleration change error
    v_a_square: Float,
}

impl KalmanFilter {
    pub const fn new() -> Self {
        KalmanFilter {
            x1: 0.0, // Initial height = 0
            x2: 0.0,

            x1_measured: 0.0,

            // The values are extracted from the test below, but a little bit larger
            // C12 and C21 are zero to indicate that x1 is indipendent of x2
            c11: 1.0,
            c12: 0.5,
            c21: 0.5,
            c22: 0.5,

            // Determined from captured data using Steigen33
            c_w: 3.0,

            kn1: 0.,
            kn2: 0.,

            v_a_square: 100.,
        }
    }

    pub fn init(&mut self, pressure_pa: Float) {
        self.x1 = pressure::barometric_formula(pressure_pa);
    }

    pub const fn height(&self) -> Float {
        self.x1
    }

    pub const fn velocity(&self) -> Float {
        self.x2
    }

    pub const fn height_measured(&self) -> Float {
        self.x1_measured
    }

    // Predict new state
    // TODO: c12 and c21 converge to the same value, so mybe it is not needed to calculate the twice
    pub fn update(&mut self, pressure_pa: Float, dt: Float) {
        // Calculation done in KalmanFilterBaro.wxm

        // Prediction step to calculate xn_nm1 and Cxn_nm1
        let x1_n_nm1 = self.x1 + self.x2 * dt;
        let x2_n_nm1 = self.x2;

        let dt2 = dt * dt;
        let dt3 = dt2 * dt;
        let dt4 = dt2 * dt2;

        // To copy the formulas directly from the maxima file
        let v_a_square = self.v_a_square;
        let c11 = self.c11;
        let c12 = self.c12;
        let c21 = self.c21;
        let c22 = self.c22;

        let c_xn_nm1_11 = (dt4 * v_a_square) / 4.0 + dt * (c22 * dt + c21) + c12 * dt + c11;
        let factor = (dt3 * v_a_square) / 2.0 + c22 * dt;
        let c_xn_nm1_12 = factor + c12;
        let c_xn_nm1_21 = factor + c21;
        let c_xn_nm1_22 = dt2 * v_a_square + c22;

        // the calculated version is already the descrete at 100Hz. So not adaption required
        let c_wn = self.c_w; // / dt;
        let y = pressure::barometric_formula(pressure_pa);
        self.x1_measured = y;

        // Inovation step to estimate xn and Cxn
        let c_yn_nm1 = c_xn_nm1_11 + c_wn;

        // TODO: Handle Cyn_nm1 == 0!!! (Cannot happen if CWn > 0 and Cx_nm1_11 becomes never negative (Which shouldn't be the case))

        self.kn1 = c_xn_nm1_11 / c_yn_nm1;
        self.kn2 = c_xn_nm1_21 / c_yn_nm1;

        self.x1 = self.kn1 * (y - x1_n_nm1) + x1_n_nm1;
        self.x2 = self.kn2 * (y - x1_n_nm1) + x2_n_nm1;

        self.c11 = c_xn_nm1_11 - c_xn_nm1_11 * self.kn1;
        self.c12 = c_xn_nm1_12 - c_xn_nm1_12 * self.kn1;
        self.c21 = c_xn_nm1_21 - c_xn_nm1_11 * self.kn2;
        self.c22 = c_xn_nm1_22 - c_xn_nm1_12 * self.kn2;
    }
}

#[cfg(test)]
mod tests {
    use super::*;
    use crate::igc;
    use plotters::coord::Shift;
    use plotters::prelude::*;
    use rand;
    use rand_distr::{Distribution, Normal};
    use std::io::Write;
    use std::{
        env,
        fs::{self, File, OpenOptions},
        path::Path,
    };

    fn min_max(vec: &Vec<Float>) -> (Float, Float) {
        let mut min = Float::MAX;
        let mut max = Float::MIN;
        for v in vec {
            if *v < min {
                min = *v;
            }
            if *v > max {
                max = *v;
            }
        }
        (min, max)
    }

    fn mean(vec: &Vec<Float>, last_n_values: usize) -> Float {
        let mut mean = 0.0;
        for i in vec.len() - last_n_values..vec.len() {
            mean += vec[i];
        }
        mean / (last_n_values as Float)
    }

    #[test]
    fn kalman_filter() {
        const TEST_DATA: &str = "test_data/02-1750.IGC";
        const INTERPOLATION_FACTOR: usize = 75;
        const PLOT_KN: bool = false;
        const PLOT_CN: bool = false;

        const IMG_WIDTH: u32 = 1024;
        const IMG_HEIGHT: u32 = 768;

        const MAX_HEIGHT_ERROR: Float = 6000.0;
        const MAX_VELOCITY_ERROR: Float = 122_000.0;

        let manifest_path = Path::new(env!("CARGO_MANIFEST_DIR"));
        let test_data_path = manifest_path.join(file!());
        let test_data_path = test_data_path.parent().unwrap();
        let test_data_path = test_data_path.join(TEST_DATA);
        let s = fs::read_to_string(test_data_path).expect("Unable to open test data file");

        // From LPS22HH_100Hz_polling_20240111.csv
        let normal = Normal::new(0.0 as Float, 0.396657).unwrap();

        let mut time_orig = Vec::new();
        // let mut pressure = Vec::new();
        // let mut pressure_noised = Vec::new();
        let mut height_orig = Vec::new();
        for l in s.split(igc::NEW_LINE) {
            match igc::parse_records(l) {
                igc::Record::B(record) => {
                    time_orig.push(record.time_utc.to_seconds() as Float);
                    let height = record.gnss_alt as Float;
                    height_orig.push(height);
                }
                _ => continue,
            }
        }

        let min_max_time = min_max(&time_orig);
        let dt = time_orig[1] - time_orig.first().unwrap();

        let mut time = Vec::new();
        time.try_reserve(time_orig.len() * INTERPOLATION_FACTOR)
            .unwrap();
        let mut height = Vec::new();
        height
            .try_reserve(time_orig.len() * INTERPOLATION_FACTOR)
            .unwrap();
        let mut velocity = Vec::new();
        velocity
            .try_reserve(time_orig.len() * INTERPOLATION_FACTOR)
            .unwrap();
        for i in 0..time_orig.len() - 1 {
            let diff_t = time_orig[i + 1] - time_orig[i];
            let diff_h = height_orig[i + 1] - height_orig[i];
            for j in 0..INTERPOLATION_FACTOR {
                let dt_interp = diff_t / (INTERPOLATION_FACTOR as Float);
                time.push(time_orig[i] + j as Float * dt_interp);
                height.push(height_orig[i] + j as Float * diff_h / (INTERPOLATION_FACTOR as Float));
                if j > 0 || i > 0 {
                    velocity
                        .push((height[height.len() - 1] - height[height.len() - 2]) / dt_interp);
                } else {
                    velocity.push(0.);
                }
            }
        }
        let dt = dt / INTERPOLATION_FACTOR as Float;

        let mut kalman_filter = KalmanFilter::new();
        kalman_filter.init(pressure::calculate_pressure(*height.first().unwrap()));
        let mut height_est = Vec::new();
        height_est.try_reserve(time.len()).unwrap();
        let mut velocity_est = Vec::new();
        velocity_est.try_reserve(time.len()).unwrap();

        let mut pressure = Vec::new();
        pressure.try_reserve(time.len()).unwrap();
        let mut pressure_noised = Vec::new();
        pressure_noised.try_reserve(time.len()).unwrap();
        let mut height_noised = Vec::new();
        height_noised.try_reserve(time.len()).unwrap();
        let mut c11 = Vec::new();
        c11.try_reserve(time.len()).unwrap();
        let mut c12 = Vec::new();
        c12.try_reserve(time.len()).unwrap();
        let mut c21 = Vec::new();
        c21.try_reserve(time.len()).unwrap();
        let mut c22 = Vec::new();
        c22.try_reserve(time.len()).unwrap();
        let mut kn1 = Vec::new();
        kn1.try_reserve(time.len()).unwrap();
        let mut kn2 = Vec::new();
        kn2.try_reserve(time.len()).unwrap();

        for h in height.iter() {
            let p = pressure::calculate_pressure(*h);
            let p_noise = p + normal.sample(&mut rand::thread_rng());
            pressure.push(p);
            pressure_noised.push(p_noise);
            height_noised.push(pressure::barometric_formula(p_noise));
            kalman_filter.update(p_noise, dt);
            height_est.push(kalman_filter.x1);
            velocity_est.push(kalman_filter.x2);
            c11.push(kalman_filter.c11);
            c12.push(kalman_filter.c12);
            c21.push(kalman_filter.c21);
            c22.push(kalman_filter.c22);
            kn1.push(kalman_filter.kn1);
            kn2.push(kalman_filter.kn2);
        }
        let min_max_height = min_max(&height);

        let export_path = manifest_path.join("target/export");
        let _ = fs::create_dir(&export_path);
        let image = export_path.join("kalman_barometer.png");
        let root = BitMapBackend::new(&image, (IMG_WIDTH, IMG_HEIGHT)).into_drawing_area();
        root.fill(&WHITE).unwrap();

        let areas = root.split_evenly((
            1 + 1 + 1 + 1 + 2 * PLOT_KN as usize + 4 * PLOT_CN as usize,
            1,
        ));

        let mut area_index = 0;
        let mut estimation_chart = ChartBuilder::on(&areas[area_index])
            .x_label_area_size(35)
            .y_label_area_size(40)
            .margin(5)
            .caption("Kalman filter Barometer", ("sans-serif", 50.0).into_font())
            .build_cartesian_2d(
                min_max_time.0..min_max_time.1,
                min_max_height.0..min_max_height.1,
            )
            .unwrap();

        // Create axes
        estimation_chart
            .configure_mesh()
            .y_desc("Height [m]")
            .y_label_formatter(&|x| format!("{:e}", x))
            .draw()
            .unwrap();

        // Draw real height
        estimation_chart
            .draw_series(LineSeries::new(
                (0..time.len()).map(|i| (time[i], height[i])),
                &BLUE,
            ))
            .unwrap()
            .label("height [m]")
            .legend(|(x, y)| PathElement::new(vec![(x, y), (x + 20, y)], BLUE));

        // Draw estimated height
        estimation_chart
            .draw_series(LineSeries::new(
                (0..time.len()).map(|i| (time[i], height_est[i])),
                &RED,
            ))
            .unwrap()
            .label("height estimated [m]")
            .legend(|(x, y)| PathElement::new(vec![(x, y), (x + 20, y)], RED));

        // Legend
        estimation_chart
            .configure_series_labels()
            .background_style(RGBColor(128, 128, 128))
            .draw()
            .unwrap();
        area_index += 1;

        let mut height_error = Vec::new();
        let mut height_error_noise = Vec::new();
        height_error.try_reserve(height.len()).unwrap();
        height_error_noise.try_reserve(height.len()).unwrap();
        for i in 0..height.len() {
            height_error.push(height[i] - height_est[i]);
            height_error_noise.push(height[i] - height_noised[i]);
        }
        let height_error_limits = min_max(&height_error);
        let mut height_error_chart = ChartBuilder::on(&areas[area_index])
            .x_label_area_size(35)
            .y_label_area_size(40)
            .margin(5)
            .caption("height_error", ("sans-serif", 50.0).into_font())
            .build_cartesian_2d(
                min_max_time.0..min_max_time.1,
                height_error_limits.0..height_error_limits.1,
            )
            .unwrap();
        height_error_chart.configure_mesh().draw().unwrap();
        height_error_chart
            .draw_series(LineSeries::new(
                (0..time.len()).map(|i| (time[i], height_error[i])),
                &RED,
            ))
            .unwrap();
        height_error_chart
            .draw_series(LineSeries::new(
                (0..time.len()).map(|i| (time[i], height_error_noise[i])),
                &BLUE,
            ))
            .unwrap();
        area_index += 1;

        let velocity_limits = min_max(&velocity_est);
        let mut velocity_chart = ChartBuilder::on(&areas[area_index])
            .x_label_area_size(35)
            .y_label_area_size(40)
            .margin(5)
            .caption("velocity", ("sans-serif", 50.0).into_font())
            .build_cartesian_2d(
                min_max_time.0..min_max_time.1,
                velocity_limits.0..velocity_limits.1,
            )
            .unwrap();
        velocity_chart.configure_mesh().draw().unwrap();
        velocity_chart
            .draw_series(LineSeries::new(
                (0..time.len()).map(|i| (time[i], velocity_est[i])),
                &RED,
            ))
            .unwrap();
        area_index += 1;

        let mut velocity_error = Vec::new();
        velocity_error.try_reserve(velocity.len()).unwrap();
        for i in 0..height.len() {
            velocity_error.push(velocity[i] - velocity_est[i]);
        }
        let velocity_error_limits = min_max(&velocity_error);
        let mut velocity_error_chart = ChartBuilder::on(&areas[area_index])
            .x_label_area_size(35)
            .y_label_area_size(40)
            .margin(5)
            .caption("velocity_error", ("sans-serif", 50.0).into_font())
            .build_cartesian_2d(
                min_max_time.0..min_max_time.1,
                velocity_error_limits.0..velocity_error_limits.1,
            )
            .unwrap();
        velocity_error_chart.configure_mesh().draw().unwrap();
        velocity_error_chart
            .draw_series(LineSeries::new(
                (0..time.len()).map(|i| (time[i], velocity_error[i])),
                &RED,
            ))
            .unwrap();
        area_index += 1;

        if PLOT_CN {
            create_simple_chart(
                &areas[area_index],
                "C11",
                &time,
                &c11,
                min_max_time.0,
                min_max_time.1,
            );
            area_index += 1;
            create_simple_chart(
                &areas[area_index],
                "C12",
                &time,
                &c12,
                min_max_time.0,
                min_max_time.1,
            );
            area_index += 1;
            create_simple_chart(
                &areas[area_index],
                "C21",
                &time,
                &c21,
                min_max_time.0,
                min_max_time.1,
            );
            area_index += 1;
            create_simple_chart(
                &areas[area_index],
                "C22",
                &time,
                &c22,
                min_max_time.0,
                min_max_time.1,
            );
            area_index += 1;
        }
        if PLOT_KN {
            create_simple_chart(
                &areas[area_index],
                "KN1",
                &time,
                &kn1,
                min_max_time.0,
                min_max_time.1,
            );
            area_index += 1;
            create_simple_chart(
                &areas[area_index],
                "KN2",
                &time,
                &kn2,
                min_max_time.0,
                min_max_time.1,
            );
            //area_index += 1;
        }

        root.present().expect("Unable to write result to file, please make sure 'export' dir exists under current dir");

        let result_data = export_path.join("kalman_filter_baro_result.csv");
        File::create(result_data.clone()).unwrap();
        let mut result_data_file = OpenOptions::new()
            .write(true)
            .append(true)
            .open(result_data)
            .unwrap();
        writeln!(
            result_data_file,
            "time,height,height_est,height_noise,pressure,pressure_noise,velocity,velocity_est,c11,c12,c21,c22,kn1,kn2"
        )
        .unwrap();
        for i in 0..time.len() {
            writeln!(
                result_data_file,
                "{:.3},{:.3},{:.3},{:.3},{:.3},{:.3},{:.3},{:.3},{:.3},{:.3},{:.3},{:.3},{:.3},{:.3}",
                time[i],
                height[i],
                height_est[i],
                height_noised[i],
                pressure[i],
                pressure_noised[i],
                velocity[i],
                velocity_est[i],
                c11[i],
                c12[i],
                c21[i],
                c22[i],
                kn1[i],
                kn2[i]
            )
            .unwrap();
        }

        let last_n_values = 100;
        println!("Mean C11: {}", mean(&c11, last_n_values));
        println!("Mean C12: {}", mean(&c12, last_n_values));
        println!("Mean C21: {}", mean(&c21, last_n_values));
        println!("Mean C22: {}", mean(&c22, last_n_values));
        println!("Mean Kn1: {}", mean(&kn1, last_n_values));
        println!("Mean Kn2: {}", mean(&kn2, last_n_values));

        assert_eq!(height.len(), height_est.len());
        let mut error = 0.0;
        for i in 0..height.len() {
            error += Float::powi(height[i] - height_est[i], 2);
        }
        println!("Height Error: {}", error);
        assert!(
            error < MAX_HEIGHT_ERROR,
            "height error = {}, expected < {}",
            error,
            MAX_HEIGHT_ERROR
        );

        assert_eq!(velocity.len(), velocity_est.len());
        let mut error = 0.0;
        for i in 0..velocity.len() {
            error += Float::powi(velocity[i] - velocity_est[i], 2);
        }
        println!("Velocity Error: {}", error);
        assert!(
            error < MAX_VELOCITY_ERROR,
            " velocity error = {}, expected < {}",
            error,
            MAX_VELOCITY_ERROR
        );

        // TODO: check that cxx converts to a constant value!
        // Stability of the Kalman filter
    }

    #[test]
    fn kalman_filter_const_height_gain() {
        const PLOT_KN: bool = false;
        const PLOT_CN: bool = false;

        const MAX_HEIGHT_ERROR: Float = 5.0;
        const MAX_VELOCITY_ERROR: Float = 10.0;

        const IMG_WIDTH: u32 = 1024;
        const IMG_HEIGHT: u32 = 768;

        const CONST_HEIGHT: bool = false;
        const FREQ_PRESSURE_SENSOR_HZ: Float = 75.0;

        let manifest_path = Path::new(env!("CARGO_MANIFEST_DIR"));

        // From LPS22HH_100Hz_polling_20240111.csv
        let normal = Normal::new(0.0 as Float, 0.396657).unwrap();

        let mut time_orig = Vec::new();
        let mut height_orig = Vec::new();

        for i in 0..10_000 {
            time_orig.push((i as f64) * 0.1);
            let mut height = 2000.0;
            if !CONST_HEIGHT {
                height += (i as f64) * (1.0 / FREQ_PRESSURE_SENSOR_HZ);
            }
            height_orig.push(height);
        }

        let min_max_time = min_max(&time_orig);
        let dt = time_orig[1] - time_orig.first().unwrap();

        let mut time = Vec::new();
        time.try_reserve(time_orig.len()).unwrap();
        let mut height = Vec::new();
        height.try_reserve(time_orig.len()).unwrap();
        let mut velocity = Vec::new();
        velocity.try_reserve(time_orig.len()).unwrap();
        for i in 0..time_orig.len() - 1 {
            let diff_t = time_orig[i + 1] - time_orig[i];
            let diff_h = height_orig[i + 1] - height_orig[i];
            for j in 0..1 {
                let dt_interp = diff_t;
                time.push(time_orig[i] + j as Float * dt_interp);
                height.push(height_orig[i] + j as Float * diff_h);
                if j > 0 || i > 0 {
                    velocity
                        .push((height[height.len() - 1] - height[height.len() - 2]) / dt_interp);
                } else {
                    velocity.push(0.);
                }
            }
        }

        let mut kalman_filter = KalmanFilter::new();
        kalman_filter.init(pressure::calculate_pressure(*height.first().unwrap()));
        let mut height_est = Vec::new();
        height_est.try_reserve(time.len()).unwrap();
        let mut velocity_est = Vec::new();
        velocity_est.try_reserve(time.len()).unwrap();

        let mut pressure = Vec::new();
        pressure.try_reserve(time.len()).unwrap();
        let mut pressure_noised = Vec::new();
        pressure_noised.try_reserve(time.len()).unwrap();
        let mut height_noised = Vec::new();
        height_noised.try_reserve(time.len()).unwrap();
        let mut c11 = Vec::new();
        c11.try_reserve(time.len()).unwrap();
        let mut c12 = Vec::new();
        c12.try_reserve(time.len()).unwrap();
        let mut c21 = Vec::new();
        c21.try_reserve(time.len()).unwrap();
        let mut c22 = Vec::new();
        c22.try_reserve(time.len()).unwrap();
        let mut kn1 = Vec::new();
        kn1.try_reserve(time.len()).unwrap();
        let mut kn2 = Vec::new();
        kn2.try_reserve(time.len()).unwrap();

        for h in height.iter() {
            let p = pressure::calculate_pressure(*h);
            let p_noise = p + normal.sample(&mut rand::thread_rng());
            pressure.push(p);
            pressure_noised.push(p_noise);
            height_noised.push(pressure::barometric_formula(p_noise));
            kalman_filter.update(p_noise, dt);
            height_est.push(kalman_filter.x1);
            velocity_est.push(kalman_filter.x2);
            c11.push(kalman_filter.c11);
            c12.push(kalman_filter.c12);
            c21.push(kalman_filter.c21);
            c22.push(kalman_filter.c22);
            kn1.push(kalman_filter.kn1);
            kn2.push(kalman_filter.kn2);
        }
        let min_max_height = min_max(&height);

        let export_path = manifest_path.join("target/export");
        let _ = fs::create_dir(&export_path);
        let image = export_path.join("kalman_barometer_const_height_gain.png");
        let root = BitMapBackend::new(&image, (IMG_WIDTH, IMG_HEIGHT)).into_drawing_area();
        root.fill(&WHITE).unwrap();

        let areas = root.split_evenly((
            1 + 1 + 1 + 1 + 2 * PLOT_KN as usize + 4 * PLOT_CN as usize,
            1,
        ));

        let mut area_index = 0;
        let mut estimation_chart = ChartBuilder::on(&areas[area_index])
            .x_label_area_size(35)
            .y_label_area_size(40)
            .margin(5)
            .caption("Kalman filter Barometer", ("sans-serif", 50.0).into_font())
            .build_cartesian_2d(
                min_max_time.0..min_max_time.1,
                min_max_height.0..min_max_height.1,
            )
            .unwrap();

        // Create axes
        estimation_chart
            .configure_mesh()
            .y_desc("Height [m]")
            .y_label_formatter(&|x| format!("{:e}", x))
            .draw()
            .unwrap();

        // Draw real height
        estimation_chart
            .draw_series(LineSeries::new(
                (0..time.len()).map(|i| (time[i], height[i])),
                &BLUE,
            ))
            .unwrap()
            .label("height [m]")
            .legend(|(x, y)| PathElement::new(vec![(x, y), (x + 20, y)], BLUE));

        // Draw estimated height
        estimation_chart
            .draw_series(LineSeries::new(
                (0..time.len()).map(|i| (time[i], height_est[i])),
                &RED,
            ))
            .unwrap()
            .label("height estimated [m]")
            .legend(|(x, y)| PathElement::new(vec![(x, y), (x + 20, y)], RED));

        // Legend
        estimation_chart
            .configure_series_labels()
            .background_style(RGBColor(128, 128, 128))
            .draw()
            .unwrap();
        area_index += 1;

        let mut height_error = Vec::new();
        let mut height_error_noise = Vec::new();
        height_error.try_reserve(height.len()).unwrap();
        height_error_noise.try_reserve(height.len()).unwrap();
        for i in 0..height.len() {
            height_error.push(height[i] - height_est[i]);
            height_error_noise.push(height[i] - height_noised[i]);
        }
        let height_error_limits = min_max(&height_error);
        let mut height_error_chart = ChartBuilder::on(&areas[area_index])
            .x_label_area_size(35)
            .y_label_area_size(40)
            .margin(5)
            .caption("height_error", ("sans-serif", 50.0).into_font())
            .build_cartesian_2d(
                min_max_time.0..min_max_time.1,
                height_error_limits.0..height_error_limits.1,
            )
            .unwrap();
        height_error_chart.configure_mesh().draw().unwrap();
        height_error_chart
            .draw_series(LineSeries::new(
                (0..time.len()).map(|i| (time[i], height_error[i])),
                &RED,
            ))
            .unwrap();
        height_error_chart
            .draw_series(LineSeries::new(
                (0..time.len()).map(|i| (time[i], height_error_noise[i])),
                &BLUE,
            ))
            .unwrap();
        area_index += 1;

        let velocity_limits = min_max(&velocity_est);
        let mut velocity_chart = ChartBuilder::on(&areas[area_index])
            .x_label_area_size(35)
            .y_label_area_size(40)
            .margin(5)
            .caption("velocity", ("sans-serif", 50.0).into_font())
            .build_cartesian_2d(
                min_max_time.0..min_max_time.1,
                velocity_limits.0..velocity_limits.1,
            )
            .unwrap();
        velocity_chart.configure_mesh().draw().unwrap();
        velocity_chart
            .draw_series(LineSeries::new(
                (0..time.len()).map(|i| (time[i], velocity_est[i])),
                &RED,
            ))
            .unwrap();
        area_index += 1;

        let mut velocity_error = Vec::new();
        velocity_error.try_reserve(velocity.len()).unwrap();
        for i in 0..height.len() {
            velocity_error.push(velocity[i] - velocity_est[i]);
        }
        let velocity_error_limits = min_max(&velocity_error);
        let mut velocity_error_chart = ChartBuilder::on(&areas[area_index])
            .x_label_area_size(35)
            .y_label_area_size(40)
            .margin(5)
            .caption("velocity_error", ("sans-serif", 50.0).into_font())
            .build_cartesian_2d(
                min_max_time.0..min_max_time.1,
                velocity_error_limits.0..velocity_error_limits.1,
            )
            .unwrap();
        velocity_error_chart.configure_mesh().draw().unwrap();
        velocity_error_chart
            .draw_series(LineSeries::new(
                (0..time.len()).map(|i| (time[i], velocity_error[i])),
                &RED,
            ))
            .unwrap();
        area_index += 1;

        if PLOT_CN {
            create_simple_chart(
                &areas[area_index],
                "C11",
                &time,
                &c11,
                min_max_time.0,
                min_max_time.1,
            );
            area_index += 1;
            create_simple_chart(
                &areas[area_index],
                "C12",
                &time,
                &c12,
                min_max_time.0,
                min_max_time.1,
            );
            area_index += 1;
            create_simple_chart(
                &areas[area_index],
                "C21",
                &time,
                &c21,
                min_max_time.0,
                min_max_time.1,
            );
            area_index += 1;
            create_simple_chart(
                &areas[area_index],
                "C22",
                &time,
                &c22,
                min_max_time.0,
                min_max_time.1,
            );
            area_index += 1;
        }
        if PLOT_KN {
            create_simple_chart(
                &areas[area_index],
                "KN1",
                &time,
                &kn1,
                min_max_time.0,
                min_max_time.1,
            );
            area_index += 1;
            create_simple_chart(
                &areas[area_index],
                "KN2",
                &time,
                &kn2,
                min_max_time.0,
                min_max_time.1,
            );
            //area_index += 1;
        }

        root.present().expect("Unable to write result to file, please make sure 'export' dir exists under current dir");

        let result_data = export_path.join("kalman_filter_baro_result_const_height_gain.csv");
        File::create(result_data.clone()).unwrap();
        let mut result_data_file = OpenOptions::new()
            .write(true)
            .append(true)
            .open(result_data)
            .unwrap();
        writeln!(
            result_data_file,
            "time,height,height_est,height_noise,pressure,pressure_noise,velocity,velocity_est,c11,c12,c21,c22,kn1,kn2"
        )
        .unwrap();
        for i in 0..time.len() {
            writeln!(
                result_data_file,
                "{},{},{},{},{},{},{},{},{},{},{},{},{},{}",
                time[i],
                height[i],
                height_est[i],
                height_noised[i],
                pressure[i],
                pressure_noised[i],
                velocity[i],
                velocity_est[i],
                c11[i],
                c12[i],
                c21[i],
                c22[i],
                kn1[i],
                kn2[i]
            )
            .unwrap();
        }

        let last_n_values = 100;
        println!("Mean C11: {}", mean(&c11, last_n_values));
        println!("Mean C12: {}", mean(&c12, last_n_values));
        println!("Mean C21: {}", mean(&c21, last_n_values));
        println!("Mean C22: {}", mean(&c22, last_n_values));
        println!("Mean Kn1: {}", mean(&kn1, last_n_values));
        println!("Mean Kn2: {}", mean(&kn2, last_n_values));

        assert_eq!(height.len(), height_est.len());
        let mut error = 0.0;
        for i in 0..height.len() {
            error += Float::powi(height[i] - height_est[i], 2);
        }
        println!("Height Error: {}", error);
        assert!(
            error < MAX_HEIGHT_ERROR,
            "height error = {}, expected < {}",
            error,
            MAX_HEIGHT_ERROR
        );

        assert_eq!(velocity.len(), velocity_est.len());
        let mut error = 0.0;
        for i in 0..velocity.len() {
            error += Float::powi(velocity[i] - velocity_est[i], 2);
        }
        println!("Velocity Error: {}", error);
        assert!(
            error < MAX_VELOCITY_ERROR,
            " velocity error = {}, expected < {}",
            error,
            MAX_VELOCITY_ERROR
        );

        // TODO: check that cxx converts to a constant value!
        // Stability of the Kalman filter
    }

    fn create_simple_chart(
        area: &DrawingArea<BitMapBackend<'_>, Shift>,
        title: &str,
        x: &Vec<Float>,
        y: &Vec<Float>,
        x_min: Float,
        x_max: Float,
    ) {
        let y_limits = min_max(&y);
        let mut chart = ChartBuilder::on(area)
            .x_label_area_size(35)
            .y_label_area_size(40)
            .margin(5)
            .caption(title, ("sans-serif", 50.0).into_font())
            .build_cartesian_2d(x_min..x_max, y_limits.0..y_limits.1)
            .unwrap();
        chart.configure_mesh().draw().unwrap();
        chart
            .draw_series(LineSeries::new((0..x.len()).map(|i| (x[i], y[i])), &RED))
            .unwrap();
    }
}
