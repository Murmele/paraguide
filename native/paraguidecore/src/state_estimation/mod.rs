mod gps;
pub mod sensor_fusion;
use crate::common::{Float, Vec3d};
mod baro_kalmanfilter;
pub mod pressure;

use chrono::{NaiveDate, NaiveTime};
use nmea;

pub struct NmeaData {
    pub fix: bool,
    pub fix_time: Option<NaiveTime>,
    pub fix_date: Option<NaiveDate>,
    pub fix_type: Option<nmea::sentences::FixType>,
    pub latitude: Option<f64>,
    pub longitude: Option<f64>,
    /// MSL Altitude in meters
    pub altitude: Option<f32>,
    pub speed_over_ground: Option<f32>,
    pub true_course: Option<f32>,
    pub num_of_fix_satellites: Option<u32>,
    pub hdop: Option<f32>,
    pub vdop: Option<f32>,
    pub pdop: Option<f32>,
    /// Geoid separation in meters
    pub geoid_separation: Option<f32>,
    // pub fix_satellites_prns: Option<Vec<u32, 12>>,
}

impl Default for NmeaData {
    fn default() -> Self {
        Self {
            fix: false,
            fix_time: None,
            fix_date: None,
            fix_type: None,
            latitude: None,
            longitude: None,
            altitude: None,
            speed_over_ground: None,
            true_course: None,
            num_of_fix_satellites: None,
            hdop: None,
            vdop: None,
            pdop: None,
            geoid_separation: None,
        }
    }
}

pub struct InputData {
    pub gps_data: NmeaData,
    pub barometric_pressure_pa: Float,
    pub temperature: Float,
    pub acceleration: Vec3d<Float>,
    pub magnetometer: Vec3d<Float>,
    pub rotational_speed: Vec3d<Float>,
    pub(crate) battery_soc_percent: Option<Float>,
}

pub struct State {
    pub pos: Vec3d<Float>,
    pub velocity: Vec3d<Float>,
    pub acceleration: Vec3d<Float>,
    pub angle: Vec3d<Float>,
    pub rotational_speed: Vec3d<Float>,
    pub time_ms: u32, // 126 years
}

pub struct StateEstimation {
    pub input_data: InputData,
    pub vario_value_buzzer: Float,
    pub state: State,
    pub baro_kalmanfilter: baro_kalmanfilter::KalmanFilter,
}

impl StateEstimation {
    pub fn new() -> Self {
        StateEstimation {
            input_data: InputData::new(),
            vario_value_buzzer: 0.0,
            state: State::new(),
            baro_kalmanfilter: baro_kalmanfilter::KalmanFilter::new(),
        }
    }

    pub fn baro_only_state(&mut self) {
        self.state.pos.z = pressure::barometric_formula(self.input_data.barometric_pressure_pa)
    }

    pub fn calculate_state(&mut self, dt: Float) {
        //self.baro_only_state(); // simplest estimation
        self.baro_kalmanfilter
            .update(self.input_data.barometric_pressure_pa, dt);

        self.state.pos.z = self.baro_kalmanfilter.height();
        self.state.velocity.z = self.baro_kalmanfilter.velocity();
    }
}

impl InputData {
    pub fn new() -> Self {
        InputData {
            gps_data: Default::default(),
            barometric_pressure_pa: pressure::calculate_pressure(0.0),
            temperature: 0.0,
            acceleration: Default::default(),
            rotational_speed: Default::default(),
            magnetometer: Default::default(),
            battery_soc_percent: None,
        }
    }
}

impl State {
    pub const fn new() -> Self {
        State {
            pos: Vec3d::<Float> {
                x: 0.0,
                y: 0.0,
                z: 0.0,
            },
            velocity: Vec3d::<Float> {
                x: 0.0,
                y: 0.0,
                z: 0.0,
            },
            acceleration: Vec3d::<Float> {
                x: 0.0,
                y: 0.0,
                z: 0.0,
            },
            angle: Vec3d::<Float> {
                x: 0.0,
                y: 0.0,
                z: 0.0,
            },
            rotational_speed: Vec3d::<Float> {
                x: 0.0,
                y: 0.0,
                z: 0.0,
            },
            time_ms: 0,
        }
    }
}
