pub trait Timekeeper {
    fn get_time_us() -> u64;
}
