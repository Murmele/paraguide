use crate::common::{Float, Vec3d};
use crate::debug;
use crate::file_handling;
use crate::helper::{float_to_string, num_to_string, unum_to_string};
use crate::igc;
use crate::logging;
use crate::state_estimation;
use crate::{config, trace};
use chrono::{Datelike, NaiveTime};
use core::env;
use core::marker::PhantomData;
use heapless::String;
use nmea::sentences as gps;
//#cfg!(feature="gui")
//use gui::widgets;

const GIT_HASH: &str = env!("GIT_HASH");
// Period between to b records in seconds
const B_RECORD_PERIOD_S: usize = 1;

// First X stands for not approved by IGC
// PG: ParaGuide
// const MANUFACTURER_ID: &str = "XPG";
// const FR: &str = "0000PG"; // 6 Characters! // TODO: make more flexible

pub use config::VarioSound;
pub use config::VarioSoundSettings;

#[derive(Debug)]
pub enum Error {
    NoTimeAvailable,
    NoDateAvailable,
    /// Failed to convert some data from one type to another
    ConversionFailed,
    /// Failed to fill some buffer
    BufferFull,
    /// If there is no logfile, it is not possible to log to a file
    NoLogFile,
    UnableToWriteToFile,
    NotANumber,
    FailedIgcWriting,
    FileAccessFailed,
    GNSSNoFix,
    NoNewLinesAvailable,
    NoConfigFile,
    NoVersionInConfig,
    InvalidConfigFile,
    UnableToWriteConfig,
    UnableToOpenFile,
    UnableToCreateHamac,
}

#[derive(Clone, Copy)]
pub struct DeviceInfo<const N: usize> {
    pub manufacturer: &'static str,
    pub model: &'static str,
    pub firmware_version: &'static str,
    pub hw_revision: &'static str,
    pub igc_manufacturer_id: &'static str, // 3 digit id
    pub igc_fr: &'static str,

    pub gnss_receiver_manufacturer: &'static str,
    pub gnss_receiver_model: &'static str,
    pub gnss_receiver_max_alt: &'static str,
    pub gnss_receiver_systems: &'static [igc::h_record::GNSSSystems; N],
    pub gnss_receiver_channels: &'static str,

    pub pressure_sensor_manufacturer: &'static str,
    pub pressure_sensor_model: &'static str,
    pub pressure_sensor_max_alt: &'static str,
}

#[derive(PartialEq, Eq, Debug, Clone, Copy)]
pub enum State {
    Idle,
    /// A start flight triggered. Go into flight mode
    Takeoff,
    Flight,
    Landing,
}

struct AutoTakeoffLanding {
    time_us: u64,
    height: Float,
}

pub struct Paraguide<'a, T, File, TIME, const N: usize> {
    pub(crate) config: config::Config,
    pub data: state_estimation::StateEstimation,
    pub(crate) file_handler: T,
    log_file: Option<File>,
    log_file_name: String<50>,
    time_last_b_record_logged_s: u32,
    pub device_info: DeviceInfo<N>,
    state: State,
    time_keeper: PhantomData<TIME>,
    igc_secret: &'a str,

    auto_takeoff_landing: Option<AutoTakeoffLanding>,
}

#[derive(PartialEq)]
enum TakeoffLanding {
    Takeoff,
    Landing,
}

impl<'a, T, TIME, const N: usize, File> Paraguide<'a, T, File, TIME, N>
where
    File: file_handling::File + igc::Writer,
    T: file_handling::FileHandler<File> + Clone,
    TIME: logging::Timekeeper,
{
    pub fn new(file_handler: T, device_info: &DeviceInfo<N>, igc_secret: &'a str) -> Self {
        Paraguide {
            config: config::Config::new(),
            data: state_estimation::StateEstimation::new(),
            device_info: *device_info,
            state: State::Idle,

            // Internals
            file_handler,
            log_file: None,
            log_file_name: String::new(),
            time_last_b_record_logged_s: 0,
            time_keeper: PhantomData,
            igc_secret,
            auto_takeoff_landing: None,
        }
    }

    pub async fn shutdown(&mut self) -> Result<(), Error> {
        debug!("Shut down");
        let _ = self.end_flight().await;
        self.write_config().await
    }

    pub async fn init(&mut self, pressure: Float) -> Result<(), ()> {
        self.data.input_data.barometric_pressure_pa = pressure;
        self.data.baro_kalmanfilter.init(pressure);
        self.data.calculate_state(0.);
        // TODO: remove when they are used. Just here to not get a compile error because of unused config
        // self.config.auto_power_off_min;
        if let Err(_) = self.read_config().await {
            debug!("Unable to read config");
        }
        Ok(())
    }

    fn lowpass_vario_value(
        mut vario_value_curr: Float,
        vario_value_should: Float,
        vario_step: Float,
    ) -> Float {
        if vario_value_curr < vario_value_should {
            vario_value_curr += vario_step;
            if vario_value_curr > vario_value_should {
                vario_value_curr = vario_value_should;
            }
        } else if vario_value_curr > vario_value_should {
            vario_value_curr -= vario_step;
            if vario_value_curr < vario_value_should {
                vario_value_curr = vario_value_should;
            }
        }
        vario_value_curr
    }

    fn takeoff_landing_condition_fullfilled(
        &mut self,
        height_threshold: Float,
        timeout_s: usize,
        takeoff_landing: TakeoffLanding,
    ) -> bool {
        if let Some(atl) = &mut self.auto_takeoff_landing {
            if TIME::get_time_us() - atl.time_us > timeout_s as u64 * 1_000_000 {
                // TODO: check if z position is available!
                atl.height = self.data.state.pos.z;
                atl.time_us = TIME::get_time_us();

                takeoff_landing == TakeoffLanding::Landing
            } else {
                let upper_threshold = atl.height + height_threshold as Float;
                let lower_threshold = atl.height - height_threshold as Float;
                if self.data.state.pos.z > upper_threshold
                    || self.data.state.pos.z < lower_threshold
                {
                    self.auto_takeoff_landing = None;

                    takeoff_landing == TakeoffLanding::Takeoff
                } else {
                    false
                }
            }
        } else {
            // TODO: check if z position is available!
            self.auto_takeoff_landing = Some(AutoTakeoffLanding {
                height: self.data.state.pos.z,
                time_us: TIME::get_time_us(),
            });
            false
        }
    }

    fn takeoff_condition_fullfilled(&mut self) -> bool {
        if !self.config.auto_takeoff() {
            return false;
        }
        self.takeoff_landing_condition_fullfilled(
            self.config.auto_takeoff_height_threshold() as Float,
            self.config.auto_takeoff_timeout(),
            TakeoffLanding::Takeoff,
        )
    }

    fn landing_condition_fullfilled(&mut self) -> bool {
        if !self.config.auto_landing() {
            return false;
        }
        self.takeoff_landing_condition_fullfilled(
            self.config.auto_landing_height_threshold() as Float,
            self.config.auto_landing_timeout(),
            TakeoffLanding::Landing,
        )
    }

    pub fn is_mute(&self) -> bool {
        self.config.is_mute()
    }

    pub fn set_mute(&mut self, mute: bool) {
        self.config.set_mute(mute);
    }

    pub fn toggle_mute(&mut self) {
        self.config.toggle_mute();
    }

    pub fn state(&self) -> State {
        self.state
    }

    /// evaluate new sensor data and calculate new states
    pub async fn step(&mut self, dt: Float) -> Result<(), Error> {
        self.data.calculate_state(dt);

        // trace!("State height: {}", self.data.state.pos.z);

        if self.config.get_vario_damping_factor() == 0 {
            self.data.vario_value_buzzer = self.data.state.velocity.z;
        } else {
            self.data.vario_value_buzzer = Self::lowpass_vario_value(
                self.data.vario_value_buzzer,
                self.data.state.velocity.z,
                (self.config.get_vario_damping_factor() as Float) / 10.,
            );
        }

        self.state_machine_step().await
    }

    async fn state_machine_step(&mut self) -> Result<(), Error> {
        match self.state {
            State::Idle => {
                if self.takeoff_condition_fullfilled() {
                    debug!(
                        "{} Takeoff condition fullfilled",
                        TIME::get_time_us() as Float / 1_000_000.
                    );
                    self.state = State::Takeoff;
                }
                Ok(())
            }
            State::Takeoff => {
                debug!("Start Flight");
                if let Err(e) = self.takeoff().await {
                    if self.landing_condition_fullfilled() {
                        self.state = State::Landing;
                    }
                    Err(e)
                } else {
                    self.state = State::Flight;
                    debug!("Start Flight SUCCESS");
                    Ok(())
                }
            }
            State::Flight => {
                if self.landing_condition_fullfilled() {
                    self.state = State::Landing;
                } else {
                    self.log_baro_values().await?;
                    self.log_kalman_values().await?;
                }
                Ok(())
            }
            State::Landing => {
                debug!(
                    "{} Landing condition fullfilled",
                    TIME::get_time_us() as Float / 1_000_000.
                );
                self.stop_logging().await?;
                self.state = State::Idle;
                Ok(())
            }
        }
    }

    pub async fn set_pressure_temperature(&mut self, pressure: Float, temperature: Float) {
        trace!("Set Pressure: {}", pressure);
        self.data.input_data.barometric_pressure_pa = pressure;
        self.data.input_data.temperature = temperature;
    }

    pub async fn set_acceleration_rotational_speed(
        &mut self,
        acceleration: Vec3d<Float>,
        rotational_speed: Vec3d<Float>,
    ) {
        self.data.input_data.acceleration = acceleration;
        self.data.input_data.rotational_speed = rotational_speed;
    }

    /// Starting a flight.
    /// A flight can be started by calling this function or if
    /// auto takeoff is enabled, when the takeoff condition is fullfilled
    pub async fn start_flight(&mut self) -> Result<(), Error> {
        self.state = State::Takeoff;
        self.state_machine_step().await
    }

    async fn takeoff(&mut self) -> Result<(), Error> {
        assert_eq!(self.state, State::Takeoff);
        self.start_logging().await
    }

    pub fn logging_active(&self) -> bool {
        self.log_file.is_some()
    }

    async fn start_logging(&mut self) -> Result<(), Error> {
        // Unable to start flight if we don't have a time or date,
        // because no igc file can be created
        let date = self
            .data
            .input_data
            .gps_data
            .fix_date
            .ok_or(Error::NoDateAvailable)?;

        let mut file_name: String<50> = String::new();
        (|| {
            file_name.push_str("LOG/")?;
            num_to_string(date.year(), 4, &mut file_name)?;
            file_name.push_str("/")?;
            unum_to_string(date.month(), 2, &mut file_name)?;
            file_name.push_str("-")?;
            unum_to_string(date.day(), 2, &mut file_name)
        })()
        .map_err(|_| Error::BufferFull)?;
        self.file_handler
            .create_folders(&file_name)
            .await
            .map_err(|_| Error::FileAccessFailed)?;

        // Short filename according to igc (The flight number is missing here. It will be determined below)
        (|| {
            file_name.push_str("/")?;
            file_name.push(igc::year_to_file_name_format(date.year() as usize))?;
            file_name.push(igc::month_to_file_name_format(date.month() as usize))?;
            file_name.push(igc::day_to_file_name_format(date.day() as usize))?;
            file_name.push_str(&self.device_info.igc_manufacturer_id)
        })()
        .map_err(|_| Error::BufferFull)?;

        let mut flight_number = 1;
        for i in 1..99 {
            let mut f: String<50> = String::new();
            (|| {
                f.push_str(&file_name)?;
                unum_to_string(i, 2, &mut f)?;
                f.push_str(".igc")
            })()
            .map_err(|_| Error::BufferFull)?;

            if !self
                .file_handler
                .file_exists(&f)
                .await
                .map_err(|_| Error::FileAccessFailed)?
            {
                flight_number = i;
                self.log_file = Some(
                    self.file_handler
                        .open(&f, file_handling::Mode::WriteCreate)
                        .await
                        .map_err(|_| Error::FileAccessFailed)?,
                );
                self.log_file_name.clear();
                self.log_file_name = f;
                break;
            }
        }

        let logfile = self.log_file.as_mut().ok_or(Error::NoLogFile)?;

        // According IGC Specification
        igc::a_record::create_record(
            self.device_info.igc_manufacturer_id,
            self.device_info.igc_fr,
            "",
            logfile,
        )
        .await
        .map_err(|_| Error::FailedIgcWriting)?;
        igc::h_record::create_record_date(
            date.day().into(),
            date.month().into(),
            date.year() as u32,
            flight_number as usize,
            logfile,
        )
        .await
        .map_err(|_| Error::FailedIgcWriting)?;
        igc::h_record::create_record_pilot(self.config.get_pilot_name(), logfile)
            .await
            .map_err(|_| Error::FailedIgcWriting)?;
        igc::h_record::create_record_glider_type(self.config.get_glider_type(), logfile)
            .await
            .map_err(|_| Error::FailedIgcWriting)?;
        igc::h_record::create_record_glider_id(self.config.get_glider_id(), logfile)
            .await
            .map_err(|_| Error::FailedIgcWriting)?;
        igc::h_record::create_record_pos_datum(igc::h_record::GPSDatum::WGS84, logfile)
            .await
            .map_err(|_| Error::FailedIgcWriting)?;
        igc::h_record::create_record_rfw(self.device_info.firmware_version, logfile)
            .await
            .map_err(|_| Error::FailedIgcWriting)?;
        igc::h_record::create_record_rhw(self.device_info.hw_revision, logfile)
            .await
            .map_err(|_| Error::FailedIgcWriting)?;
        igc::h_record::create_record_fty(
            self.device_info.manufacturer,
            self.device_info.model,
            logfile,
        )
        .await
        .map_err(|_| Error::FailedIgcWriting)?;
        igc::h_record::create_record_gnss_receiver(
            self.device_info.gnss_receiver_manufacturer,
            self.device_info.gnss_receiver_model,
            self.device_info.gnss_receiver_channels,
            self.device_info.gnss_receiver_max_alt,
            self.device_info.gnss_receiver_systems,
            logfile,
        )
        .await
        .map_err(|_| Error::FailedIgcWriting)?;
        igc::h_record::create_record_pressure_sensor(
            self.device_info.pressure_sensor_manufacturer,
            self.device_info.pressure_sensor_model,
            self.device_info.pressure_sensor_max_alt,
            logfile,
        )
        .await
        .map_err(|_| Error::FailedIgcWriting)?;
        igc::h_record::create_record_security(igc::h_record::HRecordSecurity::OK, logfile)
            .await
            .map_err(|_| Error::FailedIgcWriting)?;

        self.log_git_hash()
            .await
            .map_err(|_| Error::FailedIgcWriting)?;

        Ok(())
    }

    pub async fn landing(&mut self) -> Result<(), Error> {
        match self.state {
            State::Flight | State::Takeoff => self.state = State::Landing,
            State::Idle | State::Landing => (),
        }
        Ok(())
    }

    pub async fn end_flight(&mut self) -> Result<(), Error> {
        if self.state == State::Takeoff || self.state == State::Flight {
            self.state = State::Landing;
            self.state_machine_step().await?;
        }
        Ok(())
    }

    async fn stop_logging(&mut self) -> Result<(), Error> {
        use hmac::{Hmac, Mac};
        use sha2::Sha256;

        type HmacSha256 = Hmac<Sha256>;

        // Calculate IGC security checksum

        // Close file to flush the data to the storage
        if let Some(file) = &mut self.log_file {
            file.close().await;
        } else {
            return Ok(());
        }
        self.log_file = None;

        // Reopen file again and calculate security so the sha256 hashing must not be done
        // during logging
        let mut log_file = self
            .file_handler
            .open(&self.log_file_name, file_handling::Mode::Read)
            .await
            .map_err(|_| Error::UnableToOpenFile)?;

        let mut line: String<{ file_handling::LINE_LENGHT }> = String::new();
        let mut hasher = HmacSha256::new_from_slice(self.igc_secret.as_bytes())
            .map_err(|_| Error::UnableToCreateHamac)?;
        while let Ok(()) = log_file.read_line(&mut line).await {
            hasher.update(line.as_bytes());
        }
        log_file.close().await;
        let result = hasher.finalize().into_bytes();

        // Append g record to file
        let mut log_file = self
            .file_handler
            .open(&self.log_file_name, file_handling::Mode::WriteAppend)
            .await
            .map_err(|_| Error::UnableToOpenFile)?;
        igc::g_record::create_record(&result[..], &mut log_file)
            .await
            .map_err(|_| Error::UnableToWriteToFile)?;
        log_file.close().await;

        Ok(())
    }

    pub async fn add_battery_state(&mut self, battery_soc_percent: Float) -> Result<(), Error> {
        self.data.input_data.battery_soc_percent = Some(battery_soc_percent);
        if self.state == State::Flight {
            self.log_battery_state().await
        } else {
            Ok(())
        }
    }

    pub fn soc(&self) -> Option<Float> {
        self.data.input_data.battery_soc_percent
    }

    pub fn has_fix(&self) -> bool {
        self.data.input_data.gps_data.fix
    }

    pub async fn add_nmea_message(&mut self, message: &nmea::ParseResult) -> Result<(), Error> {
        self.data.input_data.gps_data.fix = false;
        match message {
            nmea::ParseResult::RMC(d) => {
                if d.status_of_fix != gps::rmc::RmcStatusOfFix::Invalid {
                    self.data.input_data.gps_data.fix = true;
                    self.data.input_data.gps_data.fix_date = d.fix_date;
                    self.data.input_data.gps_data.fix_time = d.fix_time;
                    self.data.input_data.gps_data.latitude = d.lat;
                    self.data.input_data.gps_data.longitude = d.lon;
                } else {
                    self.data.input_data.gps_data.fix = false;
                    return Err(Error::GNSSNoFix);
                }
            }
            nmea::ParseResult::VTG(_d) => {}
            nmea::ParseResult::GGA(d) => {
                self.data.input_data.gps_data.fix_type = d.fix_type;
                if let Some(fix_type) = d.fix_type {
                    if fix_type.is_valid() {
                        self.data.input_data.gps_data.fix = true;
                        self.data.input_data.gps_data.fix_time = d.fix_time;
                        self.data.input_data.gps_data.altitude = d.altitude;
                        self.data.input_data.gps_data.latitude = d.latitude;
                        self.data.input_data.gps_data.longitude = d.longitude;
                    } else {
                        self.data.input_data.gps_data.fix = false;
                        return Err(Error::GNSSNoFix);
                    }
                    let fix_validity = match d.fix_type {
                        Some(v) => {
                            if v.is_valid() {
                                igc::b_record::FixValidity::A
                            } else {
                                igc::b_record::FixValidity::V
                            }
                        }
                        None => igc::b_record::FixValidity::V,
                    };
                    let time_s = (TIME::get_time_us() / 1_000_000) as u32;
                    if time_s - self.time_last_b_record_logged_s >= B_RECORD_PERIOD_S as u32 {
                        if let Some(logfile) = &mut self.log_file {
                            igc::b_record::create_record(
                                d.fix_time
                                    .unwrap_or(NaiveTime::from_hms_opt(0, 0, 0).unwrap()),
                                d.latitude.unwrap_or(0.) as Float,
                                d.longitude.unwrap_or(0.) as Float,
                                fix_validity,
                                self.data.baro_kalmanfilter.height_measured(),
                                d.altitude.unwrap_or(0.) as Float,
                                logfile,
                            )
                            .await
                            .map_err(|_| Error::FailedIgcWriting)?;
                        }
                        self.time_last_b_record_logged_s = time_s; // Only if writing did not fail
                    }
                }
            }
            _ => {}
        }
        Ok(())
    }

    pub fn get_vario_sound(&mut self) -> config::VarioSoundSettings {
        if self.state == State::Takeoff || self.state == State::Flight {
            self.config.get_vario_sound(self.data.vario_value_buzzer)
        } else {
            config::VarioSoundSettings::new_muted()
        }
    }

    async fn log_baro_values(&mut self) -> Result<(), Error> {
        let logfile = self.log_file.as_mut().ok_or(Error::NoLogFile)?;

        let mut line: String<{ igc::LINE_STRING_SIZE }> = String::new();
        (|| {
            line.push('L')?;
            line.push_str(self.device_info.igc_manufacturer_id)?;
            line.push_str("2_,")?;
            unum_to_string(self.data.state.time_ms, 0, &mut line)?;
            line.push(',')?;
            float_to_string(self.data.input_data.barometric_pressure_pa, 3, &mut line)?;
            line.push_str(igc::NEW_LINE)
        })()
        .map_err(|_| Error::FailedIgcWriting)?;

        logfile
            .push_str(&line)
            .await
            .map_err(|_| Error::FailedIgcWriting)
    }

    async fn log_kalman_values(&mut self) -> Result<(), Error> {
        let logfile = self.log_file.as_mut().ok_or(Error::NoLogFile)?;

        let mut line: String<{ igc::LINE_STRING_SIZE }> = String::new();
        (|| {
            line.push('L')?;
            line.push_str(self.device_info.igc_manufacturer_id)?;
            line.push_str("1_,")?;
            unum_to_string(self.data.state.time_ms, 0, &mut line)?;
            line.push(',')?;
            float_to_string(self.data.baro_kalmanfilter.height_measured(), 3, &mut line)?;
            line.push(',')?;
            float_to_string(self.data.baro_kalmanfilter.height(), 3, &mut line)?;
            line.push(',')?;
            float_to_string(self.data.baro_kalmanfilter.velocity(), 3, &mut line)?;
            line.push_str(igc::NEW_LINE)
        })()
        .map_err(|_| Error::FailedIgcWriting)?;

        logfile
            .push_str(&line)
            .await
            .map_err(|_| Error::FileAccessFailed)
    }

    async fn log_git_hash(&mut self) -> Result<(), Error> {
        let logfile = self.log_file.as_mut().ok_or(Error::NoLogFile)?;

        let mut line: String<{ igc::LINE_STRING_SIZE }> = String::new();
        (|| {
            line.push('L')?;
            line.push_str(self.device_info.igc_manufacturer_id)?;
            line.push_str("GIT")?;
            line.push_str(GIT_HASH)?;
            line.push_str(igc::NEW_LINE)
        })()
        .map_err(|_| Error::BufferFull)?;

        logfile
            .push_str(&line)
            .await
            .map_err(|_| Error::FileAccessFailed)
    }

    async fn log_battery_state(&mut self) -> Result<(), Error> {
        let logfile = self.log_file.as_mut().ok_or(Error::NoLogFile)?;

        if let Some(soc) = self.data.input_data.battery_soc_percent {
            let mut line: String<{ igc::LINE_STRING_SIZE }> = String::new();
            (|| {
                line.push('L')?;
                line.push_str(self.device_info.igc_manufacturer_id)?;
                line.push_str("BAT")?;
                float_to_string(soc, 3, &mut line)?;
                line.push_str(igc::NEW_LINE)
            })()
            .map_err(|_| Error::FailedIgcWriting)?;

            logfile
                .push_str(&line)
                .await
                .map_err(|_| Error::FileAccessFailed)
        } else {
            Ok(())
        }
    }
}

#[cfg(test)]
mod tests {
    use super::*;
    use crate::state_estimation::pressure;
    use crate::{logging::Timekeeper, state_estimation::pressure::calculate_pressure};
    use chrono::{NaiveDate, Utc};
    use core::{f64::consts::PI, marker::PhantomData};
    use file_handling::{self, File as FileTrait, FileHandler as FileHandlerTrait, Mode};
    use nmea::{
        sentences::{
            gga::GgaData,
            rmc::{RmcData, RmcStatusOfFix},
        },
        ParseResult,
    };
    use std::path::PathBuf;
    use std::sync::Mutex;

    pub const DUMMY_DEVICE_INFO: DeviceInfo<0> = DeviceInfo {
        manufacturer: "Martin Marmsoler",
        model: "Paraguide",
        firmware_version: "0.1.0",
        hw_revision: "3.3",
        igc_fr: "Paragd",           // less than equal 6 characters!
        igc_manufacturer_id: "XPG", // X + 2 Characters

        gnss_receiver_manufacturer: "Quectel",
        gnss_receiver_model: "GNSS L76-L",
        gnss_receiver_max_alt: "80000",
        gnss_receiver_channels: "5",
        gnss_receiver_systems: &[],
        pressure_sensor_manufacturer: "ST",
        pressure_sensor_model: "LPS22HDTR",
        pressure_sensor_max_alt: "11000",
    };

    // #[derive(Clone)] // Why this doesn't work?
    pub struct FileHandler {
        file: PhantomData<File>,
    }

    impl Clone for FileHandler {
        fn clone(&self) -> Self {
            Self { file: self.file }
        }
    }

    impl FileHandler {
        pub fn new() -> Self {
            Self { file: PhantomData }
        }
    }

    pub struct File {}

    impl File {
        fn new(_file_path: PathBuf, _mode: Mode) -> Result<Self, Error> {
            Ok(Self {})
        }
    }

    pub enum Error {
        Unknown,
    }

    impl FileHandlerTrait<File> for FileHandler {
        type Error = Error;

        async fn file_exists(&self, _file_path: &str) -> Result<bool, Self::Error> {
            Ok(false)
        }

        async fn open(&mut self, file_path: &str, mode: Mode) -> Result<File, Self::Error> {
            File::new(file_path.into(), mode)
        }

        async fn create_folders(&self, _path: &str) -> Result<(), Self::Error> {
            Ok(())
        }
    }

    impl igc::Writer for File {
        async fn push(&mut self, _c: char) -> Result<(), ()> {
            Ok(())
        }
        async fn push_str(&mut self, _content: &str) -> Result<(), ()> {
            Ok(())
        }
    }

    impl FileTrait for File {
        type Error = Error;
        async fn close(&mut self) -> () {}
        async fn read_line(
            &mut self,
            _out: &mut heapless::String<{ file_handling::LINE_LENGHT }>,
        ) -> Result<(), Self::Error> {
            Err(Self::Error::Unknown)
        }

        async fn write(&mut self, _text: &str) -> Result<(), Self::Error> {
            Err(Error::Unknown)
        }
    }

    #[derive(Debug)]
    enum WaitError {
        InitialStateInvalid(State, State),
        NotExpectedNewState(State, State),
        AtLeastOneStateNotReached(State),
        /// No next state expected, but we got one
        NoNextState(State),
    }

    async fn wait<'a, T, File, TIME, const N: usize>(
        pg: &mut Paraguide<'a, T, File, TIME, N>,
        wait_time: u64,
        step_period: f64,
        step_pressure: Option<Vec<Float>>,
        state_transitions: Vec<State>,
        time: &std::sync::Mutex<u64>,
    ) -> Result<(), WaitError>
    where
        File: file_handling::File + igc::Writer,
        T: file_handling::FileHandler<File> + Clone,
        TIME: logging::Timekeeper,
    {
        let mut iter = state_transitions.into_iter();
        let mut last_transition = iter.next().unwrap(); // At least one must be in there
        if pg.state != last_transition {
            return Err(WaitError::InitialStateInvalid(last_transition, pg.state));
        }

        let mut next = iter.next();

        let mut pressure_index = 0;

        for _ in 0..(wait_time as f64 / step_period).ceil() as u64 {
            if let Some(p) = &step_pressure {
                if pressure_index < p.len() {
                    let p = p[pressure_index];
                    pg.set_pressure_temperature(p, 0.0).await;
                    pressure_index += 1;
                }
            }
            if let Err(e) = pg.step(step_period).await {
                println!("Paraguide step Error: {:?}", e);
            }

            if pg.state != last_transition {
                let state = next.ok_or(WaitError::NoNextState(pg.state))?; // Next must be available otherwise there cannot be a transition
                if pg.state != state {
                    return Err(WaitError::NotExpectedNewState(pg.state, state));
                }
                println!("New state: {:?}", pg.state);
                next = iter.next();
                last_transition = pg.state;
            }

            *time.lock().unwrap() += (step_period * 1_000_000.) as u64;
        }

        if next.is_some() {
            // Check that no more elements are available
            Err(WaitError::AtLeastOneStateNotReached(next.unwrap()))
        } else {
            Ok(())
        }
    }

    #[tokio::test]
    async fn test_auto_takeoff_landing() {
        static TIME_US: Mutex<u64> = Mutex::new(0);

        pub struct TimeKeeper {}

        impl logging::Timekeeper for TimeKeeper {
            fn get_time_us() -> u64 {
                return *TIME_US.lock().unwrap();
            }
        }

        let mut pg: Paraguide<'_, _, File, TimeKeeper, 0> =
            Paraguide::new(FileHandler::new(), &DUMMY_DEVICE_INFO, "Secret");

        pg.init(calculate_pressure(1000.)).await.unwrap();

        const STEP_PERIOD: f64 = 13. / 1000.;
        const EXPECTED_TAKEOFF_TIMEOUT: u64 = 60;
        const EXPECTED_TAKEOFF_HEIGHT_THRESHOLD: usize = 6;
        const EXPECTED_LANDING_TIMEOUT: u64 = 120;
        const EXPECTED_LANDING_HEIGHT_THRESHOLD: usize = 1;

        pg.config.set_auto_takeoff(true);
        pg.config
            .set_auto_takeoff_height_threshold(EXPECTED_TAKEOFF_HEIGHT_THRESHOLD);
        pg.config
            .set_auto_takeoff_timeout(EXPECTED_TAKEOFF_TIMEOUT as usize);
        pg.config.set_auto_landing(true);
        pg.config
            .set_auto_landing_height_threshold(EXPECTED_LANDING_HEIGHT_THRESHOLD);
        pg.config
            .set_auto_landing_timeout(EXPECTED_LANDING_TIMEOUT as usize);
        assert_eq!(pg.config.auto_takeoff(), true);
        assert_eq!(
            pg.config.auto_takeoff_height_threshold(),
            EXPECTED_TAKEOFF_HEIGHT_THRESHOLD
        );
        assert_eq!(
            pg.config.auto_takeoff_timeout(),
            EXPECTED_TAKEOFF_TIMEOUT as usize
        );
        assert_eq!(pg.config.auto_landing(), true);
        assert_eq!(
            pg.config.auto_landing_height_threshold(),
            EXPECTED_LANDING_HEIGHT_THRESHOLD
        );
        assert_eq!(
            pg.config.auto_landing_timeout(),
            EXPECTED_LANDING_TIMEOUT as usize
        );

        assert_eq!(pg.takeoff_condition_fullfilled(), false);
        assert_eq!(pg.state, State::Idle);

        wait(
            &mut pg,
            EXPECTED_TAKEOFF_TIMEOUT + 2,
            STEP_PERIOD,
            None,
            vec![State::Idle],
            &TIME_US,
        )
        .await
        .unwrap();

        assert_eq!(pg.takeoff_condition_fullfilled(), false);
        assert_eq!(pg.state, State::Idle);

        wait(
            &mut pg,
            EXPECTED_TAKEOFF_TIMEOUT + 2,
            STEP_PERIOD,
            None,
            vec![State::Idle],
            &TIME_US,
        )
        .await
        .unwrap();

        // Height did not change
        assert_eq!(pg.takeoff_condition_fullfilled(), false);
        assert_eq!(pg.state, State::Idle);

        println!("Set pressure. Below takeoff treshold");
        pg.set_pressure_temperature(
            calculate_pressure(1000. + EXPECTED_TAKEOFF_HEIGHT_THRESHOLD as Float - 3.0),
            0.,
        )
        .await;

        wait(
            &mut pg,
            EXPECTED_TAKEOFF_TIMEOUT + 2,
            STEP_PERIOD,
            None,
            vec![State::Idle],
            &TIME_US,
        )
        .await
        .unwrap();

        // Height did not change that much
        assert_eq!(pg.state, State::Idle);

        println!(
            "{} Set pressure. Above takeoff treshold. Starting flight",
            TimeKeeper::get_time_us() as Float / 1_000_000.
        );
        pg.set_pressure_temperature(
            calculate_pressure(1000. + EXPECTED_TAKEOFF_HEIGHT_THRESHOLD as Float + 5.0),
            0.,
        )
        .await;

        wait(
            &mut pg,
            EXPECTED_TAKEOFF_TIMEOUT + 2,
            STEP_PERIOD,
            None,
            vec![State::Idle, State::Takeoff],
            &TIME_US,
        )
        .await
        .unwrap();
        assert_eq!(pg.state, State::Takeoff);

        wait(
            &mut pg,
            EXPECTED_LANDING_TIMEOUT,
            STEP_PERIOD,
            None,
            vec![State::Takeoff, State::Landing, State::Idle],
            &TIME_US,
        )
        .await
        .unwrap();

        // Landing, because we didn't move anymore
        // Due to an error we weren't able to go into flight mode
        assert_eq!(pg.state, State::Idle);

        // Now setting all parameters to go also in flight mode
        let date = NaiveDate::from_ymd_opt(2015, 6, 30);
        let time = date
            .unwrap()
            .and_hms_nano_opt(23, 59, 59, 1_000_000_000)
            .unwrap()
            .and_local_timezone(Utc)
            .unwrap()
            .time();
        let gga_data = GgaData {
            altitude: Some(2000.),
            geoid_separation: None,
            hdop: None,
            fix_satellites: Some(5),
            fix_time: Some(time),
            fix_type: Some(nmea::sentences::FixType::Gps),
            latitude: Some(45.3),
            longitude: Some(10.),
        };
        pg.add_nmea_message(&ParseResult::GGA(gga_data))
            .await
            .unwrap();

        let rmc_data = RmcData {
            fix_date: date,
            fix_time: Some(time),
            faa_mode: None,
            lat: None,
            lon: None,
            magnetic_variation: None,
            speed_over_ground: None,
            true_course: None,
            nav_status: None,
            status_of_fix: RmcStatusOfFix::Autonomous,
        };
        pg.add_nmea_message(&ParseResult::RMC(rmc_data))
            .await
            .unwrap();

        println!(
            "{} Set pressure. Above takeoff treshold. Starting flight2",
            TimeKeeper::get_time_us() as Float / 1_000_000.
        );
        pg.set_pressure_temperature(
            calculate_pressure(
                pg.data.state.pos.z + EXPECTED_TAKEOFF_HEIGHT_THRESHOLD as Float + 5.0,
            ),
            0.,
        )
        .await;
        wait(
            &mut pg,
            EXPECTED_TAKEOFF_TIMEOUT + 2,
            STEP_PERIOD,
            None,
            vec![State::Idle, State::Takeoff, State::Flight],
            &TIME_US,
        )
        .await
        .unwrap();
        assert_eq!(pg.state, State::Flight);

        assert_eq!(pg.state, State::Flight);
        let mut pressure_vec = Vec::with_capacity(
            ((EXPECTED_LANDING_TIMEOUT * 5) as Float / STEP_PERIOD as Float) as usize,
        );
        for i in 0..pressure_vec.capacity() {
            let height =
                pg.data.state.pos.z + 200. * f64::sin((i as f64 * STEP_PERIOD) * 2. * PI * 0.01);
            //println!("{}, {}", i as f64 * STEP_PERIOD, height);
            pressure_vec.push(pressure::calculate_pressure(height));
        }
        wait(
            &mut pg,
            EXPECTED_LANDING_TIMEOUT * 5,
            STEP_PERIOD,
            Some(pressure_vec),
            vec![State::Flight],
            &TIME_US,
        )
        .await
        .unwrap();

        // Landing again (No change in the height)
        wait(
            &mut pg,
            EXPECTED_LANDING_TIMEOUT * 2,
            STEP_PERIOD,
            None,
            vec![State::Flight, State::Landing, State::Idle],
            &TIME_US,
        )
        .await
        .unwrap();
        assert_eq!(pg.state, State::Idle);
    }

    #[tokio::test]
    async fn test_manual_takeoff_landing() {
        static TIME_US: Mutex<u64> = Mutex::new(0);

        pub struct TimeKeeper {}

        impl logging::Timekeeper for TimeKeeper {
            fn get_time_us() -> u64 {
                return *TIME_US.lock().unwrap();
            }
        }

        let mut pg: Paraguide<'_, _, File, TimeKeeper, 0> =
            Paraguide::new(FileHandler::new(), &DUMMY_DEVICE_INFO, "Secret");

        pg.init(calculate_pressure(1000.)).await.unwrap();

        const STEP_PERIOD: f64 = 13. / 1000.;
        const EXPECTED_TAKEOFF_TIMEOUT: u64 = 60;
        const EXPECTED_TAKEOFF_HEIGHT_THRESHOLD: usize = 6;
        const EXPECTED_LANDING_TIMEOUT: u64 = 120;
        const EXPECTED_LANDING_HEIGHT_THRESHOLD: usize = 1;

        pg.config.set_auto_takeoff(false);
        pg.config
            .set_auto_takeoff_height_threshold(EXPECTED_TAKEOFF_HEIGHT_THRESHOLD);
        pg.config
            .set_auto_takeoff_timeout(EXPECTED_TAKEOFF_TIMEOUT as usize);
        pg.config.set_auto_landing(false);
        pg.config
            .set_auto_landing_height_threshold(EXPECTED_LANDING_HEIGHT_THRESHOLD);
        pg.config
            .set_auto_landing_timeout(EXPECTED_LANDING_TIMEOUT as usize);
        assert_eq!(pg.config.auto_takeoff(), false);
        assert_eq!(
            pg.config.auto_takeoff_height_threshold(),
            EXPECTED_TAKEOFF_HEIGHT_THRESHOLD
        );
        assert_eq!(
            pg.config.auto_takeoff_timeout(),
            EXPECTED_TAKEOFF_TIMEOUT as usize
        );
        assert_eq!(pg.config.auto_landing(), false);
        assert_eq!(
            pg.config.auto_landing_height_threshold(),
            EXPECTED_LANDING_HEIGHT_THRESHOLD
        );
        assert_eq!(
            pg.config.auto_landing_timeout(),
            EXPECTED_LANDING_TIMEOUT as usize
        );

        assert_eq!(pg.takeoff_condition_fullfilled(), false);
        assert_eq!(pg.state, State::Idle);

        println!(
            "{} Set pressure. Above takeoff treshold. Starting flight",
            TimeKeeper::get_time_us() as Float / 1_000_000.
        );
        pg.set_pressure_temperature(
            calculate_pressure(1000. + EXPECTED_TAKEOFF_HEIGHT_THRESHOLD as Float + 5.0),
            0.,
        )
        .await;

        wait(
            &mut pg,
            EXPECTED_TAKEOFF_TIMEOUT * 5,
            STEP_PERIOD,
            None,
            vec![State::Idle],
            &TIME_US,
        )
        .await
        .unwrap();
        assert_eq!(pg.state, State::Idle);

        // Now setting all parameters to go also in flight mode
        let date = NaiveDate::from_ymd_opt(2015, 6, 30);
        let time = date
            .unwrap()
            .and_hms_nano_opt(23, 59, 59, 1_000_000_000)
            .unwrap()
            .and_local_timezone(Utc)
            .unwrap()
            .time();
        let gga_data = GgaData {
            altitude: Some(2000.),
            geoid_separation: None,
            hdop: None,
            fix_satellites: Some(5),
            fix_time: Some(time),
            fix_type: Some(nmea::sentences::FixType::Gps),
            latitude: Some(45.3),
            longitude: Some(10.),
        };
        pg.add_nmea_message(&ParseResult::GGA(gga_data))
            .await
            .unwrap();

        let rmc_data = RmcData {
            fix_date: date,
            fix_time: Some(time),
            faa_mode: None,
            lat: None,
            lon: None,
            magnetic_variation: None,
            speed_over_ground: None,
            true_course: None,
            nav_status: None,
            status_of_fix: RmcStatusOfFix::Autonomous,
        };
        pg.add_nmea_message(&ParseResult::RMC(rmc_data))
            .await
            .unwrap();

        pg.start_flight().await.unwrap();
        assert_eq!(pg.state, State::Flight);
        wait(
            &mut pg,
            EXPECTED_LANDING_TIMEOUT * 5,
            STEP_PERIOD,
            None,
            vec![State::Flight],
            &TIME_US,
        )
        .await
        .unwrap();
        // We did not switch to idle, because auto landing is off
        assert_eq!(pg.state, State::Flight);

        // Landing again (No change in the height)
        pg.end_flight().await.unwrap();
        assert_eq!(pg.state, State::Idle);
    }
}
