pub use super::config::TimeRepresentation;

#[cfg(feature = "use_f32")]
pub type Float = f32;

#[cfg(not(feature = "use_f32"))]
pub type Float = f64;

#[derive(Default)]
pub struct Vec3d<T> {
    pub x: T,
    pub y: T,
    pub z: T,
}

#[macro_export]
macro_rules! error {
    ($($arg:tt)*) => {
        #[cfg(feature = "defmt")]
        {
            use defmt::error as defmt_err;
            defmt_err!($($arg)*);
        }

        #[cfg(feature = "std")]
        {
            use std::time::SystemTime;
            let duration = SystemTime::now().duration_since(SystemTime::UNIX_EPOCH).unwrap().as_micros() as f64 / 1e6;
            print!("{:?} ", duration);
            println!($($arg)*);
        }

        // Suppress unused argument warnings
        #[cfg(not(any(feature = "std", feature = "defmt")))]
        let _ = ($($arg)*);
    };
}

#[macro_export]
macro_rules! warn {
    ($($arg:tt)*) => {
        #[cfg(feature = "defmt")]
        {
            use defmt::warn as defmt_warn;
            defmt_warn!($($arg)*);
        }

        #[cfg(feature = "std")]
        {
            use std::time::SystemTime;
            let duration = SystemTime::now().duration_since(SystemTime::UNIX_EPOCH).unwrap().as_micros() as f64 / 1e6;
            print!("{:?} ", duration);
            println!($($arg)*);
        }

        // Suppress unused argument warnings
        #[cfg(not(any(feature = "std", feature = "defmt")))]
        let _ = ($($arg)*);
    };
}

#[macro_export]
macro_rules! info {
    ($($arg:tt)*) => {
        #[cfg(feature = "defmt")]
        {
            use defmt::info as defmt_info;
            defmt_info!($($arg)*);
        }

        #[cfg(feature = "std")]
        {
            use std::time::SystemTime;
            let duration = SystemTime::now().duration_since(SystemTime::UNIX_EPOCH).unwrap().as_micros() as f64 / 1e6;
            print!("{:?} ", duration);
            println!($($arg)*);
        }

        // Suppress unused argument warnings
        #[cfg(not(any(feature = "std", feature = "defmt")))]
        let _ = ($($arg)*);
    };
}

#[macro_export]
macro_rules! debug {
    ($($arg:tt)*) => {
        #[cfg(feature = "defmt")]
        {
            use defmt::debug as defmt_debug;
            defmt_debug!($($arg)*);
        }

        #[cfg(feature = "std")]
        {
            use std::time::SystemTime;
            let duration = SystemTime::now().duration_since(SystemTime::UNIX_EPOCH).unwrap().as_micros() as f64 / 1e6;
            print!("{:?} ", duration);
            println!($($arg)*);
        }

        // Suppress unused argument warnings
        #[cfg(not(any(feature = "std", feature = "defmt")))]
        let _ = ($($arg)*);
    };
}

#[macro_export]
macro_rules! trace {
    ($($arg:tt)*) => {
        #[cfg(feature = "defmt")]
        {
            use defmt::trace as defmt_trace;
            defmt_trace!($($arg)*);
        }

        #[cfg(feature = "std")]
        {
            use std::time::SystemTime;
            let duration = SystemTime::now().duration_since(SystemTime::UNIX_EPOCH).unwrap().as_micros() as f64 / 1e6;
            print!("{:?} ", duration);
            println!($($arg)*);
        }

        // Suppress unused argument warnings
        #[cfg(not(any(feature = "std", feature = "defmt")))]
        let _ = ($($arg)*);
    };
}
