//! Handling IGC data
//! Limitations: it will not be checked if the character is a valid character according to Annex A6 of the igc specification

// https://xp-soaring.github.io/igc_file_format/igc_fr_specification_with_al8_2023-2-1_0.pdf
// Lat/long and GNSS altitude figures must be with respect to the WGS 84 ellipsoid
//

use core::future::Future;

pub mod a_record;
pub mod b_record;
pub mod f_record;
pub mod g_record;
pub mod h_record;
pub mod l_record;
mod util;

pub use util::NEW_LINE;

pub use h_record::GNSSSystems;
pub use h_record::GLIDER_ID_MAX_CHARACTERS;
pub use h_record::GLIDER_TYPE_MAX_CHARACTERS;
pub use h_record::MAX_FLIGHT_NUMBER;
pub use h_record::PILOT_NAME_MAX_CHARACTERS;
pub use util::{
    day_to_file_name_format, month_to_file_name_format, year_to_file_name_format, LINE_STRING_SIZE,
};

pub enum Record {
    B(b_record::Record),
    None,
}

/// Parsing igc records
/// Currently only B records can be parsed
pub fn parse_records(record: &str) -> Record {
    if record.starts_with('B') {
        return b_record::parse_record(record);
    }
    Record::None
}

pub trait Writer {
    fn push_str(&mut self, content: &str) -> impl Future<Output = Result<(), ()>> + Send;
    fn push(&mut self, c: char) -> impl Future<Output = Result<(), ()>> + Send;
}
