#![cfg_attr(not(any(test, feature = "std")), no_std)]
// #![deny(warnings)]

//pub mod common;
pub mod common;
mod config;
pub mod file_handling;
pub mod helper;
pub mod igc;
pub mod logging;
pub mod paraguide;
mod state_estimation;
//mod igc;

// dev profile: easier to debug panics; can put a breakpoint on `rust_begin_unwind`
//#[cfg(debug_assertions)]
//use panic_halt as _;

// release profile: minimize the binary size of the application
//#[cfg(not(debug_assertions))]
//use panic_abort as _;
