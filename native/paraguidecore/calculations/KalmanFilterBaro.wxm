/* [wxMaxima batch file version 1] [ DO NOT EDIT BY HAND! ]*/
/* [ Created with wxMaxima version 24.02.1 ] */
/* [wxMaxima: title   start ]
Kalman Filter to estimate Height from Pressure
   [wxMaxima: title   end   ] */


/* [wxMaxima: comment start ]
Heap size by sbcl. Change to a higher value, because big equations might run into memory limit
   [wxMaxima: comment end   ] */


/* [wxMaxima: input   start ] */
:lisp (sb-ext:dynamic-space-size)
/* [wxMaxima: input   end   ] */


/* [wxMaxima: section start ]
Differential Equation Definition
   [wxMaxima: section end   ] */


/* [wxMaxima: comment start ]
[1] https://gitlab.com/Murmele/State-Estimation-of-an-Airborne-Wind-Energy-System/-/raw/master/Text/Masterarbeit.pdf?ref_type=heads&inline=false
[2] https://github.com/XCSoar/XCSoar/blob/293b4bb87f0be82cae3cb1ac5dfdcd6699ae3ae2/src/Math/KalmanFilter1d.cpp
[3] Time Dependence in Kalman Filter Tuning [Zhaozhong Chen, Christoffer Heckman, Simon Julier, Nisar Ahmed]
[4] https://dsp.stackexchange.com/questions/48911/how-to-use-kalman-filter-for-altitude-prediction-based-on-barometer-data
[5] Time Dependence in Kalman Filter Tuning ()https://arxiv.org/pdf/2108.10712)
[6] Wikipedia: Kalman filter: https://en.wikipedia.org/w/index.php?title=Kalman_filter&oldid=484054295
[7] https://www.robots.ox.ac.uk/~ian/Teaching/Estimation/LectureNotes2.pdf
[8] https://users.wpi.edu/~zli11/teaching/rbe595_2017/LectureSlide_PDF/discretization.pdf
[9] Computing Integrals Involving the Matrix Exponential, Charles F. Van Loan
[10] Robert Grover; Brown Patrick; Y.C.Hwang. Introduction to Random Signals and Applied Kalman Filtering. New York, NY: Wiley, 2012.
   [wxMaxima: comment end   ] */


/* [wxMaxima: comment start ]
This sheet describes the kalman filter used to estimate the height and the vertical velocity change using a pressure sensor.

As states the height and the vertical velocity are used:

x1 = h, x2 = dh/dt = vertical velocity

With this we get the linear system

dx/dt = G * x(t) + V * v(t)    and 
y = H * x(t) + w(t)

where as y is the measurement of the height (calculated from the pressure and the U.S. standard atmosphere model), v is the process noise and w is the measurement noise. With the assumption of a slowly changing vertical velocity it can be assumed that

dx^2/dt^2 is approximetly 0. IMPORTANT: Make sure that the sample rate of the pressure sensor is high enough (>10Hz)!

with this G can be determined as
   [wxMaxima: comment end   ] */


/* [wxMaxima: input   start ] */
G :  matrix([0, 1], [0, 0]);
/* [wxMaxima: input   end   ] */


/* [wxMaxima: comment start ]
Because dx1/dt = x2 and dx2/dt = 0. H is simply, because the measured value y is the height
   [wxMaxima: comment end   ] */


/* [wxMaxima: input   start ] */
H: matrix([1, 0]);
/* [wxMaxima: input   end   ] */


/* [wxMaxima: comment start ]
Assuming that process noise for the velocity is zero (dx1/dt),  for the acceleration (dx2/dt) it is v_a [2], the process noise vector is
   [wxMaxima: comment end   ] */


/* [wxMaxima: input   start ] */
V: matrix([0], [v_a]);
/* [wxMaxima: input   end   ] */


/* [wxMaxima: comment start ]
and the measurement noise vector (w_h) is a scalar because we have only one measurement from the barometer
   [wxMaxima: comment end   ] */


/* [wxMaxima: comment start ]
The power spectral density of v is W. W is unit matrix because v(t) is disturbed by white noise [1]
   [wxMaxima: comment end   ] */


/* [wxMaxima: input   start ] */
W: 1;
/* [wxMaxima: input   end   ] */


/* [wxMaxima: input   start ] */
/* TODO define values!!!!*/
/*w: 0.0001; */;
/* [wxMaxima: input   end   ] */


/* [wxMaxima: section start ]
Discretizing continuous state space model
   [wxMaxima: section end   ] */


/* [wxMaxima: comment start ]
Different methods for discretization are tested. Depending of the method, the Covariance CV_n looks different. This is because the values between two sample points are handled differently.
   [wxMaxima: comment end   ] */


/* [wxMaxima: subsect start ]
Discretizing differential equation [5]
   [wxMaxima: subsect end   ] */


/* [wxMaxima: comment start ]
This calculation results in the same results than the van Loan method below, but here an integration must be solved and with the van Loan method only a matrix exponential!
Formula also in [9] (equation 1.2), [10] (equation 3.9.10)
   [wxMaxima: comment end   ] */


/* [wxMaxima: input   start ] */
/*Gn: matrixexp(G * dt);
CV_n: integrate(Gn . V . W . transpose(V) . matrixexp(transpose(G)*dt), dt)$
CV_n: CV_n - ev(CV_n, dt = 0);*/
/* [wxMaxima: input   end   ] */


/* [wxMaxima: subsect start ]
Discretizing differential equation
   [wxMaxima: subsect end   ] */


/* [wxMaxima: comment start ]
This method is used in the Kalman filter implementation to calculate the covariance matrix CV_n
Using [8], [10]
   [wxMaxima: comment end   ] */


/* [wxMaxima: input   start ] */
Gn: matrixexp(G * dt);
Vk:  integrate(Gn.V, dt);
/* [wxMaxima: input   end   ] */


/* [wxMaxima: comment start ]
[6], [7], [10] are used for the formula of CV_n
   [wxMaxima: comment end   ] */


/* [wxMaxima: input   start ] */
CV_n: Vk.transpose(Vk)$
CV_n: subst(v_a_square, v_a^2, CV_n)$
CV_n: subst(dt4, dt^4, CV_n)$
CV_n: subst(dt3, dt^3, CV_n)$
CV_n: subst(dt2, dt^2, CV_n);
/* [wxMaxima: input   end   ] */


/* [wxMaxima: subsect start ]
Another discretization
   [wxMaxima: subsect end   ] */


/* [wxMaxima: comment start ]
https://dsp.stackexchange.com/questions/68193/discretize-process-noise-in-kalman-filter
   [wxMaxima: comment end   ] */


/* [wxMaxima: input   start ] */
/*CV_n: Gn . V . transpose(V) . transpose(Gn) * dt;*/
/* [wxMaxima: input   end   ] */


/* [wxMaxima: subsect start ]
Convert to Time discrete differential equation using van Loan method [1] (Alternative method)
   [wxMaxima: subsect end   ] */


/* [wxMaxima: comment start ]
This system must be converted from a time continuous system to a time discrete system [1]. This leads to a discrete matrix G_tilde described in the next section.

https://math.stackexchange.com/questions/881182/how-to-derive-the-process-noise-co-variance-matrix-q-in-this-kalman-filter-examp
   [wxMaxima: comment end   ] */


/* [wxMaxima: subsubsect start ]
Create G_tilde
   [wxMaxima: subsubsect end   ] */


/* [wxMaxima: comment start ]
VWVT and G inserted in formula 2.8.2 of  [1] leads to G_tilde. Important: VWVT must be symmetric (VWVT = transpose(VWVT) and semidefinit)!
   [wxMaxima: comment end   ] */


/* [wxMaxima: input   start ] */
VWVT: V. W.transpose(V);
/* [wxMaxima: input   end   ] */


/* [wxMaxima: comment start ]
Check if semidifinit
   [wxMaxima: comment end   ] */


/* [wxMaxima: input   start ] */
x: matrix([x1], [x2]);
r: transpose(x).VWVT.x;
is(notequal(ev(r, v_a=1, x1 = 1, x2 = 1), 0)); /* if expression is not zero, it means the matrix is positive semidefinit */
/* [wxMaxima: input   end   ] */


/* [wxMaxima: input   start ] */
G_tilde: -G$
/* transpose is important, because otherwise the list contains a list of rows instead of a list of columns */
for i: 1 thru length(VWVT[1]) do (G_tilde: addcol(G_tilde, args(transpose(VWVT)[i])))$

Gt: transpose(G)$

rowCount: length(Gt)$
columnCount: length(Gt[1])$

/* create second (lower) part of G_tilde */
t: zeromatrix(rowCount, columnCount)$
for i: 1 thru columnCount do (t: addcol(t, args(transpose(Gt))[i]))$

/* add rows of t to G_tilde */
for i: 1 thru length(t) do (G_tilde: addrow(G_tilde, args(t)[i]))$
G_tilde: G_tilde * dt;
/*
Other possibility. Everything must be in one line and every expression is separated by a comma!
Or a block() can be used, which is more flexible
G_tilde: args(-G);
for i: 1 thru length(G) do (g_: G_tilde[i], vwvt_: args(VWVT)[i], G_tilde[i]: append(g_, vwvt_))$
G_tilde: apply('matrix, G_tilde);
*/;
/* [wxMaxima: input   end   ] */


/* [wxMaxima: subsubsect start ]
Calculate Matrix exponentional
   [wxMaxima: subsubsect end   ] */


/* [wxMaxima: input   start ] */
domxexpt: false$
G_exp: matrixexp(G_tilde);
/* [wxMaxima: input   end   ] */


/* [wxMaxima: input   start ] */
w_var;
/* [wxMaxima: input   end   ] */


/* [wxMaxima: input   start ] */
Hn: H;
/* [wxMaxima: input   end   ] */


/* [wxMaxima: subsubsect start ]
Calculate Gn and CV_n
   [wxMaxima: subsubsect end   ] */


/* [wxMaxima: comment start ]
Extract the upper left and lower left part of the matrix G_exp [1]
   [wxMaxima: comment end   ] */


/* [wxMaxima: heading5 start ]
TODO: enable this again, once it is clear, why the van Loan method results in different CV_n!
   [wxMaxima: heading5 end   ] */


/* [wxMaxima: input   start ] */
/*Gn_T: submatrix(1, length(G_exp) - length(G), G_exp, 1, length(G_exp) -length(G) ); /* lower right part of the matrix exponential matrixexp */
Gn: transpose(Gn_T);
Gn_invCv: submatrix(length(G_exp) - length(G) + 1, length(G_exp) , G_exp, 1, length(G_exp) -length(G) ); /* upper right part of the matrix exponential matrixexp */*/;
/* [wxMaxima: input   end   ] */


/* [wxMaxima: comment start ]
Multiply Gn_invCv from the left side with Gn to receive Cv
   [wxMaxima: comment end   ] */


/* [wxMaxima: input   start ] */
/*CV_n: Gn.Gn_invCv$
CV_n: subst(v_a_square, v_a^2, CV_n)$
CV_n: subst(dt4, dt^4, CV_n)$
CV_n: subst(dt3, dt^3, CV_n)$
CV_n: subst(dt2, dt^2, CV_n);*/;
/* [wxMaxima: input   end   ] */


/* [wxMaxima: section start ]
Kalman Filter
   [wxMaxima: section end   ] */


/* [wxMaxima: subsect start ]
Predict Step
   [wxMaxima: subsect end   ] */


/* [wxMaxima: comment start ]
State
   [wxMaxima: comment end   ] */


/* [wxMaxima: input   start ] */
xnm1_nm1: matrix([x1], [x2]);
/* [wxMaxima: input   end   ] */


/* [wxMaxima: comment start ]
Integration
   [wxMaxima: comment end   ] */


/* [wxMaxima: input   start ] */
xn_nm1: Gn . xnm1_nm1;
/* [wxMaxima: input   end   ] */


/* [wxMaxima: input   start ] */
CXnm1_nm1: matrix([c11, c12], [c21, c22]);
CXn_nm1: Gn.CXnm1_nm1.transpose(Gn) + CV_n;

CXn_nm1_11_sol: CXn_nm1[1][1];
CXn_nm1_12_sol: CXn_nm1[1][2];
CXn_nm1_21_sol: CXn_nm1[2][1];
CXn_nm1_22_sol: CXn_nm1[2][2];

CXn_nm1: matrix([Cxn_nm1_11, Cxn_nm1_12], [Cxn_nm1_21, Cxn_nm1_22])$ /* redefine so the values are not inserted below */

xn_nm1: matrix([x1_n_nm1], [x2_n_nm1])$  /* redefine so the values are not inserted below */

CW_n: w / dt;
Cyn_nm1: Hn.CXn_nm1.transpose(Hn) + 'CW_n;
/* [wxMaxima: input   end   ] */


/* [wxMaxima: subsect start ]
Update step
   [wxMaxima: subsect end   ] */


/* [wxMaxima: input   start ] */
Kn: CXn_nm1.transpose(Hn)*1/'Cyn_nm1;

Kn: matrix([Kn1], [Kn2])$/* redefine so the values are not inserted below */
CXn_n: CXn_nm1 - Kn.Hn.CXn_nm1;
xn_n: xn_nm1 + Kn*(yn - Hn.xn_nm1);
/* [wxMaxima: input   end   ] */


/* [wxMaxima: section start ]
Reverse calculation of xcsoar kalman
   [wxMaxima: section end   ] */


/* [wxMaxima: comment start ]
Just for checking. At the end we should get the same result!
   [wxMaxima: comment end   ] */


/* [wxMaxima: input   start ] */
/* read from the code of xc soar [2] */
CXn_nm1_xcsoar: matrix(
    [C11 + dt*C21 + dt*C12 + dt^2 * C22 + v_a * dt^4 / 4, C12 + dt * C22 + v_a * dt^3 / 2], 
    [C21 + dt * C22 + v_a * dt^3 / 2, C22 + v_a * dt^2]
);
/* extracted from CXn_nm1_xcsoar */
CV_n_xcsoar: matrix([dt^4/4*v_a, dt^3*v_a/2], [dt^3/2*v_a, dt^2*v_a]);
Gn_invCv_xcsoar: invert(Gn).CV_n_xcsoar;

/* Create G_exp_xcsoar */
G_exp_xcsoar: invert(Gn)$
for i: 1 thru length(Gn_invCv_xcsoar[1]) do (G_exp_xcsoar: addcol(G_exp_xcsoar, args(transpose(Gn_invCv_xcsoar))[i]))$
rowCount: length(Gn)$
columnCount: length(Gn[1])$
/* create second (lower) part of G_tilde */
t: zeromatrix(rowCount, columnCount)$
for i: 1 thru columnCount do (t: addcol(t, args(Gn)[i]))$
for i: 1 thru length(t) do (G_exp_xcsoar: addrow(G_exp_xcsoar, args(t)[i]))$
G_exp_xcsoar: G_exp_xcsoar;
/* [wxMaxima: input   end   ] */


/* [wxMaxima: input   start ] */
/* Create generic expm(M) to compare with G_exp_xcsoar */
G_tilde_test: matrix([a11, a12, a13, a14], [a21, a22, a23, a24], [a31, a32, a33, a34], [a41, a42, a43, a44])$
G_tilde_test[1][1]: 0$
G_tilde_test[1][2]: -dt$
G_tilde_test[2][1]: 0$
G_tilde_test[2][2]: 0$
G_tilde_test[3][1]: 0$
G_tilde_test[3][2]: 0$
G_tilde_test[4][1]: 0$
G_tilde_test[4][2]: 0$
G_tilde_test[3][3]: 0$
G_tilde_test[3][4]: 0$
G_tilde_test[4][3]: dt$
G_tilde_test[4][4]: 0$
G_tilde_test: G_tilde_test;
G_test_square: G_tilde_test.G_tilde_test;
G_test_cub: G_test_square.G_tilde_test;
G_test_quat: G_test_square.G_test_square;
G_test_exp_manual: diagmatrix(length(G_tilde_test), 1) + G_tilde_test  + G_test_square / 2! + G_test_cub / 3! + G_test_quat / 4!;
/* [wxMaxima: input   end   ] */


/* [wxMaxima: input   start ] */
solveexplicit:true$
rowcount: length(G_test_exp_manual[1])$
columncount: length(G_test_exp_manual)$

liste: []$
for r: 1 thru rowcount do block (
    for c: 1 thru columncount do (liste: append(liste, [ G_test_exp_manual[r][c] = G_exp_xcsoar[r][c]] ))
)$
liste: liste;
sol: solve(liste, ['a13, 'a14, 'a23, 'a24]);

/* axx includes also the *dt, so it must be divided by it */
a13_sol: rhs(sol[1][1])$
a14_sol: rhs(sol[1][2])$
a23_sol: rhs(sol[1][3])$
a24_sol: rhs(sol[1][4])$

/* calculate back to check if correct! */
G_tilde_xcsoar_calc_back: copymatrix(G_tilde_test)$
G_tilde_xcsoar_calc_back: ev(G_tilde_xcsoar_calc_back, 'a13 = a13_sol, 'a14 = a14_sol, 'a23 = a23_sol, 'a24 = a24_sol);

VWVT_xcsoar: submatrix( length(G) + 1, length(G_tilde_xcsoar_calc_back), G_tilde_xcsoar_calc_back,   1, length(G_tilde_xcsoar_calc_back) - length(G[1]) )$
VWVT_xcsoar: VWVT_xcsoar / dt;

G_exp_xcsoar_calc_back: matrixexp(G_tilde_xcsoar_calc_back)$

Gn_T_xcsoar: submatrix(1, length(G_exp_xcsoar_calc_back) - length(G), G_exp_xcsoar_calc_back, 1, length(G_exp_xcsoar_calc_back) - length(G[1]) )$ /* lower right part of the matrix exponential matrixexp */
Gn_xcsoar: transpose(Gn_T_xcsoar);
Gn_invCv_xcsoar: submatrix(length(G_exp_xcsoar_calc_back) - length(G) + 1, length(G_exp_xcsoar_calc_back) , G_exp_xcsoar_calc_back, 1, length(G_exp_xcsoar_calc_back) - length(G[1]) )$ /* upper right part of the matrix exponential matrixexp */

CV_n_xcsoar: Gn.Gn_invCv_xcsoar$
CV_n_xcsoar: subst(v_a_square, v_a^2, CV_n_xcsoar)$
CV_n_xcsoar: subst(dt4, dt^4, CV_n_xcsoar)$
CV_n_xcsoar: subst(dt3, dt^3, CV_n_xcsoar)$
CV_n_xcsoar: subst(dt2, dt^2, CV_n_xcsoar);


CXnm1_nm1_xcsoar: matrix([C11, C12], [C21, C22]);
CXn_nm1_xcsoar_calc_back: Gn.CXnm1_nm1_xcsoar.transpose(Gn) + CV_n_xcsoar;
CXn_nm1_xcsoar: CXn_nm1_xcsoar;
/* if CXn_nm1_xcsoar_calc_back and CXn_nm1_xcsoar are equal the calculation is correct */
/* [wxMaxima: input   end   ] */


/* [wxMaxima: input   start ] */
V_generic: matrix([v1], [v2]);
VWVT_generic: V_generic . transpose(V_generic);

rowcount: length(VWVT_generic[1])$
columncount: length(VWVT_generic)$
liste: []$
for r: 1 thru rowcount do block (
    for c: 1 thru columncount do (liste: append(liste, [ VWVT_generic[r][c] = VWVT_xcsoar[r][c]] ))
)$
liste: liste;

solve(liste, ['v1, 'v2]);
/* [wxMaxima: input   end   ] */



/* Old versions of Maxima abort on loading files that end in a comment. */
"Created with wxMaxima 24.02.1"$
