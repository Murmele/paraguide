import os
import platform
import psutil
import re
import subprocess
import sys
import time

application_name = 'paraguide'

try:
    import tkinter as tk
    from tkinter import messagebox
except ImportError:
    print("tkinter not found, error dialogs will not be shown")


def launch_uri(filename):
    if platform.system() == 'Linux':
        subprocess.run(['xdg-open', filename])
    elif platform.system() == 'Darwin':
        subprocess.run(['open', filename])
    elif platform.system() == 'Windows':
        os.startfile(filename)
    else:
        print(f'Platform {platform.system()} not supported')


def find_dart_process():
    start = time.time()
    while time.time() - start < 3:
        for proc in psutil.process_iter(['pid', 'cmdline']):
            try:
                cmdline = ' '.join(proc.info['cmdline'])
                if 'dart' in cmdline and (
                    re.search(r'\.so\b', cmdline)        # Linux
                    or re.search(r'\.dll\b', cmdline)    # Windows
                    or re.search(r'\.dylib\b', cmdline)  # macOS          
                ):
                    return proc.info['pid']
            except (psutil.NoSuchProcess, psutil.AccessDenied):
                pass
    raise Exception("Failed to find Dart process for native debugging")


def present_error(message):
    root = tk.Tk()
    root.withdraw()  # hides the main window
    messagebox.showerror("Error", message)
    root.destroy()


def attach_codelldb(pid: int):
    uri = f"vscode://vadimcn.vscode-lldb/launch/config?{{'name':'Dart native','request':'attach','pid':{pid}}}" # ,'program':'{application_name}'
    print("Opening " + uri)
    launch_uri(uri)


if len(sys.argv) == 1:
    if platform.system() == "Windows":
        subprocess.Popen([__file__, "fork"], creationflags=subprocess.CREATE_NEW_PROCESS_GROUP)
    subprocess.Popen(["nohup", __file__, "fork"], stdout=subprocess.DEVNULL, stderr=subprocess.DEVNULL)
    sys.exit(0)

try:
    pid = find_dart_process()
    attach_codelldb(pid)
except Exception as e:
    present_error(str(e))
    sys.exit(1)
