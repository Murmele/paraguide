# Arch Linux
## Prerequisites
- rust: `sudo pacman -S rust`
- llvm: `sudo pacman -S llvm`
- dart: `sudo pacman -S dart`
   - disabling telemetry: `dart --disable-analytics`
- Flutter: `yay -S flutter` checkout also: https://www.rockyourcode.com/how-to-get-flutter-and-android-working-on-arch-linux/
    - `mv "/opt/flutter/.git-refs" "/opt/flutter/.git"` otherwise an error will be raised
    - `flutter --disable-telemetry`

- Android NDK: `yay -S android-ndk`
- Android studio: `yay -S android-studio`
- rust targets Android: `rustup target add aarch64-linux-android armv7-linux-androideabi x86_64-linux-android i686-linux-android`

## Flutter rust bridegen
Used to create the rust bindings for flutter. Follow the installation steps at: https://cjycode.com/flutter_rust_bridge/integrate/new_crate.html


# Setup Project (Not needed, because project already exists
## Create Flutter project
```
PROJECT_NAME="paraguide"
flutter create --ios-language swift --android-language kotlin $PROJECT_NAME
```
## Create rust lib
Creates a rust folder with an initial Cargo.toml file
```
cd $PROJECT_NAME
cargo new --lib --name native native
```

## Flutter rust bridge
https://cjycode.com/flutter_rust_bridge/quickstart.html
If the `flutter_rust_bridge_codegen` executable is not found, it is because `~/.cargo/bin` where the binary is located is not in path

### Linux & Windows with cmake use corrosion
Use corrosion-rs: https://github.com/corrosion-rs/corrosion

## Importing the library on Android
According to https://medium.com/flutter-community/finally-running-rust-natively-on-a-flutter-plugin-here-is-how-6f2826eb1735
the lib must be in `android/src/main/jniLibs` but for some reason when creating a new project, the libraries must be in
`android/app/src/main/jniLibs`. Hint: compile the project and look into the apk if the library is bundled or not.

## Flutter Debug VSCodium
(https://docs.flutter.dev/development/tools/vs-code)
- install Dart and Flutter extensions in VSCodium

It is possible to use your phone or an emulator for debugging

### Smartphone
- Turn on developer tools and debugging on the phone

Check in the bottom right location if the device is found. Otherwise click on "No device"

### Emulator
Follow: https://www.rockyourcode.com/how-to-get-flutter-and-android-working-on-arch-linux/

## Flutter Debug Android studio
More comfortable than VSCodium



