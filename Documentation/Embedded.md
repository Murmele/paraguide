# STM32 Rust

https://medium.com/digitalfrontiers/rust-on-a-stm32-microcontroller-90fac16f6342

# Rust embedded HAL architecture explanation

https://blog.japaric.io/brave-new-io/



## Frameworks

# NRF Hal
https://github.com/nrf-rs/nrf-hal/tree/master
Pure HAL for nrf chips. Can be combined with RTIC
NRF Hal uses still Embedded Hal (v0.2.7) (2024-01-26)

# RTIC The hardware accelerated Rust RTOS
Limited to ARM cores
https://rtic.rs/2/book/en/

# Embassy
Hal library for stm32, nrf and esp + async functionality
Embassy vs RTIC: https://rtic.rs/dev/book/en/rtic_and_embassy.html
https://github.com/embassy-rs/embassy

# SD card / Filesystem

| Name           | Comments                                        | Non Blocking?        | Link                                       |
|----------------|-------------------------------------------------|----------------------|--------------------------------------------|
| embedded_sdmmc | Pure Rust,                                      | I ported it to async | https://docs.rs/embedded-sdmmc/latest/embedded_sdmmc/ |
| fatfs          |                                                 |      ?               |               |https://docs.rs/fatfs/latest/fatfs/ | 
| fatfs-embedded | uses embassy, alloc required (bad?), uses C code, `fatfs/source/ff.c:22:10: fatal error: string.h: No such file or directory` |      ?               |https://lib.rs/crates/fatfs-embedded |
