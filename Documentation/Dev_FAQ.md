# Starting under linux
## Failed to start Flutter renderer: GL-Kontext kann nicht erstellt werden
- Arch linux: `yay -S android-sdk-cmdline-tools-latest` and add `/opt/android-sdk/cmdline-tools/latest/bin` to path

# Flutter Rust Bridge
## fatal error: 'stdbool.h'
  - Set CPATH correctly

# General
## Failed to load dynamic library 'libnative.so'
- Fixed in 7ed4ad3806f5454e5c3fb1551d8a7ad7a1305f45: the library name is api not native

# Embedded

## Debugging with Probe rs
- Build program for example `cd steigen3000 && cargo build`
- Debug `probe-rs run --speed 1000 --chip <chip> --log-format "{t} {s}" <path_elf_file>`
  Steigen3000 example:
  `probe-rs run --speed 1000 --chip nRF52840_xxAA --log-format "{t} {s}" target/thumbv7em-none-eabihf/debug/steigen3000`
  Use speed to change the debug speed (kHz).


