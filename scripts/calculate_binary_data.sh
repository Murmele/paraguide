#!/bin/bash

# Requirements:
# cargo install cargo-binutils
# rustup component add llvm-tools-preview

set -e

POSITIONAL_ARGS=()
while [[ $# -gt 0 ]]; do
  case $1 in
    -f|--flash_size)
      FLASH_SIZE="$2"
      shift # past argument
      shift # past value
      ;;
    -r|--ram_size)
      RAM_SIZE="$2"
      shift # past argument
      shift # past value
      ;;
    -fl|--flash_limit_percent)
      FLASH_LIMIT="$2"
      shift # past argument
      shift # past value
      ;;
    -rl|--ram_limit_percent)
      RAM_LIMIT="$2"
      shift # past argument
      shift # past value
      ;;
    -h|--help)
      shift # past argument
      echo "calculate_binary_data.sh -f <Flash Size kByte> -fl <Flash Limit Percent> -r <Ram Size kByte> -rl <Ram Limit Percent>"
      exit 0
      ;;
    -*|--*)
      echo "Unknown option $1"
      exit 1
      ;;
    *)
      POSITIONAL_ARGS+=("$1") # save positional arg
      shift # past argument
      ;;
  esac
done

if [ -z $FLASH_SIZE ]; then
    echo "No flash size"
    exit -5
fi
if [ -z $FLASH_LIMIT ]; then
    echo "No flash limit"
    exit -5
fi
if [ -z $RAM_SIZE ]; then
    echo "No ram size"
    exit -5
fi
if [ -z $RAM_LIMIT ]; then
    echo "No ram limit"
    exit -5
fi

PROJECT="steigen3000"
cd $PROJECT
SIZE_TEXT="$(cargo size --bin $PROJECT --quiet --release -- -A | grep -w ".text")"
SIZE_DATA="$(cargo size --bin $PROJECT --quiet --release -- -A | grep -w ".data")"
SIZE_BSS="$(cargo size --bin $PROJECT --quiet --release -- -A | grep -w ".bss")"
SIZE_RODATA="$(cargo size --bin $PROJECT --quiet --release -- -A | grep -w ".rodata")"

data=-1
get_data()
{
    data=-1
    COUNTER=0
    for entry in $1
    do
        if [ $COUNTER -eq 1 ]; then
            data=$entry
        fi
        let COUNTER=COUNTER+1
    done
}

echo $SIZE_TEXT
echo $SIZE_DATA
echo $SIZE_BSS
echo $SIZE_RODATA

get_data "$SIZE_TEXT"
SIZE_TEXT=$data
get_data "$SIZE_DATA"
SIZE_DATA=$data
get_data "$SIZE_BSS"
SIZE_BSS=$data
get_data "$SIZE_RODATA"
SIZE_RODATA=$data

# All ram variables are stored also in the RAM if they are not zero
RAM_S=$(python3 -c "import math;print(f'{math.ceil(($SIZE_DATA+$SIZE_BSS)/1000)}')")
FLASH_S=$(python3 -c "import math;print(f'{math.ceil(($SIZE_TEXT + $SIZE_RODATA + $SIZE_DATA)/1000)}')")

# in kByte
MAX_RAM_USAGE=$(python3 -c "import math;print(f'{math.floor($RAM_SIZE*$RAM_LIMIT/100)}')")
MAX_FLASH_USAGE=$(python3 -c "import math;print(f'{math.floor($FLASH_SIZE*$FLASH_LIMIT/100)}')")

echo "MAX FLASH USAGE: " $MAX_FLASH_USAGE "kB"
echo "MAX RAM USAGE: " $MAX_RAM_USAGE "kB"
echo "REQUIRED_RAM: " $RAM_S "kB"
echo "REQUIRED FLASH: " $FLASH_S "kB"

if [ "$RAM_S" -gt "$MAX_RAM_USAGE" ]; then
    echo "Required ram size greater than ram size of the microcontroller"
    exit -1
fi

if [ "$FLASH_S" -gt "$MAX_FLASH_USAGE" ]; then
    echo "Required flash size greater than flash size of the microcontroller"
    exit -2
fi

echo "SUCCESS: The used RAM and FLASH are below the defined limits!"
exit 0
