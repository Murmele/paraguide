pub const BATTERY_0: [[u8; 7]; 24] = [
    [0x00, 0x00, 0x00, 0x00, 0x00, 0x03, 0xFF],
    [0x7F, 0xFF, 0xFF, 0xFF, 0xFF, 0xFB, 0xFF],
    [0x7F, 0xFF, 0xFF, 0xFF, 0xFF, 0xFB, 0xFF],
    [0x7F, 0xFF, 0xFF, 0xFF, 0xFF, 0xF9, 0xFF],
    [0x7F, 0xFF, 0xFF, 0xFF, 0xFF, 0xF8, 0x3F],
    [0x7F, 0xFF, 0xFF, 0xFF, 0xFF, 0xF8, 0x3F],
    [0x7F, 0xFF, 0xFF, 0xFF, 0xFF, 0xF8, 0x3F],
    [0x7F, 0xFF, 0xFF, 0xFF, 0xFF, 0xF8, 0x3F],
    [0x7F, 0xFF, 0xFF, 0xFF, 0xFF, 0xF8, 0x3F],
    [0x7F, 0xFF, 0xFF, 0xFF, 0xFF, 0xF8, 0x3F],
    [0x7F, 0xFF, 0xFF, 0xFF, 0xFF, 0xF8, 0x3F],
    [0x7F, 0xFF, 0xFF, 0xFF, 0xFF, 0xF8, 0x3F],
    [0x7F, 0xFF, 0xFF, 0xFF, 0xFF, 0xF8, 0x3F],
    [0x7F, 0xFF, 0xFF, 0xFF, 0xFF, 0xF8, 0x3F],
    [0x7F, 0xFF, 0xFF, 0xFF, 0xFF, 0xF8, 0x3F],
    [0x7F, 0xFF, 0xFF, 0xFF, 0xFF, 0xF8, 0x3F],
    [0x7F, 0xFF, 0xFF, 0xFF, 0xFF, 0xF8, 0x3F],
    [0x7F, 0xFF, 0xFF, 0xFF, 0xFF, 0xF8, 0x3F],
    [0x7F, 0xFF, 0xFF, 0xFF, 0xFF, 0xF8, 0x3F],
    [0x7F, 0xFF, 0xFF, 0xFF, 0xFF, 0xF8, 0x3F],
    [0x7F, 0xFF, 0xFF, 0xFF, 0xFF, 0xF8, 0x3F],
    [0x7F, 0xFF, 0xFF, 0xFF, 0xFF, 0xF9, 0xFF],
    [0x7F, 0xFF, 0xFF, 0xFF, 0xFF, 0xFB, 0xFF],
    [0x00, 0x00, 0x00, 0x00, 0x00, 0x03, 0xFF]
];