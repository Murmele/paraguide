from PIL import Image
import numpy as np
import cairosvg
import os

def function(x, threshold):
    if x > threshold:
        return 0
    else:
        return 255

icon_dir = ".."
output_width = 24
output_height = 24

def convert_svg(icon_dir, input_file_name, output_width, output_height, rotation = 180, threshold = 128):
    print(f"Processing: {input_file_name}")
    temp_folder = os.path.join("/", "tmp", "convert_svg")
    input_file_no_extension, _ = os.path.splitext(input_file_name)

    input_file = os.path.abspath(os.path.join(icon_dir, input_file_name))
    png_file = os.path.join(temp_folder, input_file_name + ".png")
    output_result = input_file + ".rs"

    os.makedirs(temp_folder, exist_ok=True)

    cairosvg.svg2png(url=input_file, write_to=png_file, output_width=output_width, output_height=output_height, background_color='white')

    image = Image.open(png_file)
    image = image.convert("L")  # Convert to grayscale
    with open(os.path.join(temp_folder, input_file_name + "_convertL.png"), "wb") as f:
        image.save(f)
    # Convert to black and white with a specific threshold
    bw_image = image.point(lambda x: function(x, threshold), '1')

    original = np.array(image, dtype=np.uint8)
    print("original")
    print(original)

    small_bw_image_data = np.array(bw_image, dtype=np.uint8)
    print("small_bw_image_data")
    print(small_bw_image_data)

    # Pack bits into bytes (8 pixels per byte)
    small_packed_bits = np.packbits(small_bw_image_data, axis=1)
    print("small_packed_bits")
    print(small_packed_bits)

    # Convert from MSB to LSB!
    bit_reverse_lookup = np.array([int(f"{i:08b}"[::-1], 2) for i in range(256)], dtype=np.uint8)
    if rotation == 0:
        # we have to convert the bytes to lsb for 0°
        # for 180° MSB is fine, because of the rotation
        small_packed_bits = bit_reverse_lookup[small_packed_bits]
    small_packed_bits = ~small_packed_bits # invert all bits because 1 means white and 0 means black

    print("small_packed_bits lsb and inverted")
    print(small_packed_bits)


    # Create a 2D Rust array representation
    rows_rust_array = ",\n    ".join("[" + ", ".join(f"0x{byte:02X}" for byte in row) + "]" for row in small_packed_bits)

    small_rust_2d_code = f"pub const {input_file_no_extension.upper()}: [[u8; {small_packed_bits.shape[1]}]; {small_packed_bits.shape[0]}] = [\n    {rows_rust_array}\n];"
    # Save to a text file for convenience
    with open(output_result, 'w') as f:
        f.write(small_rust_2d_code)

convert_svg(icon_dir, "mute.svg", output_width, output_height)
convert_svg(icon_dir, "speaker.svg", output_width, output_height)
convert_svg(icon_dir, "windsock.svg", output_width, output_height)
convert_svg(icon_dir, "globe.svg", output_width, output_height)
convert_svg(icon_dir, "logging.svg", output_width, output_height)

convert_svg(icon_dir, "battery_100.svg", None, output_height)
convert_svg(icon_dir, "battery_75.svg", None, output_height)
convert_svg(icon_dir, "battery_50.svg", None, output_height)
convert_svg(icon_dir, "battery_25.svg", None, output_height)
convert_svg(icon_dir, "battery_0.svg", None, output_height)




