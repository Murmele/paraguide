# Icon sources:

mute.svg: https://commons.wikimedia.org/wiki/File:Mute_Icon.svg
bluetooth.svg: https://commons.wikimedia.org/wiki/File:Bluetooth.svg
speaker.svg: https://commons.wikimedia.org/wiki/File:Speaker_Icon.svg
windsock.svg: https://game-icons.net/1x1/delapouite/windsock.html
    - inverted colors

globe.svg: https://commons.wikimedia.org/wiki/File:Globe_icon.svg
logging.svg: https://commons.wikimedia.org/wiki/Category:Floppy_disk_icons#/media/File:High-contrast-media-floppy.svg

battery_xx.svg: Created by Martin Marmsoler for steigen3000
