pub const SPEAKER: [[u8; 3]; 24] = [
    [0xFF, 0xFF, 0xFF],
    [0xFF, 0xFF, 0xFF],
    [0xFF, 0xFF, 0xFF],
    [0xFF, 0xE7, 0xE7],
    [0xFF, 0xC7, 0xF3],
    [0xFF, 0x87, 0x93],
    [0xFF, 0x07, 0x99],
    [0xFE, 0x07, 0xC9],
    [0x00, 0x06, 0x4C],
    [0x00, 0x06, 0x6C],
    [0x00, 0x07, 0x64],
    [0x00, 0x07, 0x64],
    [0x00, 0x07, 0x64],
    [0x00, 0x07, 0x64],
    [0x00, 0x06, 0x6C],
    [0x00, 0x06, 0x4C],
    [0xFE, 0x06, 0xC9],
    [0xFF, 0x07, 0x99],
    [0xFF, 0x87, 0x93],
    [0xFF, 0xC7, 0xF3],
    [0xFF, 0xE7, 0xE7],
    [0xFF, 0xFF, 0xFF],
    [0xFF, 0xFF, 0xFF],
    [0xFF, 0xFF, 0xFF]
];