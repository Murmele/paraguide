pub const WINDSOCK: [[u8; 3]; 24] = [
    [0x9F, 0xFF, 0xFF],
    [0x1C, 0x3F, 0xFF],
    [0x13, 0xEF, 0xFF],
    [0xFF, 0xC7, 0xFF],
    [0xB7, 0xC7, 0xFF],
    [0xB7, 0x87, 0xFF],
    [0xB7, 0x8F, 0x7F],
    [0xBB, 0x1E, 0x3F],
    [0xBB, 0x1E, 0x1F],
    [0xBE, 0x3C, 0x3F],
    [0xBF, 0xFC, 0x3F],
    [0xBF, 0xFC, 0x79],
    [0xBF, 0xFF, 0xF0],
    [0xBF, 0xFF, 0xF1],
    [0xBF, 0xFF, 0xFF],
    [0xBF, 0xFF, 0xFF],
    [0xBF, 0xFF, 0xFF],
    [0xBF, 0xFF, 0xFF],
    [0xBF, 0xFF, 0xFF],
    [0xBF, 0xFF, 0xFF],
    [0xBF, 0xFF, 0xFF],
    [0xBF, 0xFF, 0xFF],
    [0xBF, 0xFF, 0xFF],
    [0xBF, 0xFF, 0xFF]
];