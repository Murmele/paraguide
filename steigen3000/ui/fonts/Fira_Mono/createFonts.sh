#!/bin/bash

#npx lv_font_conv --font FiraMono-Regular.ttf --no-compress --full-info --bpp 1 --size 60 --format bin -r 0x20-0x7F -o firaMono.bin
npx lv_font_conv --font FiraMono-Regular.ttf --no-compress --full-info --bpp 1 --size 14 --format dump -r 0x20-0x7F -o FIRA_MONO_REGULAR_14Dump # Small
npx lv_font_conv --font FiraMono-Regular.ttf --no-compress --full-info --bpp 1 --size 26 --format dump -r 0x20-0x7F -o FIRA_MONO_REGULAR_26Dump # Title
npx lv_font_conv --font FiraMono-Regular.ttf --no-compress --full-info --bpp 1 --size 53 --format dump -r 0x20-0x7F -o FIRA_MONO_REGULAR_53Dump # Value
