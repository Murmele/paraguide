#!/bin/bash

FONT_FILE=FiraSans-Regular.ttf
RESULT_PREFIX=FIRA_SANS_REGULAR

npx lv_font_conv --font ${FONT_FILE} --no-compress --full-info --bpp 1 --size 14 --format dump -r 0x20-0x7F -o ${RESULT_PREFIX}_14Dump # Small
npx lv_font_conv --font ${FONT_FILE} --no-compress --full-info --bpp 1 --size 26 --format dump -r 0x20-0x7F -o ${RESULT_PREFIX}_26Dump # Title
npx lv_font_conv --font ${FONT_FILE} --no-compress --full-info --bpp 1 --size 53 --format dump -r 0x20-0x7F -o ${RESULT_PREFIX}_53Dump # Value
