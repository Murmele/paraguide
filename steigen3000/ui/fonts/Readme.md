https://github.com/lvgl/lvgl/blob/master/src/font/

# Profont

https://tobiasjung.name/profont/
https://github.com/MicahElliott/Orp-Font/blob/master/lib/profont.bdf

## Using ForgeFont
**Did not work**
The 25pixel version was created with ForgeFont:
1) Open profont.bdf in ForgeFont
2) Element -> Available Bitmap versions and select desired pixel size (Ctrl + Shift + B)
3) File -> Export Font (Ctrl + Shift + G)
4) Use font-converter in the Tools folder to convert the generated bdf to a data file which can be used in rust

## Using bdfresize
1) Find a font in bdf format
2) Resize the font with bdfresize: I just created a cmake project for easier compilation: https://github.com/Murmele/bdfresize
3) use eg_font_converter (See Tools folder) to convert the rescaled font to a embedded graphics supported format
4) For floating point factors pass them as fractions: 2.5 -> 5/2

# profont_ascii.bdf
Derived from profont.bdf, with only ascii characters and the upper three lines removed to reduce memory
