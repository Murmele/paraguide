from common import float_pattern

import re

log1 = re.compile(f"LXS31_,(?P<time_ms>{float_pattern}),(?P<height_meas>{float_pattern}),(?P<height_est>{float_pattern}),(?P<vel>{float_pattern})\n")
log2 = re.compile(f"LXS32_,(?P<time_ms>{float_pattern}),(?P<pressure_pa>{float_pattern})\n")
log3 = re.compile(f"LXS33_,(?P<battery_voltage>{float_pattern})\n")

file = "00001.IGC"
data = {
    "time_ms": 0.0,
    "height_meas": 0.0,
    "height_est": 0.0,
    "vel": 0.0,
    "pressure_pa": 0.0,
    "battery_voltage": 0.0,
}

with open(file, "r") as f:
    with open("igc_evaluation.csv", "w") as export_file:
        export_file.write("time,height_meas,height_est,ve,pressure_pa,battery_voltage\n")
        for line in f:
            handled = False
            m = log1.match(line)
            if m is not None:
                data["time_ms"] = float(m["time_ms"])
                data["height_meas"] = float(m["height_meas"])
                data["height_est"] = float(m["height_est"])
                data["vel"] = float(m["vel"])
                handled = True

            m = log2.match(line)
            if m is not None:
                data["time_ms"] = float(m["time_ms"])
                data["pressure_pa"] = float(m["pressure_pa"])
                handled = True

            m = log3.match(line)
            if m is not None:
                data["battery_voltage"] = float(m["battery_voltage"])
                handled = True

            if handled:
                export_file.write(f"{data['time_ms']/1000},{data['height_meas']},{data['height_est']},{data['vel']},{data['pressure_pa']},{data['battery_voltage']}\n")
