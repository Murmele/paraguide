#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon Apr  8 18:48:53 2024

Evaluate the tracing logs from Steigen3000

probe-rs run --log-format "{t} {s}" --chip nRF52840_xxAA target/thumbv7em-none-eabihf/debug/steigen3000  > logfile.log 2>&1

@author: martin
"""

import re
from enum import Enum
from common import float_pattern

print("Starting TracingLogs.py")

OnTaskStartExec = re.compile(f"^(?P<time>{float_pattern}).*TgSt(?P<id>[0-9]*)")  # Start executing the task with id `id`
OnTaskStopExec = re.compile(f"^(?P<time>{float_pattern}).*TgSp")  # Stop a task. Triggered by a wait
TaskStartReady = re.compile(f"^(?P<time>{float_pattern}).*TgR(?P<id>[0-9]*)")
OnTaskCreate = re.compile(f"^(?P<time>{float_pattern}).*TgC(?P<id>[0-9]*)")
OnIdle = re.compile(f"^(?P<time>{float_pattern}).*I")
Marker = re.compile(f"^(?P<time>{float_pattern}).*TgMk(?P<id>[0-9]+)")
MarkerStart = re.compile(f"^(?P<time>{float_pattern}).*TgMkt(?P<id>[0-9]+)")
MarkerStop = re.compile(f"^(?P<time>{float_pattern}).*TgMkp(?P<id>[0-9]+)")

StartPrintMarkers = re.compile(f"(?P<time>{float_pattern}) Start Print markers")
Markers = re.compile(f"(?P<time>{float_pattern}) (?P<name>[A-Za-z0-9]*)=(?P<value>[0-9]*)")
EndPrintMarkers = re.compile(f"(?P<time>{float_pattern}) End Print markers")

TaskName = re.compile("TgSpn (?P<task_name>.*)")

logfile = "../logfile.log"

class Event(Enum):
    StartTask = 1
    StopTask = 2
    TaskReady = 3
    Idle = 4
    CreateTask = 5
    Marker = 6
    MarkerStart = 7
    MarkerStop = 8


result_header = "Time,Event,CurrentTaskIndex,CurrentTaskName,MarkerId\n"


def task_line(time, event, current_task_info, marker_id):
    return f"{time},{event},{current_task_info['index']},{current_task_info['name']},{marker_id}\n"


def marker_line(time, event, current_task_info, marker_id):
    return f"{time},{event},{current_task_info['index']},{current_task_info['name']},{marker_id}\n"

current_task = 0
spawned_task_name = ""
tasks = {}

marker_stack = []

def get_marker_id():
    if len(marker_stack) > 0:
        return marker_stack[-1]
    return -1

marker_definition = False
marker_definitions = []

tasks[0] = {"index": 0, "name": "Idle"}
with open(logfile, "r") as f:
    first_task = True
    with open("result.csv", "w") as result_file:
        result_file.write(result_header)
        lines = f.readlines()
        for line in lines:
            m = StartPrintMarkers.match(line)
            if m is not None:
                marker_definition = True
                continue
            
            if marker_definition == True:
                if EndPrintMarkers.match(line) is not None:
                    marker_definition = False
                    continue
                m = Markers.match(line)
                assert m is not None
                name = m["name"]
                value = int(m["value"])
                marker_definitions.append({"name": name, "value": value})
            
            task_name = TaskName.search(line)
            if task_name is not None:
                spawned_task_name = task_name["task_name"]
            create = OnTaskCreate.search(line)
            if create is not None:
               if first_task:
                   first_task = False
                   spawned_task_name = "Main"
               current_task = int(create["id"])
               if current_task not in tasks:
                   tasks[current_task] = {"index": len(tasks), "name": spawned_task_name}
               time = float(create["time"])
               result_file.write(task_line(time, Event.CreateTask.value, tasks[current_task], get_marker_id()))
            start = OnTaskStartExec.search(line)
            if start is not None:
                current_task = int(start["id"])
                time = float(start["time"])
                result_file.write(task_line(time, Event.StartTask.value, tasks[current_task], get_marker_id()))
            stop = OnTaskStopExec.search(line)
            if stop is not None:
                time = float(stop["time"])
                result_file.write(task_line(time, Event.StopTask.value, tasks[current_task], get_marker_id()))
            ready = TaskStartReady.search(line)
            if ready is not None:
                time = float(ready["time"])
                result_file.write(task_line(time, Event.TaskReady.value, tasks[current_task], get_marker_id()))
            idle = OnIdle.search(line)
            if idle is not None:
                time = float(idle["time"])
                current_task = 0
                result_file.write(task_line(time, Event.Idle.value, tasks[current_task], get_marker_id()))
            marker = Marker.search(line)
            if marker is not None:
                time = float(marker["time"])
                marker_id = int(marker["id"])
                result_file.write(marker_line(time, Event.Marker.value, tasks[current_task], marker_id))
            marker = MarkerStart.search(line)
            if marker is not None:
                time = float(marker["time"])
                marker_id = int(marker["id"])
                print(f"{time} Start Marker: {marker_id}")
                marker_stack.append(marker_id)
                result_file.write(marker_line(time, Event.MarkerStart.value, tasks[current_task], marker_id))
            marker = MarkerStop.search(line)
            if marker is not None:
                time = float(marker["time"])
                marker_id = int(marker["id"])
                print(f"{time} End Marker: {marker_id}")
                if get_marker_id() == marker_id:
                    marker_stack = marker_stack[:-1]
                else:
                    # assert False
                    print("Start Marker lost")
                    marker_stack = []
                result_file.write(marker_line(time, Event.MarkerStop.value, tasks[current_task], marker_id))
                
                
with open("tasks.csv", "w") as file:
    file.write(f"TaskIndex,Taskname\n")
    for key in tasks:
        file.write(f"{tasks[key]['index']},{tasks[key]['name']}\n")
        
with open("markers.csv", "w") as file:
    file.write("Name,Value\n")
    for marker_definition in marker_definitions:
        file.write(f"{marker_definition['name']},{marker_definition['value']}\n")

