## Open source Vario by Charles Pope

## Linux
To program the firmware on the microcontroller a udev rule for the usb programmer is required!
For example:
1) output of `lsusb`: `Bus 001 Device 005: ID 1366:1015 SEGGER J-Link`
2) Therefore create a file 99-jlink.rules in `/etc/udev/rules.d/` and add the following content: `ATTR{idProduct}=="1015", ATTR{idVendor}=="1366", MODE="666"`
3) Reboot the System

## Std support
To run Steigen3000 on Linux the following packages must be installed

### Fedora

`sudo dnf install rust-alsa-sys-devel SDL2-devel`

## Flashing firmware
1) cargo build
2) probe-rs run --log-format "{t} {s}" --chip nRF52840_xxAA target/thumbv7em-none-eabihf/release/steigen3000

## Audio tool

With the audio_gui tool, the buzzer sound can be varied from a host computer
1) Build the audio_gui project in Tools folder: `cargo build --release` This tool checks all serial ports and when it finds the steigen3000, it connects to it and you are able to vary the vario tone.
2) Build steigen3000 firmware with `usb_vario_simulation` feature enabled:
`cargo build --release -F usb_vario_simulation`
