# Script to evaluate the performance of the usb mass storage capabilities

import re
from enum import Enum
import numpy as np

file = "../../logfile.log"

time_pattern = "[0-9]*(\.[0-9]*){0,1}"

start_reading_pattern = re.compile(f"(?P<time>{time_pattern}) srd. Count: (?P<count>[0-9]*).*")
write_data_pattern = re.compile(f"(?P<time>{time_pattern}) WIn.*")
stop_reading_pattern = re.compile(f"(?P<time>{time_pattern}) stop_multiblock_read")

start_sd_read = re.compile(f"(?P<time>{time_pattern}) Srfsd");
start_usb_send = re.compile(f"(?P<time>{time_pattern}) ssth")
finished_to_send = re.compile(f"(?P<time>{time_pattern}) fts")

show_duration_usb = False

def convert_time(time: str):
    return float(time) * 1000

show_not_equal = True

read_result = None

with open(file, "r") as f:
    for line in f.readlines():
        m = start_reading_pattern.match(line)
        if m is not None:
            # reset all parameters
            start_time = convert_time(m["time"])
            count = int(m["count"])
            
            read_result = {
                "start_time": start_time,
                "count": count,
                "duration_usb": [],
                "duration_sd": [],
                "write_datas": [],
                "start_usb": None,
                "start_sd": None,
            }
            continue

        m = write_data_pattern.match(line)
        if m is not None and read_result is not None:
            time = convert_time(m["time"])
            #print(line)
            read_result["write_datas"].append(time)
            continue
        
        m = start_sd_read.match(line)
        if m is not None and read_result is not None:
            time = convert_time(m["time"])
            read_result["start_sd"] = time
            if read_result["start_usb"] is not None:
                diff = time - read_result["start_usb"]
                if show_duration_usb:
                    print(f"Duration usb: {diff}")
                read_result["duration_usb"].append(diff)
                if len(read_result["duration_usb"]) != len(read_result["duration_sd"]):
                    read_result = None
            continue
        
        m = start_usb_send.match(line)
        if m is not None and read_result is not None:
            if read_result["start_sd"] is None:
                read_result = None # fail
                continue
            time = convert_time(m["time"])
            read_result["start_usb"] = time
            diff = time - read_result["start_sd"]
            if show_duration_usb:
                print(f"Duration sd: {diff}")
            read_result["duration_sd"].append(diff)
            
            if len(read_result["duration_sd"]) != len(read_result["duration_usb"]) + 1:
                if show_not_equal:
                    print(f"Not equal: {time}")
                read_result = None
            continue
        
        m = finished_to_send.match(line)
        if m is not None and read_result is not None:
            end_time = convert_time(m["time"])
            diff = end_time - read_result["start_usb"]
            if show_duration_usb:
                print(f"Duration usb: {diff}")
            read_result["duration_usb"].append(diff)
            if len(read_result["duration_usb"]) != len(read_result["duration_sd"]):
                if show_not_equal:
                    print(f"Not equal: {end_time}")
                read_result = None
                
            if read_result is not None:
                start_time = read_result["start_time"]
                count = read_result["count"]
                if count > 100:
                    if len(read_result["write_datas"]) > 1:
                        diff_write_datas = np.diff(read_result["write_datas"])
                        min = np.min(diff_write_datas)
                        max = np.max(diff_write_datas)
                        sum = np.sum(diff_write_datas)
                        mean = np.mean(diff_write_datas)
                        print(f"Diff: {end_time - start_time:5.2f}ms, Count: {count}, Mean: {mean:0.2f}ms, Min: {min:0.2f}ms, Max: {max:0.2f}ms, Sum: {sum:0.2f}ms")
                        
                        dur_sd = read_result["duration_sd"]
                        min = np.min(dur_sd)
                        max = np.max(dur_sd)
                        sum = np.sum(dur_sd)
                        mean = np.mean(dur_sd)
                        print(f"Duration SD: Mean: {mean:0.2f}ms, Min: {min:0.2f}, Max: {max:0.2f}, Sum: {sum:0.2f}")
                        
                        dur_usb = read_result["duration_usb"]
                        min = np.min(dur_usb)
                        max = np.max(dur_usb)
                        sum = np.sum(dur_usb)
                        mean = np.mean(dur_usb)
                        print(f"Duration USB: Mean: {mean:0.2f}ms, Min: {min:0.2f}ms, Max: {max:0.2f}ms, Sum: {sum:0.2f}ms")
                    else:
                        print(f"Diff: {end_time - start_time:5.2f}ms, Count: {count}")
                read_result = None # reset
            

        m = stop_reading_pattern.match(line)
        if m is not None and read_result is not None:
            assert start_time is not None
            end_time = convert_time(m["time"])
            diff = np.diff(write_datas)
            min = np.min(diff)
            max = np.max(diff)
            sum = np.sum(diff)
            print(f"Diff: {end_time - start_time:5.2f}, Count: {count}, Mean: {np.mean(diff):0.2f}, Min: {min:0.2f}, Max: {max:0.2f}, Sum: {sum:0.2f}")
 
#assert len(duration_sd) == len(duration_usb)
# with open("usb_sd_execution_time_evaluation.csv", "w") as f:
    