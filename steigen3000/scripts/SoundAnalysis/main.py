import re

float_pattern = "-{0,1}[0-9]*(\.[0-9]*){0,1}"
time_pattern = f"(?P<time>{float_pattern})"

state_pattern = re.compile(f"{time_pattern} State: (?P<pos_z_meas>{float_pattern}), (?P<pos_z>{float_pattern}), (?P<vel_z>{float_pattern})")

vario_sound_index_pattern = re.compile(f"{time_pattern} Vario sound setting index: (?P<index>[0-9]*)")
vario_sound_pattern = re.compile(f"{time_pattern} Vario sound settings: \((?P<freq>[0-9]*), (?P<on_time_ms>[0-9]*), (?P<off_time_ms>[0-9]*)\)")
buzzer_vario_sound_pattern = re.compile(f"{time_pattern} Buzzer: Vario sound settings: \((?P<freq>[0-9]*), (?P<on_time_ms>[0-9]*), (?P<off_time_ms>[0-9]*)\)")
buzzer_on_pattern = re.compile(f"{time_pattern} Buzzer on")
buzzer_off_pattern = re.compile(f"{time_pattern} Buzzer off")
pressure_pattern = re.compile(f"{time_pattern} Pressure: (?P<pressure>{float_pattern})")

time_height = ["time,height"]

def cond_last_value(data, key, default_value):
    if len(data[key]) == 0:
        return default_value
    return data[key][-1]

defaults = {
    "pos_z_meas": None,
    "pos_z": None,
    "vel_z": None,
    "sound_changed": None,
    "sound_index": None,
    "sound_frequency": None,
    "sound_on_time_ms": None,
    "sound_off_time_ms": None,
    "buzzer_sound_frequency": None,
    "buzzer_sound_on_time_ms": None,
    "buzzer_sound_off_time_ms": None,
    "buzzer_on_off": 0,
    "pressure": None,
}

def append_last_values(data):
    length = len(data["time"])
    for key in data:
        if not key in defaults:
            continue
        v = defaults[key]
        while len(data[key]) < length:
            if v is not None:
                data[key].append(cond_last_value(data, key, v))
            elif len(data[key]) > 0:
                data[key].append(data[key][-1])
            else:
                break

def add_pressure(data, time, pressure):
    data["time"].append(time)
    data["pressure"].append(pressure)
    data["sound_changed"].append(0)
    data["buzzer_sound_changed"].append(0)
    append_last_values(data)

def add_buzzer_on(data, time):
    data["time"].append(time)
    data["sound_changed"].append(0)
    data["buzzer_sound_changed"].append(0)
    data["buzzer_on_off"].append(1)
    append_last_values(data)

def add_buzzer_off(data, time):
    data["time"].append(time)
    data["sound_changed"].append(0)
    data["buzzer_sound_changed"].append(0)
    data["buzzer_on_off"].append(0)
    append_last_values(data)

def add_state(data, time, pos_z_meas, pos_z, vel_z):
    data["time"].append(time)
    data["pos_z_meas"].append(pos_z_meas)
    data["pos_z"].append(pos_z)
    data["vel_z"].append(vel_z)
    data["sound_changed"].append(0)
    data["buzzer_sound_changed"].append(0)
    append_last_values(data)
    time_height.append(f"{time},{pos_z}")

def add_vario_sound_settings(data, time, freq, on_time, off_time):
    changed = 0

    data["time"].append(time)
    if len(data["sound_frequency"]) == 0 or data["sound_frequency"][-1] != freq or data["sound_on_time_ms"][-1] != on_time or data["sound_off_time_ms"][-1] != off_time:
        changed = 1
    data["sound_changed"].append(changed)
    data["sound_frequency"].append(freq)
    data["sound_on_time_ms"].append(on_time)
    data["sound_off_time_ms"].append(off_time)
    data["buzzer_sound_changed"].append(0)
    append_last_values(data)

def add_buzzer_vario_sound_settings(data, time, freq, on_time, off_time):
    changed = 0

    data["time"].append(time)
    data["sound_changed"].append(0)
    if len(data["buzzer_sound_frequency"]) == 0 or data["buzzer_sound_frequency"][-1] != freq or data["buzzer_sound_on_time_ms"][-1] != on_time or data["buzzer_sound_off_time_ms"][-1] != off_time:
        changed = 1
    data["buzzer_sound_changed"].append(changed)
    data["buzzer_sound_frequency"].append(freq)
    data["buzzer_sound_on_time_ms"].append(on_time)
    data["buzzer_sound_off_time_ms"].append(off_time)
    append_last_values(data)
    
def add_vario_sound_index(data, time, index):
    data["time"].append(time)
    data["sound_index"].append(index)
    data["buzzer_sound_changed"].append(0)
    append_last_values(data)

if __name__ == "__main__":
    data = {
        "time": [],
        "pos_z": [],
        "vel_z": [],
        "pos_z_meas": [],
        "pressure": [],
        "sound_changed": [],
        "sound_index": [],
        "sound_frequency": [],
        "sound_on_time_ms": [],
        "sound_off_time_ms": [],
        "buzzer_sound_changed": [],
        "buzzer_sound_frequency": [],
        "buzzer_sound_on_time_ms": [],
        "buzzer_sound_off_time_ms": [],
        "buzzer_on_off": [],
    }
    with open("log.txt", "r") as input_file:
        counter = 0
        for line in input_file:
            counter += 1
            print(f"Line: {counter}")
            if counter == 16116:
                pass
            #try:
            m = state_pattern.match(line)
            if m is not None:
                add_state(data, float(m["time"]), float(m["pos_z_meas"]), float(m["pos_z"]), float(m["vel_z"]))
                continue

            m = vario_sound_pattern.match(line)
            if m is not None:
                add_vario_sound_settings(data, float(m["time"]), int(m["freq"]), int(m["on_time_ms"]), int(m["off_time_ms"]))
                continue

            m = buzzer_vario_sound_pattern.match(line)
            if m is not None:
                add_buzzer_vario_sound_settings(data, float(m["time"]), int(m["freq"]), int(m["on_time_ms"]),
                                         int(m["off_time_ms"]))
                continue
            m = vario_sound_index_pattern.match(line)
            if m is not None:
                add_vario_sound_index(data, float(m["time"]), int(m["index"]))
                continue

            m = buzzer_on_pattern.match(line)
            if m is not None:
                add_buzzer_on(data, float(m["time"]))
                continue

            m = buzzer_off_pattern.match(line)
            if m is not None:
                add_buzzer_off(data, float(m["time"]))
                continue

            m = pressure_pattern.match(line)
            if m is not None:
                add_pressure(data, float(m["time"]), float(m["pressure"]))
                continue
                #assert False
            #except:
            #    print(line)

    with open("analysis.csv", "w") as file:
        file.write("time,pressure,pos_z,vel_z,v_changed,v_index,v_freq,v_on_time,v_off_time,b_changed,b_freq,b_on_time,b_off_time,buzzer_state\n")
        for i in range(len(data["time"])):
            file.write(
                f"{data['time'][i]},{data['pressure'][i]},{data['pos_z'][i]},{data['vel_z'][i]},{data['sound_changed'][i]},{data['sound_index'][i]},{data['sound_frequency'][i]},{data['sound_on_time_ms'][i]},{data['sound_off_time_ms'][i]},{data['buzzer_sound_changed'][i]},{data['buzzer_sound_frequency'][i]},{data['buzzer_sound_on_time_ms'][i]},{data['buzzer_sound_off_time_ms'][i]},{data['buzzer_on_off'][i]}\n"
            )
    with open("time_height.csv", "w") as file:
        for line in time_height:
            file.write(f"{line}\n")
