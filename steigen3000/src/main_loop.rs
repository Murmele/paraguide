use crate::interface;
#[cfg(feature = "steigen33")]
use crate::tasks;
use crate::tasks::display::{CheckState, Page};
use crate::DisplayStateMutex;
#[cfg(not(feature = "usb_vario_simulation"))]
use crate::VarioSoundSender;
use crate::{error, info, trace};
use chrono::{NaiveTime, TimeDelta};
#[cfg(any(
    all(feature = "steigen33", not(feature = "usb_vario_simulation")),
    feature = "std"
))]
use paraguide::VarioSoundSettings;
use paraguidecore::common::Float;
use paraguidecore::logging::Timekeeper;
use paraguidecore::paraguide::{self, Paraguide, State};

const DUMMY_IGC_SECRET: &str = "dummy igc secret";

const LOOP_TIME_US: u64 = 14000; // ~75Hz as the barometer Main Loop

struct Time {
    t_us: u64,
    time: NaiveTime,
}

impl Time {
    fn new() -> Self {
        Self {
            t_us: interface::TimeKeeper::get_time_us(),
            time: NaiveTime::from_hms_opt(0, 0, 0).unwrap(),
        }
    }

    fn get_time(&mut self) -> NaiveTime {
        let t = interface::TimeKeeper::get_time_us();
        let diff = t - self.t_us;
        let diff = TimeDelta::microseconds(diff as i64);
        self.time = self.time.overflowing_add_signed(diff).0;

        self.t_us = t;

        self.time
    }
}

pub async fn main_loop<const N: usize>(
    device_info: paraguide::DeviceInfo<N>,
    #[cfg(not(feature = "usb_vario_simulation"))] vario_sound_signal: &VarioSoundSender,
    display_state: &DisplayStateMutex,
) -> ! {
    let initial_pressure = loop {
        if let Ok(v) = interface::pressure_temperature().await {
            break v.pressure;
        };
        interface::sleep_us(10_000).await;
    };
    let mut lock = display_state.lock().await;
    lock.check.baro = CheckState::Pass;
    drop(lock);

    let mut time = Time::new();

    info!("Initializing paraguide");
    let mut pg: Paraguide<'_, _, _, interface::TimeKeeper, _> = Paraguide::new(
        interface::FileHandler::new(),
        &device_info,
        DUMMY_IGC_SECRET,
    );
    let res = match pg.init(initial_pressure as Float).await {
        Ok(_) => {
            info!("Paraguide init SUCCESS");
            CheckState::Pass
        }
        Err(_) => {
            error!("Paraguide init FAILED");
            CheckState::Fail
        }
    };

    // Init display state
    let mut lock = display_state.lock().await;
    lock.mute = pg.is_mute();
    lock.check.paraguide_core = res;
    drop(lock);

    interface::sleep_us(2_000_000).await; // To show check page

    let mut lock = display_state.lock().await;
    lock.page = Page::Start;
    drop(lock);

    let mut start_time = interface::TimeKeeper::get_time_us();
    let mut end_time_pg;
    let mut start_time_pg = start_time;

    #[cfg(all(feature = "steigen33", feature = "usb_debug"))]
    let mut writer = tasks::usb::USBWriter::new();

    loop {
        start_time = interface::TimeKeeper::get_time_us();
        trace!("Main Loop");
        let mut ok = false;
        match interface::pressure_temperature().await {
            Ok(tp) => {
                trace!("Pressure: {}", tp.pressure);
                pg.set_pressure_temperature(tp.pressure as Float, tp.temperature as Float)
                    .await;
                ok = true;
            }
            Err(e) => {
                error!("Baro Failed: {:?}", e);
            }
        }

        // TODO: maybe considering not copying the data?
        // It is not possible to execute an async inside of the lambda
        if let Ok(ag) = &interface::acceleration_rotational_speed().await {
            trace!("Accel: ({},{},{})", ag.accel_x, ag.accel_y, ag.accel_z);
            trace!("Gyro: ({},{},{})", ag.gyro_x, ag.gyro_y, ag.gyro_z);

            #[cfg(all(feature = "steigen33", feature = "usb_debug"))]
            usb_debug_acceleration_gyroscope(ag, &mut writer).await;
        }

        #[cfg(feature = "steigen33")]
        if let Some(battery_voltage) = tasks::adc::ADC_BATTERY_SIGNAL.try_take() {
            let soc = interface::calculate_soc(battery_voltage);
            trace!("Voltage: {}, Soc: {}", battery_voltage, soc);
            if let Err(_) = pg.add_battery_state(soc).await {
                // TODO Handle
            }
            if battery_voltage < 3.0 {
                // Turn off
                tasks::buttons::BUTTON1_PULL_SIGNAL.signal(embassy_nrf::gpio::Pull::Down);
            }
            let mut lock = display_state.lock().await;
            lock.battery_soc = soc;
            drop(lock);
        }

        while let Ok(nmea_message) = interface::receive_nmea_messages().await {
            #[cfg(feature = "steigen33")]
            trace!(
                "Steigen3000 Lib: New nmea message for pg: {}",
                tasks::gps::nmea_parse_result_to_string(&nmea_message)
            );
            let _ = pg.add_nmea_message(&nmea_message).await;
            let mut lock = display_state.lock().await;
            lock.fix_available = pg.has_fix();
            if pg.has_fix() {
                if let Some(t) = pg.data.input_data.gps_data.fix_time {
                    time.time = t;
                }
            }
        }

        #[cfg(feature = "steigen33")]
        if let Some(battery_voltage) = tasks::adc::ADC_BATTERY_SIGNAL.try_take() {
            let soc = interface::calculate_soc(battery_voltage);
            trace!("Voltage: {}, Soc: {}", battery_voltage, soc);
            if let Err(_) = pg.add_battery_state(soc).await {
                // TODO Handle
            }
            let mut lock = display_state.lock().await;
            lock.battery_soc = soc;
        }

        #[cfg(all(not(feature = "std"), feature = "nrf"))]
        let mut button1_long_press = false;
        #[cfg(all(not(feature = "std"), feature = "nrf"))]
        if let Some(state) = tasks::buttons::BUTTON1_STATE.try_take() {
            match state {
                tasks::buttons::ButtonState::LongPress => button1_long_press = true,
                tasks::buttons::ButtonState::ShortPress => (), /*Nothing to do yet */
            }
        }
        #[cfg(feature = "std")]
        let button1_long_press = false;

        if (pg.soc().is_some() && pg.soc().unwrap() <= 0.) || button1_long_press {
            // Turn off
            let _ = pg.shutdown().await;
            #[cfg(all(feature = "steigen33", not(feature = "usb_vario_simulation")))]
            vario_sound_signal.signal(VarioSoundSettings::new_muted());
            #[cfg(feature = "std")]
            let _ = vario_sound_signal.send(VarioSoundSettings::new_muted());
            let mut lock = display_state.lock().await;
            lock.display_on = false;
            drop(lock);
            #[cfg(feature = "nrf")]
            tasks::buttons::BUTTON1_PULL_SIGNAL.signal(embassy_nrf::gpio::Pull::Down);
            // Wait until we turned off
            loop {
                interface::sleep_us(3_000_000).await;
            }
        }

        #[cfg(all(not(feature = "std"), feature = "nrf"))]
        if let Some(state) = tasks::buttons::BUTTON2_STATE.try_take() {
            let _ = match state {
                tasks::buttons::ButtonState::LongPress => match pg.state() {
                    State::Idle => pg.start_flight().await,
                    State::Flight | State::Takeoff => pg.end_flight().await,
                    State::Landing => Ok(()),
                },
                tasks::buttons::ButtonState::ShortPress => {
                    pg.toggle_mute();
                    let mut lock = display_state.lock().await;
                    lock.mute = pg.is_mute();
                    Ok(())
                }
            };
        }

        if ok {
            end_time_pg = interface::TimeKeeper::get_time_us();
            pg.data.state.time_ms = (end_time_pg / 1000) as u32;
            let dt_ms = (end_time_pg - start_time_pg) as f32 / 1000.;
            trace!("Diff: {}", dt_ms);
            start_time_pg = interface::TimeKeeper::get_time_us();
            match pg.step(dt_ms as Float / 1000.).await {
                Ok(_) => (),
                Err(_) => (), // TODO: handle
            }
            {
                let mut lock = display_state.lock().await;
                lock.logging = pg.logging_active();
                lock.vario_value = pg.data.vario_value_buzzer;
                match pg.state() {
                    State::Takeoff | State::Flight => lock.page = Page::FlightMain,
                    State::Idle | State::Landing => lock.page = Page::Start,
                }
                lock.height_msl = pg.data.state.pos.z;
                lock.time = Some(time.get_time());
            }
            let v = pg.data.state.velocity.z;
            let height_meas = pg.data.baro_kalmanfilter.height_measured();
            trace!("State: {}, {}, {}", height_meas, pg.data.state.pos.z, v);

            #[cfg(not(feature = "usb_vario_simulation"))]
            {
                let s = pg.get_vario_sound();
                trace!(
                    "Vario sound settings: ({}, {}, {})",
                    s.frequency,
                    s.on_time_ms,
                    s.off_time_ms
                );
                #[cfg(feature = "steigen33")]
                vario_sound_signal.signal(s);
                #[cfg(feature = "std")]
                let _ = vario_sound_signal.send(s);
            }
        }
        sleep(start_time).await;
    }
}

async fn sleep(start_time: u64) {
    let end_time = interface::TimeKeeper::get_time_us();
    let diff = end_time - start_time;
    if diff < LOOP_TIME_US {
        let mut wait_time: i64 = (LOOP_TIME_US - diff) as i64;
        // let start_wait = interface::TimeKeeper::get_time_us();
        let mut w = (wait_time / 10) as u64;
        while wait_time > 0 {
            let start_wait = interface::TimeKeeper::get_time_us();
            interface::sleep_us(w).await;
            wait_time -= (interface::TimeKeeper::get_time_us() - start_wait) as i64;
            if wait_time > 0 && (wait_time as u64) < w {
                w = wait_time as u64;
            }
        }
        // info!(
        //     "Finished wait: {}",
        //     (TimeKeeper::get_time_us() - start_wait) as f32 / 1000.
        // );
    } else {
        //error!("NO WAIT##########");
    }
}

#[cfg(all(feature = "steigen33", feature = "usb_debug"))]
async fn usb_debug_acceleration_gyroscope(
    ag: &crate::sensors::AccelGyro,
    writer: &mut tasks::usb::USBWriter,
) {
    let mut message: heapless::String<{ tasks::usb::BUFFER_SIZE }> = heapless::String::new();
    let _ = paraguidecore::helper::float_to_string(
        (interface::TimeKeeper::get_time_us() / 1000) as f32,
        3,
        &mut message,
    );
    let _ = message.push(',');
    let _ = paraguidecore::helper::float_to_string(ag.accel_x, 6, &mut message);
    let _ = message.push(',');
    let _ = paraguidecore::helper::float_to_string(ag.accel_y, 6, &mut message);
    let _ = message.push(',');
    let _ = paraguidecore::helper::float_to_string(ag.accel_z, 6, &mut message);
    let _ = message.push('\n');
    writer.write(&message).await;
}
