#[cfg(feature = "rtos-trace")]
use rtos_trace;

pub struct Marker {
    #[cfg(feature = "rtos-trace")]
    marker: Markers,
}

impl Marker {
    #[allow(dead_code)]
    pub fn new(_marker: Markers) -> Self {
        #[cfg(feature = "rtos-trace")]
        rtos_trace::trace::marker_begin(_marker.into());
        Marker {
            #[cfg(feature = "rtos-trace")]
            marker: _marker,
        }
    }
}

impl Drop for Marker {
    fn drop(&mut self) {
        #[cfg(feature = "rtos-trace")]
        rtos_trace::trace::marker_end(self.marker.into());
    }
}

#[cfg_attr(feature = "rtos-trace", derive(strum_macros::EnumIter, defmt::Format))]
#[derive(Clone, Copy)]
#[repr(u32)]
pub enum Markers {
    USBMSCRead = 0,
    USBMSCReadCopy = 1,
    SDWrite = 2,
    SDOpenWrite = 3,
    SDGetFileOperation = 4,
}

impl From<Markers> for u32 {
    fn from(value: Markers) -> Self {
        value as u32
    }
}
