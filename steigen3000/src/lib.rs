// make `std` available when testing: https://ferrous-systems.com/blog/test-embedded-app/
#![cfg_attr(not(feature = "std"), no_std)]
#![deny(warnings)]
#![allow(incomplete_features)]
#![feature(generic_const_exprs)]
#![feature(generic_arg_infer)]

mod main_loop;
pub use main_loop::main_loop;
pub mod actuators;
pub mod sensors;
pub mod tasks;
pub use paraguidecore::{self, common, debug, error, info, trace, warn};

mod tracing_markers;
cfg_if::cfg_if! {
if #[cfg(feature = "rtos-trace")] {
    use rtos_trace;
    mod tracing;
    use tracing::SystemView;
    rtos_trace::global_trace! {SystemView}
    struct TraceInfo();
    impl rtos_trace::RtosTraceApplicationCallbacks for TraceInfo {
        fn system_description() {}
        fn sysclock() -> u32 {
            64000000
        }
    }
    rtos_trace::global_application_callbacks! {TraceInfo}
}
}

// Important, otherwise the tests get not executed
#[cfg(any(feature = "steigen33", feature = "steigen31", test))]
pub mod platform_steigen3000;

#[cfg(feature = "defmt")]
use defmt_rtt as _;

cfg_if::cfg_if! {
    if #[cfg(any(feature = "steigen33", feature = "steigen31"))] {
        use embassy_sync::{blocking_mutex::raw::CriticalSectionRawMutex, signal, mutex::Mutex};
        pub use platform_steigen3000 as platform;
        type VarioSoundSender = signal::Signal<
            CriticalSectionRawMutex,
            paraguidecore::paraguide::VarioSoundSettings,>;
        type VarioSoundReceiver = signal::Signal<
            CriticalSectionRawMutex,
            paraguidecore::paraguide::VarioSoundSettings,>;
        type DisplayStateMutex = Mutex<CriticalSectionRawMutex, tasks::display::State>;
    } else if #[cfg(feature = "std")] {
        pub mod platform_std;
        pub use platform_std as platform;
        use tokio::sync::{watch, Mutex};
        use std::sync::Arc;

        type VarioSoundSender = watch::Sender<paraguidecore::paraguide::VarioSoundSettings>;
        type VarioSoundReceiver = watch:: Receiver<paraguidecore::paraguide::VarioSoundSettings>;
        type DisplayStateMutex = Arc<Mutex<tasks::display::State>>;
    }
}

pub trait DisplayApply<E> {
    fn apply(&mut self) -> impl core::future::Future<Output = Result<(), E>>;
}

pub trait DisplayImage<E>: DisplayBytes<E> {
    fn draw_image<const WIDTH: usize, const HEIGHT: usize>(
        &mut self,
        x: usize,
        y: usize,
        image: &[[u8; WIDTH]; HEIGHT],
    ) -> Result<(), E>;
}

pub trait DisplayBytes<E> {
    /// Draw 8 pixels on the display. Every bit is a pixel
    /// x and y position are in pixels not in bytes!
    fn draw_byte(&mut self, x: i32, y: i32, byte: u8) -> Result<(), E>;
}

pub use platform::interface;
