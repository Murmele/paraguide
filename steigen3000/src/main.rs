// https://docs.rust-embedded.org/book/intro/index.html
#![cfg_attr(not(feature = "std"), no_std)]
#![cfg_attr(not(feature = "std"), no_main)]
// #![deny(warnings)]

#[cfg(feature = "defmt")]
use defmt_rtt as _;
#[cfg(not(feature = "std"))]
use panic_probe as _;

#[cfg(any(feature = "steigen33", feature = "steigen31"))]
#[embassy_executor::main]
async fn main(spawner: embassy_executor::Spawner) -> ! {
    steigen3000::platform::main::steigen3000(spawner).await
}

#[cfg(feature = "std")]
#[tokio::main]
async fn main() {
    use std::sync::Arc;
    use steigen3000::platform::main::{buzzer, display, main, simulation_task};
    use steigen3000::tasks::display::{Page, State};
    use tokio;
    use tokio::sync::{watch, Mutex};

    let (mpsc_tx_vario_sound_signal, mpsc_rx_vario_sound_signal) =
        watch::channel(steigen3000::paraguidecore::paraguide::VarioSoundSettings {
            frequency: 0,
            on_time_ms: 0,
            off_time_ms: 0,
            duty: 500,
        });

    let data1 = Arc::new(Mutex::new(State::new()));
    let data2 = Arc::clone(&data1);

    let mut lock = data2.lock().await;
    lock.page = Page::FlightMain;
    drop(lock);

    let main = main(mpsc_tx_vario_sound_signal, &data2);
    let simu = simulation_task();
    let buzzer = buzzer(mpsc_rx_vario_sound_signal);
    let display = display(data1);
    display.await;
    // tokio::join!(main, buzzer, simu, display);
}
