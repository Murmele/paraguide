use crate::trace;
use rtos_trace::RtosTrace;
use rtos_trace::TaskInfo;

pub struct SystemView;

impl SystemView {
    #[allow(unused)]
    pub const fn new() -> SystemView {
        SystemView
    }

    #[allow(unused)]
    pub fn init(&self) {
        trace!("TgI");
    }

    #[allow(unused)]
    pub fn send_system_description(desc: &str) {
        trace!("TgSD{}", desc);
    }
}

impl RtosTrace for SystemView {
    fn task_new(id: u32) {
        trace!("TgC{}", id);
    }

    fn task_send_info(id: u32, info: TaskInfo) {
        trace!(
            "TgSendTaskInfo: Id:{},Name:{},Prio:{},StackBase:{},StackSize:{}",
            id,
            info.name,
            info.priority,
            info.stack_base,
            info.stack_size
        );
    }

    fn task_terminate(id: u32) {
        trace!("{=istr}{=u32}", defmt::intern!("TgTerm"), id);
    }

    fn task_exec_begin(id: u32) {
        trace!("{=istr}{=u32}", defmt::intern!("TgSt"), id);
    }

    fn task_exec_end() {
        trace!("TgSp");
    }

    fn task_ready_begin(id: u32) {
        trace!("{=istr}{=u32}", defmt::intern!("TgR"), id);
    }

    fn task_ready_end(id: u32) {
        trace!("{=istr}{=u32}", defmt::intern!("TgSpRdy"), id);
    }

    fn system_idle() {
        trace!("TgI");
    }

    fn isr_enter() {
        trace!("TgRecordEnterISR");
    }

    fn isr_exit() {
        trace!("TgRecordExitISR");
    }

    fn isr_exit_to_scheduler() {
        trace!("TgRecordExitISRToScheduler");
    }

    fn marker(id: u32) {
        trace!("{=istr}{=u32}", defmt::intern!("TgMk"), id);
    }

    fn marker_begin(id: u32) {
        trace!("{=istr}{=u32}", defmt::intern!("TgMkt"), id);
    }

    fn marker_end(id: u32) {
        trace!("{=istr}{=u32}", defmt::intern!("TgMkp"), id);
    }
}
