#![allow(dead_code)]

use super::lps22hd_register::*;

pub struct PressTemp {
    pub pressure: f32,
    pub temperature: f32,
}

pub fn ctrl_cfg1_value(
    data_rate: OutputDataRate,
    lowpass_filter: LowPassFilter,
    block_data_update: BlockDataUpdate,
    spi_interface_mode: SerialInterfaceMode,
) -> CTRL_REG1 {
    let value = data_rate.bits()
        | lowpass_filter.bits()
        | block_data_update.bits()
        | spi_interface_mode.bits();
    value
}

pub fn ctrl_cfg2_value(
    one_shot: OneShot,
    sw_reset: SWRESET,
    auto_increment: AutoIncrement,
    boot: BootMode,
) -> CTRL_REG2 {
    let value = one_shot.bits() | sw_reset.bits() | auto_increment.bits() | boot.bits();
    value
}

pub fn ctrl_cfg3_value(
    interrupt_config: InterruptConfiguration,
    interrupt_drdy: InterruptDRDY,
    interrupt_fifo_ovr: InterruptFIFOOVR,
    interrupt_fifo_wtm: InterruptFIFOWTM,
    interrupt_fifo_full: InterruptFIFOFULL,
    pp_od: InterruptPadMode,
) -> CTRL_REG3 {
    let value = interrupt_config.bits()
        | interrupt_drdy.bits()
        | interrupt_fifo_ovr.bits()
        | interrupt_fifo_wtm.bits()
        | interrupt_fifo_full.bits()
        | pp_od.bits();
    value
}

#[repr(u8)]
pub enum OutputDataRate {
    // OneShot, // Device is in power down mode.  This is handled by the module
    F1Hz,
    F10Hz,
    F25Hz,
    F50Hz,
    F75Hz,
}

impl OutputDataRate {
    pub fn bits(self) -> CTRL_REG1 {
        match self {
            // Self::OneShot => CTRL_REG1::empty(),
            Self::F1Hz => CTRL_REG1::ODR0,
            Self::F10Hz => CTRL_REG1::ODR1,
            Self::F25Hz => CTRL_REG1::ODR0 | CTRL_REG1::ODR1,
            Self::F50Hz => CTRL_REG1::ODR2,
            Self::F75Hz => CTRL_REG1::ODR0 | CTRL_REG1::ODR2,
        }
    }
}

#[derive(Default)]
#[repr(u8)]
pub enum SerialInterfaceMode {
    #[default]
    _4Wire,
    _3Wire,
}

impl SerialInterfaceMode {
    pub fn bits(self) -> CTRL_REG1 {
        match self {
            Self::_4Wire => CTRL_REG1::empty(),
            Self::_3Wire => CTRL_REG1::SIM,
        }
    }
}

#[repr(u8)]
pub enum BlockDataUpdate {
    ContinuousUpdate,
    MSBLSBRead,
}

impl BlockDataUpdate {
    pub fn bits(self) -> CTRL_REG1 {
        match self {
            Self::ContinuousUpdate => CTRL_REG1::empty(),
            Self::MSBLSBRead => CTRL_REG1::BDU,
        }
    }
}

#[repr(u8)]
pub enum LowPassFilter {
    Disabled,
    Odr9,
    Odr20,
}

impl LowPassFilter {
    pub fn bits(self) -> CTRL_REG1 {
        match self {
            Self::Disabled => CTRL_REG1::empty(),
            Self::Odr9 => CTRL_REG1::EN_LPFP,
            Self::Odr20 => CTRL_REG1::EN_LPFP | CTRL_REG1::LPFP_CFG,
        }
    }
}

#[derive(Default)]
#[repr(u8)]
pub enum OneShot {
    #[default]
    IdleMode,
    NewDatasetAcquired,
}

impl OneShot {
    pub fn bits(self) -> CTRL_REG2 {
        match self {
            Self::IdleMode => CTRL_REG2::empty(),
            Self::NewDatasetAcquired => CTRL_REG2::ONE_SHOT,
        }
    }
}

#[repr(u8)]
pub enum SWRESET {
    Normal,
    Reset,
}

impl SWRESET {
    pub fn bits(self) -> CTRL_REG2 {
        match self {
            Self::Normal => CTRL_REG2::empty(),
            Self::Reset => CTRL_REG2::SWRESET,
        }
    }
}

#[repr(u8)]
pub enum AutoIncrement {
    Disable,
    Enable,
}

impl AutoIncrement {
    pub fn bits(self) -> CTRL_REG2 {
        match self {
            Self::Disable => CTRL_REG2::empty(),
            Self::Enable => CTRL_REG2::IF_ADD_INC,
        }
    }
}

#[repr(u8)]
pub enum InterruptPadMode {
    PushPull,
    OpenDrain,
}

impl InterruptPadMode {
    pub fn bits(self) -> CTRL_REG3 {
        match self {
            Self::PushPull => CTRL_REG3::empty(),
            Self::OpenDrain => CTRL_REG3::PP_OD,
        }
    }
}

#[repr(u8)]
pub enum BootMode {
    NormalMode,
    Reboot,
}

impl BootMode {
    pub fn bits(self) -> CTRL_REG2 {
        match self {
            Self::NormalMode => CTRL_REG2::empty(),
            Self::Reboot => CTRL_REG2::BOOT,
        }
    }
}

// ----------------------------------------------------------------------------
// CTRL_REG3

#[repr(u8)]
pub enum InterruptActiveLevel {
    High = CTRL_REG3::empty().bits(),
    Low = CTRL_REG3::INT_H_L.bits(),
}

#[repr(u8)]
pub enum InterruptConfiguration {
    DataSignal,
    PressureHigh,
    PressureLow,
    PressureLowOrHigh,
}

impl InterruptConfiguration {
    pub fn bits(self) -> CTRL_REG3 {
        match self {
            Self::DataSignal => CTRL_REG3::empty(),
            Self::PressureHigh => CTRL_REG3::INT_S1,
            Self::PressureLow => CTRL_REG3::INT_S2,
            Self::PressureLowOrHigh => CTRL_REG3::INT_S1 | CTRL_REG3::INT_S2,
        }
    }
}

#[repr(u8)]
pub enum InterruptDRDY {
    Disable,
    Enable,
}

impl InterruptDRDY {
    pub fn bits(self) -> CTRL_REG3 {
        match self {
            Self::Disable => CTRL_REG3::empty(),
            Self::Enable => CTRL_REG3::DRDY,
        }
    }
}

#[repr(u8)]
pub enum InterruptFIFOOVR {
    Disable,
    Enable,
}

impl InterruptFIFOOVR {
    pub fn bits(self) -> CTRL_REG3 {
        match self {
            Self::Disable => CTRL_REG3::empty(),
            Self::Enable => CTRL_REG3::F_OVR,
        }
    }
}

#[repr(u8)]
pub enum InterruptFIFOWTM {
    Disable,
    Enable,
}

impl InterruptFIFOWTM {
    pub fn bits(self) -> CTRL_REG3 {
        match self {
            Self::Disable => CTRL_REG3::empty(),
            Self::Enable => CTRL_REG3::F_FTH,
        }
    }
}

#[repr(u8)]
pub enum InterruptFIFOFULL {
    Disable,
    Enable,
}

impl InterruptFIFOFULL {
    pub fn bits(self) -> CTRL_REG3 {
        match self {
            Self::Disable => CTRL_REG3::empty(),
            Self::Enable => CTRL_REG3::F_FSS5,
        }
    }
}

// ----------------------------------------------------------------------------
// FIFO_CTRL

#[repr(u8)]
pub enum FifoMode {
    Bypass,
    FIFOMode,
    Continuous,
    BypassToFIFO,
    BypassToContinuous,
    ContinuousToFIFO,
}

impl FifoMode {
    pub fn bits(self) -> FIFO_CTRL {
        match self {
            Self::Bypass => FIFO_CTRL::empty(),
            Self::FIFOMode => FIFO_CTRL::F_MODE0,
            Self::Continuous => FIFO_CTRL::F_MODE1,
            Self::BypassToFIFO => FIFO_CTRL::F_MODE0 | FIFO_CTRL::F_MODE1 | FIFO_CTRL::F_MODE2,
            Self::BypassToContinuous => FIFO_CTRL::F_MODE2,
            Self::ContinuousToFIFO => FIFO_CTRL::F_MODE0 | FIFO_CTRL::F_MODE1,
        }
    }
}
