#![allow(dead_code)]
pub struct Status {
    t_or: bool,
    p_or: bool,
    t_da: bool,
    p_da: bool,
}

impl Status {
    pub fn new(data: u8) -> Self {
        Status {
            t_or: (data & 0b00100000) > 0,
            p_or: (data & 0b00010000) > 0,
            t_da: (data & 0b00000010) > 0,
            p_da: (data & 0b00000001) > 0,
        }
    }
}
