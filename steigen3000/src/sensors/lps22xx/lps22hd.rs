//! Implementation of the LPS22HD
pub use super::lps22hd_conf as conf;
use super::lps22hd_register::{self as register, Register};
use super::lps22xx_conf::*;
use super::*;

use core::fmt::Debug;
use core::result::Result::{self, Err, Ok};
use embedded_hal_async as hal;
use hal::i2c::I2c;

pub struct LPS22HD<Interface, AquireMode> {
    interface: Interface,
    sa0: super::SA0,
    #[allow(dead_code)]
    one_shot: AquireMode,
}

/// Implementation indipendend of the aquire type
impl<Interface, AquireMode, E> LPS22HD<Interface, AquireMode>
where
    Interface: I2c<Error = E>,
    E: Debug,
{
    fn address(&self) -> u8 {
        return register::BASE_ADDRESS + self.sa0 as u8;
    }

    pub async fn device_detected(&mut self) -> Result<(), Error<E>> {
        let device_id = self.get_device_id().await?;
        if device_id != register::WHO_AM_I {
            return Err(ErrorKind::WrongDeviceId.into());
        } else {
            return Ok(());
        }
    }

    async fn get_device_id(&mut self) -> Result<u8, Error<E>> {
        let input = [Register::WHO_AM_I.addr()];
        let mut output = [0u8];

        self.interface
            .write_read(self.address(), &input, &mut output)
            .await
            .map_err(|e| ErrorKind::I2CError.with(e))?;
        return Ok(output[0]);
    }

    // TODO: Seems that the compiler does not optimize this out, so
    // returning a struct instead of filling reference values might be inefficient
    // Or will it be optimized out?
    #[allow(dead_code)]
    pub async fn get_status(&mut self) -> Result<Status, Error<E>> {
        let input = [Register::STATUS.addr()];
        let mut output = [0u8];

        self.interface
            .write_read(self.address(), &input, &mut output)
            .await
            .map_err(|e| ErrorKind::I2CError.with(e))?;
        return Ok(Status::new(output[0]));
    }

    pub async fn get_pressure_temperature(&mut self) -> Result<PressTemp, Error<E>> {
        // Check if autoincrement and implement it!
        // Maybe also at compile time?
        const AUTOINCREMENT: bool = false;
        if AUTOINCREMENT {
            let mut output = [0u8, 0u8, 0u8, 0u8, 0u8];
            // PRESS_OUT_XL is the lowest address
            self.interface
                .write_read(
                    self.address(),
                    &[Register::PRESS_OUT_XL.addr()],
                    &mut output,
                )
                .await
                .map_err(|e| ErrorKind::I2CError.with(e))?;

            Ok(PressTemp {
                pressure: calculate_pressure(output[2], output[1], output[0]),
                temperature: calculate_temperature(output[4], output[3]),
            })
        } else {
            let mut output = [0u8];

            self.interface
                .write_read(
                    self.address(),
                    &[Register::PRESS_OUT_XL.addr()],
                    &mut output,
                )
                .await
                .map_err(|e| ErrorKind::I2CError.with(e))?;
            let pressure_xl = output[0];

            self.interface
                .write_read(self.address(), &[Register::PRESS_OUT_L.addr()], &mut output)
                .await
                .map_err(|e| ErrorKind::I2CError.with(e))?;
            let pressure_l = output[0];

            self.interface
                .write_read(self.address(), &[Register::PRESS_OUT_H.addr()], &mut output)
                .await
                .map_err(|e| ErrorKind::I2CError.with(e))?;
            let pressure_h = output[0];

            self.interface
                .write_read(self.address(), &[Register::TEMP_OUT_L.addr()], &mut output)
                .await
                .map_err(|e| ErrorKind::I2CError.with(e))?;
            let temperature_l = output[0];

            self.interface
                .write_read(self.address(), &[Register::TEMP_OUT_H.addr()], &mut output)
                .await
                .map_err(|e| ErrorKind::I2CError.with(e))?;
            let temperature_h = output[0];
            Ok(PressTemp {
                pressure: calculate_pressure(pressure_h, pressure_l, pressure_xl),
                temperature: calculate_temperature(temperature_h, temperature_l),
            })
        }
    }

    pub async fn set_ctrl_cfg3(
        &mut self,
        interrupt_config: conf::InterruptConfiguration,
        interrupt_drdy: conf::InterruptDRDY,
        interrupt_fifo_ovr: conf::InterruptFIFOOVR,
        interrupt_fifo_wtm: conf::InterruptFIFOWTM,
        interrupt_fifo_full: conf::InterruptFIFOFULL,
        interrupt_pad_mode: conf::InterruptPadMode,
    ) -> Result<(), Error<E>> {
        let bytes = [
            Register::CTRL_REG3.addr(),
            conf::ctrl_cfg3_value(
                interrupt_config,
                interrupt_drdy,
                interrupt_fifo_ovr,
                interrupt_fifo_wtm,
                interrupt_fifo_full,
                interrupt_pad_mode,
            )
            .bits(),
        ];
        self.interface
            .write(self.address(), &bytes)
            .await
            .map_err(|e| ErrorKind::I2CError.with(e))?;
        Ok(())
    }
}

impl<Interface, E> LPS22HD<Interface, Continuous>
where
    Interface: I2c<Error = E>,
    E: Debug,
{
    /// Create a new lps22hd device in continuous mode
    pub async fn new_continuous(
        interface: Interface,
        sa0: SA0,
        data_rate: conf::OutputDataRate,
        lowpass_filter: conf::LowPassFilter,
        block_data_update: conf::BlockDataUpdate,
    ) -> Result<Self, Error<E>> {
        let mut lps22hd = LPS22HD {
            interface,
            sa0,
            one_shot: Continuous,
        };

        lps22hd.device_detected().await?;

        // TODO: do this in a single i2c sequence not in 3
        lps22hd
            .set_ctrl_cfg1(data_rate, lowpass_filter, block_data_update)
            .await?;
        lps22hd
            .set_ctrl_cfg2(
                conf::SWRESET::Normal,
                conf::AutoIncrement::Enable,
                conf::BootMode::NormalMode,
            )
            .await?;
        lps22hd
            .set_ctrl_cfg3(
                conf::InterruptConfiguration::DataSignal,
                conf::InterruptDRDY::Enable,
                conf::InterruptFIFOOVR::Disable,
                conf::InterruptFIFOWTM::Disable,
                conf::InterruptFIFOFULL::Disable,
                conf::InterruptPadMode::PushPull,
            )
            .await?;

        Ok(lps22hd)
    }

    async fn set_ctrl_cfg1(
        &mut self,
        data_rate: conf::OutputDataRate,
        lowpass_filter: conf::LowPassFilter,
        block_data_update: conf::BlockDataUpdate,
    ) -> Result<(), Error<E>> {
        // For I2C SerialInterfaceMode is irrelevant, so use the default
        let bytes = [
            Register::CTRL_REG1.addr(),
            {
                let spi_interface_mode = conf::SerialInterfaceMode::default();
                let value = data_rate.bits()
                    | lowpass_filter.bits()
                    | block_data_update.bits()
                    | spi_interface_mode.bits();
                value
            }
            .bits(),
        ];
        self.interface
            .write(self.address(), &bytes)
            .await
            .map_err(|e| ErrorKind::I2CError.with(e))?;
        Ok(())
    }

    async fn set_ctrl_cfg2(
        &mut self,
        sw_reset: conf::SWRESET,
        auto_increment: conf::AutoIncrement,
        boot: conf::BootMode,
    ) -> Result<(), Error<E>> {
        // One shot must be off for continuous
        let bytes = [
            Register::CTRL_REG2.addr(),
            conf::ctrl_cfg2_value(conf::OneShot::default(), sw_reset, auto_increment, boot).bits(),
        ];
        self.interface
            .write(self.address(), &bytes)
            .await
            .map_err(|e| ErrorKind::I2CError.with(e))?;
        Ok(())
    }

    /// switch from continuous mode to single shot mode
    #[allow(dead_code)]
    pub async fn into_single_shot(
        mut self,
        lp: conf::LowPassFilter,
        block_data_update: conf::BlockDataUpdate,
        spi_interface_mode: conf::SerialInterfaceMode,
    ) -> Result<LPS22HD<Interface, SingleShot>, Error<E>> {
        // Set odr to 0x00
        let addr_data = [
            Register::CTRL_REG1.addr(),
            (lp.bits() | block_data_update.bits() | spi_interface_mode.bits()).bits(),
        ];
        self.interface
            .write(self.address(), &addr_data)
            .await
            .map_err(|e| ErrorKind::I2CError.with(e))?;
        Ok(LPS22HD {
            interface: self.interface,
            sa0: self.sa0,
            one_shot: SingleShot,
        })
    }
}

// By default the aquire mode is SingleShot (reset config of the chip)
impl<Interface, E> LPS22HD<Interface, SingleShot>
where
    Interface: I2c<Error = E>,
    E: Debug,
{
    /// Create a new lps22hd device in single shot mode
    #[allow(dead_code)]
    pub async fn new_single_shot(interface: Interface, sa0: SA0) -> Result<Self, Error<E>> {
        let mut lps22hd = LPS22HD {
            interface,
            sa0,
            one_shot: SingleShot,
        };

        lps22hd.device_detected().await?;
        Ok(lps22hd)
    }
}

/// Functions which can be used only when the device is in single shot mode
impl<Interface, E> LPS22HD<Interface, SingleShot>
where
    Interface: I2c<Error = E>,
    E: Debug,
{
    // switch from single shot mode to continuous mode
    #[allow(dead_code)]
    pub async fn into_continuous(
        mut self,
        data_rate: conf::OutputDataRate,
        lp: conf::LowPassFilter,
        block_data_update: conf::BlockDataUpdate,
        spi_interface_mode: conf::SerialInterfaceMode,
    ) -> Result<LPS22HD<Interface, Continuous>, Error<E>> {
        // Required to set ONE_SHOT to zero?
        let bytes = [
            Register::CTRL_REG1.addr(),
            (data_rate.bits() | lp.bits() | block_data_update.bits() | spi_interface_mode.bits())
                .bits(),
        ];
        self.interface
            .write(self.address(), &bytes)
            .await
            .map_err(|e| ErrorKind::I2CError.with(e))?;
        Ok(LPS22HD {
            interface: self.interface,
            sa0: self.sa0,
            one_shot: Continuous,
        })
    }
    // pub fn trigger_one_shot(&self) -> Result<(), Error<E>> {
    //     let bytes = [Register::CTRL_REG2, ]
    //     match self.interface.write(self.address(), bytes)
    // }
}
