//! Driver for the LPS22HH pressure sensor using I2C

#![forbid(unsafe_code)]

#[cfg(feature = "steigen31")]
pub mod lps22hd;
#[cfg(feature = "steigen33")]
pub mod lps22hh;
mod lps22xx_conf;

pub mod lps22hd_conf;
mod lps22hd_register;

pub mod lps22hh_conf;
mod lps22hh_register;

use super::PressTemp;
use core::convert::From;
use core::fmt::Debug;
use core::marker::Copy;
use core::option::Option::{self, None, Some};

#[derive(Clone, Copy)]
pub enum SA0 {
    #[allow(dead_code)]
    High = 1,
    #[allow(dead_code)]
    Low = 0,
}

// Type states for the AquireMode in LPS22HH
#[derive(Clone, Copy)]
pub struct SingleShot;
#[derive(Clone, Copy)]
pub struct Continuous;

#[derive(Debug, Clone, Copy)]
pub enum ErrorKind {
    WrongDeviceId,
    I2CError,
    NotStarted, // Dummy
}

#[derive(Debug, Clone, Copy)]
pub struct Error<E> {
    #[allow(dead_code)]
    pub kind: ErrorKind,
    #[allow(dead_code)]
    pub source: Option<E>,
}

impl<E> Error<E> {
    pub const fn new() -> Self {
        Self {
            kind: ErrorKind::NotStarted,
            source: None,
        }
    }
}

impl<E> From<ErrorKind> for Error<E>
where
    E: Debug,
{
    fn from(kind: ErrorKind) -> Error<E> {
        let source = None;
        Error { source, kind }
    }
}

impl ErrorKind {
    fn with<E>(self, source: E) -> Error<E> {
        let s = Some(source);
        Error {
            source: s,
            kind: self,
        }
    }
}

#[cfg(feature = "defmt")]
impl<E: core::fmt::Debug> defmt::Format for Error<E> {
    fn format(&self, f: defmt::Formatter) {
        defmt::write!(f, "Error {{ kind: ");
        match self.kind {
            ErrorKind::WrongDeviceId => defmt::write!(f, "WrongDeviceId"),
            ErrorKind::I2CError => defmt::write!(f, "I2CError"),
            ErrorKind::NotStarted => defmt::write!(f, "NotStarted"),
        }
        if let Some(source) = &self.source {
            defmt::write!(f, ", source: ");
            defmt::write!(f, "{:?}", defmt::Debug2Format(source));
        }
        defmt::write!(f, " }}");
    }
}

/// Calculating the temperature from the register values
/// The result is in `°C`
fn calculate_temperature(temp_out_h: u8, temp_out_l: u8) -> f32 {
    let temperature = ((temp_out_h as u16) << 8 | temp_out_l as u16) as i16;
    (temperature as f32) / 100.0
}

/// Calculating the pressure in pa from the register values
/// The result is in `Pa`
fn calculate_pressure(press_out_h: u8, press_out_l: u8, press_out_xl: u8) -> f32 {
    let pressure =
        ((press_out_h as u32) << 16 | (press_out_l as u32) << 8 | press_out_xl as u32) as i32;
    (pressure as f32) / 4096.0 * 100.0
}

#[cfg(all(feature = "std", test))]
mod test {
    #[test]
    fn calculate_pressure() {
        assert_eq!(
            super::calculate_pressure(0x3F, 0xF5, 0x8D),
            4191629.0 / 4096.0 * 100.0
        );
    }

    #[test]
    fn calculate_temperature() {
        assert_eq!(super::calculate_temperature(0x09, 0xC4), 25.0);
        // 2 complement calculated with https://www.exploringbinary.com/twos-complement-converter/
        assert_eq!(super::calculate_temperature(0b11111110, 0b00001100), -5.0);
    }
}
