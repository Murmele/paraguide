//! LPS22HH register addresses

#![allow(non_camel_case_types, clippy::unreadable_literal)]
#![allow(dead_code)]

use bitflags::bitflags;

pub const BASE_ADDRESS: u8 = 0b1011100; // SA0 connected to GND
pub const WHO_AM_I: u8 = 0b10110011;

#[repr(u8)]
pub enum Register {
    // Reserved =0x00 – 0x0A
    INTERRUPT_CFG = 0x0B,
    THS_P_L = 0x0C,
    THS_P_H = 0x0D,
    IF_CTRL = 0x0E,
    WHO_AM_I = 0x0F,
    CTRL_REG1 = 0x10,
    CTRL_REG2 = 0x11,
    CTRL_REG3 = 0x12,
    FIFO_CTRL = 0x13,
    FIFO_WTM = 0x14,
    REF_P_L = 0x15,
    REF_P_H = 0x16,
    // Reserved = 0x17
    RPDS_L = 0x18,
    RPDS_H = 0x19,
    // Reserved =0x1A – 0x23
    INT_SOURCE = 0x24,
    FIFO_STATUS1 = 0x25,
    FIFO_STATUS2 = 0x26,
    STATUS = 0x27,
    PRESS_OUT_XL = 0x28,
    PRESS_OUT_L = 0x29,
    PRESS_OUT_H = 0x2A,
    TEMP_OUT_L = 0x2B,
    TEMP_OUT_H = 0x2C,
    // Reserved =0x2D – 0x77
    FIFO_DATA_OUT_PRESS_XL = 0x78,
    FIFO_DATA_OUT_PRESS_L = 0x79,
    FIFO_DATA_OUT_PRESS_H = 0x7A,
}

impl Register {
    pub fn addr(self) -> u8 {
        self as u8
    }

    pub fn read_only(self) -> bool {
        match self {
            Self::WHO_AM_I
            | Self::REF_P_L
            | Self::REF_P_H
            | Self::INT_SOURCE
            | Self::FIFO_STATUS1
            | Self::FIFO_STATUS2
            | Self::STATUS
            | Self::PRESS_OUT_XL
            | Self::PRESS_OUT_L
            | Self::PRESS_OUT_H
            | Self::TEMP_OUT_L
            | Self::TEMP_OUT_H
            | Self::FIFO_DATA_OUT_PRESS_XL
            | Self::FIFO_DATA_OUT_PRESS_L
            | Self::FIFO_DATA_OUT_PRESS_H => true,
            _ => false,
        }
    }
}

bitflags! {
    pub struct CTRL_REG1: u8 {
        const ODR2 =     0b01000000;
        const ODR1 =     0b00100000;
        const ODR0 =     0b00010000;
        const EN_LPFP =  0b00001000;
        const LPFP_CFG = 0b00000100;
        const BDU =      0b00000010;
        const SIM =      0b00000001;
    }
}

bitflags! {
    pub struct CTRL_REG2: u8 {
        const BOOT=          0b10000000;
        const INT_H_L=       0b01000000;
        const PP_OD =        0b00100000;
        const IF_ADD_INC=    0b00010000;
        const SWRESET=       0b00000100;
        const LOW_NOISE_EN = 0b00000010;
        const ONE_SHOT=      0b00000001;
    }
}

bitflags! {
    pub  struct CTRL_REG3: u8 {
        const INT_F_FULL   =   0b00100000;
        const INT_F_WTM    =   0b00010000;
        const INT_F_OVR    =   0b00001000;
        const DRDY         =   0b00000100;
        const INT_S_HIGH =     0b00000010;
        const INT_S_LOW =      0b00000001;
    }
}

bitflags! {
    pub struct FIFO_CTRL: u8 {
        const STOP_ON_WTM = 0b00001000;
        const TRIG_MODES =  0b00000100;
        const F_MODE1 =     0b00000010;
        const F_MODE0 =     0b00000001;
    }
}

bitflags! {
    pub struct IF_CTRL: u8 {
        const INT_EN_I3C = 0b10000000;
        const SDA_PU_EN =  0b00010000;
        const SDO_PU_EN =  0b00001000;
        const PD_DIS_INT1 =0b00000100;
        const I3C_DISABLE= 0b00000010;
        const I2C_DISABLE= 0b00000001;
    }
}
