//! Implementation of the LPS22HH
#![allow(dead_code)]
pub use super::lps22hh_conf as conf;
use super::lps22hh_register::{self as register, Register};
use super::lps22xx_conf::*;
use super::*;
use core::fmt::Debug;
use core::result::Result::{self, Err, Ok};
use embedded_hal_async as hal;
use hal::i2c::I2c;

#[derive(Clone, Copy)]
pub struct RawLPS22HH<AquireMode> {
    sa0: super::SA0,
    one_shot: AquireMode,
}

impl<AquireMode> RawLPS22HH<AquireMode>
where
    AquireMode: Copy + Clone,
{
    pub fn to_lps22hh<'a, Interface>(
        self,
        interface: &'a mut Interface,
    ) -> LPS22HH<'a, Interface, AquireMode> {
        LPS22HH {
            interface,
            raw: self,
        }
    }
}

pub struct LPS22HH<'a, Interface, AquireMode> {
    interface: &'a mut Interface,
    raw: RawLPS22HH<AquireMode>,
}

/// Implementation indipendend of the aquire type
impl<'a, Interface, AquireMode, E> LPS22HH<'a, Interface, AquireMode>
where
    Interface: I2c<Error = E>,
    AquireMode: Copy + Clone,
    E: Debug,
{
    fn address(&self) -> u8 {
        return register::BASE_ADDRESS + self.raw.sa0 as u8;
    }

    pub async fn device_detected(&mut self) -> Result<(), Error<E>> {
        let device_id = self.get_device_id().await?;
        if device_id != register::WHO_AM_I {
            return Err(ErrorKind::WrongDeviceId.into());
        } else {
            return Ok(());
        }
    }

    pub fn to_raw(self) -> RawLPS22HH<AquireMode> {
        let r = self.raw.clone();
        core::mem::forget(self);
        r
    }

    async fn get_device_id(&mut self) -> Result<u8, Error<E>> {
        let mut output = [0u8];

        self.read(Register::WHO_AM_I, &mut output).await?;
        return Ok(output[0]);
    }

    // TODO: Seems that the compiler does not optimize this out, so
    // returning a struct instead of filling reference values might be inefficient
    // Or will it be optimized out?
    pub async fn get_status(&mut self) -> Result<Status, Error<E>> {
        let mut output = [0u8];
        self.read(Register::STATUS, &mut output).await?;
        return Ok(Status::new(output[0]));
    }

    pub async fn get_pressure_temperature(&mut self) -> Result<PressTemp, Error<E>> {
        let mut output = [0u8, 0u8, 0u8, 0u8, 0u8];
        // PRESS_OUT_XL is the lowest address
        self.read(Register::PRESS_OUT_XL, &mut output).await?;

        Ok(PressTemp {
            pressure: calculate_pressure(output[2], output[1], output[0]),
            temperature: calculate_temperature(output[4], output[3]),
        })
    }

    pub async fn set_ctrl_cfg3(
        &mut self,
        interrupt_config: conf::InterruptConfiguration,
        interrupt_drdy: conf::InterruptDRDY,
        interrupt_fifo_ovr: conf::InterruptFIFOOVR,
        interrupt_fifo_wtm: conf::InterruptFIFOWTM,
        interrupt_fifo_full: conf::InterruptFIFOFULL,
    ) -> Result<(), Error<E>> {
        let bytes = [
            Register::CTRL_REG3.addr(),
            conf::ctrl_cfg3_value(
                interrupt_config,
                interrupt_drdy,
                interrupt_fifo_ovr,
                interrupt_fifo_wtm,
                interrupt_fifo_full,
            )
            .bits(),
        ];
        self.write(&bytes).await
    }

    async fn read<const N: usize>(
        &mut self,
        register: register::Register,
        read: &mut [u8; N],
    ) -> Result<(), Error<E>> {
        self.interface
            .write_read(self.address(), &[register.addr()], read)
            .await
            .map_err(|e| ErrorKind::I2CError.with(e))
    }

    async fn write<const N: usize>(&mut self, data: &[u8; N]) -> Result<(), Error<E>> {
        self.interface
            .write(self.address(), data)
            .await
            .map_err(|e| ErrorKind::I2CError.with(e))
    }
}

impl<'a, Interface, E> LPS22HH<'a, Interface, Continuous>
where
    Interface: I2c<Error = E>,
    E: Debug,
{
    /// Create a new lps22hh device in continuous mode
    pub async fn new_continuous(
        interface: &'a mut Interface,
        sa0: SA0,
        data_rate: conf::OutputDataRate,
        lowpass_filter: conf::LowPassFilter,
        block_data_update: conf::BlockDataUpdate,
    ) -> Result<RawLPS22HH<Continuous>, Error<E>> {
        let mut lps22hh = LPS22HH {
            interface,
            raw: RawLPS22HH {
                sa0,
                one_shot: Continuous,
            },
        };

        lps22hh.device_detected().await?;

        // TODO: do this in a single i2c sequence not in 3
        lps22hh
            .set_ctrl_cfg1(data_rate, lowpass_filter, block_data_update)
            .await?;
        lps22hh
            .set_ctrl_cfg2(
                conf::MODE::LowNoiseMode,
                conf::SWRESET::Normal,
                conf::AutoIncrement::Enable,
                conf::InterruptPadMode::OpenDrain,
                conf::InterruptActiveLevel::High,
                conf::BootMode::NormalMode,
            )
            .await?;
        lps22hh
            .set_ctrl_cfg3(
                conf::InterruptConfiguration::DataSignal,
                conf::InterruptDRDY::Enable,
                conf::InterruptFIFOOVR::Disable,
                conf::InterruptFIFOWTM::Disable,
                conf::InterruptFIFOFULL::Disable,
            )
            .await?;

        Ok(lps22hh.raw)
    }

    pub async fn set_ctrl_cfg1(
        &mut self,
        data_rate: conf::OutputDataRate,
        lowpass_filter: conf::LowPassFilter,
        block_data_update: conf::BlockDataUpdate,
    ) -> Result<(), Error<E>> {
        // For I2C SerialInterfaceMode is irrelevant, so use the default
        let bytes = [
            Register::CTRL_REG1.addr(),
            {
                let spi_interface_mode = conf::SerialInterfaceMode::default();
                let value = data_rate.bits()
                    | lowpass_filter.bits()
                    | block_data_update.bits()
                    | spi_interface_mode.bits();
                value
            }
            .bits(),
        ];
        self.write(&bytes).await
    }

    pub async fn set_ctrl_cfg2(
        &mut self,
        low_noise: conf::MODE,
        sw_reset: conf::SWRESET,
        auto_increment: conf::AutoIncrement,
        interrupt_pad_mode: conf::InterruptPadMode,
        interrupt_active_mode: conf::InterruptActiveLevel,
        boot: conf::BootMode,
    ) -> Result<(), Error<E>> {
        // One shot must be off for continuous
        let bytes = [
            Register::CTRL_REG2.addr(),
            conf::ctrl_cfg2_value(
                conf::OneShot::default(),
                low_noise,
                sw_reset,
                auto_increment,
                interrupt_pad_mode,
                interrupt_active_mode,
                boot,
            )
            .bits(),
        ];
        self.write(&bytes).await
    }

    /// switch from continuous mode to single shot mode
    pub async fn into_single_shot(
        mut self,
        lp: conf::LowPassFilter,
        block_data_update: conf::BlockDataUpdate,
        spi_interface_mode: conf::SerialInterfaceMode,
    ) -> Result<LPS22HH<'a, Interface, SingleShot>, Error<E>> {
        // Set odr to 0x00
        let addr_data = [
            Register::CTRL_REG1.addr(),
            (lp.bits() | block_data_update.bits() | spi_interface_mode.bits()).bits(),
        ];
        self.write(&addr_data).await?;
        let sa0 = self.raw.sa0;
        Ok(LPS22HH {
            interface: self.interface,
            raw: RawLPS22HH {
                sa0,
                one_shot: SingleShot,
            },
        })
    }
}

// By default the aquire mode is SingleShot (reset config of the chip)
impl<'a, Interface, E> LPS22HH<'a, Interface, SingleShot>
where
    Interface: I2c<Error = E>,
    E: Debug,
{
    /// Create a new lps22hh device in single shot mode
    #[allow(dead_code)]
    pub async fn new_single_shot(interface: &'a mut Interface, sa0: SA0) -> Result<Self, Error<E>> {
        let mut lps22hh = LPS22HH {
            interface,
            raw: RawLPS22HH {
                sa0,
                one_shot: SingleShot,
            },
        };

        lps22hh.device_detected().await?;
        Ok(lps22hh)
    }
}

/// Functions which can be used only when the device is in single shot mode
impl<'a, Interface, E> LPS22HH<'a, Interface, SingleShot>
where
    Interface: I2c<Error = E>,
    E: Debug,
{
    // switch from single shot mode to continuous mode
    #[allow(dead_code)]
    pub async fn into_continuous(
        mut self,
        data_rate: conf::OutputDataRate,
        lp: conf::LowPassFilter,
        block_data_update: conf::BlockDataUpdate,
        spi_interface_mode: conf::SerialInterfaceMode,
    ) -> Result<LPS22HH<'a, Interface, Continuous>, Error<E>> {
        // Required to set ONE_SHOT to zero?
        let bytes = [
            Register::CTRL_REG1.addr(),
            (data_rate.bits() | lp.bits() | block_data_update.bits() | spi_interface_mode.bits())
                .bits(),
        ];
        self.write(&bytes).await?;
        Ok(LPS22HH {
            interface: self.interface,
            raw: RawLPS22HH {
                sa0: self.raw.sa0,
                one_shot: Continuous,
            },
        })
    }
    // pub fn trigger_one_shot(&self) -> Result<(), Error<E>> {
    //     let bytes = [Register::CTRL_REG2, ]
    //     match self.interface.write(self.address(), bytes)
    // }
}
