//! Asynchronous driver for the mmc5603nj magnetometer

mod mmc5603nj_conf;
use mmc5603nj_conf as conf;
mod mmc5603nj_register;
use core::fmt::Debug;
use embedded_hal_async as hal;
use hal::delay::DelayNs;
use hal::i2c::I2c;
use mmc5603nj_register::{self as register, Control0, Control1, Control2, Register};

pub struct Value {
    pub x: f32,
    pub y: f32,
    pub z: f32,
}

#[derive(Copy, Clone)]
pub struct RawMMC5603NJ<DELAYER> {
    delay: DELAYER,
}

impl<DELAYER> RawMMC5603NJ<DELAYER> {
    pub fn to_mmc5603nj<'a, Interface>(
        self,
        interface: &'a mut Interface,
    ) -> MMC5603NJ<'a, Interface, DELAYER> {
        MMC5603NJ {
            interface,
            raw: self,
        }
    }
}

pub struct MMC5603NJ<'a, Interface, DELAYER> {
    interface: &'a mut Interface,
    raw: RawMMC5603NJ<DELAYER>,
}

impl<'a, E: Debug, Interface: I2c<Error = E>, DELAYER: DelayNs + Clone>
    MMC5603NJ<'a, Interface, DELAYER>
{
    pub async fn new_continuous(
        sample_freq: usize,
        interface: &'a mut Interface,
        delay: DELAYER,
    ) -> Result<RawMMC5603NJ<DELAYER>, Error<E>> {
        let mut s = Self {
            interface,
            raw: RawMMC5603NJ { delay },
        };

        s.configure_continuous(sample_freq).await?;
        Ok(s.raw)
    }

    pub async fn configure_continuous(&mut self, sample_freq: usize) -> Result<(), Error<E>> {
        self.device_detected().await?;

        self.magnet_set_reset().await?;

        let mut control1 = Control1::empty();
        let mut control2 = Control2::empty();
        let mut odr: u8 = sample_freq as u8;
        if sample_freq <= 75 {
            // Nothing to do
        } else if sample_freq <= 150 {
            control1 |= Control1::BW0;
        } else if sample_freq <= 255 {
            control1 |= Control1::BW1;
        } else if sample_freq == 1000 {
            odr = 255;
            control1 |= Control1::BW0 | Control1::BW1;
            control2 |= Control2::hpower;
        } else {
            return Err(ErrorKind::InvalidFrequency.into());
        }

        control2 |= Control2::Cmm_en;

        self.write_register(Register::ODR, odr).await?;

        let control0 = Control0::Cmm_freq_en; // Start calculation
        self.write_register(Register::InternalControl0, control0.bits())
            .await?;
        self.write_register(Register::InternalControl1, control1.bits())
            .await?;
        self.write_register(Register::InternalControl2, control2.bits())
            .await
    }

    pub async fn magnet_set_reset(&mut self) -> Result<(), Error<E>> {
        // Write 'Set' command
        self.write_register(Register::InternalControl0, Control0::Do_Set.bits())
            .await?;

        self.raw.delay.delay_ms(1).await;

        // Write 'Reset' command
        self.write_register(Register::InternalControl0, Control0::Do_Reset.bits())
            .await?;

        self.raw.delay.delay_ms(1).await;

        Ok(())
    }

    pub fn to_raw(self) -> RawMMC5603NJ<DELAYER> {
        let r = self.raw.clone();
        core::mem::forget(self);
        r
    }

    async fn device_detected(&mut self) -> Result<(), Error<E>> {
        let v = self.get_device_id().await?;
        if v != register::WHO_AM_I {
            return Err(ErrorKind::WrongDeviceId.into());
        } else {
            return Ok(());
        }
    }

    async fn get_device_id(&mut self) -> Result<u8, Error<E>> {
        let mut output = [0u8];

        self.read_register(Register::ProductID, &mut output).await?;
        return Ok(output[0]);
    }

    pub async fn get_magnet_values(&mut self, low_res: bool) -> Result<Value, Error<E>> {
        // For higher speed read only the first 6 bytes -> less resolution because lsbs are missing
        if !low_res {
            let mut output = [0u8; 9];
            self.read_register(Register::Xout0, &mut output).await?;
            Self::calculate_magnet_values(&output)
        } else {
            let mut output = [0u8; 6];
            self.read_register(Register::Xout0, &mut output).await?;
            Self::calculate_magnet_values(&output)
        }
    }

    /// Result is in mG
    fn calculate_magnet_values(a: &[u8]) -> Result<Value, Error<E>> {
        if a.len() < 6 {
            return Err(ErrorKind::InvalidCalculation.into());
        }

        let mut x = (a[0] as u32) << 12 | (a[1] as u32) << 4;
        let mut y = (a[2] as u32) << 12 | (a[3] as u32) << 4;
        let mut z = (a[4] as u32) << 12 | (a[5] as u32) << 4;

        if a.len() == 9 {
            x |= a[6] as u32 >> 4;
            y |= a[7] as u32 >> 4;
            z |= a[8] as u32 >> 4;
        }

        let x = (x as i32) - conf::OFFSET_20;
        let y = (y as i32) - conf::OFFSET_20;
        let z = (z as i32) - conf::OFFSET_20;

        let x = x as f32 * conf::RESOLUTION_20;
        let y = y as f32 * conf::RESOLUTION_20;
        let z = z as f32 * conf::RESOLUTION_20;

        Ok(Value { x, y, z })
    }

    fn calculate_temperature(t: u8) -> f32 {
        t as f32 * conf::RESOLUTION_TEMP - 75.
    }

    pub async fn get_status(&mut self) -> Result<u8, Error<E>> {
        let mut output = [0u8];
        self.read_register(Register::Status1, &mut output).await?;
        Ok(output[0])
    }

    pub async fn get_temperature(&mut self) -> Result<f32, Error<E>> {
        let mut output = [0u8];
        self.read_register(Register::Tout, &mut output).await?;
        Ok(Self::calculate_temperature(output[0]))
    }

    async fn write_register(&mut self, register: Register, data: u8) -> Result<(), Error<E>> {
        self.interface
            .write(Self::address(), &[register.addr(), data])
            .await
            .map_err(|e| ErrorKind::I2CError.with(e))
    }

    async fn read_register(
        &mut self,
        register: Register,
        data_out: &mut [u8],
    ) -> Result<(), Error<E>> {
        self.interface
            .write_read(Self::address(), &[register.addr()], data_out)
            .await
            .map_err(|e| ErrorKind::I2CError.with(e))
    }

    fn address() -> u8 {
        return register::BASE_ADDRESS;
    }
}

pub enum ErrorKind {
    WrongDeviceId,
    I2CError,
    InvalidFrequency,
    InvalidCalculation,
    NotInitialized,
    NotStarted,
}

pub struct Error<E> {
    pub kind: ErrorKind,
    pub source: Option<E>,
}

impl<E> Error<E> {
    pub const fn new() -> Self {
        Self {
            kind: ErrorKind::NotStarted,
            source: None,
        }
    }
}

impl<E: Debug> From<ErrorKind> for Error<E> {
    fn from(kind: ErrorKind) -> Error<E> {
        Error { source: None, kind }
    }
}

impl ErrorKind {
    fn with<E>(self, source: E) -> Error<E> {
        Error {
            kind: self,
            source: Some(source),
        }
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_calculate_magnet_values() {
        let values: [u8; 9] = [
            0x80, 0x00, // x
            0x80, 0x00, // y
            0x80, 0x00, // z
            0x00, 0x00, 0x00, // x,y,z lsb
        ];
        assert_eq!(
            calculate_magnet_values(&values),
            Ok(Value {
                x: 0.0,
                y: 0.0,
                z: 0.0
            })
        );
    }
}
