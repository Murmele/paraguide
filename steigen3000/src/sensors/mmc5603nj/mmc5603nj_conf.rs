/// Resolution for 20Bit value
pub const RESOLUTION_20: f32 = 0.0625; // [mG/lsb]
/// Offset for 20bit
pub const OFFSET_20: i32 = 524288;
pub const RESOLUTION_TEMP: f32 = 0.8; // [°C/lsb]
