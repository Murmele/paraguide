#![allow(non_camel_case_types, clippy::unreadable_literal)]
#[allow(dead_code)]
use bitflags::bitflags;

pub const BASE_ADDRESS: u8 = 0b0110000;
pub const WHO_AM_I: u8 = 0b00010000;

#[allow(dead_code)]
#[repr(u8)]
pub(crate) enum Register {
    Xout0 = 0x00,            // Xout[19:12]
    Xout1 = 0x01,            // Xout[11:4]
    Yout0 = 0x02,            // Yout[19:12]
    Yout1 = 0x03,            // Yout[11:4]
    Zout0 = 0x04,            // Zout[19:12]
    Zout1 = 0x05,            // Zout[11:4]
    Xout2 = 0x06,            // Xout[3:0]
    Yout2 = 0x07,            // Yout[3:0]
    Zout2 = 0x08,            // Zout[3:0]
    Tout = 0x09,             // Temperature output
    Status1 = 0x18,          // Device status1
    ODR = 0x1A,              // Output data rate
    InternalControl0 = 0x1B, // Control register 0
    InternalControl1 = 0x1C, // Control register 1
    InternalControl2 = 0x1D, // Control register 2
    ST_X_TH = 0x1E,          // X-axis selftest threshold
    ST_Y_TH = 0x1F,          // Y-axis selftest threshold
    ST_Z_TH = 0x20,          // Z-axis selftest threshold
    ST_X = 0x27,             // X-axis selftest set value
    ST_Y = 0x28,             // Y-axis selftest set value
    ST_Z = 0x29,             // Z-axis selftest set value
    ProductID = 0x39,        // Product ID
}

impl Register {
    pub fn addr(self) -> u8 {
        self as u8
    }
}

bitflags! {
    pub (crate) struct Status: u8 {
        const Meas_t_done = 0b10000000;
        const Meas_m_done = 0b01000000;
        const Sat_sensor =  0b00100000;
        const OTP_read_done = 0b00010000;
        const ST_FAIL =     0b00001000;
        const Mdt_flag_int =0b00000100;
        const Meas_t_done_int = 0b00000010;
        const Meas_m_done_int = 0b00000001;
    }
}

// bitflags! {
//     pub (crate) struct Odr: u8 {

//     }
// }

bitflags! {
    pub (crate) struct Control0: u8 {
        const Cmm_freq_en = 0b10000000;
        const Auto_st_en =  0b01000000;
        const Auto_SR_en =  0b00100000;
        const Do_Reset   =  0b00010000;
        const Do_Set     =  0b00001000;
        const Start_MDT  =  0b00000100;
        const Take_meas_T = 0b00000010;
        const Take_meas_M = 0b00000001;
    }
}

bitflags! {
    pub (crate) struct Control1: u8 {
        const Sw_reset    = 0b10000000;
        const St_enm      = 0b01000000;
        const St_enp      = 0b00100000;
        const Z_inhibit   = 0b00010000;
        const Y_inhibit   = 0b00001000;
        const X_inhibit   = 0b00000100;
        const BW1         = 0b00000010;
        const BW0         = 0b00000001;
    }
}

bitflags! {
    pub (crate) struct Control2: u8 {
        const hpower      = 0b10000000;
        const INT_meas_done_en = 0b01000000;
        const INT_mdt_en  = 0b00100000;
        const Cmm_en      = 0b00010000;
        const En_prd_set  = 0b00001000;
        const Prd_set2    = 0b00000100;
        const Prd_set1    = 0b00000010;
        const Prd_set0    = 0b00000001;
    }
}
