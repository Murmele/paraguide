// https://github.com/rust-embedded/awesome-embedded-rust#driver-crates

#[cfg(not(feature = "std"))]
pub mod lps22xx;
#[cfg(not(feature = "std"))]
pub mod lsm6dsl;
#[cfg(not(feature = "std"))]
pub mod mmc5603nj;

#[derive(Clone, Copy)]
pub struct AccelGyro {
    pub accel_x: f32,
    pub accel_y: f32,
    pub accel_z: f32,
    pub gyro_x: f32,
    pub gyro_y: f32,
    pub gyro_z: f32,
}

#[derive(Clone, Copy)]
pub struct PressTemp {
    pub pressure: f32,
    pub temperature: f32,
}
