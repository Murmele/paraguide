//! LSM6DSL register addresses
#![allow(non_camel_case_types, clippy::unreadable_literal, unused)]
use bitflags::bitflags;

pub(crate) const BASE_ADDRESS: u8 = 0b1101010; // SDO/SA0 pin low. SDO/SA0 pin high -> LSB = 1
pub(crate) const WHO_AM_I_ID: u8 = 0b01101010;

#[repr(u8)]
pub enum Register {
    // RESERVED = 0x0,                  // Reserved
    FUNC_CFG_ACCESS = 0x1, // Embedded functions configuration register
    // RESERVED = 0x2,               // Reserved
    // RESERVED = 0x3,               // Reserved
    SENSOR_SYNC_TIME_FRAME = 0x4, // Sensor sync
    SENSOR_SYNC_RES_RATIO = 0x5,  // configuration register
    FIFO_CTRL1 = 0x6,             //
    FIFO_CTRL2 = 0x7,             //
    FIFO_CTRL3 = 0x8,             // FIFO configuration registers
    FIFO_CTRL4 = 0x9,             //
    FIFO_CTRL5 = 0x0A,            //
    DRDY_PULSE_CFG_G = 0x0B,      //
    // RESERVED = 0x0C,              // Reserved
    INT1_CTRL = 0x0D,     // INT1 pin control
    INT2_CTRL = 0x0E,     // INT2 pin control
    WHO_AM_I = 0x0F,      // Who I am ID
    CTRL1_XL = 0x10,      //
    CTRL2_G = 0x11,       //
    CTRL3_C = 0x12,       //
    CTRL4_C = 0x13,       //
    CTRL5_C = 0x14,       // Accelerometer and gyroscope control
    CTRL6_C = 0x15,       // registers
    CTRL7_G = 0x16,       //
    CTRL8_XL = 0x17,      //
    CTRL9_XL = 0x18,      //
    CTRL10_C = 0x19,      //
    MASTER_CONFIG = 0x1A, // I2C master configuration register
    WAKE_UP_SRC = 0x1B,   //
    TAP_SRC = 0x1C,       // Interrupt registers
    D6D_SRC = 0x1D,       //
    STATUS_REG = 0x1E,    // Status data register for user interface
    // RESERVED = 0x1F,   //
    OUT_TEMP_L = 0x20,      // Temperature output
    OUT_TEMP_H = 0x21,      // data registers
    OUTX_L_G = 0x22,        //
    OUTX_H_G = 0x23,        //
    OUTY_L_G = 0x24,        // Gyroscope output
    OUTY_H_G = 0x25,        // registers for user interface
    OUTZ_L_G = 0x26,        //
    OUTZ_H_G = 0x27,        //
    OUTX_L_XL = 0x28,       //
    OUTX_H_XL = 0x29,       //
    OUTY_L_XL = 0x2A,       // Accelerometer output
    OUTY_H_XL = 0x2B,       // registers
    OUTZ_L_XL = 0x2C,       //
    OUTZ_H_XL = 0x2D,       //
    SENSORHUB1_REG = 0x2E,  //
    SENSORHUB2_REG = 0x2F,  //
    SENSORHUB3_REG = 0x30,  //
    SENSORHUB4_REG = 0x31,  //
    SENSORHUB5_REG = 0x32,  //
    SENSORHUB6_REG = 0x33,  // Sensor hub output
    SENSORHUB7_REG = 0x34,  // registers
    SENSORHUB8_REG = 0x35,  //
    SENSORHUB9_REG = 0x36,  //
    SENSORHUB10_REG = 0x37, //
    SENSORHUB11_REG = 0x38, //
    SENSORHUB12_REG = 0x39, //
    FIFO_STATUS1 = 0x3A,    //
    FIFO_STATUS2 = 0x3B,    //
    FIFO_STATUS3 = 0x3C,    // FIFO status registers
    FIFO_STATUS4 = 0x3D,    //
    FIFO_DATA_OUT_L = 0x3E, // FIFO data output
    FIFO_DATA_OUT_H = 0x3F, // registers
    TIMESTAMP0_REG = 0x40,  //
    TIMESTAMP1_REG = 0x41,  // Timestamp output registers
    TIMESTAMP2_REG = 0x42,  //
    // RESERVED = 0x43 - 48,            // Reserved
    STEP_TIMESTAMP_L = 0x49, // Step counter
    STEP_TIMESTAMP_H = 0x4A, // timestamp registers
    STEP_COUNTER_L = 0x4B,   // Step counter output
    STEP_COUNTER_H = 0x4C,   // registers
    SENSORHUB13_REG = 0x4D,  //
    SENSORHUB14_REG = 0x4E,  //
    SENSORHUB15_REG = 0x4F,  // Sensor hub output
    SENSORHUB16_REG = 0x50,  // registers
    SENSORHUB17_REG = 0x51,  //
    SENSORHUB18_REG = 0x52,  //
    FUNC_SRC1 = 0x53,        // Interrupt registers
    FUNC_SRC2 = 0x54,        //
    WRIST_TILT_IA = 0x55,    // Interrupt register
    // RESERVED = 0x56 - 57,            // Reserved
    TAP_CFG = 0x58,                  //
    TAP_THS_6D = 0x59,               //
    INT_DUR2 = 0x5A,                 //
    WAKE_UP_THS = 0x5B,              //
    WAKE_UP_DUR = 0x5C,              // Interrupt registers
    FREE_FALL = 0x5D,                //
    MD1_CFG = 0x5E,                  //
    MD2_CFG = 0x5F,                  //
    MASTER_CMD_CODE = 0x60,          //
    SENS_SYNC_SPI_ERROR_CODE = 0x61, //
    // RESERVED = 0x62 - 65,            // Reserved
    OUT_MAG_RAW_X_L = 0x66, //
    OUT_MAG_RAW_X_H = 0x67, //
    OUT_MAG_RAW_Y_L = 0x68, // External magnetometer raw data output registers
    OUT_MAG_RAW_Y_H = 0x69, // External magnetometer raw data output registers
    OUT_MAG_RAW_Z_L = 0x6A, //
    OUT_MAG_RAW_Z_H = 0x6B, //
    // RESERVED  = 0x6C-72, // Reserved
    X_OFS_USR = 0x73, //
    Y_OFS_USR = 0x74, // Accelerometer user offset correction
    Z_OFS_USR = 0x75, //
                      //RESERVED  = 0x76-7F , // Reserved
}

impl Register {
    pub fn addr(self) -> u8 {
        self as u8
    }
}

bitflags! {
    pub struct CTRL1_XL: u8 {
        const ODR_XL3 =     0b10000000;
        const ODR_XL2 =     0b01000000;
        const ODR_XL1 =     0b00100000;
        const ODR_XL0 =     0b00010000;
        const FS_XL1 =      0b00001000;
        const FS_XL0 =      0b00000100;
        const LPF1_BW_SEL = 0b00000010;
        const BW0_XL =      0b00000001;
    }
}

bitflags! {
    pub struct CTRL2_G: u8 {
        const ODR_G3 =  0b10000000;
        const ODR_G2 =  0b01000000;
        const ODR_G1 =  0b00100000;
        const ODR_G0 =  0b00010000;
        const FS_G1 =   0b00001000;
        const FS_G0 =   0b00000100;
        const FS_125 =  0b00000010;
        // const 0 =    0b00000001;
    }
}

bitflags! {
    pub struct CTRL3_C: u8 {
        const BOOT =        0b10000000;
        const BDU =         0b01000000;
        const H_LACTIVE =   0b00100000;
        const PP_OD =       0b00010000;
        const SIM =         0b00001000;
        const IF_INC =      0b00000100;
        const BLE =         0b00000010;
        const SW_RESET =    0b00000001;
    }
}

bitflags! {
    pub struct INT1_CTRL: u8 {
        const INT1_STEP_DETECTOR =  0b10000000;
        const INT1_SIGN_MOT =       0b01000000;
        const INT1_FULL_FLAG =      0b00100000;
        const INT1_FIFO_OVR =       0b00010000;
        const INT1_FTH =            0b00001000;
        const INT1_BOOT =           0b00000100;
        const INT1_DRDY_G =         0b00000010;
        const INT1_DRDY_XL =        0b00000001;
    }
}

bitflags! {
    pub struct INT2_CTRL: u8 {
        const INT2_STEP_DELTA =  0b10000000;
        const INT2_STEP_COUNT_OV =       0b01000000;
        const INT2_FULL_FLAG =      0b00100000;
        const INT1_FIFO_OVR =       0b00010000;
        const INT1_FTH =            0b00001000;
        const INT2_DRDY_TEMP =           0b00000100;
        const INT1_DRDY_G =         0b00000010;
        const INT1_DRDY_XL =        0b00000001;
    }
}
