#![allow(unused)]
use super::register::*;

pub fn configure_accelerometer(
    freq: AccelerometerFrequency,
    scale: AccelerometerScale,
    bandwidth: AccelerometerBandwidth,
) -> CTRL1_XL {
    <AccelerometerFrequency as Into<CTRL1_XL>>::into(freq) | scale.into() | bandwidth.into()
}

pub fn accelerometer_sensitivity(scale: AccelerometerScale) -> f32 {
    let s = match scale {
        AccelerometerScale::PM2G => 0.061,
        AccelerometerScale::PM4G => 0.122,
        AccelerometerScale::PM8G => 0.244,
        AccelerometerScale::PM16G => 0.488,
    } / 1000.0;
    s
}

pub fn gyroscope_sensitivity(scale: GyroscopeScale) -> f32 {
    let s = match scale {
        GyroscopeScale::F125DPS => 4.375,
        GyroscopeScale::F250DPS => 8.75,
        GyroscopeScale::F500DPS => 17.50,
        GyroscopeScale::F1000DPS => 35.0,
        GyroscopeScale::F2000DPS => 70.0,
    } / 1000.0;
    s
}

pub fn configure_gyroscope(freq: GyroscopeFrequency, scale: GyroscopeScale) -> CTRL2_G {
    <GyroscopeFrequency as Into<CTRL2_G>>::into(freq) | scale.into()
}

pub fn configure_ctrl3(
    boot: Boot,
    data_update: DataUpdate,
    interrupt_pad: InteruptPad,
    interrupt_pin_config: InterruptPinConfig,
    sim: SerialInterfaceMode,
    auto_inc: RegisterAutoincrement,
    byte_order: ByteOrder,
) -> CTRL3_C {
    <Boot as Into<CTRL3_C>>::into(boot)
        | data_update.into()
        | interrupt_pad.into()
        | interrupt_pin_config.into()
        | sim.into()
        | auto_inc.into()
        | byte_order.into()
}

pub enum AccelerometerFrequency {
    Off,
    F1_6Hz,
    F12_5Hz,
    F26Hz,
    F52Hz,
    F104Hz,
    F208Hz,
    F416Hz,
    F833Hz,
    F1_66kHz,
    F3_33kHz,
    F6_66kHz,
}

#[derive(Clone, Copy)]
pub enum AccelerometerScale {
    PM2G,
    PM4G,
    PM8G,
    PM16G,
}

pub enum AccelerometerBandwidth {
    F1_5kHz,
    F400Hz,
}

pub enum GyroscopeFrequency {
    Off,
    F12_5Hz,
    F26Hz,
    F52Hz,
    F104Hz,
    F208Hz,
    F416Hz,
    F833Hz,
    F1_66kHz,
    F3_33kHz,
    F6_66kHz,
}

#[derive(Clone, Copy)]
pub enum GyroscopeScale {
    F125DPS,
    F250DPS,
    F500DPS,
    F1000DPS,
    F2000DPS,
}

pub enum Boot {
    Normal,
    RebootMemoryContent,
}

pub enum DataUpdate {
    ContinuousUpdate,
    WaitUntilRead,
}

pub enum InteruptPad {
    ActiveHigh,
    ActiveLow,
}

pub enum InterruptPinConfig {
    PushPull,
    OpenDrain,
}

pub enum SerialInterfaceMode {
    M4WireInterface,
    M3WireInterface,
}

pub enum RegisterAutoincrement {
    On,
    Off,
}

pub enum ByteOrder {
    BigEndian,
    LittleEndian, //
}

pub enum Reset {
    NoReset,
    Reset,
}

impl From<InterruptPinConfig> for CTRL3_C {
    fn from(value: InterruptPinConfig) -> Self {
        match value {
            InterruptPinConfig::PushPull => CTRL3_C::empty(),
            InterruptPinConfig::OpenDrain => CTRL3_C::PP_OD,
        }
    }
}

impl From<Reset> for CTRL3_C {
    fn from(value: Reset) -> Self {
        match value {
            Reset::NoReset => CTRL3_C::empty(),
            Reset::Reset => CTRL3_C::SW_RESET,
        }
    }
}

impl From<ByteOrder> for CTRL3_C {
    fn from(value: ByteOrder) -> Self {
        match value {
            ByteOrder::BigEndian => CTRL3_C::BLE,
            ByteOrder::LittleEndian => CTRL3_C::empty(),
        }
    }
}

impl From<RegisterAutoincrement> for CTRL3_C {
    fn from(value: RegisterAutoincrement) -> Self {
        match value {
            RegisterAutoincrement::On => CTRL3_C::IF_INC,
            RegisterAutoincrement::Off => CTRL3_C::empty(),
        }
    }
}

impl From<SerialInterfaceMode> for CTRL3_C {
    fn from(value: SerialInterfaceMode) -> Self {
        match value {
            SerialInterfaceMode::M4WireInterface => CTRL3_C::empty(),
            SerialInterfaceMode::M3WireInterface => CTRL3_C::SIM,
        }
    }
}

impl From<InteruptPad> for CTRL3_C {
    fn from(value: InteruptPad) -> Self {
        match value {
            InteruptPad::ActiveHigh => CTRL3_C::empty(),
            InteruptPad::ActiveLow => CTRL3_C::H_LACTIVE,
        }
    }
}

impl From<DataUpdate> for CTRL3_C {
    fn from(value: DataUpdate) -> Self {
        match value {
            DataUpdate::ContinuousUpdate => CTRL3_C::empty(),
            DataUpdate::WaitUntilRead => CTRL3_C::BDU,
        }
    }
}

impl From<Boot> for CTRL3_C {
    fn from(value: Boot) -> Self {
        match value {
            Boot::Normal => CTRL3_C::empty(),
            Boot::RebootMemoryContent => CTRL3_C::BOOT,
        }
    }
}

impl From<GyroscopeScale> for CTRL2_G {
    fn from(value: GyroscopeScale) -> Self {
        match value {
            GyroscopeScale::F125DPS => CTRL2_G::FS_125,
            GyroscopeScale::F250DPS => CTRL2_G::empty(),
            GyroscopeScale::F500DPS => CTRL2_G::FS_G0,
            GyroscopeScale::F1000DPS => CTRL2_G::FS_G1,
            GyroscopeScale::F2000DPS => CTRL2_G::FS_G1 | CTRL2_G::FS_G0,
        }
    }
}

impl From<GyroscopeFrequency> for CTRL2_G {
    fn from(value: GyroscopeFrequency) -> Self {
        match value {
            GyroscopeFrequency::Off => CTRL2_G::empty(),
            GyroscopeFrequency::F12_5Hz => CTRL2_G::ODR_G0,
            GyroscopeFrequency::F26Hz => CTRL2_G::ODR_G1,
            GyroscopeFrequency::F52Hz => CTRL2_G::ODR_G1 | CTRL2_G::ODR_G0,
            GyroscopeFrequency::F104Hz => CTRL2_G::ODR_G2,
            GyroscopeFrequency::F208Hz => CTRL2_G::ODR_G2 | CTRL2_G::ODR_G0,
            GyroscopeFrequency::F416Hz => CTRL2_G::ODR_G2 | CTRL2_G::ODR_G1,
            GyroscopeFrequency::F833Hz => CTRL2_G::ODR_G2 | CTRL2_G::ODR_G1 | CTRL2_G::ODR_G0,
            GyroscopeFrequency::F1_66kHz => CTRL2_G::ODR_G3,
            GyroscopeFrequency::F3_33kHz => CTRL2_G::ODR_G3 | CTRL2_G::ODR_G0,
            GyroscopeFrequency::F6_66kHz => CTRL2_G::ODR_G3 | CTRL2_G::ODR_G1,
        }
    }
}

impl From<AccelerometerBandwidth> for CTRL1_XL {
    fn from(value: AccelerometerBandwidth) -> Self {
        match value {
            AccelerometerBandwidth::F1_5kHz => CTRL1_XL::empty(),
            AccelerometerBandwidth::F400Hz => CTRL1_XL::BW0_XL,
        }
    }
}

impl From<AccelerometerScale> for CTRL1_XL {
    fn from(value: AccelerometerScale) -> Self {
        match value {
            AccelerometerScale::PM2G => CTRL1_XL::empty(),
            AccelerometerScale::PM16G => CTRL1_XL::FS_XL0,
            AccelerometerScale::PM4G => CTRL1_XL::FS_XL1,
            AccelerometerScale::PM8G => CTRL1_XL::FS_XL1 | CTRL1_XL::FS_XL0,
        }
    }
}

impl From<AccelerometerFrequency> for CTRL1_XL {
    fn from(value: AccelerometerFrequency) -> Self {
        match value {
            AccelerometerFrequency::Off => CTRL1_XL::empty(),
            AccelerometerFrequency::F1_6Hz => {
                CTRL1_XL::ODR_XL3 | CTRL1_XL::ODR_XL1 | CTRL1_XL::ODR_XL0
            }
            AccelerometerFrequency::F12_5Hz => CTRL1_XL::ODR_XL0,
            AccelerometerFrequency::F26Hz => CTRL1_XL::ODR_XL1,
            AccelerometerFrequency::F52Hz => CTRL1_XL::ODR_XL1 | CTRL1_XL::ODR_XL0,
            AccelerometerFrequency::F104Hz => CTRL1_XL::ODR_XL2,
            AccelerometerFrequency::F208Hz => CTRL1_XL::ODR_XL2 | CTRL1_XL::ODR_XL0,
            AccelerometerFrequency::F416Hz => CTRL1_XL::ODR_XL2 | CTRL1_XL::ODR_XL1,
            AccelerometerFrequency::F833Hz => {
                CTRL1_XL::ODR_XL2 | CTRL1_XL::ODR_XL1 | CTRL1_XL::ODR_XL0
            }
            AccelerometerFrequency::F1_66kHz => CTRL1_XL::ODR_XL3,
            AccelerometerFrequency::F3_33kHz => CTRL1_XL::ODR_XL3 | CTRL1_XL::ODR_XL0,
            AccelerometerFrequency::F6_66kHz => CTRL1_XL::ODR_XL3 | CTRL1_XL::ODR_XL1,
        }
    }
}
