//! Driver for the LSM6DSL inertial measurement unit using I2C (SPI Not done yet)

#![forbid(unsafe_code)]

mod conf;
mod register;

use super::AccelGyro;
pub use conf::*;
use core::fmt::Debug;
use core::result::Result;
#[cfg(feature = "defmt")]
use defmt;
use embedded_hal_async as hal;
use hal::i2c::I2c;
#[allow(unused)]
pub use register::INT1_CTRL;

#[derive(Debug, Clone, Copy)]
pub enum ErrorKind {
    WrongDeviceId,
    I2CError,
    NotStarted, // Dummy
}

#[derive(Debug, Clone, Copy)]
pub struct Error<E> {
    pub kind: ErrorKind,
    pub source: Option<E>,
}

#[derive(Clone, Copy)]
pub struct RawLSM6DSL {
    sa0_high: bool,
    acceleration_gain: f32,
    gyroscope_gain: f32,
}

impl RawLSM6DSL {
    pub fn to_lsm6sdl<'a, Interface>(self, interface: &'a mut Interface) -> LSM6DSL<'a, Interface> {
        LSM6DSL {
            interface,
            raw: self,
        }
    }
}

pub struct LSM6DSL<'a, Interface> {
    interface: &'a mut Interface,
    raw: RawLSM6DSL,
}

impl<'a, Interface, E> LSM6DSL<'a, Interface>
where
    Interface: I2c<Error = E>,
    E: Debug,
{
    pub async fn new(interface: &'a mut Interface, sa0_high: bool) -> Result<Self, Error<E>> {
        let mut lsm6dsl = LSM6DSL {
            interface,
            raw: RawLSM6DSL {
                sa0_high,
                acceleration_gain: conf::accelerometer_sensitivity(AccelerometerScale::PM2G),
                gyroscope_gain: conf::gyroscope_sensitivity(GyroscopeScale::F250DPS),
            },
        };

        lsm6dsl.is_device().await?;
        Ok(lsm6dsl)
    }

    pub fn to_raw(self) -> RawLSM6DSL {
        let r = self.raw;
        core::mem::forget(self);
        r
    }

    pub async fn set_interrupt_pad1(&mut self, ctrl: register::INT1_CTRL) -> Result<(), Error<E>> {
        let data: [u8; 2] = [register::Register::INT1_CTRL.addr(), ctrl.bits()];
        self.write(&data).await
    }

    #[allow(unused)]
    pub async fn set_interrupt_pad2(&mut self, ctrl: register::INT2_CTRL) -> Result<(), Error<E>> {
        let data: [u8; 2] = [register::Register::INT2_CTRL.addr(), ctrl.bits()];
        self.write(&data).await
    }

    async fn is_device(&mut self) -> Result<(), Error<E>> {
        let device_id = self.get_device_id().await?;
        if device_id != register::WHO_AM_I_ID {
            return Err(ErrorKind::WrongDeviceId.into());
        }
        Ok(())
    }

    pub async fn read_gyro_acceleration(&mut self) -> Result<AccelGyro, Error<E>> {
        let mut read: [u8; 12] = [0; 12];
        self.read(register::Register::OUTX_L_G, &mut read).await?;
        Ok(Self::convert(
            &read,
            self.raw.acceleration_gain,
            self.raw.gyroscope_gain,
        ))
    }

    pub async fn configure_accelerometer(
        &mut self,
        freq: AccelerometerFrequency,
        scale: AccelerometerScale,
        bandwidth: AccelerometerBandwidth,
    ) -> Result<(), Error<E>> {
        self.raw.acceleration_gain = conf::accelerometer_sensitivity(scale);
        let v = conf::configure_accelerometer(freq, scale, bandwidth);
        let write = [register::Register::CTRL1_XL.addr(), v.bits()];
        self.write(&write).await
    }

    pub async fn configure_gyroscope(
        &mut self,
        freq: GyroscopeFrequency,
        scale: GyroscopeScale,
    ) -> Result<(), Error<E>> {
        self.raw.gyroscope_gain = conf::gyroscope_sensitivity(scale);
        let v = conf::configure_gyroscope(freq, scale);
        let write = [register::Register::CTRL2_G.addr(), v.bits()];
        self.write(&write).await
    }

    async fn read<const N: usize>(
        &mut self,
        register: register::Register,
        read: &mut [u8; N],
    ) -> Result<(), Error<E>> {
        self.interface
            .write_read(self.address(), &[register.addr()], read)
            .await
            .map_err(|e| ErrorKind::I2CError.with(e))
    }

    async fn write<const N: usize>(&mut self, data: &[u8; N]) -> Result<(), Error<E>> {
        self.interface
            .write(self.address(), data)
            .await
            .map_err(|e| ErrorKind::I2CError.with(e))
    }

    fn address(&self) -> u8 {
        return register::BASE_ADDRESS + self.raw.sa0_high as u8;
    }

    async fn get_device_id(&mut self) -> Result<u8, Error<E>> {
        let mut output = [0u8];
        self.read(register::Register::WHO_AM_I, &mut output).await?;

        Ok(output[0])
    }
    fn convert(data: &[u8; 12], acceleration_gain: f32, gyroscope_gain: f32) -> AccelGyro {
        let v = AccelGyro {
            gyro_x: (Self::bytes_to_i16(data[1], data[0]) as f32) * gyroscope_gain,
            gyro_y: (Self::bytes_to_i16(data[3], data[2]) as f32) * gyroscope_gain,
            gyro_z: (Self::bytes_to_i16(data[5], data[4]) as f32) * gyroscope_gain,
            accel_x: (Self::bytes_to_i16(data[7], data[6]) as f32) * acceleration_gain,
            accel_y: (Self::bytes_to_i16(data[9], data[8]) as f32) * acceleration_gain,
            accel_z: (Self::bytes_to_i16(data[11], data[10]) as f32) * acceleration_gain,
        };
        v
    }

    fn bytes_to_i16(msb: u8, lsb: u8) -> i16 {
        ((msb as i16) << 8) | (lsb as i16)
    }
}

impl<E> Error<E> {
    pub const fn new() -> Self {
        Self {
            kind: ErrorKind::NotStarted,
            source: None,
        }
    }
}

impl<E> From<ErrorKind> for Error<E>
where
    E: Debug,
{
    fn from(kind: ErrorKind) -> Error<E> {
        let source = None;
        Error { source, kind }
    }
}

impl ErrorKind {
    fn with<E>(self, source: E) -> Error<E> {
        let s = Some(source);
        Error {
            source: s,
            kind: self,
        }
    }
}

#[cfg(feature = "defmt")]
impl<E: core::fmt::Debug> defmt::Format for Error<E> {
    fn format(&self, f: defmt::Formatter) {
        defmt::write!(f, "Error {{ kind: ");
        match self.kind {
            ErrorKind::WrongDeviceId => defmt::write!(f, "WrongDeviceId"),
            ErrorKind::I2CError => defmt::write!(f, "I2CError"),
            ErrorKind::NotStarted => defmt::write!(f, "NotStarted"),
        }
        if let Some(source) = &self.source {
            defmt::write!(f, ", source: ");
            defmt::write!(f, "{:?}", defmt::Debug2Format(source));
        }
        defmt::write!(f, " }}");
    }
}
