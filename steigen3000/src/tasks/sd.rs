//! SD Task
//! This task is intended to handle all readings and writings from an sd card
#[cfg(feature = "nrf")]
use super::Spi2Irqs;
use crate::tracing_markers;
use crate::{error, info, warn};
#[cfg(all(not(feature = "std"), feature = "nrf"))]
use core::cell::RefCell;
#[cfg(feature = "nrf")]
use embassy_executor;
#[cfg(feature = "nrf")]
use embassy_nrf::{self, gpio, peripherals, spim};
#[cfg(all(not(feature = "std"), feature = "nrf"))]
use embassy_sync::blocking_mutex::{raw::ThreadModeRawMutex, Mutex as BlockMutex};
use embassy_sync::{
    blocking_mutex::raw::CriticalSectionRawMutex, channel::Channel, mutex::Mutex, signal::Signal,
};
pub(crate) use embedded_sdmmc::RawFile as File;
#[cfg(feature = "nrf")]
use embedded_sdmmc::{self, sdcard::DummyCsPin};
use embedded_sdmmc::{FilenameError, TimeSource};
use heapless::{String, Vec};

mod helper;
//use helper::*;
pub(crate) use helper::get_file_operation_mutex;
use helper::get_read_file_operation_mutex;
#[cfg(feature = "nrf")]
use helper::Clock;
#[cfg(test)]
pub(crate) use helper::FILE_OPERATION_MULTIPLEXERS;

pub const MAX_FILES: usize = 4; // Max files parallel opened
pub(crate) const MAX_DIRS: usize = 2; // Max directories in parallel opened
pub const CONTENT_BUFFER_SIZE: usize = 1024; // multiple of sd block size (512 bytes)
#[cfg(feature = "nrf")]
const SD_CARD_SPI_FREQUENCY: spim::Frequency = spim::Frequency::M8; // TODO: increase when not using logic analyzer
const PATH_SEPARATOR: char = '/';

#[cfg(all(not(feature = "std"), feature = "nrf"))]
#[derive(Clone, Copy)]
pub enum SDStatus {
    NotStarted,
    Ok,
    Error,
}

#[derive(Clone, Copy, PartialEq, Eq, Debug)]
pub enum Status {
    Free,
    Occupied, // To process by the sd task or is currently in use in the file handler
}

/// Requester struct to request a new operation
pub struct FileOperationRequest {
    pub process_status: Status,
    pub operation: Operation,
    //pub (self) id: usize,
}

#[cfg(all(not(feature = "std"), feature = "nrf"))]
pub(crate) static SD_STATUS: BlockMutex<ThreadModeRawMutex, RefCell<SDStatus>> =
    BlockMutex::new(RefCell::new(SDStatus::NotStarted));

#[cfg_attr(test, derive(Clone, Debug, PartialEq))]
pub(crate) enum Operation {
    Open((embedded_sdmmc::Mode, heapless::String<50>)),
    Close,
    Write(Vec<u8, CONTENT_BUFFER_SIZE>),
    Read,
    FileExists(heapless::String<50>),
    CreateDir(heapless::String<50>),
    #[cfg(test)]
    STOP,
}

#[cfg(any(test, feature = "std"))]
type BlockDeviceError = std::io::Error;
#[cfg(not(feature = "std"))]
type BlockDeviceError = embedded_sdmmc::sdcard::Error;
pub type VolumeError = embedded_sdmmc::Error<BlockDeviceError>;

static FILE_OPEN_SIGNAL: Signal<CriticalSectionRawMutex, Result<File, VolumeError>> = Signal::new();
static FILE_CLOSED_SIGNAL: Signal<CriticalSectionRawMutex, ()> = Signal::new();

pub async fn open(filename: &str, mode: embedded_sdmmc::Mode) -> Result<File, VolumeError> {
    let m = get_file_operation_mutex().await;
    let mut fo = m.lock().await;
    if let Operation::Open((m, f)) = &mut fo.operation {
        f.clear();
        f.push_str(filename)
            .map_err(|_| VolumeError::FilenameError(FilenameError::NameTooLong))?;
        *m = mode;
    } else {
        fo.operation = Operation::Open((mode, String::new()));
        let Operation::Open((_, f)) = &mut fo.operation else {
            unreachable!()
        };
        f.push_str(filename)
            .map_err(|_| VolumeError::FilenameError(FilenameError::NameTooLong))?;
    }
    fo.process_status = Status::Occupied;
    drop(fo);
    FILE_OPERATION.send((None, m)).await;
    return FILE_OPEN_SIGNAL.wait().await;
}

pub async fn close(file: File) {
    let m = get_file_operation_mutex().await;
    let mut fo = m.lock().await;
    fo.operation = Operation::Close;
    fo.process_status = Status::Occupied;
    drop(fo);
    FILE_OPERATION.send((Some(file), m)).await;
    FILE_CLOSED_SIGNAL.wait().await;
}

pub async fn new_file_operation(
    file: File,
    mutex: &'static Mutex<CriticalSectionRawMutex, FileOperationRequest>,
) {
    let mut fo = mutex.lock().await;
    fo.process_status = Status::Occupied;
    drop(fo); // important
    FILE_OPERATION.send((Some(file), mutex)).await;
}

pub async fn file_exists_operation(file: &str) -> Result<bool, VolumeError> {
    let m = get_file_operation_mutex().await;
    {
        let mut fo = m.lock().await;
        if let Operation::FileExists(filename) = &mut fo.operation {
            filename.clear();
            filename
                .push_str(file)
                .map_err(|_| VolumeError::FilenameError(FilenameError::NameTooLong))?;
        } else {
            fo.operation = Operation::FileExists(String::new());
            let Operation::FileExists(filename) = &mut fo.operation else {
                unreachable!()
            };
            filename
                .push_str(file)
                .map_err(|_| VolumeError::FilenameError(FilenameError::NameTooLong))?;
        }
        fo.process_status = Status::Occupied;
    }
    // fo must be dropped, otherwise a deadlock happens, because
    // fo still locks and the sd task wanna lock the same mutex as well
    FILE_OPERATION.send((None, m)).await;

    let exists = FILE_EXISTS_SIGNAL.wait().await?;
    Ok(exists)
}

pub async fn create_folders(path: &str) -> Result<(), VolumeError> {
    let m = get_file_operation_mutex().await;
    {
        let mut fo = m.lock().await;
        if let Operation::CreateDir(p) = &mut fo.operation {
            p.clear();
            p.push_str(path)
                .map_err(|_| VolumeError::FilenameError(FilenameError::NameTooLong))?;
        } else {
            fo.operation = Operation::CreateDir(String::new());
            let Operation::CreateDir(p) = &mut fo.operation else {
                unreachable!()
            };
            p.push_str(path)
                .map_err(|_| VolumeError::FilenameError(FilenameError::NameTooLong))?;
        }
    }
    // fo must be dropped, otherwise a deadlock happens, because
    // fo still locks and the sd task wanna lock the same mutex as well
    FILE_OPERATION.send((None, m)).await;
    CREATE_FOLDER_SIGNAL.wait().await
}

pub async fn read_operation(
    file: File,
) -> Result<&'static Mutex<CriticalSectionRawMutex, ReadOperation>, VolumeError> {
    let m = get_file_operation_mutex().await;
    {
        let mut fo = m.lock().await;
        fo.operation = Operation::Read;
        fo.process_status = Status::Occupied;
    } // fo must be dropped, otherwise a deadlock happens!
    FILE_OPERATION.send((Some(file), m)).await;
    // Wait for the first results
    Ok(READ_SIGNAL.receive().await) // Init mutex
}

#[cfg(test)]
pub async fn stop_operation() -> Result<(), ()> {
    let m = get_file_operation_mutex().await;
    let mut fo = m.lock().await;
    fo.process_status = Status::Occupied;
    fo.operation = Operation::STOP;
    drop(fo);
    FILE_OPERATION.send((None, m)).await;
    Ok(())
}

/// Channel over which the mutexes are send to the sd task
static FILE_OPERATION: Channel<
    CriticalSectionRawMutex,
    (
        Option<File>,
        &'static Mutex<CriticalSectionRawMutex, FileOperationRequest>,
    ),
    10,
> = Channel::new();

#[derive(PartialEq, Eq, Debug)]
pub enum ReadStatus {
    NotStarted,
    Ongoing,
    Finished,
}
pub struct ReadOperation {
    pub status: ReadStatus,
    pub process_status: Status,
    pub content: Vec<u8, CONTENT_BUFFER_SIZE>,
    pub result: Result<(), VolumeError>,
}

/// Signal used to signal that read operation is finished
/// Using a channel, so the data will not be overwritten
pub static READ_SIGNAL: Channel<
    CriticalSectionRawMutex,
    &'static Mutex<CriticalSectionRawMutex, ReadOperation>,
    1,
> = Channel::new();
pub static READ_FINISHED_SIGNAL: Signal<CriticalSectionRawMutex, ReadStatus> = Signal::new();

pub static FILE_EXISTS_SIGNAL: Signal<CriticalSectionRawMutex, Result<bool, VolumeError>> =
    Signal::new();
pub static CREATE_FOLDER_SIGNAL: Signal<CriticalSectionRawMutex, Result<(), VolumeError>> =
    Signal::new();

#[cfg(all(not(feature = "std"), feature = "nrf"))]
#[embassy_executor::task]
pub async fn sd(
    spi_: peripherals::SPI2,
    sck: gpio::AnyPin,
    cs: gpio::AnyPin,
    mosi: gpio::AnyPin,
    miso: gpio::AnyPin,
) {
    use crate::info;
    use embassy_embedded_hal::SetConfig;
    use embassy_time::Delay;
    use embedded_sdmmc::sdcard::AcquireOpts;
    use embedded_sdmmc::SdCard;

    info!("Initializing SD Card");
    let mut config = spim::Config::default();
    config.frequency = spim::Frequency::K250; // Initialization frequency must be below 400kHz
    let spi = spim::Spim::new(spi_, Spi2Irqs, sck, miso, mosi, config);
    let cs_pin = gpio::Output::new(cs, gpio::Level::High, gpio::OutputDrive::Standard);
    let delay = Delay {};

    // embedded_hal_bus
    let spi_device = embedded_hal_bus::spi::ExclusiveDevice::new(spi, DummyCsPin, Delay);
    let mut options = AcquireOpts::default();
    options.acquire_retries = 2;

    let sdcard = SdCard::new_with_options(spi_device, cs_pin, delay, options);
    // Initialisation done here. So afterwards we can increase the clock speed
    match sdcard.get_card_type().await {
        Some(t) => {
            info!("SD Card type: {}", t);
            SD_STATUS.lock(|l| {
                *l.borrow_mut() = SDStatus::Ok;
            });
        }
        None => {
            error!("No sd card found / initialization failed");
            SD_STATUS.lock(|l| {
                *l.borrow_mut() = SDStatus::Error;
            });
        } // No sd card detected, TODO: what to do here? Maybe an infinity loop and trying to detect?
    }
    sdcard.spi(|spi| {
        let mut config = spim::Config::default();
        config.frequency = SD_CARD_SPI_FREQUENCY;
        let _ = spi.bus_mut().set_config(&config);
    });

    // let num_bytes = sdcard.num_bytes().await;
    // TODO: how to determine if there is enough space?
    // TODO: remove all panics!
    // TODO: what happens if the card will be remove during write? Only an error result?

    let time_source = Clock {}; // Required when writing to the sd card!
    let mut volume_mgr: embedded_sdmmc::VolumeManager<_, _, MAX_DIRS, MAX_FILES, 1> =
        embedded_sdmmc::VolumeManager::new_with_limits(sdcard, time_source, 1);

    match sd_generic(&mut volume_mgr).await {
        Err(_) => {
            error!("Unexpectly left sd task. Error.");
        }
        Ok(_) => {
            error!("Unexpectly left sd task. Ok.");
        }
    }
    SD_STATUS.lock(|l| {
        *l.borrow_mut() = SDStatus::Error;
    });
    loop {
        // Process all file operations to not block other tasks if they are waiting
        let op = FILE_OPERATION.receive().await;
        let mut fo = op.1.lock().await;
        fo.process_status = Status::Free;
        match &fo.operation {
            Operation::Open(_) => FILE_OPEN_SIGNAL.signal(Err(VolumeError::BadHandle)),
            Operation::Read => {
                let mutex = get_read_file_operation_mutex().await;
                let mut ro = mutex.lock().await;
                ro.process_status = Status::Free; // Release because of error
                ro.result = Err(VolumeError::BadHandle);
                ro.content.clear();
                drop(ro);
                READ_SIGNAL.send(mutex).await; // must be done always, otherwise it will be waited infinitely
            }
            Operation::Close => FILE_CLOSED_SIGNAL.signal(()),
            Operation::FileExists(_) => FILE_EXISTS_SIGNAL.signal(Err(VolumeError::BadHandle)),
            Operation::CreateDir(_) => CREATE_FOLDER_SIGNAL.signal(Err(VolumeError::BadHandle)),
            #[cfg(test)]
            Operation::STOP => (),
            Operation::Write(_) => (),
        }
        info!("SD Task left. Dummy all incomming requests.");
    }
}

fn split_filename<'a>(filename: &'a str) -> (&'a str, &'a str) {
    if let Some(pos) = filename.rfind(PATH_SEPARATOR) {
        if pos < filename.len() {
            (&filename[0..pos], &filename[pos + 1..filename.len()])
        } else {
            (&filename[0..pos], "")
        }
    } else {
        ("", filename)
    }
}

pub(crate) async fn sd_generic<D, T, const MAX_DIRS: usize, const MAX_FILES: usize>(
    volume_mgr: &mut embedded_sdmmc::VolumeManager<D, T, MAX_DIRS, MAX_FILES>,
) -> Result<(), ()>
where
    D: embedded_sdmmc::BlockDevice,
    T: TimeSource,
    <D as embedded_sdmmc::BlockDevice>::Error: core::fmt::Debug,
{
    // The volume mgr is the only object which directly communicates with the sd card. Operations must be done always on this object
    // So when having a raw directory nothing can be done directly with it (but it is copyable). So before doing any operation
    // the raw directory must be converted first to a directory.
    let volume0 = match volume_mgr.open_volume(embedded_sdmmc::VolumeIdx(0)).await {
        Ok(v) => v.to_raw_volume(),
        Err(_) => {
            //error!("{:?}", e);
            return Err(());
        }
    };

    loop {
        let op = FILE_OPERATION.receive().await;
        let mut fo = op.1.lock().await;
        fo.process_status = Status::Free;
        #[cfg(test)]
        log_file_operation(op.0, &fo).await;

        match &fo.operation {
            Operation::Open((mode, filepath)) => {
                let (path, filename) = split_filename(&filepath);
                let mut volume = volume0.to_volume(volume_mgr);
                let mut file_open_signal;
                match volume.open_root_dir() {
                    Ok(mut dir) => {
                        file_open_signal = Err(VolumeError::BadHandle);
                        let mut ok = true;
                        let mut it = path.split(PATH_SEPARATOR);
                        while let Some(p) = it.next() {
                            if let Err(e) = dir.change_dir(p).await {
                                file_open_signal = match e {
                                    embedded_sdmmc::Error::NotFound => Err(VolumeError::NotFound),
                                    _ => Err(VolumeError::BadHandle),
                                };
                                ok = false;
                                break;
                            }
                        }
                        if ok {
                            file_open_signal = match dir.open_file_in_dir(filename, *mode).await {
                                Ok(file) => Ok(file.to_raw_file()),
                                Err(e) => match e {
                                    embedded_sdmmc::Error::NotFound => Err(VolumeError::NotFound),
                                    _ => Err(VolumeError::BadHandle),
                                },
                            }
                        }
                    }
                    Err(_e) => file_open_signal = Err(VolumeError::BadHandle),
                }
                volume.to_raw_volume(); // prevent droping
                FILE_OPEN_SIGNAL.signal(file_open_signal);
            }
            Operation::Close => {
                if let Some(file) = op.0 {
                    if let Err(_) = volume_mgr.close_file(file).await {
                        error!("Unable to close file!");
                    }
                    // drop(file.to_file(volume_mgr)); only for embassy implemented. So for unittests not suitable
                }
                FILE_CLOSED_SIGNAL.signal(());
            }
            Operation::CreateDir(path) => {
                let mut volume = volume0.to_volume(volume_mgr);
                let mut it = path.split(PATH_SEPARATOR);
                let mut result;
                if let Ok(mut dir) = volume.open_root_dir() {
                    result = Ok(());
                    while let Some(p) = it.next() {
                        #[cfg(feature = "std")]
                        println!("Change to: {}", p);
                        if let Err(e) = dir.change_dir(p).await {
                            #[cfg(feature = "std")]
                            println!("Change dir Error: {:?}", e);
                            match e {
                                embedded_sdmmc::Error::NotFound => {
                                    #[cfg(feature = "std")]
                                    println!("Create dir: {}", p);
                                    if let Err(_e) = dir.make_dir_in_dir(p).await {
                                        #[cfg(feature = "std")]
                                        println!("make_dir_in_dir Error: {:?}", _e);
                                        result = Err(VolumeError::BadHandle);
                                        break;
                                    } else if let Err(_e) = dir.change_dir(p).await {
                                        #[cfg(feature = "std")]
                                        println!("change_dir Error: {:?}", _e);
                                        result = Err(VolumeError::BadHandle);
                                        break;
                                    }
                                }
                                _ => {
                                    result = Err(VolumeError::BadHandle);
                                    break;
                                }
                            }
                        } else {
                            #[cfg(feature = "std")]
                            println!("Folder found: {}", p);
                        }
                    }
                } else {
                    result = Err(VolumeError::BadHandle);
                }
                CREATE_FOLDER_SIGNAL.signal(result)
            }
            Operation::FileExists(filepath) => {
                let (path, filename) = split_filename(&filepath);
                let mut volume = volume0.to_volume(volume_mgr);
                let mut signal;
                if let Ok(mut dir) = volume.open_root_dir() {
                    signal = Ok(false);
                    let mut it = path.split(PATH_SEPARATOR);
                    while let Some(p) = it.next() {
                        if let Err(_e) = dir.change_dir(p).await {
                            signal = Err(VolumeError::BadHandle);
                            break;
                        }
                    }
                    if signal.is_ok() {
                        match dir.find_directory_entry(filename).await {
                            Ok(_) => signal = Ok(true),
                            Err(e) => match e {
                                embedded_sdmmc::Error::NotFound => signal = Ok(false),
                                _ => signal = Err(VolumeError::BadHandle),
                            },
                        }
                    }
                } else {
                    signal = Err(VolumeError::BadHandle)
                }
                volume.to_raw_volume(); // prevent droping
                FILE_EXISTS_SIGNAL.signal(signal);
            }
            Operation::Write(write_buffer) => {
                if let Some(file) = op.0 {
                    let _m = tracing_markers::Marker::new(tracing_markers::Markers::SDWrite);
                    let mut file = file.to_file(volume_mgr);
                    if let Err(_) = file.write(&write_buffer).await {
                        info!("Write data to file failed");
                        // TODO: handle
                    }
                    // Required, otherwise on the sd card the file is empty. This is probably because the
                    // dir entry contains the wrong file size (but the FAT is correct). The OS uses the file size
                    // to determine how many memory to allocate, but if it is empty nothing will be shown.
                    if let Err(_) = file.flush().await {
                        info!("Unable to flush data");
                    }
                    file.to_raw_file(); // Preventing that the file will be dropped
                }
            }
            Operation::Read => {
                let mutex = get_read_file_operation_mutex().await;
                if let Some(file) = op.0 {
                    let mut file = file.to_file(volume_mgr);
                    let mut eof = true;
                    if let Err(_e) = file.seek_from_start(0) {
                        let mut ro = mutex.lock().await;
                        ro.process_status = Status::Free;
                        ro.result = Err(VolumeError::BadHandle);
                    } else {
                        while !file.is_eof() {
                            let mut ro = mutex.lock().await;
                            ro.process_status = Status::Occupied;
                            ro.result = Err(VolumeError::BadHandle);
                            ro.content.clear();
                            let buffer_cap = ro.content.capacity();
                            let _ = ro.content.resize_default(buffer_cap);
                            match file.read(&mut ro.content).await {
                                Ok(n) => {
                                    ro.result = Ok(());
                                    ro.content.truncate(n);
                                    ro.result = Ok(()); // File is empty
                                    if n == 0 {
                                        warn!("Nothing read!");
                                        ro.status = ReadStatus::Finished;
                                        eof = false;
                                        break;
                                    } else if n < buffer_cap {
                                        ro.status = ReadStatus::Finished;
                                        warn!("Finished reading");
                                        eof = false;
                                        break;
                                    } else {
                                        ro.status = ReadStatus::Ongoing;
                                        drop(ro); // Important. Release mutex
                                        READ_SIGNAL.send(mutex).await;
                                        READ_FINISHED_SIGNAL.wait().await;
                                        warn!("Ongoing: read data");
                                    }
                                }
                                Err(_e) => {
                                    ro.result = Err(VolumeError::BadHandle);
                                    ro.process_status = Status::Free;
                                    eof = false;
                                    break;
                                }
                            };
                        }
                    }
                    file.to_raw_file(); // To prevent droping (closing) the file
                    if eof {
                        let mut ro = mutex.lock().await;
                        ro.content.clear();
                        ro.process_status = Status::Occupied;
                        ro.status = ReadStatus::Finished;
                    }
                } else {
                    let mut ro = mutex.lock().await;
                    ro.process_status = Status::Free; // Release because of error
                    ro.result = Err(VolumeError::BadHandle);
                    ro.content.clear();
                }
                READ_SIGNAL.send(mutex).await; // must be done always, otherwise it will be waited infinitely
            }
            #[cfg(test)]
            Operation::STOP => return Ok(()),
        }
    }
}

// #[cfg(test)]
// impl Operation {
//     fn to_string(&self) -> &'static str {
//         match self {
//             Self::FileExists(_) => "FileExists",
//             Self::Write(_) => "Write",
//             Self::Read => "Read",
//             #[cfg(test)]
//             Self::STOP => "STOP",
//             Self::Open(_) => "Open",
//             Self::Close => "Close",
//         }
//     }
// }

#[cfg(test)]
pub(crate) static FILE_OPERATION_STATISTIC: Mutex<
    CriticalSectionRawMutex,
    std::vec::Vec<FileOperationRequest>,
> = Mutex::new(std::vec::Vec::new());

#[cfg(test)]
async fn log_file_operation(_file: Option<File>, fo: &FileOperationRequest) {
    let f = FileOperationRequest {
        operation: fo.operation.clone(),
        process_status: fo.process_status,
    };
    // if f.file_operation == Operation::Write {
    //     // let mut s = std::string::String::new();
    //     // for c in &fo.content {
    //     //     s.push(*c as char);
    //     // }
    //     // println!("{}", s);
    // }
    println!("File operation: {:?}", f.operation);
    FILE_OPERATION_STATISTIC.lock().await.push(f);
}

#[cfg(all(feature = "std", test))]
pub(crate) mod tests {
    use tokio::{
        self,
        time::{sleep, Duration},
    };

    use crate::tasks::sd::READ_FINISHED_SIGNAL;

    use super::{sd_generic, VolumeError};
    pub(crate) mod linux_block_device;
    pub(crate) mod test_helpers;
    use core::{panic, str::FromStr};
    use embedded_sdmmc::VolumeManager;
    use heapless::{String, Vec};
    use linux_block_device::{Clock, LinuxBlockDevice};
    use rand::prelude::*;
    use std::string::String as StdString;
    use std::{
        env, fs,
        path::{self, Path},
    };

    const PRINT_BLOCKS: bool = false;
    const DISKNAME: &str = "DISK1.img";

    #[test]
    fn test_split_filename() {
        let (path, filename) = super::split_filename("Some/Path/Filename.txt");
        assert_eq!(path, "Some/Path");
        assert_eq!(filename, "Filename.txt");
    }

    #[test]
    fn test_split_filename2() {
        let (path, filename) = super::split_filename("Some/Path/Filename/");
        assert_eq!(path, "Some/Path/Filename");
        assert_eq!(filename, "");
    }

    async fn exists(mut counter: usize) -> Result<(), ()> {
        let mut file_exists_selection = true;
        while counter > 0 {
            let mut file_name = "config.pg";
            if !file_exists_selection {
                file_name = "NOTEX.txt";
            }
            match super::file_exists_operation(file_name).await {
                Ok(exists) => {
                    counter -= 1;
                    assert_eq!(exists, file_exists_selection);
                    file_exists_selection = !file_exists_selection;
                }
                Err(_) => {
                    panic!("Error!")
                }
            }
        }

        sleep(Duration::from_millis(1000)).await;
        // Stop other thread
        super::stop_operation().await.unwrap();
        Ok(())
    }

    #[tokio::test]
    async fn file_exists() {
        super::FILE_OPERATION_STATISTIC.lock().await.clear();
        // TODO: really needed to do all the time?
        let td = get_test_disk(DISKNAME);

        let lbd: LinuxBlockDevice<std::string::String> =
            LinuxBlockDevice::new(td.file_name(), PRINT_BLOCKS)
                .await
                .unwrap();
        let mut volume_mgr: VolumeManager<_, _, { super::MAX_DIRS }, { super::MAX_FILES }> =
            VolumeManager::new_with_limits(lbd, Clock, 100);
        let sd_future = sd_generic(&mut volume_mgr);

        const REPEAT_COUNTS: usize = super::helper::FILE_OPERATION_MULTIPLEXERS + 100;

        let (sd_result, second_result) = tokio::join!(sd_future, exists(REPEAT_COUNTS));

        assert_eq!(sd_result, Ok(()));
        assert_eq!(second_result, Ok(()));
        let m = super::FILE_OPERATION_STATISTIC.lock().await;
        assert_eq!(m.len(), REPEAT_COUNTS + 1); // Last is the stop command
        let mut file_exists_selection = true;
        for i in 0..REPEAT_COUNTS {
            let mut file_name = "config.pg";
            if !file_exists_selection {
                file_name = "NOTEX.txt";
            }
            file_exists_selection = !file_exists_selection;
            assert_eq!(m[i].process_status, super::Status::Free);
            let mut s: String<50> = String::new();
            s.push_str(file_name).unwrap();
            assert_eq!(m[i].operation, super::Operation::FileExists(s));
        }
    }

    async fn read(mut counter: usize) -> Result<(), ()> {
        let file = super::open("config.pg", embedded_sdmmc::Mode::ReadOnly)
            .await
            .map_err(|_| ())
            .unwrap();
        while counter > 0 {
            let m = super::read_operation(file).await.unwrap();
            let mut read = m.lock().await;
            assert!(read.result.is_ok());
            assert_eq!(read.process_status, super::Status::Occupied);
            assert_eq!(read.status, super::ReadStatus::Finished);
            let mut vec: Vec<u8, { super::CONTENT_BUFFER_SIZE }> = Vec::new();
            let reference = "ParaguideConfig:0\n\
            PilotName:Max Mustermann\n\
            GliderType:Icaro Gravis 2 S\n\
            [Beep]\n\
            Frequency:10, 20,100\n\
            OnTime:1200,3,421\n\
            OffTime:192, 2192, 32\n\
            [/Beep]\n";
            assert_eq!(read.content.len(), reference.len());

            read.content.clone_into(&mut vec);
            let s: String<{ super::CONTENT_BUFFER_SIZE }> =
                String::from_utf8(vec).map_err(|_| ())?;
            assert_eq!(s, reference);
            read.process_status = super::Status::Free;
            counter -= 1;
        }
        super::close(file).await;
        super::stop_operation().await.unwrap();
        Ok(())
    }

    #[tokio::test]
    async fn simple_read() {
        super::FILE_OPERATION_STATISTIC.lock().await.clear();
        // TODO: really needed to do all the time?
        let td = get_test_disk(DISKNAME);

        let lbd: LinuxBlockDevice<std::string::String> =
            LinuxBlockDevice::new(td.file_name(), PRINT_BLOCKS)
                .await
                .unwrap();
        let mut volume_mgr: VolumeManager<_, _, { super::MAX_DIRS }, { super::MAX_FILES }> =
            VolumeManager::new_with_limits(lbd, Clock, 100);
        let sd_future = sd_generic(&mut volume_mgr);

        const REPEAT_COUNTS: usize = super::helper::FILE_OPERATION_MULTIPLEXERS + 100;

        let (sd_result, second_result) = tokio::join!(sd_future, read(REPEAT_COUNTS));

        assert_eq!(sd_result, Ok(()));
        assert_eq!(second_result, Ok(()));
        let m = super::FILE_OPERATION_STATISTIC.lock().await;
        assert_eq!(m.len(), REPEAT_COUNTS + 1 + 1 + 1); // First is Open, Last is the stop command
        let mut filename = String::new();
        filename.push_str("config.pg").unwrap();
        assert_eq!(
            m[0].operation,
            super::Operation::Open((embedded_sdmmc::Mode::ReadOnly, filename))
        );
        assert_eq!(m[0].process_status, super::Status::Free);
        for i in 1..REPEAT_COUNTS {
            assert_eq!(m[i].process_status, super::Status::Free);
            assert_eq!(m[i].operation, super::Operation::Read);
        }
        assert_eq!(
            m[REPEAT_COUNTS + 3 - 22].process_status,
            super::Status::Free
        );
        assert_eq!(m[REPEAT_COUNTS + 3 - 2].operation, super::Operation::Close);
        assert_eq!(m[REPEAT_COUNTS + 3 - 1].process_status, super::Status::Free);
        assert_eq!(m[REPEAT_COUNTS + 3 - 1].operation, super::Operation::STOP);
    }

    // TODO: testing close!

    // // Simple read file does not exist

    async fn multiread_last_buffer_not_complete_full(mut counter: usize) -> Result<(), ()> {
        let file = super::open("3333C.txt", embedded_sdmmc::Mode::ReadOnly)
            .await
            .map_err(|_| ())
            .unwrap();
        while counter > 0 {
            let m = super::read_operation(file).await.unwrap();
            {
                let mut read = m.lock().await;
                assert!(read.result.is_ok());
                assert_eq!(read.process_status, super::Status::Occupied);
                assert_eq!(read.status, super::ReadStatus::Ongoing);
                read.process_status = super::Status::Free; // Release
                READ_FINISHED_SIGNAL.signal(super::ReadStatus::Ongoing);
                // read out of scope so unlocking.
            }
            for _ in 0..2 {
                let mut read = super::READ_SIGNAL.receive().await.lock().await;
                assert!(read.result.is_ok());
                assert_eq!(read.process_status, super::Status::Occupied);
                assert_eq!(read.status, super::ReadStatus::Ongoing);
                assert_eq!(read.content.len(), super::CONTENT_BUFFER_SIZE);
                //assert_eq!(read.content == ...)
                read.process_status = super::Status::Free; // Release
                READ_FINISHED_SIGNAL.signal(super::ReadStatus::Ongoing);
            }
            // Last read
            let mut read = super::READ_SIGNAL.receive().await.lock().await;
            assert!(read.result.is_ok());
            assert_eq!(read.process_status, super::Status::Occupied);
            assert_eq!(read.status, super::ReadStatus::Finished);
            assert!(333 != super::CONTENT_BUFFER_SIZE);
            assert_eq!(read.content.len(), 3333 - (3 * super::CONTENT_BUFFER_SIZE)); // last character is new line
            read.process_status = super::Status::Free; // Release
            counter -= 1;
            // READ_FINISHED_SIGNAL.signal(super::ReadStatus::Finished); // Not needed
        }
        // Stop other thread
        super::stop_operation().await.unwrap();
        Ok(())
    }

    #[tokio::test]
    async fn multi_read() {
        super::FILE_OPERATION_STATISTIC.lock().await.clear();
        // TODO: really needed to do all the time?
        let td = get_test_disk(DISKNAME);

        let lbd: LinuxBlockDevice<std::string::String> =
            LinuxBlockDevice::new(td.file_name(), PRINT_BLOCKS)
                .await
                .unwrap();
        let mut volume_mgr: VolumeManager<_, _, { super::MAX_DIRS }, { super::MAX_FILES }> =
            VolumeManager::new_with_limits(lbd, Clock, 100);
        let sd_future = sd_generic(&mut volume_mgr);

        const REPEAT_COUNTS: usize = super::helper::FILE_OPERATION_MULTIPLEXERS + 100;

        let (sd_result, second_result) = tokio::join!(
            sd_future,
            multiread_last_buffer_not_complete_full(REPEAT_COUNTS)
        );

        assert_eq!(sd_result, Ok(()));
        assert_eq!(second_result, Ok(()));
        let m = super::FILE_OPERATION_STATISTIC.lock().await;
        assert_eq!(m.len(), REPEAT_COUNTS + 1 + 1); // First is Open, Last is the stop command
        let mut filename = String::new();
        filename.push_str("3333C.txt").unwrap();
        assert_eq!(
            m[0].operation,
            super::Operation::Open((embedded_sdmmc::Mode::ReadOnly, filename))
        );
        assert_eq!(m[0].process_status, super::Status::Free);
        for i in 1..REPEAT_COUNTS {
            assert_eq!(m[i].process_status, super::Status::Free);
            assert_eq!(m[i].operation, super::Operation::Read);
        }
        assert_eq!(m[REPEAT_COUNTS + 2 - 1].process_status, super::Status::Free);
        assert_eq!(m[REPEAT_COUNTS + 2 - 1].operation, super::Operation::STOP);
    }

    async fn multiread_last_buffer_complete_full(mut counter: usize) -> Result<(), ()> {
        let file = super::open("10240C.txt", embedded_sdmmc::Mode::ReadOnly)
            .await
            .map_err(|_| ())
            .unwrap();
        while counter > 0 {
            let m = super::read_operation(file).await.unwrap();
            {
                let mut read = m.lock().await;
                assert!(read.result.is_ok());
                assert_eq!(read.process_status, super::Status::Occupied);
                assert_eq!(read.status, super::ReadStatus::Ongoing);
                read.process_status = super::Status::Free; // Release
                READ_FINISHED_SIGNAL.signal(super::ReadStatus::Ongoing);
                // read out of scope so unlocking.
            }
            for _ in 0..9 {
                let mut read = super::READ_SIGNAL.receive().await.lock().await;
                assert!(read.result.is_ok());
                assert_eq!(read.content.len(), super::CONTENT_BUFFER_SIZE);
                assert_eq!(read.process_status, super::Status::Occupied);
                assert_eq!(read.status, super::ReadStatus::Ongoing);
                //assert_eq!(read.content == ...)
                read.process_status = super::Status::Free; // Release
                READ_FINISHED_SIGNAL.signal(super::ReadStatus::Ongoing);
            }
            // Last read
            let mut read = super::READ_SIGNAL.receive().await.lock().await;
            assert!(read.result.is_ok());
            //assert_eq!(read.process_status, super::Status::Free); // No new data, so it does not matter
            assert_eq!(read.status, super::ReadStatus::Finished);
            assert_eq!(read.content.len(), 0); // last character is new line
            read.process_status = super::Status::Free; // Release
            counter -= 1;
            // READ_FINISHED_SIGNAL.signal(super::ReadStatus::Finished); // Not needed
        }
        // Stop other thread
        super::stop_operation().await.unwrap();
        Ok(())
    }

    #[tokio::test]
    async fn multi_read_last_buffer_full() {
        super::FILE_OPERATION_STATISTIC.lock().await.clear();
        // TODO: really needed to do all the time?
        let td = get_test_disk(DISKNAME);

        let lbd: LinuxBlockDevice<std::string::String> =
            LinuxBlockDevice::new(td.file_name(), PRINT_BLOCKS)
                .await
                .unwrap();
        let mut volume_mgr: VolumeManager<_, _, { super::MAX_DIRS }, { super::MAX_FILES }> =
            VolumeManager::new_with_limits(lbd, Clock, 100);
        let sd_future = sd_generic(&mut volume_mgr);

        const REPEAT_COUNTS: usize = super::helper::FILE_OPERATION_MULTIPLEXERS + 100;

        let (sd_result, second_result) = tokio::join!(
            sd_future,
            multiread_last_buffer_complete_full(REPEAT_COUNTS)
        );

        assert_eq!(sd_result, Ok(()));
        assert_eq!(second_result, Ok(()));
        let m = super::FILE_OPERATION_STATISTIC.lock().await;
        assert_eq!(m.len(), REPEAT_COUNTS + 1 + 1); // First is Open, Last is the stop command
        let mut filename = String::new();
        filename.push_str("10240C.txt").unwrap();
        assert_eq!(
            m[0].operation,
            super::Operation::Open((embedded_sdmmc::Mode::ReadOnly, filename))
        );
        assert_eq!(m[0].process_status, super::Status::Free);
        for i in 1..REPEAT_COUNTS {
            assert_eq!(m[i].process_status, super::Status::Free);
            assert_eq!(m[i].operation, super::Operation::Read);
        }
        assert_eq!(m[REPEAT_COUNTS + 2 - 1].process_status, super::Status::Free);
        assert_eq!(m[REPEAT_COUNTS + 2 - 1].operation, super::Operation::STOP);
    }

    pub async fn write_op(file: super::File, s: &str) -> Result<(), ()> {
        let m = super::get_file_operation_mutex().await;
        let mut fo = m.lock().await;
        fo.process_status = super::Status::Occupied;
        if let super::Operation::Write(f) = &mut fo.operation {
            f.clear();
            for b in s.as_bytes() {
                f.push(*b).map_err(|_| ())?;
            }
        } else {
            fo.operation = super::Operation::Write(Vec::new());
            let super::Operation::Write(f) = &mut fo.operation else {
                unreachable!()
            };
            for b in s.as_bytes() {
                f.push(*b).map_err(|_| ())?;
            }
        }
        drop(fo);
        super::FILE_OPERATION.send((Some(file), m)).await;
        Ok(())
    }

    async fn helper_write(mut counter: usize, testfile: &str) -> Result<(), VolumeError> {
        let mut rng = rand::thread_rng();

        let mut reference = StdString::new();
        let file = match super::open(testfile, embedded_sdmmc::Mode::ReadWriteCreateOrAppend).await
        {
            Ok(f) => f,
            Err(e) => {
                super::stop_operation().await.unwrap();
                return Err(e);
            }
        };
        while counter > 0 {
            let y: f64 = rng.gen();
            let y = '0' as u8 + (y * ('Z' as u8 - '0' as u8) as f64) as u8;
            let s = (y as char).to_string();
            let s = s.as_str();
            if let Err(_) = write_op(file, s).await {
                panic!("Timeout")
            } else {
                counter -= 1;
                reference.push_str(&s);
            }
        }
        super::close(file).await;
        sleep(Duration::from_millis(1000)).await;

        // Check
        let file = super::open(testfile, embedded_sdmmc::Mode::ReadOnly)
            .await
            .map_err(|_| ())
            .unwrap();
        let m = super::read_operation(file).await.unwrap();
        let mut read = m.lock().await;
        assert!(read.result.is_ok());
        assert_eq!(read.process_status, super::Status::Occupied);
        assert_eq!(read.status, super::ReadStatus::Finished);
        let mut vec: Vec<u8, { super::CONTENT_BUFFER_SIZE }> = Vec::new();
        assert_eq!(read.content.len(), reference.len());

        read.content.clone_into(&mut vec);
        let s: String<{ super::CONTENT_BUFFER_SIZE }> = String::from_utf8(vec).unwrap();
        assert_eq!(s, reference.as_str());
        read.process_status = super::Status::Free;
        super::close(file).await;

        // Stop other thread
        super::stop_operation().await.unwrap();
        Ok(())
    }

    #[tokio::test]
    async fn simple_write() {
        super::FILE_OPERATION_STATISTIC.lock().await.clear();

        let td = get_test_disk(DISKNAME);

        let lbd: LinuxBlockDevice<std::string::String> =
            LinuxBlockDevice::new(td.file_name(), PRINT_BLOCKS)
                .await
                .unwrap();
        let mut volume_mgr: VolumeManager<_, _, { super::MAX_DIRS }, { super::MAX_FILES }> =
            VolumeManager::new_with_limits(lbd, Clock, 100);
        let sd_future = sd_generic(&mut volume_mgr);

        const REPEAT_COUNTS: usize = super::helper::FILE_OPERATION_MULTIPLEXERS + 100;
        let testfile = "Test.txt";
        let (sd_result, second_result) =
            tokio::join!(sd_future, helper_write(REPEAT_COUNTS, testfile));

        assert_eq!(sd_result, Ok(()));
        second_result.unwrap();
        let m = super::FILE_OPERATION_STATISTIC.lock().await;
        // 1 Open
        // 110 Write
        // 1 Close
        // 1 Open
        // 1 Read
        // 1 Close
        // 1 STOP
        assert_eq!(m.len(), REPEAT_COUNTS + 6);
        let mut filename = String::new();
        filename.push_str("Test.txt").unwrap();
        assert_eq!(
            m[0].operation,
            super::Operation::Open((embedded_sdmmc::Mode::ReadWriteCreateOrAppend, filename))
        );
        assert_eq!(m[0].process_status, super::Status::Free);
        for i in 1..REPEAT_COUNTS + 1 {
            assert_eq!(m[i].process_status, super::Status::Free);
            if let super::Operation::Write(_) = m[i].operation {
            } else {
                assert!(false);
            }
        }

        let mut filename = String::new();
        filename.push_str("Test.txt").unwrap();
        assert_eq!(m[REPEAT_COUNTS + 2 - 1].process_status, super::Status::Free);
        assert_eq!(m[REPEAT_COUNTS + 2 - 1].operation, super::Operation::Close);
        assert_eq!(m[REPEAT_COUNTS + 3 - 1].process_status, super::Status::Free);
        assert_eq!(
            m[REPEAT_COUNTS + 3 - 1].operation,
            super::Operation::Open((embedded_sdmmc::Mode::ReadOnly, filename))
        );
        assert_eq!(m[REPEAT_COUNTS + 4 - 1].process_status, super::Status::Free);
        assert_eq!(m[REPEAT_COUNTS + 4 - 1].operation, super::Operation::Read);
        assert_eq!(m[REPEAT_COUNTS + 5 - 1].process_status, super::Status::Free);
        assert_eq!(m[REPEAT_COUNTS + 5 - 1].operation, super::Operation::Close);
        assert_eq!(m[REPEAT_COUNTS + 6 - 1].process_status, super::Status::Free);
        assert_eq!(m[REPEAT_COUNTS + 6 - 1].operation, super::Operation::STOP);
    }

    #[tokio::test]
    async fn test_write_nonexisting_folder() {
        super::FILE_OPERATION_STATISTIC.lock().await.clear();

        let td = get_test_disk(DISKNAME);

        let lbd: LinuxBlockDevice<std::string::String> =
            LinuxBlockDevice::new(td.file_name(), PRINT_BLOCKS)
                .await
                .unwrap();
        let mut volume_mgr: VolumeManager<_, _, { super::MAX_DIRS }, { super::MAX_FILES }> =
            VolumeManager::new_with_limits(lbd, Clock, 100);
        let sd_future = sd_generic(&mut volume_mgr);

        const REPEAT_COUNTS: usize = super::helper::FILE_OPERATION_MULTIPLEXERS + 100;
        let testfile = "NOEXIST/Test.txt"; // Folder does not exist
        let (sd_result, second_result) =
            tokio::join!(sd_future, helper_write(REPEAT_COUNTS, testfile));

        assert_eq!(sd_result, Ok(()));
        match second_result.unwrap_err() {
            embedded_sdmmc::Error::NotFound => (), // Expected
            _ => assert!(false),
        }
    }

    async fn read_subfolder(mut counter: usize) -> Result<(), ()> {
        let file = super::open("LOG/tst_sf.txt", embedded_sdmmc::Mode::ReadOnly)
            .await
            .map_err(|_| ())
            .unwrap();
        while counter > 0 {
            let m = super::read_operation(file).await.unwrap();
            let mut read = m.lock().await;
            assert!(read.result.is_ok());
            assert_eq!(read.process_status, super::Status::Occupied);
            assert_eq!(read.status, super::ReadStatus::Finished);
            let mut vec: Vec<u8, { super::CONTENT_BUFFER_SIZE }> = Vec::new();
            let reference = "This is a testfile to read\n\
                                    from a file in a subfolder\n";
            assert_eq!(read.content.len(), reference.len());

            read.content.clone_into(&mut vec);
            let s: String<{ super::CONTENT_BUFFER_SIZE }> =
                String::from_utf8(vec).map_err(|_| ())?;
            assert_eq!(s, reference);
            read.process_status = super::Status::Free;
            counter -= 1;
        }
        super::close(file).await;
        super::stop_operation().await.unwrap();
        Ok(())
    }

    #[tokio::test]
    async fn test_read_subfolder() {
        super::FILE_OPERATION_STATISTIC.lock().await.clear();
        // TODO: really needed to do all the time?
        let td = get_test_disk(DISKNAME);

        let lbd: LinuxBlockDevice<std::string::String> =
            LinuxBlockDevice::new(td.file_name(), PRINT_BLOCKS)
                .await
                .unwrap();
        let mut volume_mgr: VolumeManager<_, _, { super::MAX_DIRS }, { super::MAX_FILES }> =
            VolumeManager::new_with_limits(lbd, Clock, 100);
        let sd_future = sd_generic(&mut volume_mgr);

        const REPEAT_COUNTS: usize = super::helper::FILE_OPERATION_MULTIPLEXERS + 100;

        let (sd_result, second_result) = tokio::join!(sd_future, read_subfolder(REPEAT_COUNTS));

        assert_eq!(sd_result, Ok(()));
        assert_eq!(second_result, Ok(()));
        let m = super::FILE_OPERATION_STATISTIC.lock().await;
        assert_eq!(m.len(), REPEAT_COUNTS + 1 + 1 + 1); // First is Open, Last is the stop command
        let mut filename = String::new();
        filename.push_str("LOG/tst_sf.txt").unwrap();
        assert_eq!(
            m[0].operation,
            super::Operation::Open((embedded_sdmmc::Mode::ReadOnly, filename))
        );
        assert_eq!(m[0].process_status, super::Status::Free);
        for i in 1..REPEAT_COUNTS {
            assert_eq!(m[i].process_status, super::Status::Free);
            assert_eq!(m[i].operation, super::Operation::Read);
        }
        assert_eq!(m[REPEAT_COUNTS + 3 - 2].process_status, super::Status::Free);
        assert_eq!(m[REPEAT_COUNTS + 3 - 2].operation, super::Operation::Close);
        assert_eq!(m[REPEAT_COUNTS + 3 - 1].process_status, super::Status::Free);
        assert_eq!(m[REPEAT_COUNTS + 3 - 1].operation, super::Operation::STOP);
    }

    async fn create_folder(path: &str) -> Result<(), VolumeError> {
        let res = super::create_folders(path).await;
        if res.is_err() {
            super::stop_operation().await.unwrap();
            return res;
        }
        let mut s = StdString::from_str(path).unwrap();
        s.push_str("/Tfile.txt");

        // Check if the folder exists by creating a file in it
        let file = super::open(&s, embedded_sdmmc::Mode::ReadWriteCreateOrAppend)
            .await
            .unwrap();
        super::close(file).await;
        super::stop_operation().await.unwrap();
        Ok(())
    }

    #[tokio::test]
    async fn test_create_folder() {
        super::FILE_OPERATION_STATISTIC.lock().await.clear();
        // TODO: really needed to do all the time?
        let td = get_test_disk(DISKNAME);

        let lbd: LinuxBlockDevice<std::string::String> =
            LinuxBlockDevice::new(td.file_name(), PRINT_BLOCKS)
                .await
                .unwrap();
        let mut volume_mgr: VolumeManager<_, _, { super::MAX_DIRS }, { super::MAX_FILES }> =
            VolumeManager::new_with_limits(lbd, Clock, 100);
        let sd_future = sd_generic(&mut volume_mgr);

        let mut path: String<50> = String::new();
        path.push_str("LOG/Sub1/SubSub2").unwrap(); // LOG exists, but Sub1 and SubSub2 do not exist!

        let (sd_result, second_result) = tokio::join!(sd_future, create_folder(&path));
        assert_eq!(sd_result, Ok(()));
        second_result.unwrap();
    }

    #[tokio::test]
    async fn test_create_folder_already_existing() {
        super::FILE_OPERATION_STATISTIC.lock().await.clear();
        // TODO: really needed to do all the time?
        let td = get_test_disk(DISKNAME);

        let lbd: LinuxBlockDevice<std::string::String> =
            LinuxBlockDevice::new(td.file_name(), PRINT_BLOCKS)
                .await
                .unwrap();
        let mut volume_mgr: VolumeManager<_, _, { super::MAX_DIRS }, { super::MAX_FILES }> =
            VolumeManager::new_with_limits(lbd, Clock, 100);
        let sd_future = sd_generic(&mut volume_mgr);

        let mut path: String<50> = String::new();
        path.push_str("LOG").unwrap(); // LOG exists already

        let (sd_result, second_result) = tokio::join!(sd_future, create_folder(&path));
        assert_eq!(sd_result, Ok(()));
        second_result.unwrap();
    }

    // #########################################################################
    // Testhelpers
    // #########################################################################

    pub(crate) struct TestDisk {
        file_name: std::string::String,
    }

    impl TestDisk {
        pub fn new(file_name: &str) -> Self {
            const IMG_REL_PATH: &str = "sd/tests"; // Relative to this file
            let manifest_path = Path::new(env!("CARGO_MANIFEST_DIR"));
            let test_data_path = manifest_path.join(file!());
            let test_data_path = test_data_path.parent().unwrap().join(IMG_REL_PATH);
            let new_file_name = Path::new("/tmp/").join(format!(
                "{}{}",
                file_name,
                rand::random::<usize>().to_string()
            ));
            let test_file = test_data_path.join(file_name);
            if !path::Path::new(&test_file).exists() {
                panic!(
                    "The testfile does not exist. Did you forget to unpack? {:?}",
                    test_file.as_os_str()
                );
            }
            fs::copy(test_file, new_file_name.clone()).unwrap();

            TestDisk {
                file_name: std::string::String::from(new_file_name.as_path().to_str().unwrap()),
            }
        }

        pub fn file_name(&self) -> std::string::String {
            self.file_name.clone()
        }
    }

    impl Drop for TestDisk {
        fn drop(&mut self) {
            fs::remove_file(self.file_name.clone()).unwrap();
        }
    }

    pub(crate) fn get_test_disk(file_name: &str) -> TestDisk {
        TestDisk::new(file_name)
    }
}
