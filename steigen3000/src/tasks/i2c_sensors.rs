#[cfg(all(feature = "std"))]
compile_error!("This task cannot be used with std functionality!");

use crate::sensors::{
    self,
    lps22xx::{self, lps22hh::RawLPS22HH},
    lsm6dsl::{self, LSM6DSL},
    mmc5603nj::{self, MMC5603NJ},
};
use crate::{debug, error, trace};
use core::cell::RefCell;
use core::fmt::Debug;
use embassy_executor::Spawner;
use embassy_nrf::{
    bind_interrupts,
    gpio::{self, AnyPin},
    peripherals,
    twim::{self, Twim},
};
use embassy_sync::blocking_mutex::{raw::ThreadModeRawMutex, Mutex};
use embassy_sync::{blocking_mutex::raw::CriticalSectionRawMutex, channel::Channel};
use embassy_time::Delay;
use embassy_time::Timer;
use embedded_hal_async as hal;
use hal::i2c::I2c;

#[cfg(feature = "steigen31")]
use lps22xx::lps22hd::{self as lps22, LPS22HD as LPS22};
#[cfg(feature = "steigen33")]
use lps22xx::lps22hh::{self as lps22, LPS22HH as LPS22};

bind_interrupts!(struct Irqs {
    TWISPI0 => twim::InterruptHandler<peripherals::TWISPI0>;
});

#[cfg(feature = "steigen31")]
const LPS22XX_SA0: lps22xx::SA0 = lps22xx::SA0::High;
#[cfg(feature = "steigen33")]
const LPS22XX_SA0: lps22xx::SA0 = lps22xx::SA0::Low;

pub(crate) static PRESS_TEMP: Mutex<
    ThreadModeRawMutex,
    RefCell<Result<sensors::PressTemp, lps22xx::Error<twim::Error>>>,
> = Mutex::new(RefCell::new(Err(lps22xx::Error::new())));

pub(crate) static ACCEL_GYRO: Mutex<
    ThreadModeRawMutex,
    RefCell<Result<sensors::AccelGyro, lsm6dsl::Error<twim::Error>>>,
> = Mutex::new(RefCell::new(Err(lsm6dsl::Error::new())));

pub(crate) static MAGNETOMETER: Mutex<
    ThreadModeRawMutex,
    RefCell<Result<mmc5603nj::Value, mmc5603nj::Error<twim::Error>>>,
> = Mutex::new(RefCell::new(Err(mmc5603nj::Error::new())));

enum ReadyType {
    Barometer,
    Imu,
    Magnetometer,
}

static READY: Channel<CriticalSectionRawMutex, ReadyType, 3> = Channel::new();

#[embassy_executor::task]
async fn baro_ready_pin(int_rdy: AnyPin) {
    let mut rdy = gpio::Input::new(int_rdy, gpio::Pull::None);
    loop {
        // Internally interrupts are used
        rdy.wait_for_high().await; // Wait for next ready
        READY.send(ReadyType::Barometer).await;
    }
}

#[embassy_executor::task]
async fn imu_ready_pin(int_rdy: AnyPin) {
    let mut rdy = gpio::Input::new(int_rdy, gpio::Pull::None);
    loop {
        // Internally interrupts are used
        rdy.wait_for_high().await; // Wait for next ready
        READY.send(ReadyType::Imu).await;
    }
}

#[embassy_executor::task]
async fn magnetometer_ready(timeout_ms: u64) {
    loop {
        Timer::after_millis(timeout_ms).await;
        READY.send(ReadyType::Magnetometer).await;
    }
}

#[embassy_executor::task]
pub async fn i2c_sensors(
    spawner: Spawner,
    twi: peripherals::TWISPI0,
    sda: AnyPin,
    scl: AnyPin,
    int_rdy: AnyPin,
    int1_ag: AnyPin,
    _int2_ag: AnyPin,
) {
    const MAGNETOMETER_SAMPLE_RATE: f32 = 75.;
    if let Err(_) = spawner.spawn(baro_ready_pin(int_rdy)) {
        error!("Unable to spawn baro ready task");
    }

    if let Err(_) = spawner.spawn(imu_ready_pin(int1_ag)) {
        error!("Unable to spawn imu read task");
    }

    if let Err(_) = spawner.spawn(magnetometer_ready(
        ((1. / MAGNETOMETER_SAMPLE_RATE) * 1000.) as u64,
    )) {
        error!("Unable to spanw magnetometer ready task");
    }

    let config = twim::Config::default();
    let mut twi = Twim::new(twi, Irqs, sda, scl, config);
    let mut lps22 = initialize_lps22(&mut twi).await;

    let mut lsm6 = match LSM6DSL::new(&mut twi, false).await {
        Ok(dev) => dev,
        Err(e) => panic!(
            "Unable to access lps22xx: {:?}, {:?}",
            e.kind,
            e.source.unwrap()
        ), // TODO: handle properly! Do not let it crash
    };
    if let Err(_) = lsm6
        .configure_accelerometer(
            lsm6dsl::AccelerometerFrequency::F208Hz,
            lsm6dsl::AccelerometerScale::PM16G,
            lsm6dsl::AccelerometerBandwidth::F1_5kHz,
        )
        .await
    {
        error!("Unable to configure accelerometer");
    }

    if let Err(_) = lsm6
        .configure_gyroscope(
            lsm6dsl::GyroscopeFrequency::F208Hz,
            lsm6dsl::GyroscopeScale::F2000DPS,
        )
        .await
    {
        error!("Unable to configure gyroscope");
    }

    if let Err(_) = lsm6
        .set_interrupt_pad1(lsm6dsl::INT1_CTRL::INT1_DRDY_XL)
        .await
    {
        error!("Unable to configure interrup pad");
    }
    let lsm6 = lsm6.to_raw();

    let magnetometer = match MMC5603NJ::new_continuous(
        MAGNETOMETER_SAMPLE_RATE as usize,
        &mut twi,
        Delay {},
    )
    .await
    {
        Ok(mag) => Some(mag),
        Err(e) => {
            if let Some(e) = e.source {
                error!("Unable to initialize magnetometer: {}", e);
            } else {
                error!("Unable to initialize magnetometer");
            }
            None
        }
    };

    loop {
        match READY.receive().await {
            ReadyType::Barometer => {
                // TODO: if it does not already have!
                if let Some(lps22raw) = lps22 {
                    let mut lps22 = lps22raw.to_lps22hh(&mut twi);
                    match lps22.get_pressure_temperature().await {
                        Ok(tp) => {
                            PRESS_TEMP.lock(|f| {
                                let mut val = f.borrow_mut();
                                *val = Ok(tp);
                            });
                        }
                        Err(e) => {
                            PRESS_TEMP.lock(|f| {
                                let mut val = f.borrow_mut();
                                *val = Err(e)
                            });
                        }
                    };
                } else {
                    debug!("Reinitialize lps22");
                    lps22 = initialize_lps22(&mut twi).await;
                }
            }
            ReadyType::Imu => {
                let mut lsm6 = lsm6.to_lsm6sdl(&mut twi);
                match lsm6.read_gyro_acceleration().await {
                    Ok(ag) => ACCEL_GYRO.lock(|f| {
                        let mut val = f.borrow_mut();
                        *val = Ok(ag);
                    }),
                    Err(e) => {
                        ACCEL_GYRO.lock(|f| {
                            let mut val = f.borrow_mut();
                            *val = Err(e)
                        });
                    }
                }
            }
            ReadyType::Magnetometer => {
                if let Some(ref mag) = magnetometer {
                    let mut magnetometer = mag.clone().to_mmc5603nj(&mut twi);
                    match magnetometer.get_magnet_values(false).await {
                        Ok(mag) => MAGNETOMETER.lock(|f| {
                            trace!("Mag: ({}, {}, {})", mag.x, mag.y, mag.z);
                            let mut val = f.borrow_mut();
                            *val = Ok(mag);
                        }),
                        Err(e) => {
                            debug!("Unable to read values");
                            MAGNETOMETER.lock(|f| {
                                let mut val = f.borrow_mut();
                                *val = Err(e);
                            });
                        }
                    }
                }
            }
        }
    }
}

async fn initialize_lps22<Interface, E>(
    twi: &mut Interface,
) -> Option<RawLPS22HH<lps22xx::Continuous>>
where
    Interface: I2c<Error = E>,
    E: Debug,
{
    match LPS22::new_continuous(
        twi,
        LPS22XX_SA0,
        lps22::conf::OutputDataRate::F75Hz,
        lps22::conf::LowPassFilter::Disabled,
        lps22::conf::BlockDataUpdate::MSBLSBRead,
    )
    .await
    {
        Ok(lps22raw) => Some(lps22raw),
        Err(_e) => {
            error!("Unable to access lps22xx");
            None
        }
    }
}
