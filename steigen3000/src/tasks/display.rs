use crate::{interface, DisplayApply, DisplayImage, DisplayStateMutex};
use crate::{trace, DisplayBytes};
use base_widget::Draw;
use chrono::NaiveTime;
use core::fmt::Debug;
#[cfg(feature = "nrf")]
use embassy_nrf::{bind_interrupts, gpio, peripherals, spim};
#[cfg(feature = "embassy")]
use embassy_sync::{blocking_mutex::raw::CriticalSectionRawMutex, mutex::Mutex};
use embedded_graphics::primitives::PrimitiveStyleBuilder;
use embedded_graphics::{draw_target::DrawTarget, pixelcolor::BinaryColor};
use paraguidecore::logging::Timekeeper;
mod base_widget;
mod battery;
mod clock_widget;
mod compass;
mod fonts;
mod height_widget;
mod images;
mod text;
use fonts::Font;
mod vario_widget;
mod wind_widget;
use crate::common::Float;
use embedded_graphics::prelude::*;
use text::MonoText;
mod ls026b7dh01a;
pub use ls026b7dh01a::{DisplayWithBuffer as Display, Orientation, LS027B7DH01A};

#[cfg(feature = "steigen33")]
bind_interrupts!(struct Spi3Irqs {
    SPIM3 => spim::InterruptHandler<peripherals::SPI3>;
});

#[derive(Clone, Copy)]
pub enum Page {
    Empty,
    Init,
    USBMsc,
    Start,
    FlightMain,
    Thermal,
    Radio,
}

impl Page {
    const fn new() -> Self {
        Page::Empty
    }
}

#[derive(Clone)]
pub struct State {
    pub page: Page,
    pub check: Check,
    pub display_on: bool,
    pub time: Option<NaiveTime>,
    pub time_representation: clock_widget::TimeRepresentation,
    pub mute: bool,
    pub fix_available: bool,
    pub logging: bool,
    pub orientation_angle: Float, // Angle to north
    pub vario_value: Float,
    pub wind_speed: Float,
    pub wind_dir_abs: Float,
    pub height_msl: Float,
    pub battery_soc: Float,
}

#[derive(Clone, Copy)]
pub enum CheckState {
    NotDone,
    Pass,
    Fail,
}

#[derive(Clone, Copy)]
pub struct Check {
    pub baro: CheckState,
    pub mag: CheckState,
    pub accel: CheckState,
    pub sd: CheckState,
    pub paraguide_core: CheckState,
}

impl Check {
    const fn new() -> Self {
        Self {
            baro: CheckState::NotDone,
            mag: CheckState::NotDone,
            accel: CheckState::NotDone,
            sd: CheckState::NotDone,
            paraguide_core: CheckState::NotDone,
        }
    }
}

impl State {
    pub const fn new() -> Self {
        State {
            page: Page::new(),
            check: Check::new(),
            display_on: true,
            time: NaiveTime::from_hms_opt(0, 0, 0),
            time_representation: clock_widget::TimeRepresentation::Hour24,
            mute: false,
            fix_available: false,
            logging: false,
            orientation_angle: 0.0,
            vario_value: 0.0,
            wind_speed: 0.0,
            wind_dir_abs: 0.0,
            height_msl: 99999.,
            battery_soc: 0.0,
        }
    }
}

#[cfg(feature = "steigen33")]
pub(crate) static STATE: Mutex<CriticalSectionRawMutex, State> = Mutex::new(State::new());

#[cfg(feature = "steigen33")]
#[embassy_executor::task]
pub async fn display(
    spi: peripherals::SPI3,
    sck: gpio::AnyPin,
    cs: gpio::AnyPin,
    mosi: gpio::AnyPin,
) {
    use embassy_nrf::spim;

    let mut config = spim::Config::default();
    config.bit_order = spim::BitOrder::LSB_FIRST;
    config.frequency = spim::Frequency::M2;
    config.mode = spim::MODE_0;
    let spi = spim::Spim::new_txonly(spi, Spi3Irqs, sck, mosi, config);

    let cs_pin = gpio::Output::new(cs, gpio::Level::Low, gpio::OutputDrive::Standard);
    let display = LS027B7DH01A::new(spi, cs_pin);

    let mut display = Display::new(display, Orientation::Degree180);

    display_common(&mut display, &STATE).await
}

pub async fn display_common<
    E: Debug,
    E2: Debug,
    Display: DrawTarget<Color = BinaryColor, Error = E2>
        + DisplayApply<E>
        + DisplayImage<E>
        + DisplayBytes<E>,
>(
    display: &mut Display,
    state: &DisplayStateMutex,
) -> ! {
    let character_style_small = fonts::FIRA_SANS_REGULAR_14;
    let character_style_title = fonts::FIRA_SANS_REGULAR_26;
    let character_style_header = fonts::FIRA_SANS_REGULAR_26;
    let character_style_value = fonts::FIRA_SANS_REGULAR_53;

    let mut vario_widget = vario_widget::Vario::new(
        Point::new(0, 50),
        &character_style_title,
        &character_style_value,
    );

    let mut wind_widget = wind_widget::WindWidget::new(
        Point::new(0, 140),
        &character_style_title,
        &character_style_value,
        &character_style_small,
        //wind_widget::WindDirRepresenation::CardinalRepresentation,
    );

    let mut height_widget_msl = height_widget::HeightWidget::new(
        Point::new(100, 50),
        &character_style_value,
        &character_style_small,
        "msl",
    );

    let mut battery_widget = battery::Battery::new(Point::new(0, 0));
    battery_widget.set_character_style(&character_style_title);
    battery_widget.set_position(Point::new(
        (display.bounding_box().size.width as i32
            - battery_widget.width() as i32) // 100%
            / 8
            * 8,
        0,
    ));

    let _bounding_rect_style = PrimitiveStyleBuilder::new()
        .stroke_color(BinaryColor::On)
        .stroke_width(1)
        .build();

    let mut time_s = 0.0;
    let mut end_time;
    let mut start_time;

    loop {
        start_time = interface::TimeKeeper::get_time_us();
        trace!("Display Start");

        let lock = state.lock().await;
        let state = lock.clone();
        drop(lock);

        let _ = display.clear(BinaryColor::Off);

        if !state.display_on {
            display.apply().await.unwrap();
            continue;
        }

        // Update all widgets
        battery_widget.update(time_s, state.battery_soc);
        wind_widget.update(state.wind_speed, state.wind_dir_abs);
        height_widget_msl.update(state.height_msl);
        vario_widget.update(state.vario_value);

        match state.page {
            Page::Empty => (),
            Page::USBMsc => update_usb_msc(&character_style_value, display),
            Page::Init => update_page_init(&state, &character_style_title, display),
            Page::Start => update_page_start(
                &mut height_widget_msl,
                &mut battery_widget,
                &state,
                &character_style_header,
                display,
            ),
            Page::FlightMain => update_page_main(
                &mut vario_widget,
                &mut wind_widget,
                &mut height_widget_msl,
                &mut battery_widget,
                &state,
                &character_style_header,
                display,
            ),
            Page::Thermal => update_page_thermal(&state, display),
            Page::Radio => update_page_radio(&state, display),
        }

        trace!("Display buffer write end");

        display.apply().await.unwrap();

        trace!("Display end");

        end_time = interface::TimeKeeper::get_time_us();
        const LOOP_TIME_S: Float = 0.2;
        let diff = (LOOP_TIME_S * 1_000_000.) as i64 - (end_time - start_time) as i64;
        if diff > 0 {
            interface::sleep_us(diff as u64).await;
        }
        time_s += LOOP_TIME_S;
    }
}

fn draw_header<'a, E, E2, Display>(
    state: &State,
    battery: &mut battery::Battery<'a>,
    character_style: &'a Font<'a>,
    display: &mut Display,
) where
    E: Debug,
    E2: Debug,
    Display: DrawTarget<Color = BinaryColor, Error = E2> + DisplayApply<E> + DisplayImage<E>,
{
    battery.set_character_style(character_style);
    let _ = battery.draw(display);

    // Speaker
    let x_pos = display.bounding_box().size.width as usize
        - images::MUTE[0].len() * 8
        - 4
        - battery.width() as usize;
    let mut x_pos = (x_pos / 8) * 8;
    if state.mute {
        let _ = display.draw_image(x_pos, 0, &images::MUTE);
    } else {
        let _ = display.draw_image(x_pos, 0, &images::SPEAKER);
    }

    // GPS Fix
    x_pos = ((x_pos - &images::GLOBE[0].len() * 8 - 5) / 8) * 8;
    if state.fix_available {
        let _ = display.draw_image(x_pos, 0, &images::GLOBE);
    }

    // SD Logging
    x_pos = ((x_pos - &images::LOGGING[0].len() * 8 - 5) / 8) * 8;
    if state.logging {
        let _ = display.draw_image(x_pos, 0, &images::LOGGING);
    }

    let mut clock_widget =
        clock_widget::Clock::new(Point::new(0, 0), state.time_representation, character_style);
    clock_widget.update(state.time);
    let _ = clock_widget.draw(display);
}

pub fn update_page_init<
    'a,
    E: Debug,
    E2: Debug,
    Display: DrawTarget<Color = BinaryColor, Error = E2> + DisplayApply<E> + DisplayImage<E>,
>(
    state: &State,
    character_style: &'a Font<'a>,
    display: &mut Display,
) {
    let x = 16;
    let mut y = 20 + character_style.total_height as i32;
    let _ = draw_text(
        Point::new(x, y),
        "SD Card:",
        state.check.sd,
        character_style,
        display,
    );
    y += character_style.total_height as i32;
    let _ = draw_text(
        Point::new(x, y),
        "Barometer:",
        state.check.baro,
        character_style,
        display,
    );
    y += character_style.total_height as i32;
    let _ = draw_text(
        Point::new(x, y),
        "Magnetometer:",
        state.check.mag,
        character_style,
        display,
    );
    y += character_style.total_height as i32;
    let _ = draw_text(
        Point::new(x, y),
        "Accelerometer:",
        state.check.accel,
        character_style,
        display,
    );
    y += character_style.total_height as i32;
    let _ = draw_text(
        Point::new(x, y),
        "Paraguide Core:",
        state.check.paraguide_core,
        character_style,
        display,
    );
    // y += character_style.total_height as i32;
}

fn update_usb_msc<
    'a,
    E: Debug,
    E2: Debug,
    Display: DrawTarget<Color = BinaryColor, Error = E2> + DisplayApply<E> + DisplayImage<E>,
>(
    character_style: &'a Font<'a>,
    display: &mut Display,
) {
    let _ = MonoText::new("USB Data", Point::new(8, 120), character_style).draw(display);
}

fn draw_text<
    'a,
    E: Debug,
    E2: Debug,
    Display: DrawTarget<Color = BinaryColor, Error = E2>
        + DisplayApply<E>
        + DisplayImage<E>
        + DisplayBytes<E>,
>(
    position: Point,
    text: &str,
    check_state: CheckState,
    character_style: &'a Font<'a>,
    display: &mut Display,
) -> Result<Point, ()> {
    if let Ok(p) = MonoText::new(text, position, character_style).draw(display) {
        match check_state {
            CheckState::NotDone => MonoText::new(" Not Done", p, character_style).draw(display),
            CheckState::Fail => MonoText::new(" Fail", p, character_style).draw(display),
            CheckState::Pass => MonoText::new(" Pass", p, character_style).draw(display),
        }
        .map_err(|_| ())
    } else {
        Err(())
    }
}

pub fn update_page_start<
    'a,
    E: Debug,
    E2: Debug,
    Display: DrawTarget<Color = BinaryColor, Error = E2> + DisplayApply<E> + DisplayImage<E>,
>(
    height_widget_msl: &mut height_widget::HeightWidget<'a>,
    battery_widget: &mut battery::Battery<'a>,
    state: &State,
    character_style: &'a Font<'a>,
    display: &mut Display,
) {
    // TODO: show temperature

    height_widget_msl.set_position(Point::new(0, 0));
    let _ = height_widget_msl.draw(display);
    draw_header(&state, battery_widget, character_style, display);
}

pub fn update_page_main<
    'a,
    E: Debug,
    E2: Debug,
    Display: DrawTarget<Color = BinaryColor, Error = E2> + DisplayApply<E> + DisplayImage<E>,
>(
    vario_widget: &mut vario_widget::Vario<'a>,
    wind_widget: &mut wind_widget::WindWidget<'a>,
    height_widget_msl: &mut height_widget::HeightWidget<'a>,
    battery_widget: &mut battery::Battery<'a>,
    state: &State,
    character_style: &'a Font<'a>,
    display: &mut Display,
) {
    // vario_widget.draw_bounding_rect(&bounding_rect_style);
    vario_widget.set_position(Point::new(0, 50));
    let _ = vario_widget.draw(display);

    // wind_widget.draw_bounding_rect(&bounding_rect_style);
    wind_widget.set_position(Point::new(0, 128));
    let _ = wind_widget.draw(display);

    // height_widget_msl.draw_bounding_rect(&bounding_rect_style);
    height_widget_msl.set_position(Point::new(152, 130));
    let _ = height_widget_msl.draw(display);

    draw_header(&state, battery_widget, character_style, display);

    compass::Compass::new(state.orientation_angle).draw(200, 100, 100, 100, display);
}

pub fn update_page_thermal<
    E: Debug,
    E2: Debug,
    Display: DrawTarget<Color = BinaryColor, Error = E2> + DisplayApply<E> + DisplayImage<E>,
>(
    _state: &State,
    _display: &mut Display,
) {
}

pub fn update_page_radio<
    E: Debug,
    E2: Debug,
    Display: DrawTarget<Color = BinaryColor, Error = E2> + DisplayApply<E> + DisplayImage<E>,
>(
    _state: &State,
    _display: &mut Display,
) {
}

pub async fn display_test_battery<
    E: Debug,
    E2: Debug,
    Display: DrawTarget<Color = BinaryColor, Error = E2> + DisplayApply<E> + DisplayImage<E>,
>(
    display: &mut Display,
) -> ! {
    let battery_height: u32 = 16;
    let mut batt_num = 0;
    let mut battery_widget_100 =
        battery::Battery::new(Point::new(20, batt_num * battery_height as i32 + 10));
    batt_num += 1;

    let mut battery_widget_75 =
        battery::Battery::new(Point::new(20, batt_num * battery_height as i32 + 10));
    batt_num += 1;

    let mut battery_widget_50 =
        battery::Battery::new(Point::new(20, batt_num * battery_height as i32 + 10));
    batt_num += 1;

    let mut battery_widget_25 =
        battery::Battery::new(Point::new(20, batt_num * battery_height as i32 + 10));
    batt_num += 1;

    let mut battery_widget_10 =
        battery::Battery::new(Point::new(20, batt_num * battery_height as i32 + 10));
    batt_num += 1;

    let mut battery_widget_4 =
        battery::Battery::new(Point::new(20, batt_num * battery_height as i32 + 10));

    let mut time_s = 0.0;

    loop {
        interface::sleep_us(100_000).await;
        time_s += 0.1;

        let _ = display.clear(BinaryColor::Off);

        battery_widget_100.update(time_s, 100.);
        let _ = battery_widget_100.draw(display);
        battery_widget_75.update(time_s, 75.);
        let _ = battery_widget_75.draw(display);
        battery_widget_50.update(time_s, 50.);
        let _ = battery_widget_50.draw(display);
        battery_widget_25.update(time_s, 25.);
        let _ = battery_widget_25.draw(display);
        battery_widget_10.update(time_s, 10.);
        let _ = battery_widget_10.draw(display);
        battery_widget_4.update(time_s, 4.);
        let _ = battery_widget_4.draw(display);

        display.apply().await.unwrap();
    }
}
