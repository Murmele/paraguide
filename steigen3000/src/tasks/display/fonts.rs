#[allow(unused)]
pub struct Font<'a> {
    /// Font size
    pub size: usize,
    /// number of lines from the baseline to the top
    pub typo_ascent: isize,
    /// number of lines from the baseline to the bottom
    pub typo_descent: isize,
    /// Maximum height of the bitmap. (type_ascent - typo_descent)
    pub total_height: usize,
    /// If a character cannot be found, use this character
    pub default_code: usize,
    /// The start character code
    pub start_code: usize,
    glyphs: &'a [GlyphFont<'a>],
}

#[allow(unused)]
pub struct GlyphFont<'a> {
    pub code: usize,
    /// Width of a the character
    pub width: usize,
    /// 0 means baseline
    pub y_start: isize,
    /// 0 means left border
    pub x_start: isize,
    /// number of bytes per row
    pub line_bytes: usize,
    pub data: &'a [u8],
}

impl<'a> Font<'a> {
    pub fn glyph(&self, character: char) -> &GlyphFont<'a> {
        let index = character as usize - self.start_code;
        if index >= self.glyphs.len() {
            &self.glyphs[self.default_code]
        } else {
            &self.glyphs[index]
        }
    }
}

include!(concat!(
    env!("CARGO_MANIFEST_DIR"),
    "/ui/fonts/Fira_Sans/FIRA_SANS_REGULAR_14.rs"
));

include!(concat!(
    env!("CARGO_MANIFEST_DIR"),
    "/ui/fonts/Fira_Sans/FIRA_SANS_REGULAR_26.rs"
));

include!(concat!(
    env!("CARGO_MANIFEST_DIR"),
    "/ui/fonts/Fira_Sans/FIRA_SANS_REGULAR_53.rs"
));

// include!(concat!(
//     env!("CARGO_MANIFEST_DIR"),
//     "/ui/fonts/Fira_Mono/FIRA_MONO_REGULAR_14.rs"
// ));

// include!(concat!(
//     env!("CARGO_MANIFEST_DIR"),
//     "/ui/fonts/Fira_Mono/FIRA_MONO_REGULAR_26.rs"
// ));

// include!(concat!(
//     env!("CARGO_MANIFEST_DIR"),
//     "/ui/fonts/Fira_Mono/FIRA_MONO_REGULAR_53.rs"
// ));
