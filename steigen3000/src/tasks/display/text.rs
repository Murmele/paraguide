use super::fonts::Font;
use crate::{trace, DisplayBytes};
use core::fmt::Debug;
use embedded_graphics::prelude::*;

pub struct MonoText<'a, S> {
    /// The string.
    text: &'a str,

    /// The position.
    position: Point,

    /// The character style.
    character_style: &'a S,
}

impl<'a> MonoText<'a, Font<'a>> {
    pub fn new(text: &'a str, position: Point, character_style: &'a Font<'a>) -> Self {
        Self {
            text,
            position,
            character_style,
        }
    }

    pub fn draw<D, E>(&self, target: &mut D) -> Result<Point, E>
    where
        D: DisplayBytes<E>,
        E: Debug,
    {
        let mut next_position = self.position;

        let base_line = self.position.y; // + (self.character_style.typo_ascent as i32).abs();

        for character in self.text.chars() {
            let g = self.character_style.glyph(character);
            trace!("Character: {}", (g.code as u8) as char);

            let mut line = base_line - g.y_start as i32; // start line
            let mut index: i32 = 0;
            for data in g.data {
                if index % g.line_bytes as i32 == 0 && index > 0 {
                    line -= 1; // we draw from bottom to top
                }
                target.draw_byte(
                    next_position.x + g.x_start as i32 + (index % (g.line_bytes as i32)) * 8,
                    line,
                    *data,
                )?;
                index += 1;
            }
            next_position.x += g.width as i32;
        }
        Ok(next_position)
    }
}
