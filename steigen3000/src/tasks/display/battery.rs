use super::base_widget::*;
use super::fonts::Font;
use super::images;
use super::text::MonoText;
use crate::common::Float;
use embedded_graphics::{pixelcolor::BinaryColor, prelude::*};
use heapless::String;
use paraguidecore::helper::unum_to_string_fill_character;

pub struct Battery<'a> {
    position: Point,
    soc: Float,

    blink_time: Float,
    blinking_on: bool,

    character_style: Option<&'a Font<'a>>,
}

impl<'a> Battery<'a> {
    pub fn new(position: Point) -> Self {
        Self {
            position,
            soc: 0.0,
            blink_time: 0.0,
            blinking_on: true,

            character_style: None,
        }
    }

    pub fn set_position(&mut self, position: Point) {
        self.position = position;
    }

    pub fn width(&self) -> usize {
        let mut width = images::BATTERY_100[0].len() * 8;
        if let Some(_style) = self.character_style {
            width += /*style.width*/15 * 4;
        }
        width
    }

    pub fn set_character_style(&mut self, character_style: &'a Font<'a>) {
        self.character_style = Some(character_style);
    }

    pub fn update(&mut self, time_s: Float, soc: Float) {
        const BLINK_PERIOD_S: Float = 1.;

        self.soc = soc;

        if time_s - self.blink_time > BLINK_PERIOD_S {
            self.blink_time = time_s;
            self.blinking_on = !self.blinking_on;
        }
    }
}

impl<'a, E, E2, Display> Draw<E, E2, Display> for Battery<'a>
where
    E: Debug,
    E2: Debug,
    Display: DrawTarget<Color = BinaryColor, Error = E2> + DisplayImage<E>,
{
    fn draw(&self, display: &mut Display) -> Result<(), ()> {
        const BLINK_THRESHOLD: Float = 5.;

        let image;
        if self.soc >= 75. {
            image = &images::BATTERY_100;
        } else if self.soc >= 50. {
            image = &images::BATTERY_75;
        } else if self.soc >= 25. {
            image = &images::BATTERY_50;
        } else if self.soc >= BLINK_THRESHOLD {
            image = &images::BATTERY_25;
        } else {
            // Blinking
            if self.blinking_on {
                image = &images::BATTERY_25;
            } else {
                image = &images::BATTERY_0;
            }
        }

        let mut x = self.position.x;

        if let Some(character_style) = self.character_style {
            let mut string: String<4> = String::new(); // 100%
            let _ = unum_to_string_fill_character(self.soc as u32, 3, ' ', &mut string);
            let _ = string.push('%');
            let p = MonoText::new(
                &string,
                Point::new(x, self.position.y + character_style.typo_ascent as i32),
                character_style,
            )
            .draw(display)
            .map_err(|_| ())?;
            x = p.x;
        }

        display
            .draw_image(x as usize + 8, self.position.y as usize, image)
            .map_err(|_| ())?;
        Ok(())
    }
}
