use super::base_widget::*;
use super::fonts::Font;
use super::text::MonoText;
use chrono::{NaiveTime, Timelike};
use embedded_graphics::{pixelcolor::BinaryColor, prelude::*};
use heapless::String;
pub use paraguidecore::common::TimeRepresentation;
use paraguidecore::helper::{unum_to_string, unum_to_string_fill_character};

pub struct Clock<'a> {
    position: Point,
    character_style: &'a Font<'a>,
    time: Option<NaiveTime>,
    representation: TimeRepresentation,
}

impl<'a> Clock<'a> {
    pub fn new(
        position: Point,
        representation: TimeRepresentation,
        character_style: &'a Font<'a>,
    ) -> Self {
        Self {
            position,
            representation,
            character_style,
            time: NaiveTime::from_hms_opt(0, 0, 0),
        }
    }

    pub fn update(&mut self, time: Option<NaiveTime>) {
        self.time = time;
    }

    // pub fn width(&self) -> u32 {
    //     match self.representation {
    //         TimeRepresentation::Hour24 => self.character_style.font.character_size.width * 5,
    //         TimeRepresentation::Hour12 => self.character_style.font.character_size.width * 7,
    //     }
    // }
}

impl<'a, E, E2, Display> Draw<E, E2, Display> for Clock<'a>
where
    E: Debug,
    E2: Debug,
    Display: DrawTarget<Color = BinaryColor, Error = E2> + DisplayImage<E>,
{
    fn draw(&self, display: &mut Display) -> Result<(), ()> {
        let time = self.time.unwrap_or_default();

        let mut string: String<7> = String::new();
        match self.representation {
            TimeRepresentation::Hour24 => {
                // 5 characters
                let _ = unum_to_string_fill_character(time.hour(), 2, ' ', &mut string);
                let _ = string.push(':');
                let _ = unum_to_string(time.minute(), 2, &mut string);
            }
            TimeRepresentation::Hour12 => {
                // 7 characters
                let hour = time.hour12();
                let _ = unum_to_string_fill_character(hour.1, 0, ' ', &mut string);
                let _ = string.push(':');
                let _ = unum_to_string(time.minute(), 2, &mut string);
                if hour.0 {
                    let _ = string.push_str("PM")?;
                } else {
                    let _ = string.push_str("AM")?;
                }
            }
        }

        MonoText::new(
            &string,
            Point::new(
                self.position.x,
                self.position.y + self.character_style.typo_ascent as i32,
            ),
            self.character_style,
        )
        .draw(display)
        .map_err(|_| ())?;
        Ok(())
    }
}
