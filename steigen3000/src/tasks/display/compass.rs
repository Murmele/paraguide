use crate::common::Float;
#[cfg(not(feature = "std"))]
use core::f32::consts;
#[cfg(feature = "std")]
use core::f64::consts;

use core::fmt::Debug;
use embedded_graphics::prelude::Point;
use embedded_graphics::prelude::Primitive;
use embedded_graphics::Drawable;
use embedded_graphics::{
    draw_target::DrawTarget,
    pixelcolor::BinaryColor,
    primitives::{PrimitiveStyle, Triangle},
};
use num_traits;

pub struct Compass {
    angle: Float,
}

impl Compass {
    pub fn new(angle: Float) -> Self {
        Compass { angle }
    }

    pub fn draw<D, E>(
        &mut self,
        pos_x: i32,
        pos_y: i32,
        width: usize,
        height: usize,
        display: &mut D,
    ) where
        D: DrawTarget<Color = BinaryColor, Error = E>,
        E: Debug,
    {
        let radius = usize::min(width, height) as Float / 2.; // Tip length

        let angle = (self.angle - 90.) * consts::PI / 180.;

        let x = num_traits::Float::cos(angle) * radius;
        let y = num_traits::Float::sin(angle) * radius;

        let w = 10.;
        let x_b1 = (num_traits::Float::cos(angle - consts::PI / 2.) * w) as i32;
        let y_b1 = (num_traits::Float::sin(angle - consts::PI / 2.) * w) as i32;

        let p1 = Point::new(pos_x + x_b1, pos_y + y_b1);
        let p2 = Point::new(pos_x - x_b1, pos_y as i32 - y_b1);

        let _ = Triangle::new(p1, Point::new(pos_x + x as i32, pos_y + y as i32), p2)
            .into_styled(PrimitiveStyle::with_fill(BinaryColor::On))
            .draw(display);

        let _ = Triangle::new(p1, Point::new(pos_x - x as i32, pos_y - y as i32), p2)
            .into_styled(PrimitiveStyle::with_stroke(BinaryColor::On, 1))
            .draw(display);
    }
}
