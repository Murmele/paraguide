include!(concat!(env!("CARGO_MANIFEST_DIR"), "/ui/icons/mute.svg.rs"));
include!(concat!(
    env!("CARGO_MANIFEST_DIR"),
    "/ui/icons/speaker.svg.rs"
));
include!(concat!(
    env!("CARGO_MANIFEST_DIR"),
    "/ui/icons/globe.svg.rs"
));

include!(concat!(
    env!("CARGO_MANIFEST_DIR"),
    "/ui/icons/logging.svg.rs"
));

include!(concat!(
    env!("CARGO_MANIFEST_DIR"),
    "/ui/icons/windsock.svg.rs"
));

include!(concat!(
    env!("CARGO_MANIFEST_DIR"),
    "/ui/icons/battery_100.svg.rs"
));

include!(concat!(
    env!("CARGO_MANIFEST_DIR"),
    "/ui/icons/battery_75.svg.rs"
));

include!(concat!(
    env!("CARGO_MANIFEST_DIR"),
    "/ui/icons/battery_50.svg.rs"
));

include!(concat!(
    env!("CARGO_MANIFEST_DIR"),
    "/ui/icons/battery_25.svg.rs"
));

include!(concat!(
    env!("CARGO_MANIFEST_DIR"),
    "/ui/icons/battery_0.svg.rs"
));
