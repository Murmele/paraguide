use super::base_widget::*;
use super::fonts::Font;
use super::text::MonoText;
use crate::common::Float;
use crate::error;
use crate::paraguidecore::helper::float_to_string_2_char;
use embedded_graphics::primitives::PrimitiveStyle;
use embedded_graphics::{
    pixelcolor::BinaryColor,
    prelude::*,
    primitives::{Rectangle, StyledDrawable},
};
use heapless::String;

pub struct Vario<'a> {
    position: Point,
    character_style_title: &'a Font<'a>,
    character_style_value: &'a Font<'a>,
    vario: Float,

    bounding_rect_style: Option<&'a PrimitiveStyle<BinaryColor>>,
}

impl<'a> Vario<'a> {
    pub fn new(
        position: Point,
        character_style_title: &'a Font<'a>,
        character_style_value: &'a Font<'a>,
    ) -> Self {
        Self {
            position,
            character_style_title,
            character_style_value,
            vario: 0.0,

            bounding_rect_style: None,
        }
    }

    pub fn set_position(&mut self, position: Point) {
        self.position = position;
    }

    pub fn update(&mut self, vario: Float) {
        self.vario = vario;
    }

    #[allow(unused)]
    pub fn draw_bounding_rect(&mut self, style: &'a PrimitiveStyle<BinaryColor>) {
        self.bounding_rect_style = Some(style);
    }
}

impl<'a, E, E2, Display> Draw<E, E2, Display> for Vario<'a>
where
    E: Debug,
    E2: Debug,
    Display: DrawTarget<Color = BinaryColor, Error = E2> + DisplayImage<E>,
{
    fn draw(&self, display: &mut Display) -> Result<(), ()> {
        let mut vario_value = Float::min(99.0, self.vario);
        vario_value = Float::max(-99.0, vario_value);

        const NUMBER_CHAR: usize = 5;
        // let total_width = self.character_style_value.width * NUMBER_CHAR;
        let mut vario_value_string: String<{ NUMBER_CHAR as usize }> = String::new(); // (1 sign, 2digits, 1 comma, 1 digit)
        if let Err(_) = float_to_string_2_char(vario_value, 1, &mut vario_value_string) {
            vario_value_string.clear();
            error!("Value cannot be converted: {}", vario_value);
        }

        let mut y = self.position.y + self.character_style_title.typo_ascent as i32;

        let x_center = self.position.x; // + (total_width / 2) as i32;

        MonoText::new("Vario", Point::new(x_center, y), self.character_style_title)
            .draw(display)
            .map_err(|_| ())?;

        // Height checked manually. Seems to be that in the data there is some white spaces
        // Count white characters above and below the character in the bdf file to get correct value
        y += self.character_style_value.typo_ascent as i32;

        let pos = MonoText::new(
            &vario_value_string,
            Point::new(self.position.x, y),
            self.character_style_value,
        )
        .draw(display)
        .map_err(|_| ())?;

        if let Some(style) = self.bounding_rect_style {
            let _ = Rectangle::new(
                self.position,
                Size::new(
                    (pos.x - self.position.x) as u32, // "Vario" has 5 characters
                    35 + 5 + 14,
                ),
            )
            .draw_styled(style, display)
            .map_err(|_| ())?;
        }

        Ok(())
    }
}
