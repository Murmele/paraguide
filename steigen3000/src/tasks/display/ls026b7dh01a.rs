pub use crate::actuators::display::ls027b7dh01a::Orientation;
pub use crate::actuators::display::ls027b7dh01a::LS027B7DH01A;
use crate::{error, warn};
use crate::{DisplayApply, DisplayBytes, DisplayImage};
use core::fmt::Debug;
use embedded_graphics::geometry::{Point, Size};
use embedded_graphics::prelude::*;
use embedded_graphics::primitives::Rectangle;
use embedded_graphics::{draw_target::DrawTarget, geometry::Dimensions, pixelcolor::BinaryColor};
use embedded_hal::digital::OutputPin;
use embedded_hal_async::spi::SpiBus;

#[derive(Debug)]
pub enum Error {
    NotByteAligned,
}

pub struct DisplayWithBuffer<Interface, CS> {
    buffer: [u8; LS027B7DH01A::BUFFER_LINE_SIZE_BYTE_MULTI_LINE * LS027B7DH01A::NUMBER_LINES
        + LS027B7DH01A::NUM_DUMMY_BYTES],
    display: LS027B7DH01A<Interface, CS>,
    orientation: Orientation,
}

impl<Interface, CS, E> DisplayWithBuffer<Interface, CS>
where
    Interface: SpiBus<Error = E>,
    E: Debug,
    CS: OutputPin,
{
    pub fn new(display: LS027B7DH01A<Interface, CS>, orientation: Orientation) -> Self {
        Self {
            buffer: [LS027B7DH01A::ALL_OFF;
                LS027B7DH01A::BUFFER_LINE_SIZE_BYTE_MULTI_LINE * LS027B7DH01A::NUMBER_LINES
                    + LS027B7DH01A::NUM_DUMMY_BYTES],
            display,
            orientation,
        }
    }

    fn draw_image_byte_aligned<const WIDTH: usize, const HEIGHT: usize>(
        &mut self,
        x: usize,
        y: usize,
        image: &[[u8; WIDTH]; HEIGHT],
    ) -> Result<(), E> {
        if x % 8 != 0 {
            error!("draw_image: not byte aligned");
            return Ok(()); //Err(Error::NotByteAligned);
        }

        for (row_idx, row_data) in image.into_iter().enumerate() {
            for (col_idx, data) in row_data.into_iter().enumerate() {
                let line = y + row_idx;
                self.draw_byte((x + col_idx * 8) as i32, line as i32, *data)?;
            }
        }

        Ok(())
    }
}

impl<Interface, CS, E> DisplayApply<E> for DisplayWithBuffer<Interface, CS>
where
    Interface: SpiBus<Error = E>,
    E: Debug,
    CS: OutputPin,
{
    async fn apply(&mut self) -> Result<(), E> {
        self.display.write_complete(&mut self.buffer).await
    }
}

impl<Interface, CS, E> DisplayBytes<E> for DisplayWithBuffer<Interface, CS>
where
    Interface: SpiBus<Error = E>,
    E: Debug,
    CS: OutputPin,
{
    fn draw_byte(&mut self, x: i32, y: i32, byte: u8) -> Result<(), E> {
        if x % 8 != 0 {
            error!("draw_byte(): Position is not aligned!");
            return Ok(());
        }
        if y < 0 {
            error!("draw_byte(): y is negative");
            return Ok(());
        }

        let x_index = x as usize / 8;
        if x_index > LS027B7DH01A::PIXEL_HORIZONTAL {
            warn!("draw_byte: x index larger than PIXEL_HORIZONTAL");
            return Ok(());
        }
        let index = match self.orientation {
            Orientation::Degree0 => {
                y as usize * LS027B7DH01A::BUFFER_LINE_SIZE_BYTE_MULTI_LINE
                    + LS027B7DH01A::NUM_DUMMY_BYTES
                    + x_index
            }
            Orientation::Degree180 => {
                if y as usize > LS027B7DH01A::NUMBER_LINES {
                    warn!("draw_byte: y negative");
                    return Ok(());
                }
                // let y = 0;
                let line = LS027B7DH01A::NUMBER_LINES - y as usize - 1; // -1, because it is an index
                let mut index = line * LS027B7DH01A::BUFFER_LINE_SIZE_BYTE_MULTI_LINE
                    + LS027B7DH01A::NUM_DUMMY_BYTES;

                if x as usize > LS027B7DH01A::PIXEL_HORIZONTAL - 1 {
                    warn!("draw_byte: x too large: {}", x);
                    return Ok(());
                }
                let pixel = (LS027B7DH01A::PIXEL_HORIZONTAL - x as usize - 1) / 8; // -1, because it is an index
                index += pixel;
                index
            }
        };
        if index >= self.buffer.len() {
            warn!("draw_byte: Buffer overflow prevention");
            return Ok(());
        }
        self.buffer[index] = byte;

        Ok(())
    }
}

impl<Interface, CS, E> DisplayImage<E> for DisplayWithBuffer<Interface, CS>
where
    Interface: SpiBus<Error = E>,
    E: Debug,
    CS: OutputPin,
{
    fn draw_image<const WIDTH: usize, const HEIGHT: usize>(
        &mut self,
        x: usize,
        y: usize,
        image: &[[u8; WIDTH]; HEIGHT],
    ) -> Result<(), E> {
        // We do not support random positioning, because it is really inefficient!

        // Faster, because bytewise can be written to the buffer
        self.draw_image_byte_aligned(x, y, image)

        // match self.orientation {
        //     Orientation::Degree0 => {
        //         for (row_idx, row_data) in image.into_iter().enumerate() {
        //             let line = y + row_idx;
        //             let index = line * LS027B7DH01A::BUFFER_LINE_SIZE_BYTE_MULTI_LINE
        //                 + LS027B7DH01A::NUM_DUMMY_BYTES;
        //             for (col_idx, data) in row_data.into_iter().enumerate() {
        //                 for pixel in 0..8 {
        //                     let x_byte_index = (x + col_idx * 8 + pixel) / 8;
        //                     let x_bit_idx = (x + col_idx * 8 + pixel) % 8;
        //                     let value = (data >> pixel) & 1;
        //                     if value == LS027B7DH01A::PIXEL_ON_VALUE {
        //                         let index = index + x_byte_index;

        //                         if x_byte_index > LS027B7DH01A::PIXEL_HORIZONTAL {
        //                             warn!("draw_image: x index larger than PIXEL_HORIZONTAL");
        //                             continue;
        //                         }

        //                         if index >= self.buffer.len() {
        //                             warn!("draw_image: Buffer overflow prevention");
        //                             continue;
        //                         }

        //                         let mut byte = self.buffer[index];
        //                         const_assert_eq!(LS027B7DH01A::PIXEL_ON_VALUE, 0);
        //                         byte &= !(1 << x_bit_idx); // Clear bit
        //                         self.buffer[index] = byte;
        //                     }
        //                 }
        //             }
        //         }
        //     }
        //     Orientation::Degree180 => {
        //         for (row_idx, row_data) in image.into_iter().enumerate() {
        //             let line = LS027B7DH01A::NUMBER_LINES - (y + row_idx);
        //             let index = line * LS027B7DH01A::BUFFER_LINE_SIZE_BYTE_MULTI_LINE
        //                 + LS027B7DH01A::NUM_DUMMY_BYTES;
        //             for (col_idx, data) in row_data.into_iter().enumerate() {
        //                 for pixel_pos_byte in 0..8 {
        //                     let pixel =
        //                         LS027B7DH01A::PIXEL_HORIZONTAL - (x + col_idx * 8 + pixel_pos_byte);
        //                     let x_byte_index = pixel / 8;
        //                     let x_bit_idx = pixel % 8;
        //                     let value = (data >> pixel_pos_byte) & 1;
        //                     if value == LS027B7DH01A::PIXEL_ON_VALUE {
        //                         let index = index + x_byte_index;

        //                         if x_byte_index > LS027B7DH01A::PIXEL_HORIZONTAL {
        //                             warn!("draw_image: x index larger than PIXEL_HORIZONTAL");
        //                             continue;
        //                         }

        //                         if index >= self.buffer.len() {
        //                             warn!("draw_image: Buffer overflow prevention");
        //                             continue;
        //                         }

        //                         let mut byte = self.buffer[index];
        //                         const_assert_eq!(LS027B7DH01A::PIXEL_ON_VALUE, 0);
        //                         byte &= !(1 << x_bit_idx); // Clear bit
        //                         self.buffer[index] = byte;
        //                     }
        //                 }
        //             }
        //         }
        //     }
        // }
    }
}

impl<Interface, CS> Dimensions for DisplayWithBuffer<Interface, CS> {
    fn bounding_box(&self) -> Rectangle {
        Rectangle {
            top_left: Point { x: 0, y: 0 },
            size: Size {
                width: LS027B7DH01A::PIXEL_HORIZONTAL as u32,
                height: LS027B7DH01A::PIXEL_VERTICAL as u32,
            },
        }
    }
}

impl<Interface, CS> DrawTarget for DisplayWithBuffer<Interface, CS> {
    type Color = BinaryColor;
    type Error = Error;
    fn clear(&mut self, color: Self::Color) -> Result<(), Self::Error> {
        match color {
            BinaryColor::On => {
                self.buffer = [LS027B7DH01A::ALL_ON;
                    LS027B7DH01A::BUFFER_LINE_SIZE_BYTE_MULTI_LINE * LS027B7DH01A::NUMBER_LINES
                        + LS027B7DH01A::NUM_DUMMY_BYTES]
            }
            BinaryColor::Off => {
                self.buffer = [LS027B7DH01A::ALL_OFF;
                    LS027B7DH01A::BUFFER_LINE_SIZE_BYTE_MULTI_LINE * LS027B7DH01A::NUMBER_LINES
                        + LS027B7DH01A::NUM_DUMMY_BYTES]
            }
        };
        Ok(())
    }

    fn draw_iter<I>(&mut self, pixels: I) -> Result<(), Self::Error>
    where
        I: IntoIterator<Item = Pixel<Self::Color>>,
    {
        let bb = self.bounding_box();

        for Pixel(pos, color) in pixels
            .into_iter()
            .filter(|Pixel(pos, _color)| bb.contains(*pos))
        {
            let pos_info =
                LS027B7DH01A::buffer_index(pos.x as usize, pos.y as usize, self.orientation);

            if pos_info.byte_index >= self.buffer.len() {
                error!(
                    "Pos: ({}, {}), Index: {}",
                    pos.x, pos.y, pos_info.byte_index
                );
                continue;
            }

            let byte = &mut self.buffer[pos_info.byte_index];
            match color {
                BinaryColor::On => *byte &= !pos_info.mask,
                BinaryColor::Off => *byte |= pos_info.mask,
            }
        }

        Ok(())
    }

    fn fill_solid(&mut self, area: &Rectangle, color: Self::Color) -> Result<(), Self::Error> {
        // self.fill_contiguous(area, core::iter::repeat(color))

        let bb = self.bounding_box();

        for p in area
            .points()
            .into_iter()
            .filter(|point| bb.contains(*point))
        {
            let pos_info = LS027B7DH01A::buffer_index(p.x as usize, p.y as usize, self.orientation);

            if pos_info.byte_index >= self.buffer.len() {
                error!("Pos: ({}, {}), Index: {}", p.x, p.y, pos_info.byte_index);
                continue;
            }

            let byte = &mut self.buffer[pos_info.byte_index];
            match color {
                BinaryColor::On => *byte &= !pos_info.mask,
                BinaryColor::Off => *byte |= pos_info.mask,
            }
        }
        Ok(())
    }
}
