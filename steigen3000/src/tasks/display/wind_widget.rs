use super::base_widget::*;
use super::fonts::Font;
use super::images;
use super::text::MonoText;
use crate::common::Float;
use crate::paraguidecore::helper::unum_to_string_fill_character;
use embedded_graphics::{
    pixelcolor::BinaryColor,
    prelude::*,
    primitives::{PrimitiveStyle, Rectangle, StyledDrawable},
};
use heapless::String;

// pub enum WindDirRepresenation {
//     Arrow,
//     CardinalRepresentation, // North, East, South, West as characters
// }

pub struct WindWidget<'a> {
    position: Point,
    character_style_title: &'a Font<'a>,
    character_style_value: &'a Font<'a>,
    character_style_small: &'a Font<'a>,
    //wind_dir_repr: WindDirRepresenation,
    wind_speed: Float,
    wind_dir_abs: Float,
    bounding_rect_style: Option<&'a PrimitiveStyle<BinaryColor>>,
}

impl<'a> WindWidget<'a> {
    pub fn new(
        position: Point,
        character_style_title: &'a Font<'a>,
        character_style_value: &'a Font<'a>,
        character_style_small: &'a Font<'a>,
        //wind_dir_repr: WindDirRepresenation,
    ) -> Self {
        WindWidget {
            position,
            character_style_title,
            character_style_value,
            character_style_small,
            //wind_dir_repr,
            wind_speed: 0.,
            wind_dir_abs: 0.,

            bounding_rect_style: None,
        }
    }

    pub fn set_position(&mut self, position: Point) {
        self.position = position;
    }

    pub fn update(&mut self, wind_speed: Float, wind_dir_abs: Float) {
        if wind_speed < 0. {
            self.wind_speed = 0.;
        } else if wind_speed > 99. {
            self.wind_speed = 99.;
        } else {
            self.wind_speed = wind_speed;
        }
        self.wind_dir_abs = wind_dir_abs;
    }

    #[allow(unused)]
    pub fn draw_bounding_rect(&mut self, style: &'a PrimitiveStyle<BinaryColor>) {
        self.bounding_rect_style = Some(style);
    }
}

fn dir_to_string(dir_degree: Float, string: &mut String<2>) -> Result<(), ()> {
    const MAX_DIR: usize = 3600;
    let dir_degree = (dir_degree * 10.) as usize % MAX_DIR; // 0-360° -> 0-3600
    const SECTION_SIZE_HALF: usize = MAX_DIR / 8 / 2;
    const EAST_DIR: usize = 90 * 10;
    const SOUTH_DIR: usize = 180 * 10;
    const WEST_DIR: usize = 270 * 10;

    string.clear();
    if dir_degree >= (MAX_DIR - SECTION_SIZE_HALF) || dir_degree < SECTION_SIZE_HALF {
        string.push('N')?;
    } else if dir_degree < EAST_DIR - SECTION_SIZE_HALF {
        string.push_str("NE")?;
    } else if dir_degree < EAST_DIR + SECTION_SIZE_HALF {
        string.push('E')?;
    } else if dir_degree < SOUTH_DIR - SECTION_SIZE_HALF {
        string.push_str("SE")?;
    } else if dir_degree < SOUTH_DIR + SECTION_SIZE_HALF {
        string.push('S')?;
    } else if dir_degree < WEST_DIR - SECTION_SIZE_HALF {
        string.push_str("SW")?;
    } else if dir_degree < WEST_DIR + SECTION_SIZE_HALF {
        string.push('W')?;
    } else if dir_degree < MAX_DIR - SECTION_SIZE_HALF {
        string.push_str("NW")?;
    } else {
        return Err(());
    }
    Ok(())
}

impl<'a, E, E2, Display> Draw<E, E2, Display> for WindWidget<'a>
where
    E: Debug,
    E2: Debug,
    Display: DrawTarget<Color = BinaryColor, Error = E2> + DisplayImage<E>,
{
    fn draw(&self, display: &mut Display) -> Result<(), ()> {
        // Above the wind direction
        let mut string: String<2> = String::new();
        dir_to_string(self.wind_dir_abs, &mut string)?;

        let mut y = self.position.y as usize;

        // Direction
        y += self.character_style_title.typo_ascent as usize;
        MonoText::new(
            &string,
            Point::new(self.position.x + 8, y as i32),
            self.character_style_title,
        )
        .draw(display)
        .map_err(|_| ())?;

        // Wind Speed
        y += self.character_style_value.typo_ascent as usize;
        string.clear();
        unum_to_string_fill_character(self.wind_speed as u32, 2, ' ', &mut string)?;
        let pos = MonoText::new(
            &string,
            Point::new(self.position.x, y as i32),
            self.character_style_value,
        )
        .draw(display)
        .map_err(|_| ())?;

        // km/h
        let pos = MonoText::new("km/h", pos, self.character_style_small)
            .draw(display)
            .map_err(|_| ())?;

        // Image
        display
            .draw_image(
                (pos.x - images::WINDSOCK[0].len() as i32 * 8) as usize,
                self.position.y as usize,
                &images::WINDSOCK,
            )
            .map_err(|_| ())?;

        if let Some(style) = self.bounding_rect_style {
            Rectangle::new(
                self.position,
                Size::new(
                    (pos.x - self.position.x) as u32,
                    (y as i32 - self.position.y) as u32,
                ),
            )
            .draw_styled(style, display)
            .map_err(|_| ())?;
        }

        Ok(())
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_dir_to_string() {
        let mut string: String<2> = String::new();
        dir_to_string(0.0, &mut string).unwrap();
        assert_eq!(&string, "N");

        dir_to_string(45.0, &mut string).unwrap();
        assert_eq!(&string, "NE");

        dir_to_string(90.0, &mut string).unwrap();
        assert_eq!(&string, "E");

        dir_to_string(135.0, &mut string).unwrap();
        assert_eq!(&string, "SE");

        dir_to_string(180.0, &mut string).unwrap();
        assert_eq!(&string, "S");

        dir_to_string(225.0, &mut string).unwrap();
        assert_eq!(&string, "SW");

        dir_to_string(270.0, &mut string).unwrap();
        assert_eq!(&string, "W");

        dir_to_string(315.0, &mut string).unwrap();
        assert_eq!(&string, "NW");

        dir_to_string(360.0, &mut string).unwrap();
        assert_eq!(&string, "N");

        dir_to_string(405.0, &mut string).unwrap();
        assert_eq!(&string, "NE");
    }
}
