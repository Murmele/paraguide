use super::base_widget::*;
use super::fonts::Font;
use super::text::MonoText;
use crate::common::Float;
use crate::paraguidecore::helper::unum_to_string_fill_character;
use embedded_graphics::primitives::PrimitiveStyle;
use embedded_graphics::{
    pixelcolor::BinaryColor,
    prelude::*,
    primitives::{Rectangle, StyledDrawable},
};
use heapless::String;

pub struct HeightWidget<'a> {
    position: Point,
    character_style_value: &'a Font<'a>,
    character_style_small: &'a Font<'a>,
    height: Float,
    height_type: &'a str,
    bounding_rect_style: Option<&'a PrimitiveStyle<BinaryColor>>,
}

impl<'a> HeightWidget<'a> {
    pub fn new(
        position: Point,
        character_style_value: &'a Font<'a>,
        character_style_small: &'a Font<'a>,
        height_type: &'a str,
    ) -> Self {
        Self {
            position,
            character_style_value,
            character_style_small,
            height: 0.,
            height_type,

            bounding_rect_style: None,
        }
    }

    pub fn set_position(&mut self, position: Point) {
        self.position = position;
    }

    pub fn update(&mut self, height: Float) {
        self.height = height;
    }

    #[allow(unused)]
    pub fn draw_bounding_rect(&mut self, style: &'a PrimitiveStyle<BinaryColor>) {
        self.bounding_rect_style = Some(style);
    }
}

impl<'a, E, E2, Display> Draw<E, E2, Display> for HeightWidget<'a>
where
    E: Debug,
    E2: Debug,
    Display: DrawTarget<Color = BinaryColor, Error = E2> + DisplayImage<E>,
{
    fn draw(&self, display: &mut Display) -> Result<(), ()> {
        let y = self.position.y + self.character_style_value.total_height as i32;
        let x = self.position.x;

        // Height
        // All values below zero are thread as zero
        let mut string: String<5> = String::new();
        unum_to_string_fill_character(self.height as u32, 5, ' ', &mut string)?;

        let pos = (|| {
            let mut pos = MonoText::new(&string, Point::new(x, y), self.character_style_value)
                .draw(display)?;

            pos = MonoText::new("m", pos, self.character_style_value).draw(display)?;

            pos.x += 8;

            // height_type
            MonoText::new(self.height_type, pos, self.character_style_small).draw(display)
        })()
        .map_err(|_| ())?;

        if let Some(style) = self.bounding_rect_style {
            Rectangle::new(
                self.position,
                Size::new(
                    (pos.x - self.position.x) as u32,
                    (y - self.position.y) as u32,
                ),
            )
            .draw_styled(style, display)
            .map_err(|_| ())?;
        }

        Err(())
    }
}
