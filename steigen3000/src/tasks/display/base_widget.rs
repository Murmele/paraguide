pub use crate::DisplayImage;
pub use core::fmt::Debug;
use embedded_graphics::pixelcolor::BinaryColor;
use embedded_graphics::prelude::*;

pub trait Draw<
    E: Debug,
    E2: Debug,
    Display: DrawTarget<Color = BinaryColor, Error = E2> + DisplayImage<E>,
>
{
    fn draw(&self, display: &mut Display) -> Result<(), ()>;
}
