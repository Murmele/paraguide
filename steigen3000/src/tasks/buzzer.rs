use crate::actuators::BuzzerTrait;
use crate::VarioSoundReceiver;
#[cfg(feature = "nrf")]
use embassy_nrf::{
    gpio::{self, AnyPin},
    peripherals, pwm,
};
#[cfg(test)]
use embassy_sync::{blocking_mutex::raw::CriticalSectionRawMutex, signal::Signal};
use paraguidecore::logging::Timekeeper;
#[cfg(feature = "std")]
use tokio;
#[cfg(test)]
static STOP_SIGNAL: Signal<CriticalSectionRawMutex, ()> = Signal::new();

#[cfg(feature = "steigen31")]
#[embassy_executor::task]
pub async fn buzzer(
    pwm: peripherals::PWM0,
    pwm_pin: AnyPin,
    cpm1_pin: AnyPin,
    cpm2_pin: AnyPin,
    vario_sound_signal: &VarioSoundSignal,
) {
    use crate::VarioSoundSignal;

    let pwm = pwm::SimplePwm::new_1ch(pwm, pwm_pin);
    pwm.set_prescaler(pwm::Prescaler::Div64); // Between 10Hz and 2kHz good
    pwm.disable();
    let cpm1 = gpio::Output::new(cpm1_pin, gpio::Level::Low, gpio::OutputDrive::Standard);
    let cpm2 = gpio::Output::new(cpm2_pin, gpio::Level::Low, gpio::OutputDrive::Standard);
    let mut buzzer = Buzzer::new(pwm, cpm1, cpm2);

    buzzer_common(buzzer, vario_sound_signal).await;
}

#[cfg(feature = "steigen33")]
#[embassy_executor::task]
pub async fn buzzer(
    supply_enable_pin: AnyPin,
    enable_pin: AnyPin,
    frequency_pwm: peripherals::PWM0,
    frequency_pin: AnyPin,
    volume_pwm: peripherals::PWM1,
    volume_pin: AnyPin,
) {
    use crate::platform::main::VARIO_SOUND_SIGNAL;

    let frequency = pwm::SimplePwm::new_1ch(frequency_pwm, frequency_pin);
    frequency.set_prescaler(pwm::Prescaler::Div64); // Between 10Hz and 2kHz good
    frequency.disable();

    let volume = pwm::SimplePwm::new_1ch(volume_pwm, volume_pin);
    volume.set_prescaler(pwm::Prescaler::Div64); // Between 10Hz and 2kHz good
    volume.disable();

    let supply_enable = gpio::Output::new(
        supply_enable_pin,
        gpio::Level::Low,
        gpio::OutputDrive::Standard,
    );
    let enable = gpio::Output::new(enable_pin, gpio::Level::Low, gpio::OutputDrive::Standard);
    let mut buzzer =
        crate::actuators::buzzer::Buzzer::new(supply_enable, enable, frequency, volume, true);
    buzzer.enable_supply(true);

    buzzer_common(buzzer, &VARIO_SOUND_SIGNAL).await;
}

enum State {
    On,
    Off,
}

const CONTINUOUS: bool = false;
const OVERWRITE_CHANGING: bool = false;

pub async fn buzzer_common<T: BuzzerTrait>(mut buzzer: T, vario_sound_rx: &VarioSoundReceiver) {
    use crate::{interface::TimeKeeper, trace};
    #[cfg(not(feature = "std"))]
    use embassy_time::Timer;
    #[cfg(feature = "std")]
    use std::time::Duration;

    #[cfg(not(feature = "disable_buzzer"))]
    buzzer.enable(true);

    let mut settings = paraguidecore::paraguide::VarioSoundSettings {
        frequency: 0,
        on_time_ms: 0,
        off_time_ms: 0,
        duty: 50,
    };

    let mut state = State::On;
    let mut start_time_ms = TimeKeeper::get_time_us() / 1000;
    trace!("Buzzer on");
    buzzer.on(true);

    loop {
        #[cfg(test)]
        match STOP_SIGNAL.try_take() {
            Some(_) => break,
            _ => (),
        }

        let mut changed = false;

        #[cfg(feature = "steigen33")]
        if let Some(s) = vario_sound_rx.try_take() {
            trace!(
                "Buzzer: Vario sound settings: ({}, {}, {})",
                s.frequency,
                s.on_time_ms,
                s.off_time_ms
            );
            if settings.frequency != s.frequency
                || settings.on_time_ms != s.on_time_ms
                || settings.off_time_ms != s.off_time_ms
                || settings.duty != s.duty
            {
                changed = true;
                settings = s;
                buzzer.set_duty(settings.duty);
                buzzer.set_frequency(settings.frequency);
            }
        }

        #[cfg(feature = "std")]
        {
            let s = vario_sound_rx.borrow();
            if settings.frequency != s.frequency
                || settings.on_time_ms != s.on_time_ms
                || settings.off_time_ms != s.off_time_ms
            {
                changed = true;
                trace!(
                    "Buzzer: Vario sound settings: ({}, {}, {})",
                    s.frequency,
                    s.on_time_ms,
                    s.off_time_ms
                );
                settings = s.clone();
                drop(s);
                buzzer.set_frequency(settings.frequency);
            }
        }

        if settings.on_time_ms != 0 || settings.off_time_ms != 0 {
            let time_ms = TimeKeeper::get_time_us() / 1000;
            let diff = time_ms - start_time_ms;
            match state {
                State::On => {
                    if !CONTINUOUS && diff >= settings.on_time_ms as u64 && settings.off_time_ms > 0
                    {
                        trace!("Buzzer off");
                        state = State::Off;
                        start_time_ms = TimeKeeper::get_time_us() / 1000;
                        buzzer.on(false);
                    } else if OVERWRITE_CHANGING && changed {
                        start_time_ms = TimeKeeper::get_time_us() / 1000;
                    }
                }
                State::Off => {
                    if (diff >= settings.off_time_ms as u64 || (OVERWRITE_CHANGING && changed))
                        && settings.on_time_ms > 0
                    {
                        trace!("Buzzer on");
                        state = State::On;
                        start_time_ms = TimeKeeper::get_time_us() / 1000;
                        buzzer.on(true);
                    }
                }
            }
        } else {
            trace!("Buzzer off");
            state = State::Off;
            buzzer.on(false);
        }
        #[cfg(not(feature = "std"))]
        Timer::after_millis(10).await;
        #[cfg(feature = "std")]
        tokio::time::sleep(Duration::from_millis(10)).await
    }
}

#[cfg(all(test, feature = "std"))]
mod tests {
    use super::*;
    use paraguidecore::paraguide::VarioSoundSettings;
    use std::cell::RefCell;
    use std::rc::Rc;
    use std::time::SystemTime;
    use std::vec::Vec;
    use tokio::{self, sync::watch};

    struct Buzzer {
        pub on_logging: Rc<RefCell<Vec<(bool, u128)>>>,
        system_time_start: SystemTime,
        pub frequency: Rc<RefCell<u16>>,
    }

    impl Buzzer {
        fn new() -> Self {
            Buzzer {
                on_logging: Rc::new(RefCell::new(Vec::new())),
                system_time_start: SystemTime::now(),
                frequency: Rc::new(RefCell::new(0)),
            }
        }

        fn logging_data(&self) -> Rc<RefCell<Vec<(bool, u128)>>> {
            self.on_logging.clone()
        }

        fn get_frequency(&self) -> Rc<RefCell<u16>> {
            self.frequency.clone()
        }
    }

    impl BuzzerTrait for Buzzer {
        type D = u16;
        fn enable(&mut self, _enable: bool) -> &mut Self {
            self
        }

        fn on(&mut self, on: bool) {
            let time = self.system_time_start.elapsed().unwrap().as_millis();
            self.on_logging.borrow_mut().push((on, time));
        }

        fn is_on(&self) -> bool {
            self.on_logging
                .borrow()
                .last()
                .unwrap_or(&(false, 0 as u128))
                .0
        }

        fn set_frequency(&mut self, freq: u16) -> &mut Self {
            *self.frequency.borrow_mut() = freq;
            self
        }

        fn set_volume(&mut self, _volume: u16) -> &mut Self {
            self
        }

        fn set_duty(&mut self, _duty: u16) {}
    }

    async fn stop_task(duration_ms: u64) {
        use std::time::Duration;

        tokio::time::sleep(Duration::from_millis(duration_ms)).await;
        STOP_SIGNAL.signal(())
    }

    // ThreadModeMutex can only be locked from thread mode
    #[tokio::test]
    async fn buzzer() -> std::io::Result<()> {
        let buzzer = Buzzer::new();

        let on_time_ms = 300;
        let off_time_ms = 700;
        let frequency = 100;

        let s = VarioSoundSettings {
            frequency,
            on_time_ms,
            off_time_ms,
            duty: 500,
        };

        let (mpsc_tx_vario_sound_signal, mpsc_rx_vario_sound_signal) =
            watch::channel(paraguidecore::paraguide::VarioSoundSettings {
                frequency: 0,
                on_time_ms: 0,
                off_time_ms: 0,
                duty: 500,
            });

        let _ = mpsc_tx_vario_sound_signal.send(s);

        const DURATION_MS: u64 = 10_000;

        assert_eq!(*buzzer.get_frequency().borrow(), 0);

        let f = buzzer.get_frequency();
        let logging_data = buzzer.logging_data();

        tokio::join!(
            stop_task(DURATION_MS),
            buzzer_common(buzzer, &mpsc_rx_vario_sound_signal)
        );

        assert_eq!(*f.borrow(), frequency);
        assert_eq!(logging_data.borrow().len(), 20);

        for i in 0..logging_data.borrow().len() {
            let nom_diff;
            if i % 2 == 0 {
                nom_diff = off_time_ms as f64;
                assert_eq!(logging_data.borrow()[i].0, true);
            } else {
                nom_diff = on_time_ms as f64;
                assert_eq!(logging_data.borrow()[i].0, false);
            }

            if i != 0 {
                let diff = (logging_data.borrow()[i].1 - logging_data.borrow()[i - 1].1) as f64;
                assert!(diff <= nom_diff * 1.1 && diff as f64 >= nom_diff * 0.9);
            }
        }
        println!("{:?}", logging_data.borrow());
        Ok(())
    }

    async fn control_task(duration_ms: u64) {
        use std::time::Duration;

        tokio::time::sleep(Duration::from_millis(duration_ms)).await;
        STOP_SIGNAL.signal(())
    }

    #[tokio::test]
    /// This task can be used for testing on a computer, because it uses the speakers as output
    async fn buzzer_linux() -> std::io::Result<()> {
        if let Ok(buzzer) = crate::actuators::buzzer::Buzzer::new() {
            let (mpsc_tx_vario_sound_signal, mpsc_rx_vario_sound_signal) =
                watch::channel(paraguidecore::paraguide::VarioSoundSettings {
                    frequency: 0,
                    on_time_ms: 0,
                    off_time_ms: 0,
                    duty: 500,
                });

            let on_time_ms = 300;
            let off_time_ms = 700;
            let frequency = 100;

            let s = VarioSoundSettings {
                frequency,
                on_time_ms,
                off_time_ms,
                duty: 500,
            };

            let _ = mpsc_tx_vario_sound_signal.send(s);

            const DURATION_MS: u64 = 1_000; // make longer, otherwise the sound cannot be heared
            tokio::join!(
                control_task(DURATION_MS),
                buzzer_common(buzzer, &mpsc_rx_vario_sound_signal)
            );
            Ok(())
        } else {
            crate::error!("Unable to create buzzer. Do you have a soundcard / audio device?");
            Ok(())
        }
    }
}
