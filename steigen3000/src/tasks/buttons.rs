#[cfg(all(feature = "std"))]
compile_error!("This task cannot be used with std functionality!");

#[cfg(feature = "nrf")]
use embassy_nrf::gpio::{self, AnyPin};
use embassy_sync::{blocking_mutex::raw::ThreadModeRawMutex, signal::Signal};
use embassy_time::with_timeout;
use embassy_time::Duration;
use embassy_time::Timer;

const LONG_BUTTON_PRESS_TIME_MS: u64 = 5000;
const DEBOUNCE_TIME_MS: u64 = 20;

pub enum ButtonState {
    ShortPress,
    LongPress,
}

pub(crate) static BUTTON1_STATE: Signal<ThreadModeRawMutex, ButtonState> = Signal::new();
pub static BUTTON1_PULL_SIGNAL: Signal<ThreadModeRawMutex, gpio::Pull> = Signal::new();

pub(crate) static BUTTON2_STATE: Signal<ThreadModeRawMutex, ButtonState> = Signal::new();
pub static BUTTON2_PULL_SIGNAL: Signal<ThreadModeRawMutex, gpio::Pull> = Signal::new();

#[embassy_executor::task]
pub async fn button1(pin: AnyPin, pull: gpio::Pull) {
    button_generic(pin, pull, &BUTTON1_STATE, &BUTTON1_PULL_SIGNAL).await
}

#[embassy_executor::task]
pub async fn button2(pin: AnyPin, pull: gpio::Pull) {
    button_generic(pin, pull, &BUTTON2_STATE, &BUTTON2_PULL_SIGNAL).await
}

// ################################################################################################
// Generic
// ################################################################################################

/// Generic button function
/// This function in combination with the gpio_event task and the timer_event task
/// create a button press detection with distinguishing between short and long press
async fn button_generic(
    gpio: AnyPin,
    pull: gpio::Pull,
    signal_button_state: &Signal<ThreadModeRawMutex, ButtonState>,
    signal_button_pull: &Signal<ThreadModeRawMutex, gpio::Pull>,
) -> ! {
    let mut gpio = gpio::Flex::new(gpio);
    gpio.set_as_input(pull);

    let mut is_low = gpio.is_low();
    loop {
        if let Some(pull) = signal_button_pull.try_take() {
            // Changing pull
            gpio.set_as_input(pull);
        }
        if is_low {
            if let Err(_) = with_timeout(Duration::from_millis(500), gpio.wait_for_high()).await {
                continue;
            }
            let _ = Timer::after_millis(DEBOUNCE_TIME_MS).await;
        }
        if let Err(_) = with_timeout(Duration::from_millis(500), gpio.wait_for_low()).await {
            continue;
        }
        is_low = true;

        let _ = Timer::after_millis(DEBOUNCE_TIME_MS).await;
        match with_timeout(
            Duration::from_millis(LONG_BUTTON_PRESS_TIME_MS),
            gpio.wait_for_rising_edge(),
        )
        .await
        {
            Ok(_) => {
                signal_button_state.signal(ButtonState::ShortPress);
                is_low = false;
                let _ = Timer::after_millis(DEBOUNCE_TIME_MS).await;
            }
            Err(_) => signal_button_state.signal(ButtonState::LongPress),
        }
    }
}
