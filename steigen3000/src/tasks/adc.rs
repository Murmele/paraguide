#[cfg(feature = "nrf")]
use embassy_executor;

#[cfg(feature = "nrf")]
use embassy_sync::{blocking_mutex::raw::ThreadModeRawMutex, signal::Signal};

#[cfg(feature = "nrf")]
use embassy_nrf::{
    bind_interrupts,
    peripherals::SAADC,
    saadc::{
        AnyInput, ChannelConfig, Config, Gain, InterruptHandler, Reference, Resolution, Saadc,
    },
};
#[cfg(feature = "nrf")]
use embassy_time::Timer;

#[cfg(feature = "nrf")]
bind_interrupts!(struct Irqs {
    SAADC => InterruptHandler;
});

#[cfg(feature = "steigen31")]
const VOLTAGE_DIVIDOR_FACTOR: f32 = 0.75897035881;
#[cfg(feature = "steigen33")]
const VOLTAGE_DIVIDOR_FACTOR: f32 = 470e3 / (470e3 + 470e3);

#[cfg(feature = "nrf")]
pub(crate) static ADC_BATTERY_SIGNAL: Signal<ThreadModeRawMutex, f32> = Signal::new();

#[cfg(feature = "nrf")]
#[embassy_executor::task]
pub async fn adc_battery(adc: SAADC, pin: AnyInput) {
    let mut channel_config = ChannelConfig::single_ended(pin);

    // Input range = (reference=0.6 V)/(gain=1/6) = 3.6 V
    channel_config.reference = Reference::INTERNAL; // 0.6;
    channel_config.gain = Gain::GAIN1_6;

    let vdd = 3.3;
    let reference_voltage = match channel_config.reference {
        Reference::INTERNAL => 0.6,
        Reference::VDD1_4 => vdd / 4.,
        _ => 0., // TODO: why this is required? All possibilities of the enum are covered
    };

    let gain = match channel_config.gain {
        Gain::GAIN1 => 1.,
        Gain::GAIN1_2 => 1.5,
        Gain::GAIN1_3 => 1. / 3.,
        Gain::GAIN1_4 => 1. / 4.,
        Gain::GAIN1_5 => 1. / 5.,
        Gain::GAIN1_6 => 1. / 6.,
        Gain::GAIN2 => 2.,
        Gain::GAIN4 => 4.,
        _ => 1., // TODO: why this is required? All possibilities of the enum are covered
    };

    let config = Config::default();
    let r = match config.resolution {
        Resolution::_14BIT => 16384.,
        Resolution::_12BIT => 4096.,
        Resolution::_10BIT => 1024.,
        Resolution::_8BIT => 256.,
        _ => 1.0, // TODO: why this is required? All possibilities of the enum are covered
    };
    let adc_factor = reference_voltage / gain / r;

    let mut saadc = Saadc::new(adc, Irqs, config, [channel_config]);
    saadc.calibrate().await;
    let mut buf = [0; 1];

    let mut result;
    loop {
        saadc.sample(&mut buf).await;
        result = buf[0] as f32 * adc_factor / VOLTAGE_DIVIDOR_FACTOR;
        ADC_BATTERY_SIGNAL.signal(result);
        Timer::after_secs(60).await;
    }
}
