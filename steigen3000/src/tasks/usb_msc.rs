#[cfg(all(feature = "std"))]
compile_error!("This task cannot be used with std functionality!");

// Wireshark filter: (usb.idVendor == 0xc0de || (usb.bus_id == 1 && usb.device_address == 11))
// bus_id and device_address can be found with `lsusb`
// The device_address changes every time, but it is easily to find again, because of the idVendor

use super::Spi2Irqs;
use super::{
    USB_MANUFACTURER, USB_MAX_PACKET_SIZE, USB_MAX_POWER, USB_PID, USB_PRODUCT, USB_SERIAL_NUMBER,
    USB_VID,
};
use crate::tracing_markers;
use crate::{debug, error, info, trace};
use core::sync::atomic::AtomicU32;
use embassy_embedded_hal::SetConfig;
use embassy_executor::Spawner;
use embassy_nrf::usb::vbus_detect::HardwareVbusDetect;
use embassy_nrf::usb::Driver;
use embassy_nrf::{bind_interrupts, gpio, peripherals, spim, usb};
use embassy_usb::class::msc::subclass::scsi::{
    block_device::{self, BlockDeviceError},
    Scsi,
};
use embassy_usb::class::msc::transport::bulk_only::{BulkOnlyTransport, State};
use embassy_usb::driver::EndpointError;
use embassy_usb::{Builder, Config, UsbDevice};
use embedded_hal::digital::OutputPin;
use embedded_hal_async::{delay::DelayNs, spi::SpiDevice};
use embedded_sdmmc;
use embedded_sdmmc::{Block, BlockDevice as SdBlockDevice, BlockIdx};
use embedded_sdmmc::{SdCard, SdCardMultiBlockWriteRead};
use static_cell::StaticCell;

const BLOCK_SIZE: usize = 512;
const SD_CARD_SPI_FREQUENCY: spim::Frequency = spim::Frequency::M8; // TODO: increase when not using logic analyzer

bind_interrupts!(struct IrqsUsb {
    USBD => usb::InterruptHandler<peripherals::USBD>;
    CLOCK_POWER => usb::vbus_detect::InterruptHandler;
});

type MyDriver = Driver<'static, peripherals::USBD, HardwareVbusDetect>;

struct BlockDevice<SPI: SpiDevice, CS: OutputPin, DELAYER: DelayNs> {
    sd_card_multiblock: SdCardMultiBlockWriteRead<SPI, CS, DELAYER>,
}

impl<SPI: SpiDevice, CS: OutputPin, DELAYER: DelayNs> BlockDevice<SPI, CS, DELAYER> {
    fn new(sd_card: SdCard<SPI, CS, DELAYER>) -> Self {
        Self {
            sd_card_multiblock: SdCardMultiBlockWriteRead::new(sd_card),
        }
    }

    fn sd_error_to_block_device_error(e: embedded_sdmmc::SdCardError) -> BlockDeviceError {
        use embedded_sdmmc::SdCardError;
        match e {
            SdCardError::Transport => BlockDeviceError::Unknown,
            SdCardError::CantEnableCRC => BlockDeviceError::Unknown,
            SdCardError::TimeoutReadBuffer => BlockDeviceError::Unknown,
            SdCardError::TimeoutWaitNotBusy => BlockDeviceError::Unknown,
            SdCardError::TimeoutCommand(_) => BlockDeviceError::Unknown,
            SdCardError::TimeoutACommand(_) => BlockDeviceError::Unknown,
            SdCardError::Cmd58Error => BlockDeviceError::Unknown,
            SdCardError::RegisterReadError => BlockDeviceError::Unknown,
            SdCardError::CrcError(_, _) => BlockDeviceError::Unknown,
            SdCardError::ReadError => BlockDeviceError::ReadError,
            SdCardError::WriteError => BlockDeviceError::WriteError,
            SdCardError::BadState => BlockDeviceError::Unknown,
            SdCardError::CardNotFound => BlockDeviceError::Unknown,
            SdCardError::GpioError => BlockDeviceError::Unknown,
        }
    }
}

impl<SPI: SpiDevice, CS: OutputPin, DELAYER: DelayNs> block_device::BlockDevice
    for BlockDevice<SPI, CS, DELAYER>
{
    fn status(&self) -> Result<(), BlockDeviceError> {
        Ok(())
    }

    async fn num_blocks(&self) -> Result<u32, BlockDeviceError> {
        self.sd_card_multiblock
            .sd_card
            .num_blocks()
            .await
            .map_err(|e| Self::sd_error_to_block_device_error(e))
            .map(|bs| bs.0)
    }

    fn block_size(&self) -> Result<usize, BlockDeviceError> {
        Ok(BLOCK_SIZE)
    }

    async fn write_block(&mut self, lba: u32, block: &[u8]) -> Result<(), BlockDeviceError> {
        use embedded_sdmmc::{Block, BlockIdx};
        let mut data = Block {
            ..Default::default()
        };
        // TODO: really inefficient!!!!!!!!!!!
        assert!(block.len() == data.contents.len());
        let len = block.len();
        data.contents[0..len].copy_from_slice(&block[0..len]);

        self.sd_card_multiblock
            .sd_card
            .write(&[data], BlockIdx(lba))
            .await
            .map_err(|e| Self::sd_error_to_block_device_error(e))
    }

    async fn read_block(&self, lba: u32, block: &mut [u8]) -> Result<(), BlockDeviceError> {
        let _m = tracing_markers::Marker::new(tracing_markers::Markers::USBMSCRead);
        static COUNTER: AtomicU32 = AtomicU32::new(0);
        let c = COUNTER.fetch_add(1, core::sync::atomic::Ordering::Relaxed);
        trace!("start read_block: {}", c);
        let data = Block {
            ..Default::default()
        };
        assert!(block.len() == data.contents.len());

        let mut blocks = [data];

        self.sd_card_multiblock
            .sd_card
            .read(&mut blocks, BlockIdx(lba), "read")
            .await
            .map_err(|e| Self::sd_error_to_block_device_error(e))?;

        let len = block.len();
        {
            let _m = tracing_markers::Marker::new(tracing_markers::Markers::USBMSCReadCopy);
            block[0..len].copy_from_slice(&blocks[0][0..len]);
        }
        // trace!("read_block {}: {}", c, block);
        trace!("read_block {}", c);
        Ok(())
    }

    async fn prepare_multiblock_write(
        &mut self,
        lba: u32,
        blocks_count: u32,
    ) -> Result<(), BlockDeviceError> {
        debug!("prepare_multiblock_write");
        self.sd_card_multiblock
            .prepare_write(BlockIdx(lba), blocks_count)
            .await
            .map_err(|e| Self::sd_error_to_block_device_error(e))
    }

    async fn write_multiblock_block(&mut self, block: &[u8]) -> Result<(), BlockDeviceError> {
        self.sd_card_multiblock
            .write(block)
            .await
            .map_err(|e| Self::sd_error_to_block_device_error(e))
    }

    async fn stop_multiblock_write(&mut self) -> Result<(), BlockDeviceError> {
        debug!("stop_multiblock_write");
        self.sd_card_multiblock
            .stop_write()
            .await
            .map_err(|e| Self::sd_error_to_block_device_error(e))
    }

    async fn prepare_multiblock_read(&mut self, lba: u32) -> Result<(), BlockDeviceError> {
        use embedded_sdmmc::BlockIdx;
        debug!("prepare_multiblock_read");
        self.sd_card_multiblock
            .prepare_read(BlockIdx(lba))
            .await
            .map_err(|e| Self::sd_error_to_block_device_error(e))
    }

    async fn read_multiblock_block(&mut self, block: &mut [u8]) -> Result<(), BlockDeviceError> {
        self.sd_card_multiblock
            .read(block)
            .await
            .map_err(|e| Self::sd_error_to_block_device_error(e))
    }

    async fn stop_multiblock_read(&mut self) -> Result<(), BlockDeviceError> {
        debug!("stop_multiblock_read");
        self.sd_card_multiblock
            .stop_read()
            .await
            .map_err(|e| Self::sd_error_to_block_device_error(e))
    }
}

#[embassy_executor::task]
async fn usb_task(mut device: UsbDevice<'static, MyDriver>) {
    device.run().await;
}

#[embassy_executor::task]
pub async fn usb_msc(
    spawner: Spawner,
    usb_peripheral: peripherals::USBD,
    spi_peripheral: peripherals::SPI2,
    sck: gpio::AnyPin,
    cs: gpio::AnyPin,
    mosi: gpio::AnyPin,
    miso: gpio::AnyPin,
) {
    use embassy_time::Delay;
    use embedded_sdmmc::sdcard::{AcquireOpts, DummyCsPin};

    let driver = Driver::new(usb_peripheral, IrqsUsb, HardwareVbusDetect::new(IrqsUsb));

    // Create embassy-usb Config
    let mut config = Config::new(USB_VID, USB_PID);
    config.manufacturer = Some(USB_MANUFACTURER);
    config.product = Some(USB_PRODUCT);
    config.serial_number = Some(USB_SERIAL_NUMBER);
    config.max_power = USB_MAX_POWER;
    config.max_packet_size_0 = USB_MAX_PACKET_SIZE;

    // Required for windows compatibility.
    // https://developer.nordicsemi.com/nRF_Connect_SDK/doc/1.9.1/kconfig/CONFIG_CDC_ACM_IAD.html#help
    config.device_class = 0xEF;
    config.device_sub_class = 0x02;
    config.device_protocol = 0x01;
    config.composite_with_iads = true;

    // Create embassy-usb DeviceBuilder using the driver and config.
    static CONFIG_DESC: StaticCell<[u8; 256]> = StaticCell::new();
    static BOS_DESC: StaticCell<[u8; 256]> = StaticCell::new();
    static MSOS_DESC: StaticCell<[u8; 128]> = StaticCell::new();
    static CONTROL_BUF: StaticCell<[u8; 128]> = StaticCell::new();
    let mut builder = Builder::new(
        driver,
        config,
        &mut CONFIG_DESC.init([0; 256])[..],
        &mut BOS_DESC.init([0; 256])[..],
        &mut MSOS_DESC.init([0; 128])[..],
        &mut CONTROL_BUF.init([0; 128])[..],
    );

    let mut config = spim::Config::default();
    config.frequency = spim::Frequency::K250; // Initialization frequency must be below 400kHz
    let spi = spim::Spim::new(spi_peripheral, Spi2Irqs, sck, miso, mosi, config);
    let cs_pin = gpio::Output::new(cs, gpio::Level::High, gpio::OutputDrive::Standard);
    let delay = Delay {};

    // embedded_hal_bus
    let spi_device = embedded_hal_bus::spi::ExclusiveDevice::new(spi, DummyCsPin, Delay);
    let mut options = AcquireOpts::default();
    options.acquire_retries = 2;

    let sdcard = embedded_sdmmc::SdCard::new_with_options(spi_device, cs_pin, delay, options);
    // Initialisation done here. So afterwards we can increase the clock speed
    match sdcard.get_card_type().await {
        Some(t) => {
            info!("SD Card type: {}", t);
        }
        None => {
            error!("No sd card found / initialization failed");
        } // No sd card detected, TODO: what to do here? Maybe an infinity loop and trying to detect?
    }
    sdcard.spi(|spi| {
        let mut config = spim::Config::default();
        config.frequency = SD_CARD_SPI_FREQUENCY;
        let _ = spi.bus_mut().set_config(&config);
    });
    let block_device = BlockDevice::new(sdcard);

    let mut scsi_buffer = [0u8; BLOCK_SIZE];
    // Create SCSI target for our block device
    let scsi = Scsi::new(
        block_device,
        &mut scsi_buffer,
        USB_MANUFACTURER,
        USB_PRODUCT,
    );

    static STATE: StaticCell<State> = StaticCell::new();
    let state = STATE.init(State::default());
    // Use bulk-only transport for our SCSI target
    let mut msc_transport = BulkOnlyTransport::new(&mut builder, state, 64, scsi);

    // Build the builder.
    let usb = builder.build();

    trace!("TgSpn USB Task");
    match spawner.spawn(usb_task(usb)) {
        Ok(_) => (),
        Err(_) => (),
    }

    msc_transport.run().await;
}

struct Disconnected {}

impl From<EndpointError> for Disconnected {
    fn from(val: EndpointError) -> Self {
        match val {
            EndpointError::BufferOverflow => panic!("Buffer overflow"),
            EndpointError::Disabled => Disconnected {},
        }
    }
}
