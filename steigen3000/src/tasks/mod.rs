pub(crate) mod buzzer; // To include tests
pub mod display;

cfg_if::cfg_if! {
    if #[cfg(not(feature = "std"))] {
        pub use display::display;

        pub(crate) mod i2c_sensors;
        pub use i2c_sensors::i2c_sensors;

        pub use buzzer::buzzer;

        pub(crate) mod gps;
        pub use gps::gps;

        pub(crate) mod buttons;
        pub use buttons::button1;
        pub use buttons::button2;

        pub(crate) mod adc;
        pub use adc::adc_battery;

        const USB_VID: u16 = 0xc0de;
        const USB_PID: u16 = 0xcafe;
        const USB_MANUFACTURER: &str = "Martin&Charles";
        const USB_PRODUCT: &str = "Steigen3000";
        const USB_SERIAL_NUMBER: &str = "12345678";
        const USB_MAX_POWER: u16 = 100; // [mA]
        const USB_MAX_PACKET_SIZE: u8 = 64;

        #[cfg(feature = "usb_debug")]
        pub(crate) mod usb;
        #[cfg(feature = "usb_debug")]
        pub use usb::usb;

        #[cfg(feature = "usb_vario_simulation")]
        pub(crate) mod usb_vario_simulation;
        #[cfg(feature = "usb_vario_simulation")]
        pub use usb_vario_simulation::usb;

        #[cfg(not(any(feature = "usb_debug", feature = "usb_vario_simulation")))]
        pub(crate) mod usb_msc;
        #[cfg(not(any(feature = "usb_debug", feature = "usb_vario_simulation")))]
        pub use usb_msc::usb_msc;
    }
}

#[cfg(any(feature = "steigen33", test))]
pub(crate) mod sd; // To include tests
#[cfg(not(feature = "std"))]
pub use sd::sd;

#[cfg(all(not(feature = "std"), feature = "nrf"))]
use embassy_nrf::{bind_interrupts, peripherals, spim};

#[cfg(all(not(feature = "std"), feature = "nrf"))]
bind_interrupts!(struct Spi2Irqs {
    SPI2 => spim::InterruptHandler<peripherals::SPI2>;
});
