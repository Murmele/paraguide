#[cfg(all(feature = "std"))]
compile_error!("This task cannot be used with std functionality!");
mod helper;
use super::{
    USB_MANUFACTURER, USB_MAX_PACKET_SIZE, USB_MAX_POWER, USB_PID, USB_PRODUCT, USB_SERIAL_NUMBER,
    USB_VID,
};
pub use helper::{USBWriter, BUFFER_SIZE};

// Wireshark filter: (usb.idVendor == 0xc0de || (usb.bus_id == 1 && usb.device_address == 11))
// The device_address changes every time, but it is easily to find again, because of the idVendor
use super::{
    USB_MANUFACTURER, USB_MAX_PACKET_SIZE, USB_MAX_POWER, USB_PID, USB_PRODUCT, USB_SERIAL_NUMBER,
    USB_VID,
};
use crate::info;
use embassy_executor::Spawner;
use embassy_nrf::usb::vbus_detect::HardwareVbusDetect;
use embassy_nrf::usb::Driver;
use embassy_nrf::{bind_interrupts, peripherals, usb};
use embassy_usb::class::cdc_acm::{CdcAcmClass, State};
use embassy_usb::driver::EndpointError;
use embassy_usb::{Builder, Config, UsbDevice};
use helper::*;
use static_cell::StaticCell;

bind_interrupts!(struct Irqs {
    USBD => usb::InterruptHandler<peripherals::USBD>;
    CLOCK_POWER => usb::vbus_detect::InterruptHandler;
});

type MyDriver = Driver<'static, peripherals::USBD, HardwareVbusDetect>;

#[embassy_executor::task]
async fn usb_task(mut device: UsbDevice<'static, MyDriver>) {
    device.run().await;
}

#[embassy_executor::task]
pub async fn usb(spawner: Spawner, peripheral: peripherals::USBD) {
    let driver = Driver::new(peripheral, Irqs, HardwareVbusDetect::new(Irqs));

    // Create embassy-usb Config
    let mut config = Config::new(USB_VID, USB_PID);
    config.manufacturer = Some(USB_MANUFACTURER);
    config.product = Some(USB_PRODUCT);
    config.serial_number = Some(USB_SERIAL_NUMBER);
    config.max_power = USB_MAX_POWER;
    config.max_packet_size_0 = USB_MAX_PACKET_SIZE;

    // Use USB IAD
    // Required for windows compatibility.
    // https://developer.nordicsemi.com/nRF_Connect_SDK/doc/1.9.1/kconfig/CONFIG_CDC_ACM_IAD.html#help
    config.device_class = 0xEF;
    config.device_sub_class = 0x02;
    config.device_protocol = 0x01;
    config.composite_with_iads = true;

    // Create embassy-usb DeviceBuilder using the driver and config.
    static CONFIG_DESC: StaticCell<[u8; 256]> = StaticCell::new();
    static BOS_DESC: StaticCell<[u8; 256]> = StaticCell::new();
    static MSOS_DESC: StaticCell<[u8; 128]> = StaticCell::new();
    static CONTROL_BUF: StaticCell<[u8; 128]> = StaticCell::new();
    let mut builder = Builder::new(
        driver,
        config,
        &mut CONFIG_DESC.init([0; 256])[..],
        &mut BOS_DESC.init([0; 256])[..],
        &mut MSOS_DESC.init([0; 128])[..],
        &mut CONTROL_BUF.init([0; 128])[..],
    );

    static STATE: StaticCell<State> = StaticCell::new();
    let state = STATE.init(State::new());

    // Create classes on the builder.
    let mut class = CdcAcmClass::new(&mut builder, state, BUFFER_SIZE as u16);

    // Build the builder.
    let usb = builder.build();

    info!("Spawn usb task");
    match spawner.spawn(usb_task(usb)) {
        Ok(_) => (),
        Err(_) => (),
    }

    info!("USB: Wait for connection");
    class.wait_connection().await;
    info!("USB: Connected");

    loop {
        let buffer_mutex = BUFFER_CHANNEL.receive().await;
        let mut guard = buffer_mutex.lock().await;
        guard.status = Status::Free;

        if let Err(_) = class.write_packet(&guard.buffer).await {
            info!("USB: Failed to send packet");
        }
        guard.buffer.clear();
    }
}

struct Disconnected {}

impl From<EndpointError> for Disconnected {
    fn from(val: EndpointError) -> Self {
        match val {
            EndpointError::BufferOverflow => panic!("Buffer overflow"),
            EndpointError::Disabled => Disconnected {},
        }
    }
}
