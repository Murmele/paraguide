pub struct USBWriter {
    #[cfg(feature = "usb_debug")]
    mutex: Option<&'static Mutex<CriticalSectionRawMutex, Buffer>>,
}

pub const BUFFER_SIZE: usize = 64;

impl USBWriter {
    pub fn new() -> Self {
        #[cfg(feature = "usb_debug")]
        return USBWriter { mutex: None };
        #[cfg(not(feature = "usb_debug"))]
        return USBWriter {};
    }

    #[cfg(feature = "usb_debug")]
    pub async fn write(&mut self, message: &str) {
        for b in message.as_bytes() {
            let mut retry_count = 0;
            while retry_count < 2 {
                retry_count += 1;
                if let None = self.mutex {
                    let m = get_mutex().await;
                    let mut g = m.lock().await;
                    g.status = Status::Occupied;
                    g.buffer.clear();
                    drop(g);
                    self.mutex = Some(m);
                }

                let mutex = self.mutex.unwrap();
                let mut g = mutex.lock().await;
                if let Ok(_) = g.buffer.push(*b) {
                    break; // Next byte
                } else {
                    // Buffer is full send it
                    BUFFER_CHANNEL.send(self.mutex.unwrap()).await;
                    self.mutex = None;
                }
            }
        }
    }

    #[cfg(not(feature = "usb_debug"))]
    pub async fn write(&mut self, _message: &str) {}
}

cfg_if::cfg_if! {
    if #[cfg(feature = "usb_debug")] {
        // use crate::{info, trace, warn};
        use embassy_sync::{blocking_mutex::raw::CriticalSectionRawMutex, channel::Channel, mutex::Mutex};
        use embassy_time::Timer;
        use heapless::Vec;
        // Use this function to get a mutex to one of the Buffers. Multiple buffers are available, in case
        // One buffer was not fully send over usb but another should be filled again.
        async fn get_mutex() -> &'static Mutex<CriticalSectionRawMutex, Buffer> {
            loop {
                for (index, mutex) in MUTEXES.iter().enumerate() {
                    if let Ok(guard) = mutex.try_lock() {
                        if guard.status == Status::Free {
                            //trace!("Use mutex: {}", index);
                            return mutex;
                        } else {
                            if index > 0 {
                                //info!("Mutex occupied: {}", index);
                            }
                        }
                    } else {
                        //warn!("Unable to lock mutex: {}", index);
                    }
                }
                let _ = Timer::after_millis(1).await;
            }
        }

        /// This are the mutexes used for hold the buffers.
        pub(super) static BUFFER_CHANNEL: Channel<
            CriticalSectionRawMutex,
            &'static Mutex<CriticalSectionRawMutex, Buffer>,
            10,
        > = Channel::new();
        const NUMBER_MULTIPLEXER: usize = 2;
        #[derive(Clone, Copy, PartialEq, Eq, Debug)]
        pub enum Status {
            Free,
            Occupied, // To process by the sd task or is currently in use in the file handler
        }
        pub(super) struct Buffer {
            pub buffer: Vec<u8, BUFFER_SIZE>,
            pub status: Status,
        }
        const fn create_mutex() -> Mutex<CriticalSectionRawMutex, Buffer> {
            Mutex::new(Buffer {
                status: Status::Free,
                buffer: Vec::new(),
            })
        }
        static MUTEXES: [Mutex<CriticalSectionRawMutex, Buffer>; NUMBER_MULTIPLEXER] =
            [create_mutex(), create_mutex()];
    }
}
