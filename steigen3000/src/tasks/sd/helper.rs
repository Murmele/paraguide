use super::{FileOperationRequest, Operation, ReadOperation, ReadStatus, Status, VolumeError};
use crate::{info, trace, warn};
use embassy_sync::{blocking_mutex::raw::CriticalSectionRawMutex, mutex::Mutex};
#[cfg(all(not(feature = "std"), feature = "nrf"))]
use embassy_time::Timer;
#[cfg(all(not(feature = "std"), feature = "nrf"))]
use embedded_sdmmc::TimeSource;
use heapless::Vec;

#[cfg(all(not(feature = "std"), feature = "nrf"))]
pub(crate) struct Clock;
#[cfg(all(not(feature = "std"), feature = "nrf"))]
impl TimeSource for Clock {
    fn get_timestamp(&self) -> embedded_sdmmc::Timestamp {
        embedded_sdmmc::Timestamp {
            year_since_1970: 0,
            zero_indexed_month: 0,
            zero_indexed_day: 0,
            hours: 0,
            minutes: 0,
            seconds: 0,
        }
    }
}

/// Function returns a mutex if one is available
pub(crate) async fn get_file_operation_mutex(
) -> &'static Mutex<CriticalSectionRawMutex, FileOperationRequest> {
    loop {
        for (index, mutex) in FILE_OPERATION_MUTEX.iter().enumerate() {
            if let Ok(guard) = mutex.try_lock() {
                if guard.process_status == Status::Free {
                    trace!("Use mutex: {}", index);
                    return mutex;
                } else {
                    if index > 0 {
                        info!("Mutex occupied: {}", index);
                    }
                }
            } else {
                warn!("Unable to lock mutex: {}", index);
            }
        }
        #[cfg(all(not(feature = "std"), feature = "nrf"))]
        let _ = Timer::after_millis(1).await;

        #[cfg(test)]
        use tokio::time::{sleep, Duration};
        #[cfg(test)]
        sleep(Duration::from_millis(1)).await;
    }
}

/// Helper function to initialize all write mutexes
const fn create_fo_mutex() -> Mutex<CriticalSectionRawMutex, FileOperationRequest> {
    Mutex::new(FileOperationRequest {
        process_status: Status::Free,
        operation: Operation::Read,
        //id: 0,
    })
}
/// This are the mutexes used for file operations
pub(crate) const FILE_OPERATION_MULTIPLEXERS: usize = 10;
static FILE_OPERATION_MUTEX: [Mutex<CriticalSectionRawMutex, FileOperationRequest>;
    FILE_OPERATION_MULTIPLEXERS] = [
    create_fo_mutex(),
    create_fo_mutex(),
    create_fo_mutex(),
    create_fo_mutex(),
    create_fo_mutex(),
    create_fo_mutex(),
    create_fo_mutex(),
    create_fo_mutex(),
    create_fo_mutex(),
    create_fo_mutex(),
];

pub(super) async fn get_read_file_operation_mutex(
) -> &'static Mutex<CriticalSectionRawMutex, ReadOperation> {
    loop {
        for m in &RM {
            if let Ok(mutex) = m.try_lock() {
                if mutex.process_status == Status::Free {
                    return m;
                }
            }
        }
        #[cfg(all(not(feature = "std"), feature = "nrf"))]
        let _ = Timer::after_millis(1).await;
    }
}

/// Helper function to initialize all read mutexes
const fn create_read_mutex() -> Mutex<CriticalSectionRawMutex, ReadOperation> {
    Mutex::new(ReadOperation {
        process_status: Status::Free,
        status: ReadStatus::NotStarted,
        content: Vec::new(),
        result: Err(VolumeError::BadHandle),
    })
}
static RM: [Mutex<CriticalSectionRawMutex, ReadOperation>; 2] = [
    // TODO: get rid of the type, redundant
    create_read_mutex(),
    create_read_mutex(),
];
