#![cfg(test)]

// #[derive(Clone, Copy)]
// struct FakeDelayer();

// impl embedded_hal_async::delay::DelayNs for FakeDelayer {
//     async fn delay_ns(&mut self, ns: u32) {
//         std::thread::sleep(std::time::Duration::from_nanos(u64::from(ns)));
//     }
// }

// struct FakeCs();

// impl embedded_hal::digital::ErrorType for FakeCs {
//     type Error = core::convert::Infallible;
// }

// impl embedded_hal::digital::OutputPin for FakeCs {
//     fn set_low(&mut self) -> Result<(), Self::Error> {
//         Ok(())
//     }

//     fn set_high(&mut self) -> Result<(), Self::Error> {
//         Ok(())
//     }
// }

// struct FakeSpiBus();

// impl embedded_hal_async::spi::ErrorType for FakeSpiBus {
//     type Error = core::convert::Infallible;
// }

// impl embedded_hal_async::spi::SpiBus<u8> for FakeSpiBus {
//     async fn read(&mut self, _: &mut [u8]) -> Result<(), Self::Error> {
//         Ok(())
//     }

//     async fn write(&mut self, _: &[u8]) -> Result<(), Self::Error> {
//         Ok(())
//     }

//     async fn transfer(&mut self, _: &mut [u8], _: &[u8]) -> Result<(), Self::Error> {
//         Ok(())
//     }

//     async fn transfer_in_place(&mut self, _: &mut [u8]) -> Result<(), Self::Error> {
//         Ok(())
//     }

//     async fn flush(&mut self) -> Result<(), Self::Error> {
//         Ok(())
//     }
// }
