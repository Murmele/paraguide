# Preconditions

This is already done. Just for documentation
1) Creating a new empty disk image: `dd if=/dev/zero of=DISK1.img bs=1M count=1200`
2) Create partition table: `msdos`. Linux: Use `parted DISK1.img` for it. `mklabel msdos`
3) Create partition also with parted: `mkpart primary fat32 1MB 1200MB`
4) Use `unit s` to change in parted the units to sectors.
5) Print content `print`
6) Exit parted and use `mkfs.vfat -v --offset 2048 DISK1.img` to create the disk. `2048` is the start sector. It can be seen in parted in the `print`


Source: [1] https://www.tecmint.com/create-virtual-harddisk-volume-in-linux/
[2] https://unix.stackexchange.com/questions/771277/creating-disk-image-with-msdos-partition-table-and-fat32
[3] Losetup/ Mount: https://askubuntu.com/questions/69363/mount-single-partition-from-image-of-entire-disk-device

Minimum size for FAT16 is 4.1MiB!
https://superuser.com/questions/74392/minimum-volume-size-for-fat16

# Modifying content

1) Unzip archive: `tar -xf DISK1.tar.xz`
2) Create folder: `mkdir DISK1_MOUNT`. Into this folder the disk will be mounted
3) Find sector start: `fdisk -lu DISK1.img` (2048)
4) Multiply sector start with sector size: 512 * 2048 = 1048576
5) lopsetup partition: `sudo losetup -o 1048576 /dev/loop0 DISK1.img`
6) Mount: `sudo mount /dev/loop0 DISK1_MOUNT`
7) Copy data to mounted partition (Root required)
8) unmount `sudo umount /dev/loop0`
9) Detach losetup: `sudo losetup -d /dev/loop0`
10) Compress disk image again

# Compressing disk image

The image shall be compressed, because then it takes only a few kB: `tar -cJf DISK1.tar.xz DISK1.img`
