use core::result::Result::{Err, Ok};

use crate::{info, trace};
#[cfg(feature = "nrf")]
use embassy_nrf::{
    self as hal, gpio, peripherals,
    uarte::{self, Uarte},
};

#[cfg(not(feature = "std"))]
use embassy_sync::{blocking_mutex::raw::ThreadModeRawMutex, channel::Channel};
use heapless::String;
#[cfg(feature = "nrf")]
use nmea::ParseResult;
use ringbuffer::{ConstGenericRingBuffer, RingBuffer};

#[cfg(feature = "nrf")]
hal::bind_interrupts!(struct Irqs {
    UARTE0 => uarte::InterruptHandler<peripherals::UARTE0>;
});

// https://en.wikipedia.org/wiki/NMEA_0183
#[cfg(feature = "nrf")]
const DMA_BUFFER_SIZE: usize = 200; // Maximum message length: 82
const NMEA_START_DELIMITER: char = '$';

#[cfg(not(feature = "std"))]
pub static NMEA_MESSAGES: Channel<ThreadModeRawMutex, nmea::ParseResult, 20> = Channel::new();

struct BufferNmea {
    pub buffer: ConstGenericRingBuffer<u8, 400>,
}

#[derive(Debug, PartialEq, Eq)]
pub enum Error {
    IncompleteSentence,
    ParseError, // TODO: implement more concrete errors
    BufferFull,
}

impl BufferNmea {
    const fn new() -> Self {
        BufferNmea {
            buffer: ConstGenericRingBuffer::new(),
        }
    }

    fn nmea_message(&mut self) -> Result<nmea::ParseResult, Error> {
        // TODO: consider making this async with a wait after every parsing, because it blocks the complete system
        // and those messages are not that important for blocking everything
        let mut buffer: String<{ nmea::SENTENCE_MAX_LEN }> = String::new();
        let mut sentence_started = false;
        for c in self.buffer.drain() {
            if c >= 128 {
                // Not ascii
                continue;
            }
            if c as char == NMEA_START_DELIMITER {
                sentence_started = true;
                buffer.clear();
                buffer.push(c as char).map_err(|_| Error::BufferFull)?;
            } else if c as char == '\r' {
            } else if c as char == '\n' {
                // ignoring if no \r was previously.
                if sentence_started {
                    sentence_started = false; // full sentence
                                              //info!("Complete message");
                    match nmea::parse_str(&buffer).map_err(|_| Error::ParseError) {
                        Ok(m) => {
                            //info!("Parse success");
                            // let mut g = heapless::Vec::<u8, 1000>::new();
                            // g.extend_from_slice(&buffer);
                            // let s = heapless::String::from_utf8(g).unwrap();
                            // let s_ = s.as_str();
                            // info!("{}", s_);
                            return Ok(m);
                        }
                        Err(_) => {}
                    }
                }
            } else {
                if sentence_started {
                    buffer.push(c as char).map_err(|_| Error::BufferFull)?;
                }
            }
        }
        //error!("Parse Error123");
        Err(Error::IncompleteSentence)
    }
}

#[cfg(feature = "nrf")]
pub fn nmea_parse_result_to_string(r: &ParseResult) -> &'static str {
    match r {
        ParseResult::AAM(_) => "AAM",
        ParseResult::ALM(_) => "ALM",
        ParseResult::BOD(_) => "BOD",
        ParseResult::GGA(_) => "GGA",
        ParseResult::RMC(_) => "RMC",
        ParseResult::GSV(_) => "GSV",
        ParseResult::GLL(_) => "GLL",
        ParseResult::GSA(_) => "GSA",
        ParseResult::GNS(_) => "GNS",
        _ => "Not handled",
    }
}

#[cfg(feature = "nrf")]
#[embassy_executor::task]
pub async fn gps(
    uart: peripherals::UARTE0,
    rx: gpio::AnyPin,
    tx: gpio::AnyPin,
    baudrate: uarte::Baudrate,
) {
    use embassy_time::Timer;

    info!("Initializing GPS Task");
    let mut config = uarte::Config::default();
    config.parity = uarte::Parity::EXCLUDED;
    config.baudrate = baudrate;

    let mut uarte = Uarte::new(uart, Irqs, rx, tx, config);

    let mut buffer_nmea = BufferNmea::new();

    //let data = [1, 2, 3];
    //uarte.write_from_ram(&data); // Uses DMA
    let mut buffer = [[0u8; DMA_BUFFER_SIZE], [0u8; DMA_BUFFER_SIZE]];

    loop {
        for buf in &mut buffer {
            let _ = uarte.read(buf).await;
            store_new_data(&buf, &mut buffer_nmea); // takes around 2ms
            buf[0] = 0; // clear buffer

            while let Ok(message) = buffer_nmea.nmea_message() {
                trace!(
                    "GPS Task: new nmea message: {}",
                    nmea_parse_result_to_string(&message)
                );
                NMEA_MESSAGES.send(message).await;
                let _ = Timer::after_micros(100); // Give other tasks time // TODO: really needed?
            }
        }
    }
}

fn store_new_data<const N: usize>(buffer: &[u8; N], buffer_nmea: &mut BufferNmea) {
    for v in buffer {
        if *v == 0 {
            break;
        }
        buffer_nmea.buffer.push(*v);
    }
}

#[cfg(all(feature = "std", test))]
mod tests {
    use super::*;

    #[test]
    fn nmea_message() {
        // 0.00,0.00,070224,,,N*56\r\n$GNRMC,193452.086,V,,,,,0.00,0.00,070224,,,N*56\r\n$GNGGA,193452.086,,,,,0,0,,,M,,M,,*50\r\n
        let buffer = [
            0x30, 0x2e, 0x30, 0x30, 0x2c, 0x30, 0x2e, 0x30, 0x30, 0x2c, 0x30, 0x37, 0x30, 0x32,
            0x32, 0x34, 0x2c, 0x2c, 0x2c, 0x4e, 0x2a, 0x35, 0x36, 0x0D, 0x0A, 0x24, 0x47, 0x4e,
            0x52, 0x4d, 0x43, 0x2c, 0x31, 0x39, 0x33, 0x34, 0x35, 0x32, 0x2e, 0x30, 0x38, 0x36,
            0x2c, 0x56, 0x2c, 0x2c, 0x2c, 0x2c, 0x2c, 0x30, 0x2e, 0x30, 0x30, 0x2c, 0x30, 0x2e,
            0x30, 0x30, 0x2c, 0x30, 0x37, 0x30, 0x32, 0x32, 0x34, 0x2c, 0x2c, 0x2c, 0x4e, 0x2a,
            0x35, 0x36, 0x0D, 0x0A, 0x24, 0x47, 0x4e, 0x47, 0x47, 0x41, 0x2c, 0x31, 0x39, 0x33,
            0x34, 0x35, 0x32, 0x2e, 0x30, 0x38, 0x36, 0x2c, 0x2c, 0x2c, 0x2c, 0x2c, 0x30, 0x2c,
            0x30, 0x2c, 0x2c, 0x2c, 0x4d, 0x2c, 0x2c, 0x4d, 0x2c, 0x2c, 0x2a, 0x35, 0x30, 0x0D,
            0x0A,
        ];
        let mut buffer_nmea = BufferNmea::new();

        store_new_data(&buffer, &mut buffer_nmea);
        let rmc = nmea::sentences::RmcData {
            fix_time: chrono::NaiveTime::from_hms_milli_opt(19, 34, 52, 86),
            faa_mode: Some(nmea::sentences::FaaMode::DataNotValid),
            fix_date: chrono::NaiveDate::from_ymd_opt(2024, 02, 07),
            lat: None,
            lon: None,
            magnetic_variation: None,
            nav_status: None,
            speed_over_ground: Some(0.0),
            status_of_fix: nmea::sentences::rmc::RmcStatusOfFix::Invalid,
            true_course: Some(0.0),
        };
        let gga = nmea::sentences::GgaData {
            altitude: None,
            fix_satellites: Some(0),
            fix_time: chrono::NaiveTime::from_hms_milli_opt(19, 34, 52, 86),
            fix_type: Some(nmea::sentences::FixType::Invalid),
            geoid_separation: None,
            hdop: None,
            latitude: None,
            longitude: None,
        };

        assert_eq!(
            buffer_nmea.nmea_message().unwrap(),
            nmea::ParseResult::RMC(rmc)
        );
        assert_eq!(
            buffer_nmea.nmea_message().unwrap(),
            nmea::ParseResult::GGA(gga)
        );
        assert_eq!(
            buffer_nmea.nmea_message().unwrap_err(),
            Error::IncompleteSentence
        );
    }
    #[test]
    // The GNRMC message does not have any new line at the end, so it should be ignored
    fn nmea_message_missing_new_line() {
        // $GNRMC,193452.086,V,,,,,0.00,0.00,070224,,,N*56$GNGGA,193452.086,,,,,0,0,,,M,,M,,*50\r\n
        let buffer = [
            0x24, 0x47, 0x4e, 0x52, 0x4d, 0x43, 0x2c, 0x31, 0x39, 0x33, 0x34, 0x35, 0x32, 0x2e,
            0x30, 0x38, 0x36, 0x2c, 0x56, 0x2c, 0x2c, 0x2c, 0x2c, 0x2c, 0x30, 0x2e, 0x30, 0x30,
            0x2c, 0x30, 0x2e, 0x30, 0x30, 0x2c, 0x30, 0x37, 0x30, 0x32, 0x32, 0x34, 0x2c, 0x2c,
            0x2c, 0x4e, 0x2a, 0x35, 0x36, 0x24, 0x47, 0x4e, 0x47, 0x47, 0x41, 0x2c, 0x31, 0x39,
            0x33, 0x34, 0x35, 0x32, 0x2e, 0x30, 0x38, 0x36, 0x2c, 0x2c, 0x2c, 0x2c, 0x2c, 0x30,
            0x2c, 0x30, 0x2c, 0x2c, 0x2c, 0x4d, 0x2c, 0x2c, 0x4d, 0x2c, 0x2c, 0x2a, 0x35, 0x30,
            0x0D, 0x0A,
        ];
        let mut buffer_nmea = BufferNmea::new();

        store_new_data(&buffer, &mut buffer_nmea);
        let gga = nmea::sentences::GgaData {
            altitude: None,
            fix_satellites: Some(0),
            fix_time: chrono::NaiveTime::from_hms_milli_opt(19, 34, 52, 86),
            fix_type: Some(nmea::sentences::FixType::Invalid),
            geoid_separation: None,
            hdop: None,
            latitude: None,
            longitude: None,
        };

        assert_eq!(
            buffer_nmea.nmea_message().unwrap(),
            nmea::ParseResult::GGA(gga)
        );
        assert_eq!(
            buffer_nmea.nmea_message().unwrap_err(),
            Error::IncompleteSentence
        );
    }
}
