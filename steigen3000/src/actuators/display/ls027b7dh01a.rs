use bitflags::bitflags;
use core::fmt::Debug;
use embedded_hal::digital::OutputPin;
use embedded_hal_async as hal;
use hal::spi::SpiBus;

pub struct LS027B7DH01A<Interface, CS> {
    interface: Interface,
    cs: CS,
}

const PIXEL_HORIZONTAL: usize = 400;
const BUFFER_LINE_SIZE_BYTE_MULTI_LINE: usize = 2 + PIXEL_HORIZONTAL / 8;
const NUM_DUMMY_BYTES: usize = 2;
const BUFFER_LINE_SIZE_BYTE: usize = BUFFER_LINE_SIZE_BYTE_MULTI_LINE + NUM_DUMMY_BYTES;
const PIXEL_VERTICAL: usize = 240;
const NUMBER_LINES: usize = PIXEL_VERTICAL;

pub struct Pos {
    pub byte_index: usize,
    pub mask: u8,
}

#[derive(PartialEq, Clone, Copy)]
pub enum Orientation {
    Degree0,
    Degree180,
}

impl LS027B7DH01A<(), ()> {
    pub const PIXEL_HORIZONTAL: usize = PIXEL_HORIZONTAL;
    pub const PIXEL_VERTICAL: usize = PIXEL_VERTICAL;
    pub const CLEAR_VALUE: usize = 1;
    // 400 pixels / 8 = 50byte + 1 address byte + 1 control byte + 2 dummy bytes
    pub const NUM_DUMMY_BYTES: usize = NUM_DUMMY_BYTES;
    pub const BUFFER_LINE_SIZE_BYTE_MULTI_LINE: usize = BUFFER_LINE_SIZE_BYTE_MULTI_LINE;
    pub const BUFFER_LINE_SIZE_BYTE: usize = BUFFER_LINE_SIZE_BYTE;
    pub const NUMBER_LINES: usize = NUMBER_LINES;
    pub const PIXEL_ON_VALUE: u8 = 0;
    pub const PIXEL_OFF_VALUE: u8 = 1;
    pub const ALL_OFF: u8 = 0xFF;
    pub const ALL_ON: u8 = 0x00;

    pub fn buffer_index(pos_x: usize, pos_y: usize, orientation: Orientation) -> Pos {
        // Assumption: LSB first
        let mut index = 2; // first 2 are mode and address
        index += pos_y * BUFFER_LINE_SIZE_BYTE_MULTI_LINE;

        let mut byte_index = index + pos_x / 8;
        let mut bit_index = pos_x % 8;

        match orientation {
            Orientation::Degree0 => (),
            Orientation::Degree180 => {
                byte_index = (2 + NUMBER_LINES * BUFFER_LINE_SIZE_BYTE_MULTI_LINE) - 1 - byte_index; // -1 because it is the index not the length!
                bit_index = 7 - bit_index;
            }
        }
        Pos {
            byte_index: byte_index,
            mask: 1 << bit_index,
        }
    }
}

bitflags! {
    pub struct Config: u8 {
        const ALL_CLEAR =       0x04;
        const FRAME_INVERSION = 0x02;
        const MODE =            0x01;
    }
}

pub enum Mode {
    DataUpdateMode,
    MaintainMemory,
}

impl Mode {
    pub fn bits(self) -> Config {
        match self {
            Self::DataUpdateMode => Config::MODE,
            Self::MaintainMemory => Config::empty(),
        }
    }
}

/// Only required if external mode is used
pub enum FrameInversion {
    HighVcom,
    LowVcom,
}

impl FrameInversion {
    pub fn bits(self) -> Config {
        match self {
            Self::LowVcom => Config::empty(),
            Self::HighVcom => Config::FRAME_INVERSION,
        }
    }
}

impl<Interface, E, CS> LS027B7DH01A<Interface, CS>
where
    Interface: SpiBus<Error = E>,
    E: Debug,
    CS: OutputPin,
{
    pub fn new(interface: Interface, mut cs: CS) -> Self {
        let _ = cs.set_low();
        Self { interface, cs }
    }

    pub async fn clear(&mut self) -> Result<(), E> {
        let _ = self.cs.set_high();
        let words: [u8; 16] = [
            Mode::MaintainMemory.bits().bits() | Config::ALL_CLEAR.bits(),
            0,
            0,
            0,
            0,
            0,
            0,
            0,
            0,
            0,
            0,
            0,
            0,
            0,
            0,
            0,
        ];
        let res = self.interface.write(&words).await;
        let _ = self.cs.set_low();
        res
    }

    pub async fn write_single_line(
        &mut self,
        line: usize,
        line_buffer: &mut [u8; BUFFER_LINE_SIZE_BYTE],
    ) -> Result<(), E> {
        let _ = self.cs.set_high();
        line_buffer[0] = Mode::DataUpdateMode.bits().bits();
        line_buffer[1] = line as u8;
        let result = self.interface.write(line_buffer).await;
        let _ = self.cs.set_low();
        result
    }

    pub async fn write_multiple_lines<const N: usize>(
        &mut self,
        buffer: &mut [u8; BUFFER_LINE_SIZE_BYTE_MULTI_LINE * N + 2],
    ) -> Result<(), E> {
        buffer[0] = Mode::DataUpdateMode.bits().bits();

        let _ = self.cs.set_high();
        let result = self.interface.write(buffer).await;
        let _ = self.cs.set_low();
        result
    }

    pub async fn write_complete(
        &mut self,
        buffer: &mut [u8; BUFFER_LINE_SIZE_BYTE_MULTI_LINE * NUMBER_LINES + 2],
    ) -> Result<(), E> {
        buffer[0] = Mode::DataUpdateMode.bits().bits();

        for line_index in 0..NUMBER_LINES {
            // First line starts with 1!
            buffer[line_index * BUFFER_LINE_SIZE_BYTE_MULTI_LINE + 1] = line_index as u8 + 1;
        }

        let _ = self.cs.set_high();
        let result = self.interface.write(buffer).await;
        let _ = self.cs.set_low();
        result
    }
}
