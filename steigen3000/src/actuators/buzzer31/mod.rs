//! Buzzer driver of Steigen3000 v3.1 and below

use core::fmt::Debug;
use embedded_hal as hal;
pub struct Buzzer<OutputPin, Pwm0> {
    frequency: Pwm0,
    cpm1: OutputPin,
    cpm2: OutputPin,
}

impl<OutputPin, Pwm0, E, D> Buzzer<OutputPin, Pwm0>
where
    OutputPin: hal::digital::OutputPin<Error = E>,
    Pwm0: super::PWM<D = D>,
    E: Debug,
    D: core::ops::Div<u16, Output = D> + Copy,
{
    pub fn new(frequency: Pwm0, mut cpm1: OutputPin, mut cpm2: OutputPin) -> Self {
        let _ = cpm1.set_low();
        let _ = cpm2.set_low();
        frequency.disable();
        Buzzer {
            frequency,
            cpm1,
            cpm2,
        }
    }

    // pub fn set_volume(&mut self, volume: D) -> &mut Self {
    //     // Currently not used
    //     self
    // }

    // pub fn set_frequency(&mut self, freq: u16) -> &mut Self {
    //     let max_duty = self.frequency.calculate_max_duty(freq);
    //     self.frequency.set_max_duty(max_duty);
    //     self.frequency.set_duty(max_duty / 2);
    //     self
    // }

    // pub fn is_on(&self) -> bool {
    //     self.frequency.is_enabled()
    // }

    // pub fn on(&mut self, on: bool) {
    //     match on {
    //         true => self.frequency.enable(),
    //         false => self.frequency.disable(),
    //     }

    //     // Set duty again to start sequence
    //     self.frequency.set_duty(self.frequency.duty());
    // }
}

impl<OutputPin, Pwm0, D, E> super::BuzzerTrait<D> for Buzzer<OutputPin, Pwm0>
where
    E: Debug,
    OutputPin: hal::digital::OutputPin<Error = E>,
    Pwm0: super::PWM<D = D>,
    D: core::ops::Div<u16, Output = D> + Copy,
{
    fn set_volume(&mut self, _volume: D) -> &mut Self {
        // Currently not used
        self
    }

    fn set_frequency(&mut self, freq: u16) -> &mut Self {
        let max_duty = self.frequency.calculate_max_duty(freq);
        self.frequency.set_max_duty(max_duty);
        self.frequency.set_duty(max_duty / 2);
        self
    }

    #[allow(unused)]
    fn is_on(&self) -> bool {
        self.frequency.is_enabled()
    }

    fn on(&mut self, on: bool) {
        match on {
            true => self.frequency.enable(),
            false => self.frequency.disable(),
        }

        // Set duty again to start sequence
        self.frequency.set_duty(self.frequency.duty());
    }

    fn enable(&mut self, enable: bool) -> &mut Self {
        if enable {
            let _ = self.cpm1.set_low();
            let _ = self.cpm2.set_high();
        } else {
            let _ = self.cpm1.set_low();
            let _ = self.cpm2.set_low();
        }
        self
    }
}
