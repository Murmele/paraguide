#[cfg(feature = "steigen33")]
mod pwm;

#[cfg(feature = "steigen33")]
pub mod buzzer;

pub mod display;

#[cfg(feature = "steigen31")]
pub mod buzzer31;
#[cfg(feature = "steigen31")]
pub use buzzer31 as buzzer;

#[cfg(feature = "std")]
pub mod buzzer_linux;
#[cfg(feature = "std")]
pub use buzzer_linux as buzzer;

#[allow(unused)]
pub trait BuzzerTrait {
    type D;
    fn enable(&mut self, enable: bool) -> &mut Self;
    fn set_volume(&mut self, volume: Self::D) -> &mut Self;
    fn set_frequency(&mut self, freq: u16) -> &mut Self;
    fn on(&mut self, on: bool);
    fn is_on(&self) -> bool;
    fn set_duty(&mut self, duty: u16);
}
