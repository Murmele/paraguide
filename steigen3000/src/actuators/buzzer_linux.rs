use rodio::{source::SineWave, OutputStream, OutputStreamHandle, Sink, Source};

pub struct Buzzer {
    enabled: bool,
    #[allow(dead_code)]
    stream: OutputStream,
    #[allow(dead_code)]
    stream_handle: OutputStreamHandle,
    sink: Sink,
    on: bool,
    frequency: u16, // Buffer, because it sounds really ugly when setting often the frequency, but it did not change
}

impl Buzzer {
    #[allow(unused)]
    pub fn new() -> Result<Self, ()> {
        // Get a output stream handle to the default physical sound device
        let (stream, stream_handle) = OutputStream::try_default().map_err(|_| ())?;
        let sink = Sink::try_new(&stream_handle).map_err(|_| ())?;

        Ok(Self {
            enabled: false,
            stream,
            stream_handle,
            sink,
            on: false,
            frequency: 0,
        })
    }
}

impl super::BuzzerTrait for Buzzer {
    type D = u16;

    fn enable(&mut self, enable: bool) -> &mut Self {
        self.enabled = enable;
        self.sink.pause();
        self
    }

    fn is_on(&self) -> bool {
        false
    }

    fn on(&mut self, on: bool) {
        self.on = on;
        if !on || !self.enabled {
            self.sink.pause();
        } else {
            self.sink.play();
        }
    }

    fn set_frequency(&mut self, freq: u16) -> &mut Self {
        if freq != self.frequency {
            self.frequency = freq;
            self.sink.clear();
            let source = SineWave::new(freq as f32).amplify(10.0);
            self.sink.append(source);
            if self.on && self.enabled {
                self.sink.play();
            }
        }
        self
    }

    fn set_duty(&mut self, _duty: u16) {}

    fn set_volume(&mut self, _volume: u16) -> &mut Self {
        self
    }
}

#[cfg(test)]
mod tests {
    use crate::actuators::BuzzerTrait;

    use super::Buzzer;

    #[test]
    fn test_buzzer() {
        // In the CI the no audio card is available, or not detected so skip the test
        if let Ok(mut buz) = Buzzer::new() {
            buz.enable(true);
            buz.set_frequency(100);
            buz.on(true);

            const WAIT_TIME: u64 = 300;

            std::thread::sleep(std::time::Duration::from_millis(WAIT_TIME));

            buz.on(false);

            std::thread::sleep(std::time::Duration::from_millis(WAIT_TIME));

            buz.on(true);

            std::thread::sleep(std::time::Duration::from_millis(WAIT_TIME));

            buz.set_frequency(2000);

            std::thread::sleep(std::time::Duration::from_millis(WAIT_TIME));

            buz.on(false);

            buz.set_frequency(7000);

            std::thread::sleep(std::time::Duration::from_millis(WAIT_TIME));
        }
    }
}
