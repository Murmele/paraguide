mod trait_impl;

#[allow(unused)]
pub trait PWM {
    type D;
    fn calculate_max_duty(&self, freq: u16) -> Self::D;
    fn set_max_duty(&mut self, max_duty: Self::D);
    fn max_duty(&self) -> Self::D;
    fn duty(&self) -> Self::D;
    fn set_duty(&mut self, duty: Self::D);
    fn is_enabled(&self) -> bool;
    fn enable(&mut self);
    fn disable(&mut self);
}
