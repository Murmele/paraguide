//! Buzzer driver for Steigen3000 V3.3

#![cfg(feature = "steigen33")]

use core::fmt::Debug;
use embedded_hal as hal;
use paraguidecore::common::Float;

pub struct Buzzer<OutputPin, Pwm0, Pwm1> {
    supply_en_pin: OutputPin,
    en_pin: OutputPin,
    frequency: Pwm0,
    volume: Pwm1,
    calculate_ideal_duty: bool,
    duty: u16,
}

impl<OutputPin, Pwm0, Pwm1, E> Buzzer<OutputPin, Pwm0, Pwm1>
where
    OutputPin: hal::digital::OutputPin<Error = E>,
    Pwm0: super::pwm::PWM<D = u16>,
    Pwm1: super::pwm::PWM<D = u16>,
    E: Debug,
{
    pub fn new(
        mut supply_en_pin: OutputPin,
        mut en_pin: OutputPin,
        mut frequency: Pwm0,
        mut volume: Pwm1,
        calculate_ideal_duty: bool,
    ) -> Self {
        let _ = supply_en_pin.set_low();
        let _ = en_pin.set_low();
        frequency.disable();
        volume.disable();
        volume.set_max_duty(volume.calculate_max_duty(2000));
        Buzzer {
            supply_en_pin,
            en_pin,
            frequency,
            volume,
            duty: 500,
            calculate_ideal_duty,
        }
    }

    pub fn enable_supply(&mut self, enable: bool) -> &mut Self {
        if enable {
            let _ = self.supply_en_pin.set_high();
        } else {
            let _ = self.supply_en_pin.set_low();
        }
        self
    }

    fn calculate_ideal_duty(frequency: u16) -> u16 {
        const FREQ_MIN_SLOPE: Float = 500.;
        const DUTY_MIN_SLOPE: Float = 473.; // @ FREQ_MIN_SLOPE 1/1000!
        const SLOPE: Float = (454. - DUTY_MIN_SLOPE) / (1000. - 500.);

        let duty = (SLOPE * (frequency as Float - FREQ_MIN_SLOPE) + DUTY_MIN_SLOPE) as u16;
        if duty > 1000 {
            return 1000;
        }
        return duty;
    }
}

impl<OutputPin, Pwm0, Pwm1, E> super::BuzzerTrait for Buzzer<OutputPin, Pwm0, Pwm1>
where
    OutputPin: hal::digital::OutputPin<Error = E>,
    Pwm0: super::pwm::PWM<D = u16>,
    Pwm1: super::pwm::PWM<D = u16>,
    E: Debug,
{
    type D = u16;

    fn set_volume(&mut self, volume: Self::D) -> &mut Self {
        self.volume.set_duty(volume);
        self
    }

    fn set_frequency(&mut self, freq: u16) -> &mut Self {
        if freq != 0 {
            let max_duty = self.frequency.calculate_max_duty(freq);
            self.frequency.set_max_duty(max_duty);
            let counter_threshold;
            let duty;
            if self.calculate_ideal_duty {
                duty = Self::calculate_ideal_duty(freq);
            } else {
                duty = self.duty;
            }
            counter_threshold = (f32::from(max_duty) * (duty as f32 / 1000.)) as u16;
            self.frequency.set_duty(counter_threshold);
        }
        self
    }

    fn on(&mut self, on: bool) {
        match on {
            true => self.frequency.enable(),
            false => self.frequency.disable(),
        }
        // Set duty again to start sequence
        self.frequency.set_duty(self.frequency.duty());
    }

    fn is_on(&self) -> bool {
        self.frequency.is_enabled()
    }

    fn enable(&mut self, enable: bool) -> &mut Self {
        if enable {
            let _ = self.en_pin.set_high();
        } else {
            let _ = self.en_pin.set_low();
        }
        self
    }

    fn set_duty(&mut self, duty: u16) {
        self.duty = duty;
    }
}

#[cfg(test)]
mod tests {
    use super::*;
    #[test]
    fn ideal_duty() {
        assert_eq!(Buzzer::calculate_ideal_duty(500), 473);
        assert_eq!(Buzzer::calculate_ideal_duty(1000), 454);
    }
}
