use embassy_nrf::pwm::{self, SimplePwm};

impl<'a, T> super::PWM for SimplePwm<'a, T>
where
    T: embassy_nrf::pwm::Instance,
{
    type D = u16;
    fn calculate_max_duty(&self, freq: u16) -> Self::D {
        ((pwm::PWM_CLK_HZ >> (self.prescaler() as u16)) / freq as u32) as Self::D
    }

    fn set_max_duty(&mut self, max_duty: Self::D) {
        SimplePwm::set_max_duty(self, max_duty);
    }

    fn duty(&self) -> Self::D {
        SimplePwm::duty(&self, 0)
    }

    fn max_duty(&self) -> Self::D {
        SimplePwm::max_duty(&self)
    }

    fn set_duty(&mut self, duty: Self::D) {
        // TODO: find out which channel!
        SimplePwm::set_duty(self, 0, duty);
    }

    fn is_enabled(&self) -> bool {
        SimplePwm::is_enabled(self)
    }

    fn enable(&mut self) {
        SimplePwm::enable(&self);
    }

    fn disable(&mut self) {
        SimplePwm::disable(&self);
    }
}
