pub use super::display::DisplaySpiMoc as Display;
pub use super::display::OutputPinMoc as OutputPin;
use crate::sensors;
use paraguidecore::{self as pg, igc};
use pg::file_handling::File as pgFile;
use std::sync::Mutex;
use std::time::{SystemTime, UNIX_EPOCH};
use tokio::time;

#[derive(Clone, Copy)]
pub struct FileHandler {}

pub enum Error {
    Unknown,
    // Finished,
}

impl FileHandler {
    pub fn new() -> Self {
        FileHandler {}
    }
}

pub struct File {}

impl pg::file_handling::FileHandler<File> for FileHandler {
    type Error = Error;

    async fn file_exists(&self, _file_name: &str) -> Result<bool, Self::Error> {
        Err(Error::Unknown)
    }

    async fn open(
        &mut self,
        _filename: &str,
        _mode: pg::file_handling::Mode,
    ) -> Result<File, Self::Error> {
        Err(Error::Unknown)
    }

    async fn create_folders(&self, _path: &str) -> Result<(), Self::Error> {
        Err(Error::Unknown)
    }
}

impl igc::Writer for File {
    async fn push(&mut self, _c: char) -> Result<(), ()> {
        Err(())
    }

    async fn push_str(&mut self, _content: &str) -> Result<(), ()> {
        Err(())
    }
}

impl pgFile for File {
    type Error = Error;
    async fn close(&mut self) -> () {}

    async fn read_line(
        &mut self,
        _out: &mut heapless::String<{ pg::file_handling::LINE_LENGHT }>,
    ) -> Result<(), Self::Error> {
        Err(Error::Unknown)
    }

    async fn write(&mut self, _text: &str) -> Result<(), Self::Error> {
        Err(Error::Unknown)
    }
}

pub struct TimeKeeper {}

impl paraguidecore::logging::Timekeeper for TimeKeeper {
    fn get_time_us() -> u64 {
        return SystemTime::now()
            .duration_since(UNIX_EPOCH)
            .unwrap()
            .as_micros() as u64;
    }
}

pub async fn sleep_us(w: u64) {
    use std::time::Duration;
    time::sleep(Duration::from_micros(w)).await;
}

pub(super) static PRESS_TEMP: Mutex<Result<sensors::PressTemp, ()>> = Mutex::new(Err(()));
pub(super) static ACCEL_GYRO: Mutex<Result<sensors::AccelGyro, ()>> = Mutex::new(Err(()));

pub async fn pressure_temperature() -> Result<sensors::PressTemp, ()> {
    let res = match PRESS_TEMP.lock() {
        Ok(mut guard) => {
            let r = *guard;
            *guard = Err(());
            r
        }
        Err(_) => Err(()),
    };
    return res;
}

pub async fn acceleration_rotational_speed() -> Result<sensors::AccelGyro, ()> {
    let res = match ACCEL_GYRO.lock() {
        Ok(mut guard) => {
            let r = *guard;
            *guard = Err(());
            r
        }
        Err(_) => Err(()),
    };
    return res;
}

pub async fn receive_nmea_messages() -> Result<nmea::ParseResult, ()> {
    return Err(());
}
