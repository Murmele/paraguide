// std related functions

use super::interface;
use crate::actuators::buzzer;
use crate::DisplayStateMutex;
use crate::{error, tasks};
use crate::{main_loop, sensors::PressTemp};
use paraguidecore::{common::Float, igc, paraguide};
use std::time::SystemTime;
use tokio::{sync::watch, time};

pub async fn main(
    mpsc_tx_vario_signal: watch::Sender<paraguidecore::paraguide::VarioSoundSettings>,
    display_state_mutex: &DisplayStateMutex,
) -> () {
    let device_info = paraguide::DeviceInfo {
        manufacturer: "Martin Marmsoler",
        model: "Paraguide",
        firmware_version: env!("CARGO_PKG_VERSION"),
        hw_revision: "na",
        igc_fr: "Prgd",             // less than 6 characters!
        igc_manufacturer_id: "XPG", // X + 2 Characters

        gnss_receiver_manufacturer: "na",
        gnss_receiver_model: "na",
        gnss_receiver_max_alt: "80000",
        gnss_receiver_channels: "0",
        gnss_receiver_systems: &[],
        pressure_sensor_manufacturer: "na",
        pressure_sensor_model: "na",
        pressure_sensor_max_alt: "na",
    };
    main_loop(device_info, &mpsc_tx_vario_signal, display_state_mutex).await;
}

enum ParsedContent {
    Pressure(Float),
    Height(Float),
}

pub async fn buzzer(
    vario_sound_signal_rx: watch::Receiver<paraguidecore::paraguide::VarioSoundSettings>,
) {
    if let Ok(buzzer) = buzzer::Buzzer::new() {
        tasks::buzzer::buzzer_common(buzzer, &vario_sound_signal_rx).await;
    } else {
        error!("Unable to start buzzer!");
    }
}

pub async fn simulation_task() {
    use std::time::Duration;
    use std::{env, fs, path::Path};

    let start = SystemTime::now();

    let manifest_path = Path::new(env!("CARGO_MANIFEST_DIR")).parent().unwrap();
    let file_path = manifest_path.join("steigen3000/resources/00010.IGC");
    let s = fs::read_to_string(file_path).expect("Unable to open test data file");
    let lines = s.split(igc::NEW_LINE);

    let mut first_line = true;
    let mut start_time_file_ms = 0.0;
    for line in lines {
        let parsed_content;
        let time_ms;
        if !line.starts_with("LXS3") {
            continue;
        }

        if line.starts_with("LXS32_") {
            let c: Vec<_> = line.split(',').collect();
            time_ms = c[1].parse::<Float>().unwrap();
            let pressure = c[2].parse::<Float>().unwrap();
            parsed_content = ParsedContent::Pressure(pressure);
        } else if line.starts_with("LXS31_") {
            let c: Vec<_> = line.split(',').collect();
            time_ms = c[1].parse::<Float>().unwrap();
            let height_meas = c[2].parse::<Float>().unwrap();
            parsed_content = ParsedContent::Height(height_meas);
        } else {
            continue;
        }

        if first_line {
            first_line = false;
            start_time_file_ms = time_ms;
        }

        let time = (time_ms - start_time_file_ms) as u128;

        let now = SystemTime::now().duration_since(start).unwrap().as_millis();

        if now < time {
            let diff = time - now;
            time::sleep(Duration::from_millis(diff as u64)).await;
        }

        match parsed_content {
            ParsedContent::Pressure(pressure) => {
                let mut guard = interface::PRESS_TEMP.lock().unwrap();
                *guard = Ok(PressTemp {
                    pressure: pressure as f32,
                    temperature: 0.0,
                });
                println!("{}", pressure);
            }
            ParsedContent::Height(_h) => (),
        }
    }
    panic!("Finished");
}

pub async fn display(data: crate::DisplayStateMutex) {
    use tasks::display::{display_common, Display, Orientation, LS027B7DH01A};

    let orientation = Orientation::Degree180;
    let display = LS027B7DH01A::new(
        interface::Display::new(orientation),
        interface::OutputPin {},
    );

    let mut display = Display::new(display, orientation);

    display_common(&mut display, &data).await
}
