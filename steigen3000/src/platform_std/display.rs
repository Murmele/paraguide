use crate::actuators::display::ls027b7dh01a::LS027B7DH01A;
use crate::tasks::display::Orientation;
use embedded_graphics::prelude::*;
use embedded_graphics::{draw_target::DrawTarget, pixelcolor::BinaryColor};
use embedded_graphics_simulator::{
    BinaryColorTheme, OutputSettingsBuilder, SimulatorDisplay, Window,
};

use embedded_hal::digital;
use embedded_hal::digital::OutputPin;
use embedded_hal::spi;
use embedded_hal_async::spi::SpiBus;

pub struct DisplaySpiMoc {
    window: Window,
    display: SimulatorDisplay<BinaryColor>,
    orientation: Orientation,
}

const PIXEL_HORIZONTAL: u32 = LS027B7DH01A::PIXEL_HORIZONTAL as u32;
const PIXEL_VERTICAL: u32 = LS027B7DH01A::PIXEL_VERTICAL as u32;

impl SpiBus for DisplaySpiMoc {
    async fn flush(&mut self) -> Result<(), Self::Error> {
        panic!("Not implemented");
    }

    async fn read(&mut self, _words: &mut [u8]) -> Result<(), Self::Error> {
        panic!("Not implemented");
    }

    async fn transfer(&mut self, _read: &mut [u8], _write: &[u8]) -> Result<(), Self::Error> {
        panic!("Not implemented");
    }

    async fn transfer_in_place(&mut self, _words: &mut [u8]) -> Result<(), Self::Error> {
        panic!("Not implemented");
    }

    async fn write(&mut self, words: &[u8]) -> Result<(), Self::Error> {
        let mut pixels = Vec::new();
        match self.orientation {
            Orientation::Degree0 => {
                for (index, word) in words.into_iter().enumerate() {
                    let line_index = index / LS027B7DH01A::BUFFER_LINE_SIZE_BYTE_MULTI_LINE;
                    let mut byte_index = index % LS027B7DH01A::BUFFER_LINE_SIZE_BYTE_MULTI_LINE;
                    if byte_index == 0 || byte_index == 1 {
                        // if byte_index == 0 {
                        //     println!("");
                        // }
                        // Control bytes
                        continue;
                    }
                    // print!("{:08b}", !*word);
                    byte_index -= LS027B7DH01A::NUM_DUMMY_BYTES;

                    // LSB first
                    for i in 0..8 {
                        let x_pixel_pos = byte_index * 8 + i;
                        let v = (*word >> i) & 0x1;
                        let color;
                        if v == LS027B7DH01A::PIXEL_ON_VALUE {
                            color = BinaryColor::On;
                        } else {
                            color = BinaryColor::Off;
                        }
                        let pixel = Pixel {
                            0: Point {
                                x: x_pixel_pos as i32,
                                y: line_index as i32,
                            },
                            1: color,
                        };
                        pixels.push(pixel);
                    }
                }
            }
            Orientation::Degree180 => {
                // Rotate back, because it makes no sense to have it on std rotated (at least for now)
                for (index, word) in words.into_iter().rev().enumerate() {
                    let line_index = index / LS027B7DH01A::BUFFER_LINE_SIZE_BYTE_MULTI_LINE;
                    let mut byte_index = index % LS027B7DH01A::BUFFER_LINE_SIZE_BYTE_MULTI_LINE;
                    if byte_index == 0 || byte_index == 1 {
                        // Control bytes
                        continue;
                    }
                    byte_index -= LS027B7DH01A::NUM_DUMMY_BYTES;

                    // LSB first
                    for i in 0..8 {
                        let x_pixel_pos = byte_index * 8 + (7 - i);
                        let v = (*word >> i) & 0x1;
                        let color;
                        if v == LS027B7DH01A::PIXEL_ON_VALUE {
                            color = BinaryColor::On;
                        } else {
                            color = BinaryColor::Off;
                        }
                        let pixel = Pixel {
                            0: Point {
                                x: x_pixel_pos as i32,
                                y: line_index as i32,
                            },
                            1: color,
                        };
                        pixels.push(pixel);
                    }
                }
            }
        }
        self.display.draw_iter(pixels).unwrap();

        // Handle events, so the display does not freeze
        self.window.update(&self.display);
        // So the sdl display can be moved and reacts
        for _e in self.window.events() {}

        Ok(())
    }
}

impl DisplaySpiMoc {
    pub fn new(orientation: Orientation) -> Self {
        let output_settings = OutputSettingsBuilder::new()
            .scale(2)
            .theme(BinaryColorTheme::Inverted)
            .build();
        Self {
            window: Window::new("Steigen3000", &output_settings),
            display: SimulatorDisplay::new(Size::new(PIXEL_HORIZONTAL, PIXEL_VERTICAL)),
            orientation,
        }
    }
}

// impl DisplayBytes<core::convert::Infallible> for Display {
//     fn draw_byte(&mut self, x: i32, y: i32, byte: u8) -> Result<(), core::convert::Infallible> {
//         if x % 8 != 0 {
//             error!("draw_byte(): Position is not aligned!");
//             panic!("Test");
//             return Ok(());
//         }
//         let mut pixels = Vec::new();
//         for i in 0..8 {
//             let v = (byte >> i) & 0x1;

//             let pos_x = x + i;

//             let color;
//             // Inverted because 1 is white on the Sharp display
//             if v > 0 {
//                 color = BinaryColor::Off;
//             } else {
//                 color = BinaryColor::On;
//             }
//             let pixel = Pixel {
//                 0: Point { x: pos_x, y },
//                 1: color,
//             };
//             pixels.push(pixel);
//         }
//         self.draw_iter(pixels)
//     }
// }

// impl DisplayApply<core::convert::Infallible> for Display {
//     async fn apply(&mut self) -> Result<(), core::convert::Infallible> {
//         self.window.update(&self.display);
//         // So the sdl display can be moved and reacts
//         for _e in self.window.events() { // Handle events
//         }
//         Ok(())
//     }
// }

// impl DisplayImage<core::convert::Infallible> for Display {
//     fn draw_image<const WIDTH: usize, const HEIGHT: usize>(
//         &mut self,
//         x: usize,
//         y: usize,
//         image: &[[u8; WIDTH]; HEIGHT],
//     ) -> Result<(), core::convert::Infallible> {
//         let mut pixels = Vec::new();
//         for (row_idx, row_data) in image.into_iter().enumerate() {
//             for (byte_idx, byte) in row_data.into_iter().enumerate() {
//                 for bit_idx in 0..8 {
//                     let color;
//                     // Inverted because 1 is white on the Sharp display
//                     if (byte & (1 << bit_idx)) > 0 {
//                         color = BinaryColor::Off;
//                     } else {
//                         color = BinaryColor::On;
//                     }
//                     let pixel = Pixel {
//                         0: Point {
//                             x: (x + byte_idx * 8 + bit_idx) as i32,
//                             y: (y + row_idx) as i32,
//                         },
//                         1: color,
//                     };
//                     pixels.push(pixel);
//                 }
//             }
//         }
//         self.draw_iter(pixels)
//     }
// }

// impl DrawTarget for Display {
//     type Color = BinaryColor;
//     type Error = core::convert::Infallible;
//     fn clear(&mut self, color: Self::Color) -> Result<(), Self::Error> {
//         self.display.clear(color)
//     }

//     fn draw_iter<I>(&mut self, pixels: I) -> Result<(), Self::Error>
//     where
//         I: IntoIterator<Item = Pixel<Self::Color>>,
//     {
//         self.display.draw_iter(pixels)
//     }

//     fn fill_contiguous<I>(&mut self, area: &Rectangle, colors: I) -> Result<(), Self::Error>
//     where
//         I: IntoIterator<Item = Self::Color>,
//     {
//         self.display.fill_contiguous(area, colors)
//     }

//     fn fill_solid(&mut self, area: &Rectangle, color: Self::Color) -> Result<(), Self::Error> {
//         self.display.fill_solid(area, color)
//     }
// }

// impl Dimensions for Display {
//     fn bounding_box(&self) -> Rectangle {
//         Rectangle {
//             top_left: Point { x: 0, y: 0 },
//             size: Size {
//                 width: PIXEL_HORIZONTAL,
//                 height: PIXEL_VERTICAL,
//             },
//         }
//     }
// }

impl spi::ErrorType for DisplaySpiMoc {
    type Error = Error;
}

#[derive(Debug)]
pub enum Error {}

impl digital::Error for Error {
    fn kind(&self) -> digital::ErrorKind {
        digital::ErrorKind::Other
    }
}

impl spi::Error for Error {
    fn kind(&self) -> spi::ErrorKind {
        spi::ErrorKind::Other
    }
}

pub struct OutputPinMoc {}

impl digital::ErrorType for OutputPinMoc {
    type Error = Error;
}

impl OutputPin for OutputPinMoc {
    fn set_high(&mut self) -> Result<(), Self::Error> {
        Ok(())
    }

    fn set_low(&mut self) -> Result<(), Self::Error> {
        Ok(())
    }

    fn set_state(&mut self, _state: embedded_hal::digital::PinState) -> Result<(), Self::Error> {
        Ok(())
    }
}
