#[cfg(feature = "steigen33")]
use crate::sensors;
use crate::tasks;
use crate::tasks::sd::{self, FileOperationRequest};
use crate::{error, trace};
use embassy_sync::{blocking_mutex::raw::CriticalSectionRawMutex, mutex::Mutex};
use embedded_sdmmc;
use heapless::Vec;
use paraguidecore::{self as pg, igc};
use pg::file_handling;
use pg::file_handling::File as pgFile;

#[cfg(feature = "usb_vario_simulation")]
pub(crate) use super::main::VARIO_SOUND_SIGNAL;

// https://www.batteryskills.com/lithium-battery-voltage-chart/
const SOC_VALUES: [f32; 21] = [
    100., 95., 90., 85., 80., 75., 70., 65., 60., 55., 50., 45., 40., 35., 30., 25., 20., 15., 10.,
    5., 0.,
];
const VOLTAGE_VALUES: [f32; 21] = [
    4.2, 4.15, 4.11, 4.08, 4.02, 3.98, 3.95, 3.91, 3.87, 3.82, 3.78, 3.74, 3.70, 3.66, 3.62, 3.58,
    3.54, 3.49, 3.42, 3.30, 3.0,
];

pub fn calculate_soc(voltage: f32) -> f32 {
    if voltage > VOLTAGE_VALUES[0] {
        return SOC_VALUES[0];
    }

    let mut idx = 0;
    let mut prev = VOLTAGE_VALUES[0];
    for value in VOLTAGE_VALUES[1..].into_iter() {
        if voltage >= *value {
            let dist = (voltage - prev) / (value - prev);
            let soc = SOC_VALUES[idx] - dist * (SOC_VALUES[idx] - SOC_VALUES[idx + 1]);
            return soc;
        }
        prev = *value;
        idx += 1;
    }

    return *SOC_VALUES.last().unwrap();
}

#[cfg(feature = "steigen33")]
pub async fn pressure_temperature() -> Result<sensors::PressTemp, ()> {
    return tasks::i2c_sensors::PRESS_TEMP
        .lock(|f| *f.borrow())
        .map_err(|_| ());
}

#[cfg(feature = "steigen33")]
pub async fn acceleration_rotational_speed() -> Result<sensors::AccelGyro, ()> {
    return tasks::i2c_sensors::ACCEL_GYRO
        .lock(|f| *f.borrow())
        .map_err(|_| ());
}

#[cfg(feature = "steigen33")]
pub async fn receive_nmea_messages() -> Result<nmea::ParseResult, ()> {
    return tasks::gps::NMEA_MESSAGES.try_receive().map_err(|_| ());
}

#[cfg(feature = "steigen33")]
pub async fn sleep_us(w: u64) {
    let _ = embassy_time::Timer::after_micros(w).await;
}

#[cfg(feature = "embassy")]
pub struct TimeKeeper {}

#[cfg(feature = "embassy")]
impl paraguidecore::logging::Timekeeper for TimeKeeper {
    fn get_time_us() -> u64 {
        use embassy_time::Instant;
        Instant::now().as_micros()
    }
}

enum Operation {
    Write(Option<&'static Mutex<CriticalSectionRawMutex, FileOperationRequest>>),
    Read(
        (
            Result<(), Error>,
            Option<&'static Mutex<CriticalSectionRawMutex, sd::ReadOperation>>,
        ),
    ),
}

pub struct File {
    file: sd::File,
    operation: Operation,
    next_character_index: usize,
}

#[derive(Clone, Copy)]
pub struct FileHandler {}

#[derive(Debug, PartialEq, Clone, Copy)]
pub enum Error {
    Unknown,
    BufferFull,
    Finished,
}

impl FileHandler {
    pub fn new() -> Self {
        FileHandler {}
    }
}

impl File {
    async fn push(&mut self, c: char) -> Result<(), Error> {
        match &mut self.operation {
            Operation::Read(_) => {
                error!("Writing not possible");
                return Err(Error::Unknown);
            }
            Operation::Write(w) => {
                let mut retry_counter = 0;
                let mut success = false;
                while retry_counter < 2 {
                    retry_counter += 1;
                    if w.is_none() {
                        let m = sd::get_file_operation_mutex().await;
                        let mut fo = m.lock().await;
                        fo.process_status = sd::Status::Occupied;
                        if let sd::Operation::Write(f) = &mut fo.operation {
                            f.clear();
                        } else {
                            fo.operation = sd::Operation::Write(Vec::new());
                        }
                        *w = Some(m);
                    }

                    let mutex = w.unwrap();
                    let mut fo = mutex.lock().await;
                    let sd::Operation::Write(f) = &mut fo.operation else {
                        unreachable!()
                    };
                    if let Err(_) = f.push(c as u8) {
                        //warn!("Buffer full: {}", fo.content.len());
                        drop(fo); // Important!
                        sd::new_file_operation(self.file, w.unwrap()).await;
                        *w = None;
                    } else {
                        success = true;
                        break;
                    }
                }
                if !success {
                    error!("No success in pusing to sd!");
                    return Err(Error::Unknown);
                }
                return Ok(());
            }
        }
    }
}

impl igc::Writer for File {
    async fn push(&mut self, c: char) -> Result<(), ()> {
        self.push(c).await.map_err(|_| ())
    }

    async fn push_str(&mut self, content: &str) -> Result<(), ()> {
        self.write(content).await.map_err(|_| {})
    }
}

impl pgFile for File {
    type Error = Error;
    /// Read a line from the file
    async fn read_line(
        &mut self,
        out: &mut heapless::String<{ pg::file_handling::LINE_LENGHT }>,
    ) -> Result<(), Self::Error> {
        out.clear();

        match &mut self.operation {
            Operation::Write(_) => {
                error!("Reading not possible");
                return Err(Error::Unknown);
            }
            Operation::Read(read_operation) => {
                // Read until success
                if read_operation.0.is_err() {
                    return read_operation.0;
                }
                loop {
                    if read_operation.1.is_none() {
                        // Read next buffer
                        self.next_character_index = 0;
                        *read_operation = (Ok(()), Some(sd::READ_SIGNAL.receive().await));
                    }

                    let mutex = read_operation.1.unwrap();
                    let mut read_status = mutex.lock().await;
                    if read_status.result.is_ok() {
                        match read_status.status {
                            sd::ReadStatus::NotStarted => {
                                *read_operation = (Err(Error::Unknown), None);
                                return Err(Error::Unknown);
                            }
                            sd::ReadStatus::Finished | sd::ReadStatus::Ongoing => {
                                for i in self.next_character_index..read_status.content.len() {
                                    let c = read_status.content[i] as char;
                                    if c == '\n' {
                                        self.next_character_index += 1;
                                        return Ok(());
                                    } else {
                                        match out.push(c) {
                                            Ok(_) => {
                                                self.next_character_index += 1;
                                            }
                                            Err(_) => {
                                                // out buffer is full, but not a complete line can be read
                                                return Err(Error::BufferFull);
                                            }
                                        }
                                    }
                                }
                                *read_operation = (Err(Error::Finished), None);
                                read_status.process_status = sd::Status::Free;
                                if read_status.status == sd::ReadStatus::Finished {
                                    // No more can be read
                                    return Err(Error::Finished); // unable to detect if the last line is complete. The new line character might miss
                                } else {
                                    // Finished read
                                    sd::READ_FINISHED_SIGNAL.signal(sd::ReadStatus::Ongoing);
                                }
                            }
                        }
                    } else {
                        //error!("Unable to read from file!");
                        *read_operation = (Err(Error::Unknown), None);
                        return Err(Error::Unknown);
                    }
                }
            }
        }
    }

    /// Write a line to the file
    async fn write(&mut self, text: &str) -> Result<(), Self::Error> {
        for c in text.chars() {
            self.push(c).await?;
        }
        return Ok(());
    }

    async fn close(&mut self) {
        if let Operation::Write(Some(mutex)) = self.operation {
            // Flush last cached data
            sd::new_file_operation(self.file, mutex).await;
            self.operation = Operation::Write(None);
        }
        sd::close(self.file).await;
    }
}

fn file_handling_mode_to_sd_mode(mode: file_handling::Mode) -> embedded_sdmmc::Mode {
    match mode {
        file_handling::Mode::Read => embedded_sdmmc::Mode::ReadOnly,
        file_handling::Mode::WriteAppend => embedded_sdmmc::Mode::ReadWriteAppend,
        file_handling::Mode::WriteCreate => embedded_sdmmc::Mode::ReadWriteCreate,
        file_handling::Mode::WriteCreateOrAppend => embedded_sdmmc::Mode::ReadWriteCreateOrAppend,
        file_handling::Mode::WriteCreateOrTruncate => {
            embedded_sdmmc::Mode::ReadWriteCreateOrTruncate
        }
        file_handling::Mode::WriteTruncate => embedded_sdmmc::Mode::ReadWriteTruncate,
    }
}

impl pg::file_handling::FileHandler<File> for FileHandler {
    type Error = sd::VolumeError;
    /// Open a file with filename filename
    /// Only one file at the time can be write/read. Close the file to read/write to a new
    /// one
    async fn open(
        &mut self,
        file_name: &str,
        mode: file_handling::Mode,
    ) -> Result<File, Self::Error> {
        let mode = file_handling_mode_to_sd_mode(mode);
        if let embedded_sdmmc::Mode::ReadOnly = mode {
            let mut file = File {
                file: sd::open(file_name, mode).await?,
                next_character_index: 0,
                operation: Operation::Read((Ok(()), None)),
            };
            // Start read operation
            if let Ok(m) = sd::read_operation(file.file).await {
                trace!("{}: Start read operation", file_name);
                file.operation = Operation::Read((Ok(()), Some(m)));
            } else {
                error!("{}: Unable to start read operation", file_name);
            }
            return Ok(file);
        } else {
            Ok(File {
                file: sd::open(file_name, mode).await?,
                next_character_index: 0,
                operation: Operation::Write(None),
            })
        }
    }

    async fn file_exists(&self, file_name: &str) -> Result<bool, Self::Error> {
        sd::file_exists_operation(file_name).await
    }

    async fn create_folders(&self, path: &str) -> Result<(), Self::Error> {
        sd::create_folders(path).await
    }
}

#[cfg(test)]
mod tests {
    use super::*;
    use paraguidecore::file_handling as fh;
    use paraguidecore::file_handling::FileHandler as FH;

    use embedded_sdmmc::VolumeManager;
    use heapless::String;
    use linux_block_device::{Clock, LinuxBlockDevice};
    use tasks::sd::{self as sd, sd_generic, tests::linux_block_device};

    const DISKNAME: &str = "DISK1.img";
    const PRINT_DEBUG: bool = false;

    async fn read() -> Result<(), ()> {
        let mut fh = FileHandler::new();
        let mut file = fh.open("config.pg", fh::Mode::Read).await.unwrap();
        let mut out: String<{ pg::file_handling::LINE_LENGHT }> = String::new();

        file.read_line(&mut out).await.unwrap();
        assert_eq!(out.as_str(), "ParaguideConfig:0");
        file.read_line(&mut out).await.unwrap();
        assert_eq!(out.as_str(), "PilotName:Max Mustermann");
        file.read_line(&mut out).await.unwrap();
        assert_eq!(out.as_str(), "GliderType:Icaro Gravis 2 S");
        file.read_line(&mut out).await.unwrap();
        assert_eq!(out.as_str(), "[Beep]");
        file.read_line(&mut out).await.unwrap();
        assert_eq!(out.as_str(), "Frequency:10, 20,100");
        file.read_line(&mut out).await.unwrap();
        assert_eq!(out.as_str(), "OnTime:1200,3,421");
        file.read_line(&mut out).await.unwrap();
        assert_eq!(out.as_str(), "OffTime:192, 2192, 32");
        file.read_line(&mut out).await.unwrap();
        assert_eq!(out.as_str(), "[/Beep]");

        assert_eq!(file.read_line(&mut out).await, Err(Error::Finished)); // File finished
        file.close().await;
        sd::stop_operation().await.unwrap();
        Ok(())
    }

    #[tokio::test]
    async fn simple_read() {
        sd::FILE_OPERATION_STATISTIC.lock().await.clear();
        // TODO: really needed to do all the time?
        let td = sd::tests::get_test_disk(DISKNAME);

        let lbd: LinuxBlockDevice<std::string::String> =
            LinuxBlockDevice::new(td.file_name(), PRINT_DEBUG)
                .await
                .unwrap();
        let mut volume_mgr: VolumeManager<_, _, { sd::MAX_DIRS }, { sd::MAX_FILES }> =
            VolumeManager::new_with_limits(lbd, Clock, 100);
        let sd_future = sd_generic(&mut volume_mgr);

        let (sd_result, second_result) = tokio::join!(sd_future, read());

        assert_eq!(sd_result, Ok(()));
        assert_eq!(second_result, Ok(()));
        let m = sd::FILE_OPERATION_STATISTIC.lock().await;
        assert_eq!(m.len(), 1 + 1 + 1 + 1); // Less than 1024 characters to read so complete file in buffer. Last is the stop command
        assert_eq!(m[0].process_status, sd::Status::Free);
        let mut filename = String::new();
        filename.push_str("config.pg").unwrap();
        assert_eq!(
            m[0].operation,
            sd::Operation::Open((embedded_sdmmc::Mode::ReadOnly, filename))
        );
        assert_eq!(m[1].process_status, sd::Status::Free);
        assert_eq!(m[1].operation, sd::Operation::Read);
        assert_eq!(m[2].process_status, sd::Status::Free);
        assert_eq!(m[2].operation, sd::Operation::Close);
        assert_eq!(m[3].process_status, sd::Status::Free);
        assert_eq!(m[3].operation, sd::Operation::STOP);
    }

    async fn read_more_than_lines() -> Result<(), ()> {
        let mut fh = FileHandler::new();
        let mut file = fh.open("config.pg", fh::Mode::Read).await.unwrap();
        let mut out: String<{ fh::LINE_LENGHT }> = String::new();

        file.read_line(&mut out).await.unwrap();
        assert_eq!(out.as_str(), "ParaguideConfig:0");
        file.read_line(&mut out).await.unwrap();
        assert_eq!(out.as_str(), "PilotName:Max Mustermann");
        file.read_line(&mut out).await.unwrap();
        assert_eq!(out.as_str(), "GliderType:Icaro Gravis 2 S");
        file.read_line(&mut out).await.unwrap();
        assert_eq!(out.as_str(), "[Beep]");
        file.read_line(&mut out).await.unwrap();
        assert_eq!(out.as_str(), "Frequency:10, 20,100");
        file.read_line(&mut out).await.unwrap();
        assert_eq!(out.as_str(), "OnTime:1200,3,421");
        file.read_line(&mut out).await.unwrap();
        assert_eq!(out.as_str(), "OffTime:192, 2192, 32");
        file.read_line(&mut out).await.unwrap();
        assert_eq!(out.as_str(), "[/Beep]");

        assert_eq!(file.read_line(&mut out).await, Err(Error::Finished)); // File finished

        // No more lines to read, try another time
        assert_eq!(file.read_line(&mut out).await.map_err(|_| ()), Err(()));

        file.close().await;
        sd::stop_operation().await.unwrap();
        Ok(())
    }

    /// Similar to simple read, but after the last line two tries to read are executed
    /// Read more lines than actually in the file. This should not lead to a crash nor to a deadlock
    #[tokio::test]
    async fn test_read_more_than_lines() {
        sd::FILE_OPERATION_STATISTIC.lock().await.clear();
        // TODO: really needed to do all the time?
        let td = sd::tests::get_test_disk(DISKNAME);

        let lbd: LinuxBlockDevice<std::string::String> =
            LinuxBlockDevice::new(td.file_name(), PRINT_DEBUG)
                .await
                .unwrap();
        let mut volume_mgr: VolumeManager<_, _, { sd::MAX_DIRS }, { sd::MAX_FILES }> =
            VolumeManager::new_with_limits(lbd, Clock, 100);
        let sd_future = sd_generic(&mut volume_mgr);

        let (sd_result, second_result) = tokio::join!(sd_future, read_more_than_lines());

        assert_eq!(sd_result, Ok(()));
        assert_eq!(second_result, Ok(()));
        let m = sd::FILE_OPERATION_STATISTIC.lock().await;
        assert_eq!(m.len(), 1 + 1 + 1 + 1); // Less than 1024 characters to read so complete file in buffer. Last is the stop command
        assert_eq!(m[0].process_status, sd::Status::Free);
        let mut filename = String::new();
        filename.push_str("config.pg").unwrap();
        assert_eq!(
            m[0].operation,
            sd::Operation::Open((embedded_sdmmc::Mode::ReadOnly, filename))
        );
        assert_eq!(m[1].process_status, sd::Status::Free);
        assert_eq!(m[1].operation, sd::Operation::Read);
        assert_eq!(m[2].process_status, sd::Status::Free);
        assert_eq!(m[2].operation, sd::Operation::Close);
        assert_eq!(m[3].process_status, sd::Status::Free);
        assert_eq!(m[3].operation, sd::Operation::STOP);
    }

    async fn write(loop_runs: usize) -> Result<(), ()> {
        let file_name = "Test.txt";
        let mut fh = FileHandler::new();
        let mut file = fh
            .open(file_name, fh::Mode::WriteCreateOrAppend)
            .await
            .unwrap();
        let mut total_character_counter = 0;
        let mut counter = loop_runs;
        while counter > 0 {
            let s = format!("TestString {}\n", counter);
            total_character_counter += s.len();
            file.write(s.as_str()).await.map_err(|_| {}).unwrap();
            counter -= 1;
        }
        // At least once pushed to the sd task
        assert!(total_character_counter > sd::CONTENT_BUFFER_SIZE);
        file.close().await;

        let mut file = fh.open(file_name, fh::Mode::Read).await.unwrap();
        let mut out: String<{ fh::LINE_LENGHT }> = String::new();
        let mut counter = loop_runs;
        while let Ok(_) = file.read_line(&mut out).await {
            assert_eq!(out.as_str(), format!("TestString {}", counter));
            counter -= 1;
        }
        assert_eq!(counter, 0);
        file.close().await;
        // Stop other thread
        sd::stop_operation().await.unwrap();
        Ok(())
    }

    #[tokio::test]
    async fn simple_write() {
        sd::FILE_OPERATION_STATISTIC.lock().await.clear();

        let td = sd::tests::get_test_disk(DISKNAME);

        let lbd: LinuxBlockDevice<std::string::String> =
            LinuxBlockDevice::new(td.file_name(), PRINT_DEBUG)
                .await
                .unwrap();
        let mut volume_mgr: VolumeManager<_, _, { sd::MAX_DIRS }, { sd::MAX_FILES }> =
            VolumeManager::new_with_limits(lbd, Clock, 100);
        let sd_future = sd_generic(&mut volume_mgr);

        const REPEAT_COUNTS: usize = sd::FILE_OPERATION_MULTIPLEXERS + 100;
        let (sd_result, second_result) = tokio::join!(sd_future, write(REPEAT_COUNTS));

        assert_eq!(sd_result, Ok(()));
        assert_eq!(second_result, Ok(()));
        let m = sd::FILE_OPERATION_STATISTIC.lock().await;
        assert_eq!(m.len(), 8); // 1 Open, 2 Write, 1 Close, 1 Open, 1 Read, 1 Close, 1 Stop
        assert_eq!(m[0].process_status, sd::Status::Free);
        let mut filename = String::new();
        filename.push_str("Test.txt").unwrap();
        assert_eq!(
            m[0].operation,
            sd::Operation::Open((embedded_sdmmc::Mode::ReadWriteCreateOrAppend, filename))
        );
        assert_eq!(m[1].process_status, sd::Status::Free);
        if let sd::Operation::Write(_) = m[1].operation {
        } else {
            assert!(false);
        }
        assert_eq!(m[2].process_status, sd::Status::Free);
        if let sd::Operation::Write(_) = m[2].operation {
        } else {
            assert!(false);
        }
        assert_eq!(m[3].process_status, sd::Status::Free);
        assert_eq!(m[3].operation, sd::Operation::Close);
        assert_eq!(m[4].process_status, sd::Status::Free);
        let mut filename = String::new();
        filename.push_str("Test.txt").unwrap();
        assert_eq!(
            m[4].operation,
            sd::Operation::Open((embedded_sdmmc::Mode::ReadOnly, filename))
        );
        assert_eq!(m[5].process_status, sd::Status::Free);
        assert_eq!(m[5].operation, sd::Operation::Read);
        assert_eq!(m[6].process_status, sd::Status::Free);
        assert_eq!(m[6].operation, sd::Operation::Close);
        assert_eq!(m[7].process_status, sd::Status::Free);
        assert_eq!(m[7].operation, sd::Operation::STOP);
    }

    #[test]
    fn test_calculate_soc() {
        assert_eq!(calculate_soc(4.3), 100.);
        assert_eq!(calculate_soc(2.9), 0.);
        assert_eq!(calculate_soc(3.95), 70.);
        assert_eq!(calculate_soc(4.0), 77.5);
    }
}
