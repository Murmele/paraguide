#[cfg(feature = "usb_vario_simulation")]
use core::panic;
#[cfg(feature = "embassy")]
use embassy_executor::Spawner;

#[cfg(feature = "steigen33")]
use crate::VarioSoundSender;
#[cfg(feature = "steigen33")]
use embassy_sync::signal;
#[cfg(feature = "steigen33")]
pub(crate) static VARIO_SOUND_SIGNAL: VarioSoundSender = signal::Signal::new();

#[cfg(all(not(feature = "rtos-trace"), feature = "nrf"))]
const TURN_ON_DELAY_MS: u64 = 2000;

#[cfg(feature = "steigen33")]
pub async fn steigen3000(spawner: Spawner) -> ! {
    use super::interface;
    use crate::main_loop;
    use crate::tasks;
    use crate::tasks::display::CheckState;
    use crate::tasks::display::Page;
    use crate::tasks::sd::SD_STATUS;
    use crate::{info, trace};
    use embassy_nrf::gpio::{self, Pin};
    use embassy_nrf::saadc::Input;
    use embassy_time::Duration;
    #[cfg(not(feature = "rtos-trace"))]
    use embassy_time::Timer;
    use paraguidecore::{igc, paraguide};

    #[cfg(feature = "rtos-trace")]
    {
        use strum::IntoEnumIterator;
        trace!("Start Print markers");
        for marker in crate::tracing_markers::Markers::iter() {
            trace!("{:?}={}", marker, marker as usize);
        }
        trace!("End Print markers");
    }

    #[cfg(feature = "nrf")]
    let p = embassy_nrf::init(Default::default());

    #[cfg(feature = "steigen31")]
    let power_button = p.P1_07;
    #[cfg(feature = "steigen33")]
    let mut power_button = p.P0_09;

    let mut buzzer_en_pin = p.P0_14;
    gpio::Output::new(
        &mut buzzer_en_pin,
        gpio::Level::Low,
        gpio::OutputDrive::Standard,
    );

    let mut buzzer_boost_en = p.P1_03;
    gpio::Output::new(
        &mut buzzer_boost_en,
        gpio::Level::Low,
        gpio::OutputDrive::Standard,
    );

    let mut radio_en = p.P1_08;
    gpio::Output::new(&mut radio_en, gpio::Level::Low, gpio::OutputDrive::Standard);

    // Enable pin for the gps
    #[cfg(feature = "steigen33")]
    let mut gps_en = p.P0_17;
    #[cfg(feature = "steigen33")]
    gpio::Output::new(&mut gps_en, gpio::Level::High, gpio::OutputDrive::Standard);

    let power_button_check = gpio::Input::new(&mut power_button, gpio::Pull::Up);
    interface::sleep_us(100_000).await; // Give time to pull up
    let _power_button_pressed = power_button_check.is_low();
    drop(power_button_check);

    let mut lock = tasks::display::STATE.lock().await;
    lock.page = Page::Empty;
    drop(lock);

    // Start display
    trace!("TgSpn LCD");
    match spawner.spawn(tasks::display(
        p.SPI3,
        p.P0_19.degrade(),
        p.P0_15.degrade(),
        p.P0_21.degrade(),
    )) {
        Ok(_) => (),
        Err(_) => (),
    }

    // ignore spawning usb_msc if usb_debug is turned on!
    #[cfg(not(any(
        feature = "usb_debug",
        feature = "usb_vario_simulation",
        feature = "disable_usb_mass_storage"
    )))]
    if !_power_button_pressed {
        let mut lock = tasks::display::STATE.lock().await;
        lock.page = Page::USBMsc;
        drop(lock);
        info!("Starting usb msc mode");
        trace!("TgSpn USB MSC");

        #[cfg(feature = "steigen33")]
        match spawner.spawn(tasks::usb_msc(
            spawner,
            p.USBD,
            p.SPI2,
            p.P0_12.degrade(),
            p.P1_09.degrade(),
            p.P0_07.degrade(),
            p.P0_23.degrade(),
        )) {
            Ok(_) => (),
            Err(_) => (),
        }
        loop {
            interface::sleep_us(1_000_000).await;
        }
    }
    // Non sd mode

    // Pull up, so the power stays turned on
    #[cfg(not(feature = "rtos-trace"))] // Issue #32
    let _ = Timer::after_millis(TURN_ON_DELAY_MS).await; // Wait, so for short presses it should not turn on!
    trace!("TgSpn Button 1");
    match spawner.spawn(tasks::button1(power_button.degrade(), gpio::Pull::Up)) {
        Ok(_) => (),
        Err(_) => (),
    }

    // Now we are sure, we are on
    let mut lock = tasks::display::STATE.lock().await;
    lock.page = Page::Init;
    drop(lock);

    trace!("TgSpn Button 2");
    match spawner.spawn(tasks::button2(p.P0_10.degrade(), gpio::Pull::Up)) {
        Ok(_) => (),
        Err(_) => (),
    }

    #[cfg(feature = "usb_debug")]
    match spawner.spawn(tasks::usb(spawner, p.USBD)) {
        Ok(_) => (),
        Err(_) => (),
    }

    #[cfg(feature = "usb_vario_simulation")]
    if let Err(e) = spawner.spawn(tasks::usb_vario_simulation::usb(spawner, p.USBD)) {
        panic!("{:?}", e);
    }

    info!("Initializing Steigen 3000");

    trace!("TgSpn Buzzer");
    #[cfg(feature = "steigen31")]
    match spawner.spawn(tasks::buzzer(
        p.PWM0,
        p.P0_22.degrade(),
        p.P1_01.degrade(),
        p.P1_03.degrade(),
    )) {
        Ok(_) => {}
        Err(_) => {}
    }
    #[cfg(feature = "steigen33")]
    match spawner.spawn(tasks::buzzer(
        buzzer_boost_en.degrade(),
        buzzer_en_pin.degrade(),
        p.PWM0,
        p.P0_22.degrade(),
        p.PWM1,
        p.P0_13.degrade(),
    )) {
        Ok(_) => {}
        Err(_) => {}
    }

    trace!("TgSpn I2C Sensors");
    #[cfg(feature = "steigen33")]
    match spawner.spawn(tasks::i2c_sensors(
        spawner,
        p.TWISPI0,
        p.P0_26.degrade(),
        p.P0_06.degrade(),
        p.P0_30.degrade(),
        p.P0_28.degrade(),
        p.P0_29.degrade(),
    )) {
        Ok(_) => {}
        Err(_) => {}
    };

    #[cfg(feature = "steigen31")]
    match spawner.spawn(tasks::i2c_sensors(
        p.TWISPI0,
        p.P0_06.degrade(),
        p.P0_26.degrade(),
        p.P0_29.degrade(),
    )) {
        Ok(_) => {}
        Err(_) => {}
    };

    trace!("TgSpn GPS");
    #[cfg(feature = "steigen31")]
    match spawner.spawn(tasks::gps(
        p.UARTE0,
        p.P0_05.degrade(),
        p.P0_04.degrade(),
        embassy_nrf::uarte::Baudrate::BAUD9600,
    )) {
        Ok(_) => {}
        Err(_) => {}
    }

    #[cfg(feature = "steigen33")]
    match spawner.spawn(tasks::gps(
        p.UARTE0,
        p.P0_04.degrade(),
        p.P0_31.degrade(),
        embassy_nrf::uarte::Baudrate::BAUD38400,
    )) {
        Ok(_) => {}
        Err(_) => {}
    }

    trace!("TgSpn SD");
    #[cfg(feature = "steigen31")]
    match spawner.spawn(tasks::sd(
        p.SPI2,
        p.P0_21.degrade(),
        p.P0_16.degrade(),
        p.P0_19.degrade(),
        p.P0_23.degrade(),
    )) {
        Ok(_) => {}
        Err(_) => {}
    }
    #[cfg(feature = "steigen33")]
    match spawner.spawn(tasks::sd(
        p.SPI2,
        p.P0_12.degrade(),
        p.P1_09.degrade(),
        p.P0_07.degrade(),
        p.P0_23.degrade(),
    )) {
        Ok(_) => {}
        Err(_) => {}
    }
    if let Err(_) = embassy_time::with_timeout(Duration::from_secs(3), async {
        loop {
            let status = SD_STATUS.lock(|l| *l.borrow());
            match status {
                tasks::sd::SDStatus::NotStarted => (),
                tasks::sd::SDStatus::Ok => {
                    let mut l = tasks::display::STATE.lock().await;
                    l.check.sd = CheckState::Pass;
                    break;
                }
                tasks::sd::SDStatus::Error => {
                    let mut l = tasks::display::STATE.lock().await;
                    l.check.sd = CheckState::Fail;
                    break;
                }
            }
            Timer::after_millis(100).await;
        }
    })
    .await
    {
        let mut l = tasks::display::STATE.lock().await;
        l.check.sd = CheckState::Fail;
    }

    #[cfg(feature = "steigen33")]
    let adc_battery_pin = p.P0_05;

    #[cfg(feature = "steigen31")]
    let adc_battery_pin = p.P0_31;

    trace!("TgSpn ADC");
    match spawner.spawn(tasks::adc_battery(p.SAADC, adc_battery_pin.degrade_saadc())) {
        Ok(_) => {}
        Err(_) => {}
    }

    let device_info = paraguide::DeviceInfo {
        manufacturer: "Charles Pope",
        model: "Steigen3000",
        firmware_version: env!("CARGO_PKG_VERSION"),
        #[cfg(feature = "steigen33")]
        hw_revision: "3.3",
        #[cfg(feature = "steigen31")]
        hw_revision: "3.1",
        igc_fr: "Stg3",             // less than 6 characters!
        igc_manufacturer_id: "XS3", // X + 2 Characters

        #[cfg(any(feature = "steigen33"))]
        gnss_receiver_manufacturer: "UBlox",
        #[cfg(any(feature = "steigen33"))]
        gnss_receiver_model: "MAX-M10",
        #[cfg(any(feature = "steigen33"))]
        gnss_receiver_max_alt: "80000",
        #[cfg(any(feature = "steigen31", feature = "steigen33"))]
        gnss_receiver_channels: "33", // TODO: correct for MAX-M10?
        #[cfg(any(feature = "steigen31", feature = "steigen33"))]
        gnss_receiver_systems: &[
            igc::GNSSSystems::GPS,
            igc::GNSSSystems::GALILEO,
            igc::GNSSSystems::GLONASS,
        ],

        #[cfg(any(feature = "steigen31", feature = "steigen33"))]
        pressure_sensor_manufacturer: "ST",
        #[cfg(any(feature = "steigen31"))]
        pressure_sensor_model: "LPS22HD",
        #[cfg(any(feature = "steigen33"))]
        pressure_sensor_model: "LPS22HH",
        pressure_sensor_max_alt: "10108",
    };

    main_loop(
        device_info,
        #[cfg(not(feature = "usb_vario_simulation"))]
        &VARIO_SOUND_SIGNAL,
        &tasks::display::STATE,
    )
    .await
}
