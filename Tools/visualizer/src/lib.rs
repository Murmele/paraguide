use std::sync::{mpsc, Arc, Mutex};
use std::thread;
mod cube_renderer;
mod read_serial;
use cube_renderer::*;
use std::f64::consts::PI;
use std::time::{SystemTime, UNIX_EPOCH};
//use tokio::runtime::Runtime;
slint::include_modules!();

struct Angles {
    pitch: f64,
    roll: f64,
}

fn calculate_angles(ax: f64, ay: f64, az: f64) -> Angles {
    // Calculate pitch angle
    let pitch = (ay / (ax.powi(2) + az.powi(2)).sqrt()).atan();

    // Calculate roll angle
    let roll = (-ax / (ay.powi(2) + az.powi(2)).sqrt()).atan();

    // // Convert radians to degrees
    // let pitch_degrees = pitch * 180.0 / PI;
    // let roll_degrees = roll * 180.0 / PI;

    Angles { pitch, roll }
}

pub fn visualizer() -> Result<(), ()> {
    let app = AppWindow::new().unwrap();
    //let rt = Runtime::new().unwrap();

    let (mpsc_cmd_tx, mpsc_cmd_rx) = mpsc::channel();
    let (mpsc_new_data_tx, mpsc_new_data_rx) = mpsc::channel();

    let buffer_handler = read_serial::BufferHandler::new(vec![
        Arc::new(Mutex::new(read_serial::Buffer::new())),
        Arc::new(Mutex::new(read_serial::Buffer::new())),
    ]);

    let angles = Arc::new(Mutex::new(Angles {
        pitch: 0.0,
        roll: 0.0,
    }));

    thread::spawn(move || {
        read_serial::serial_port_reader(mpsc_cmd_rx, mpsc_new_data_tx, buffer_handler.clone())
    });

    let app_weak2 = app.as_weak();
    let mut underlay = None;
    let angles_1 = angles.clone();
    if let Err(error) =
        app.window()
            .set_rendering_notifier(move |state, graphics_api| match state {
                slint::RenderingState::RenderingSetup => {
                    let context = match graphics_api {
                        slint::GraphicsAPI::NativeOpenGL { get_proc_address } => unsafe {
                            glow::Context::from_loader_function_cstr(|s| get_proc_address(s))
                        },
                        _ => return,
                    };
                    underlay = Some(DemoRenderer::new(context))
                }
                slint::RenderingState::BeforeRendering => {
                    if let (Some(underlay), Some(app)) = (underlay.as_mut(), app_weak2.upgrade()) {
                        let guard = angles_1.lock().unwrap();
                        let angle_x = guard.roll as f32;
                        let angle_y = 0.0;
                        let angle_z = guard.pitch as f32; // TODO: something is wrong. because pitch is around the y axis!

                        let texture = underlay.render(
                            0.5,
                            0.5,
                            0.5,
                            angle_x,
                            angle_y,
                            angle_z,
                            app.get_requested_texture_width() as u32,
                            app.get_requested_texture_width() as u32,
                        );
                        app.set_texture(slint::Image::from(texture));
                        app.window().request_redraw();
                    }
                }
                slint::RenderingState::AfterRendering => {}
                slint::RenderingState::RenderingTeardown => {
                    drop(underlay.take());
                }
                _ => {}
            })
    {
        match error {
                slint::SetRenderingNotifierError::Unsupported => eprintln!("This example requires the use of the GL backend. Please run with the environment variable SLINT_BACKEND=GL set."),
                _ => unreachable!()
            }
        std::process::exit(1);
    }

    let angles_mutex2 = angles.clone();
    let app_weak = app.as_weak();
    thread::spawn(move || {
        let mut start = SystemTime::now();
        loop {
            let buffer = match mpsc_new_data_rx.recv() {
                Ok(b) => b,
                Err(_) => break,
            };

            let now = SystemTime::now();
            let diff = (now.duration_since(start).unwrap().as_millis() as f64) / 1000.0;
            const UPDATE_RATE_HZ: f64 = 50.0;
            if diff < 1.0 / UPDATE_RATE_HZ {
                continue;
            }
            start = now;

            let angles_mutex = angles_mutex2.clone();
            app_weak
                .upgrade_in_event_loop(move |app| {
                    let guard = buffer.lock().unwrap();
                    match &guard.last() {
                        Ok(d) => {
                            app.set_time(d.time as f32);
                            app.set_ax(d.ax as f32);
                            app.set_ay(d.ay as f32);
                            app.set_az(d.az as f32);

                            let mut angles = angles_mutex.lock().unwrap();
                            let a_ = calculate_angles(d.ax, d.ay, d.az);
                            angles.pitch = a_.pitch;
                            angles.roll = a_.roll;
                            println!("{:?}", d);
                        }
                        Err(_) => println!("Unable to get last!"),
                    };
                })
                .unwrap();
        }
    });

    // Callback defined in the mainwindow
    app.on_end(move || {
        if let Err(_) = mpsc_cmd_tx.send(read_serial::Command::Stop) {
            println!("Unable to send stop command. Already send once?");
        }
    });

    app.run().unwrap();
    Ok(())
}
