use serialport;
use std::io::Read;
use std::sync::{mpsc, Arc, Mutex};

const BUFFER_SIZE: usize = 2;
const PRODUCT_NAME: &str = "Steigen3000";
const BAUD_RATE: u32 = 115200;

pub enum Command {
    Stop,
}

#[derive(Debug, Clone, Copy)]
pub struct Data {
    pub time: f64,
    pub ax: f64,
    pub ay: f64,
    pub az: f64,
}

impl Data {
    fn new() -> Self {
        Data {
            time: 0.0,
            ax: 0.0,
            ay: 0.0,
            az: 0.0,
        }
    }
}

pub struct Buffer {
    data: [Data; BUFFER_SIZE],
    length: usize,
}

impl Buffer {
    pub fn new() -> Self {
        Buffer {
            data: [Data::new(); BUFFER_SIZE],
            length: 0,
        }
    }

    fn add_data(&mut self, data: Data) -> Result<(), ()> {
        if self.length < self.data.len() {
            self.data[self.length] = data;
            self.length += 1;
            Ok(())
        } else {
            Err(())
        }
    }

    pub fn last(&self) -> Result<Data, ()> {
        if self.length == 0 {
            Err(())
        } else {
            Ok(self.data[self.length - 1])
        }
    }
}

enum ParseState {
    Start,
    Time,
    Ax,
    Ay,
    Az,
}

pub struct BufferHandler {
    buffer_mutex_vec: Vec<Arc<Mutex<Buffer>>>,
    curr_buffer_index: usize,
}

impl BufferHandler {
    pub fn new(buffer_mutex_vec: Vec<Arc<Mutex<Buffer>>>) -> Self {
        BufferHandler {
            buffer_mutex_vec,
            curr_buffer_index: 0,
        }
    }

    fn curr_buffer<'a>(&'a self) -> &'a Arc<Mutex<Buffer>> {
        &self.buffer_mutex_vec[self.curr_buffer_index]
    }

    fn next_buffer(&mut self) {
        if self.curr_buffer_index < self.buffer_mutex_vec.len() - 1 {
            self.curr_buffer_index += 1;
        } else {
            self.curr_buffer_index = 0;
        }
        self.curr_buffer().lock().unwrap().length = 0;
    }
}

impl Clone for BufferHandler {
    fn clone(&self) -> Self {
        BufferHandler::new(self.buffer_mutex_vec.clone())
    }
}

pub fn serial_port_reader(
    mpsc_cmd_rx: mpsc::Receiver<Command>,
    mpsc_new_data_tx: mpsc::Sender<Arc<Mutex<Buffer>>>,
    mut buffer_handler: BufferHandler,
) -> Result<(), serialport::Error> {
    let ports = serialport::available_ports().expect("No ports found!");
    let mut port = serialport::SerialPortInfo {
        port_name: String::new(),
        port_type: serialport::SerialPortType::Unknown,
    };
    let mut found = false;
    for p in ports {
        if let serialport::SerialPortType::UsbPort(ref i) = p.port_type {
            if let Some(product) = &i.product {
                if product == PRODUCT_NAME {
                    port = p.clone();
                    found = true;
                    break;
                }
            }
        }
    }

    if !found {
        return Err(serialport::Error {
            kind: serialport::ErrorKind::NoDevice,
            description: "Steigen3000 device not found".to_string(),
        });
    }

    let mut port = serialport::new(port.port_name, BAUD_RATE).open_native()?;

    const READ_BUFFER_SIZE: usize = 1000;
    let mut buf: [u8; READ_BUFFER_SIZE] = [0; READ_BUFFER_SIZE];
    let mut parse_state = ParseState::Start;
    let mut curr_string = String::new();
    let mut guard = buffer_handler.curr_buffer().lock().unwrap();
    let mut data = Data::new();
    loop {
        if let Ok(cmd) = mpsc_cmd_rx.try_recv() {
            match cmd {
                Command::Stop => {
                    println!("Stop serial_port_reader()");
                    break;
                }
            }
        }
        match port.read(&mut buf) {
            Err(_e) => {
                //print!("Error: {}", e);
            }
            Ok(n) => {
                //let s = String::from_utf8(buf.to_vec()).unwrap();
                //println!("Line: {}", s);
                for i in 0..n {
                    let b = buf[i];
                    match parse_state {
                        ParseState::Time => match handle_character(b, &mut curr_string) {
                            HandleCharacterResult::Running => continue,
                            HandleCharacterResult::Finished(v) => {
                                data.time = v;
                                parse_state = ParseState::Ax;
                                curr_string.clear();
                            }
                        },
                        ParseState::Ax => match handle_character(b, &mut curr_string) {
                            HandleCharacterResult::Running => continue,
                            HandleCharacterResult::Finished(v) => {
                                data.ax = v;
                                parse_state = ParseState::Ay;
                                curr_string.clear();
                            }
                        },
                        ParseState::Ay => match handle_character(b, &mut curr_string) {
                            HandleCharacterResult::Running => continue,
                            HandleCharacterResult::Finished(v) => {
                                data.ay = v;
                                parse_state = ParseState::Az;
                                curr_string.clear();
                            }
                        },
                        ParseState::Az => match handle_character(b, &mut curr_string) {
                            HandleCharacterResult::Running => continue,
                            HandleCharacterResult::Finished(v) => {
                                data.az = v;
                                parse_state = ParseState::Time;
                                curr_string.clear();

                                if b != b'\n' {
                                    panic!("No end of line!")
                                }

                                let res = guard.add_data(data);
                                if let Err(_) = res {
                                    // buffer is full, switch to the other one
                                    drop(guard);
                                    let curr_buffer = buffer_handler.curr_buffer().clone();
                                    buffer_handler.next_buffer();
                                    guard = buffer_handler.curr_buffer().lock().unwrap();

                                    guard.add_data(data).unwrap(); // This time it must work!
                                    mpsc_new_data_tx.send(curr_buffer).unwrap();
                                }
                                data = Data::new();
                            }
                        },
                        ParseState::Start => {
                            if b == b'\n' {
                                parse_state = ParseState::Time;
                            }
                        }
                    }
                }
            }
        }
    }

    Ok(())
}

enum HandleCharacterResult {
    Running,
    Finished(f64),
}

fn handle_character(b: u8, s: &mut String) -> HandleCharacterResult {
    let float_parts = (b >= b'0' && b <= b'9') || b == b'.' || b == b'-';
    let value_end = b == b'\n' || b == b',';
    if float_parts {
        s.push(b as char);
        return HandleCharacterResult::Running;
    } else if value_end {
        if let Ok(v) = s.parse::<f64>() {
            return HandleCharacterResult::Finished(v);
        } else {
            panic!("Failed to parse");
        }
    } else {
        panic!("Should not happen")
    }
}
