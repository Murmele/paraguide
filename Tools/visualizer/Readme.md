# Visualizer

A simple cube which rotates with the values from an accelerometer send over serial interface
time,ax,ay,az\n where ax,ay,az are the acceleration components from the accelerometer. All values are float.

![Main Window](mainwindow.png)