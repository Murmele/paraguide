// reading the binary format

#[derive(Debug)]
enum FormatType {
    Format0,
    FormatSparse,
    Format0Tiny,
    FormatSparseTiny
}

impl Into<FormatType> for u8 {
    fn into(self) -> FormatType {
        match self {
            0 => FormatType::Format0,
            1 => FormatType::FormatSparse,
            2 => FormatType::Format0Tiny,
            3 => FormatType::FormatSparseTiny,
            _ => panic!("Invalid")
        }
    }
}

#[derive(Debug)]
enum IndexToLocaFormat {
    Offset16,
    Offset32,
}

impl Into<IndexToLocaFormat> for u8 {
    fn into(self) -> IndexToLocaFormat {
        match self {
            0 => IndexToLocaFormat::Offset16,
            1 => IndexToLocaFormat::Offset32,
            _ => panic!("Invalid Index to local format!")
        }
    }
}

#[derive(Debug, PartialEq)]
enum CompressionAlgorithm {
    Raw,
    RleWithXorPrefilter,
    RleWithoutPrefilter,
}

impl Into<CompressionAlgorithm> for u8 {
    fn into(self) -> CompressionAlgorithm {
        match self {
            0 => CompressionAlgorithm::Raw,
            1 => CompressionAlgorithm::RleWithXorPrefilter,
            2 => CompressionAlgorithm::RleWithoutPrefilter,
            _ => panic!("Invalid Index to local format!")
        }
    }
}

#[derive(Debug)]
enum GlyphIdFormat {
    Byte1,
    Byte2,
}

impl Into<GlyphIdFormat> for u8 {
    fn into(self) -> GlyphIdFormat {
        match self {
            0 => GlyphIdFormat::Byte1,
            1 => GlyphIdFormat::Byte2,
            _ => panic!("Invalid GlyphIdFormat!"),
        }
    }
}

#[derive(Debug)]
struct LvglConvBinaryHeader {
    record_size: u32,
    marker: String,
    version: u32,
    number_additional_tables: u16,
    font_size_px: u16,
    ascent: u16,
    descent: i16,
    typo_ascent: u16,
    typo_descent: i16,
    typo_line_gap: u16,
    min: i16,
    max: i16,
    default_advance_width: u16,
    kerning_scale: u16,
    index_to_loc_format: IndexToLocaFormat,
    glyph_id_format: GlyphIdFormat,
    advance_width_format: u8,
    bits_per_pixel: u8,
    glyph_bbox_x_y: u8,
    glyph_bbox_w_h: u8,
    glyph_advance_width: u8,
    compression_alg: CompressionAlgorithm,
    subpixel_rendering: u8,
    // reserved: u8,
    underline_position: i16,
    underline_thickness: u16,
    // unused: align header length to 4x
}

#[derive(Debug)]
struct SubTableHeader {
    data_offset: u32,
    range_start: u32,
    range_length: u16,
    glyph_id: u16,
    data_entries_count: u16,
    format_type: FormatType,
}

#[derive(Debug)]
struct TableLoca {
    record_size: u32,
    marker: String,
    entries_count: u32,
    id_offset: Vec<u32>,
}

#[derive(Debug)]
struct TableGlyf {
    record_size: u32,
    marker: String,
    glyph_data: Vec<u32>, // ???
}

#[derive(Debug)]
struct Cmap {
    record_size: u32,
    marker: String,
    subtables_count: u32,
    subtables: Vec<SubTableHeader>,  
}

#[derive(Debug)]
struct GlyphData {
    advance_width: u32,
    bbox_x: u32,
    bbox_y: u32,
    bbox_width: u32,
    bbox_height: u32,
    bitmap: Vec<u8>,
}

use std::{
    fs::File, path::Path,
    io::Read,
};



fn main() {
    let font_path = Path::new(env!("CARGO_MANIFEST_DIR")).parent().unwrap().parent().unwrap().join("steigen3000").join("ui").join("fonts").join("Fira_Mono");
    let file_name = "firaMono.bin";

    let mut f = File::open(font_path.join(file_name)).unwrap();

    let mut buffer = Vec::new();
    f.read_to_end(&mut buffer).unwrap();

    let header = LvglConvBinaryHeader {
        record_size: u32::from_le_bytes([buffer[0], buffer[1], buffer[2], buffer[3]]),
        marker: String::from_utf8(buffer[4..8].to_vec()).unwrap(),
        version: u32::from_le_bytes([buffer[8], buffer[9], buffer[10], buffer[11]]),
        number_additional_tables: u16::from_le_bytes([buffer[12], buffer[13]]),
        font_size_px: u16::from_le_bytes([buffer[14], buffer[15]]),
        ascent: u16::from_le_bytes([buffer[16], buffer[17]]),
        descent: i16::from_le_bytes([buffer[18], buffer[19]]),
        typo_ascent: u16::from_le_bytes([buffer[20], buffer[21]]),
        typo_descent: i16::from_le_bytes([buffer[22], buffer[23]]),
        typo_line_gap: u16::from_le_bytes([buffer[24], buffer[25]]),
        min: i16::from_le_bytes([buffer[26], buffer[27]]),
        max: i16::from_le_bytes([buffer[28], buffer[29]]),
        default_advance_width: u16::from_le_bytes([buffer[30], buffer[31]]),
        kerning_scale: u16::from_le_bytes([buffer[32], buffer[33]]),
        index_to_loc_format: u8::from_le_bytes([buffer[34]]).into(),
        glyph_id_format: u8::from_le_bytes([buffer[35]]).into(),
        advance_width_format: u8::from_le_bytes([buffer[36]]),
        bits_per_pixel: u8::from_le_bytes([buffer[37]]),
        glyph_bbox_x_y: u8::from_le_bytes([buffer[38]]),
        glyph_bbox_w_h: u8::from_le_bytes([buffer[39]]),
        glyph_advance_width: u8::from_le_bytes([buffer[40]]),
        compression_alg: u8::from_le_bytes([buffer[41]]).into(),
        subpixel_rendering: u8::from_le_bytes([buffer[42]]),
        // reserved: u8, buffer[43]
        underline_position: i16::from_le_bytes([buffer[44], buffer[45]]),
        underline_thickness: u16::from_le_bytes([buffer[46], buffer[47]]),
        // unused: align header length to 4x
    };
    assert_eq!(header.marker, "head");

    let mut offset = 48;
    let mut cmap = Cmap {
        record_size: u32::from_le_bytes([buffer[offset + 0], buffer[offset + 1], buffer[offset + 2], buffer[offset + 3]]),
        marker: String::from_utf8(buffer[offset + 4..offset + 8].to_vec()).unwrap(),
        subtables_count: u32::from_le_bytes([buffer[offset + 8], buffer[offset + 9], buffer[offset + 10], buffer[offset + 11]]),
        subtables: Vec::new(),
    };
    assert_eq!(cmap.marker, "cmap");
    offset += 12;

    for _ in 0..cmap.subtables_count {
        cmap.subtables.push(SubTableHeader {
            data_offset: u32::from_le_bytes([buffer[offset], buffer[offset + 1], buffer[offset + 2], buffer[offset + 3]]),
            range_start: u32::from_le_bytes([buffer[offset + 4], buffer[offset + 5], buffer[offset + 6], buffer[offset + 7]]),
            range_length: u16::from_le_bytes([buffer[offset + 8], buffer[offset + 9]]),
            glyph_id: u16::from_le_bytes([buffer[offset + 10], buffer[offset + 11]]),
            data_entries_count: u16::from_le_bytes([buffer[offset + 12], buffer[offset + 13]]),
            format_type: u8::from_le_bytes([buffer[offset + 14]]).into(),
        });

        offset += 16;
    }

    match cmap.subtables[0].format_type {
        FormatType::Format0 => panic!("Not implemented"),
        FormatType::FormatSparse => panic!("Not implemented"),
        FormatType::Format0Tiny => {
            offset += 0;
        },
        FormatType::FormatSparseTiny => panic!("Not implemented"),
    }

    let mut table_loca = TableLoca {
        record_size: u32::from_le_bytes([buffer[offset + 0], buffer[offset + 1], buffer[offset + 2], buffer[offset + 3]]),
        marker: String::from_utf8(buffer[offset + 4..offset + 8].to_vec()).unwrap(),
        entries_count: u32::from_le_bytes([buffer[offset + 8], buffer[offset + 9], buffer[offset + 10], buffer[offset + 11]]),
        id_offset: Vec::new(),
    };
    assert_eq!(table_loca.marker, "loca");
    offset += 4 * 3; // record_size, marker, entries_count

    for _ in 0..table_loca.entries_count {
        match header.index_to_loc_format {
            IndexToLocaFormat::Offset16 => {
                table_loca.id_offset.push(u16::from_le_bytes([buffer[offset + 0], buffer[offset + 1]]) as u32);
                offset += 2;
            },
            IndexToLocaFormat::Offset32 => {
                table_loca.id_offset.push(u32::from_le_bytes([buffer[offset + 0], buffer[offset + 1], buffer[offset + 2], buffer[offset + 3]]) as u32);
                offset += 4;
            },
        }
    }

    let mut table_glyf = TableGlyf {
        record_size: u32::from_le_bytes([buffer[offset + 0], buffer[offset + 1], buffer[offset + 2], buffer[offset + 3]]),
        marker: String::from_utf8(buffer[offset + 4..offset + 8].to_vec()).unwrap(),
        glyph_data: Vec::new(), 
    };
    // offset += 8; // record_size, marker
    assert_eq!(table_glyf.marker, "glyf");

    assert_eq!(header.compression_alg, CompressionAlgorithm::Raw); // Only this supported!

    println!("Header: {:?}", header);
    println!("Cmap: {:?}", cmap);
    println!("Table loca: {:?}", table_loca);
    println!("Table glyf: {:?}", table_glyf);

    let advance_width_length_bits = header.glyph_advance_width;
    let bbox_w_h_length_bits = header.glyph_bbox_w_h;
    let bbox_x_y_length_bits = header.glyph_bbox_x_y;

    // https://learn.microsoft.com/en-us/typography/opentype/spec/glyf
    for i in 0..table_loca.id_offset.len() - 1 {
        // The size of each glyph data block is inferred from the difference between two consecutive offsets in the 'loca' table
        let size = table_loca.id_offset[i + 1] - table_loca.id_offset[i];
        // offset += size as usize;
        let offset = offset + table_loca.id_offset[i] as usize;

        assert!(bbox_w_h_length_bits + bbox_x_y_length_bits + advance_width_length_bits <= 32);
        let mut v = u32::from_le_bytes([buffer[offset + 0], buffer[offset + 1], buffer[offset + 2], buffer[offset + 3]]);

        if advance_width_length_bits != 0 {
            panic!("Not implemented");
        }
        v >>= advance_width_length_bits;

        let bbox_x = v & ((1 << bbox_x_y_length_bits) - 1);
        v >>= bbox_x_y_length_bits;

        let bbox_y = v & ((1 << bbox_x_y_length_bits) - 1);
        v >>= bbox_x_y_length_bits;

        let bbox_w = v & ((1 << bbox_w_h_length_bits) - 1);
        v >>= bbox_w_h_length_bits;

        let bbox_h = v & ((1 << bbox_w_h_length_bits) - 1);
        // v >>= bbox_w_h_length_bits;

        println!("Glyph Geometry: Bbox_x: {bbox_x}, Bbox_y: {bbox_y}, Bbox_w: {bbox_w}, Bbox_h: {bbox_h}");
    }

}
