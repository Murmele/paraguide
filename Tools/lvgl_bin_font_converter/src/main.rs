use serde::Deserialize;
use std::fmt::Display;
use std::fs;
use std::path::PathBuf;
use std::{io::Write, path::Path};

enum ColorRepresentation {
    Normal,   // Zero is white
    Inverted, // Zero is black
}

// LS026B7DH01A Display:
// 0° rotated: BitOrder::LSB
// 180° rotated: BitOrder::MSB
// all other parameters stay as they are

/// Bit order in the bytes
#[derive(Clone, Copy)]
enum BitOrder {
    LSB,
    MSB,
}

enum Order {
    /// First bytes in the resulting array are from the bottom
    BottomToTop,
    /// First bytes in the resulting array are the top
    TopToBottom,
}

fn main() {
    let font_path = Path::new(env!("CARGO_MANIFEST_DIR"))
        .parent()
        .unwrap()
        .parent()
        .unwrap()
        .join("steigen3000")
        .join("ui")
        .join("fonts");

    const BIT_ORDER: BitOrder = BitOrder::MSB;

    convert_font(
        font_path.clone().join("Fira_Mono"),
        "FIRA_MONO_REGULAR_14",
        "Dump",
        ColorRepresentation::Inverted,
        BIT_ORDER,
        Order::BottomToTop,
        false,
    );
    convert_font(
        font_path.clone().join("Fira_Mono"),
        "FIRA_MONO_REGULAR_26",
        "Dump",
        ColorRepresentation::Inverted,
        BIT_ORDER,
        Order::BottomToTop,
        false,
    );
    convert_font(
        font_path.clone().join("Fira_Mono"),
        "FIRA_MONO_REGULAR_53",
        "Dump",
        ColorRepresentation::Inverted,
        BIT_ORDER,
        Order::BottomToTop,
        false,
    );

    convert_font(
        font_path.clone().join("Fira_Sans"),
        "FIRA_SANS_REGULAR_14",
        "Dump",
        ColorRepresentation::Inverted,
        BIT_ORDER,
        Order::BottomToTop,
        false,
    );
    convert_font(
        font_path.clone().join("Fira_Sans"),
        "FIRA_SANS_REGULAR_26",
        "Dump",
        ColorRepresentation::Inverted,
        BIT_ORDER,
        Order::BottomToTop,
        false,
    );
    convert_font(
        font_path.clone().join("Fira_Sans"),
        "FIRA_SANS_REGULAR_53",
        "Dump",
        ColorRepresentation::Inverted,
        BIT_ORDER,
        Order::BottomToTop,
        false,
    );
}

#[derive(Debug, Deserialize)]
struct Position {
    x: isize,
}

#[derive(Debug, Deserialize)]
struct Freetype {
    advance: Position,
}

#[derive(Debug, Deserialize)]
struct Bbox {
    x: isize,
    y: isize,
    width: usize,
    height: usize,
}

#[derive(Debug, Deserialize)]
struct Glyph {
    code: usize,
    bbox: Bbox,
    freetype: Freetype,
    pixels: Vec<Vec<u8>>,
}

#[derive(Debug, Deserialize)]
struct Font {
    #[allow(non_snake_case)]
    typoAscent: isize,
    #[allow(non_snake_case)]
    typoDescent: isize,
    size: usize,
    glyphs: Vec<Glyph>,
}

struct GlyphFont {
    /// Character code
    code: usize,
    /// Width of the character
    width: usize,
    /// 0 means baseline
    y_start: isize,
    /// 0 means left border
    x_start: isize,
    /// number of bytes per row
    line_bytes: usize,
    data: Vec<u8>,
}

impl Display for GlyphFont {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        writeln!(f, "GlyphFont {{ /* {} */", (self.code as u8) as char)?;
        writeln!(f, "\tcode: {},", self.code)?;
        writeln!(f, "\twidth: {},", self.width)?;
        writeln!(f, "\ty_start: {},", self.y_start)?;
        writeln!(f, "\tx_start: {},", self.x_start)?;
        writeln!(f, "\tline_bytes: {},", self.line_bytes)?;

        write!(f, "\tdata: &[")?;
        for (index, byte) in self.data.clone().into_iter().enumerate() {
            if index % self.line_bytes == 0 {
                writeln!(f, "")?;
                write!(f, "\t\t")?;
            }
            write!(f, "0x{:02x}, ", byte)?;
        }
        writeln!(f, "\n\t]")?;

        write!(f, "}} /* GlyphFont */")
    }
}

struct FontEmbedded {
    /// Name of the font
    name: String,
    /// Font size
    size: usize,
    /// number of lines from the baseline to the top
    typo_ascent: isize,
    /// number of lines from the baseline to the bottom
    typo_descent: isize,
    /// Maximum height of the bitmap. (type_ascent - typo_descent)
    total_height: isize,
    /// The start character code
    start_code: usize,
    default_code: usize,
    glyphs: Vec<GlyphFont>,
}

impl Display for FontEmbedded {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        writeln!(f, "pub const {}: Font = Font {{", self.name)?;
        writeln!(f, "\tsize: {},", self.size)?;
        writeln!(f, "\ttypo_ascent: {},", self.typo_ascent)?;
        writeln!(f, "\ttypo_descent: {},", self.typo_descent)?;
        writeln!(f, "\ttotal_height: {},", self.total_height)?;
        writeln!(f, "\tstart_code: {},", self.start_code)?;
        writeln!(f, "\tdefault_code: {},", self.default_code)?;

        writeln!(f, "\tglyphs: &[")?;
        for g in &self.glyphs {
            writeln!(f, "{},", g)?;
        }

        writeln!(f, "\t],")?;

        writeln!(f, "}}; /* FontEmbedded */")
    }
}

fn convert_font(
    font_path: PathBuf,
    font_name: &str,
    folder_name_suffix: &str,
    representation: ColorRepresentation,
    bit_order: BitOrder,
    order: Order,
    debug: bool,
) {
    println!("Converting: {}", font_name);
    let s = fs::read_to_string(
        font_path
            .join(font_name.to_owned() + folder_name_suffix)
            .join("font_info.json"),
    )
    .unwrap();

    let f: Font = serde_json::from_str(&s).expect("JSON was not well-formatted");

    let mut font = FontEmbedded {
        name: font_name.to_string(),
        size: f.size,
        typo_ascent: f.typoAscent,
        typo_descent: f.typoDescent,
        total_height: f.typoAscent - f.typoDescent,
        glyphs: Vec::new(),
        start_code: f.glyphs.first().unwrap().code,
        default_code: '?' as usize,
    };

    for glyph in f.glyphs {
        let code = glyph.code;
        // if (code as u8) as char != '%' {
        //     continue;
        // }
        if debug {
            println!("\nCode: {}, Character: {}", code, (code as u8) as char);
        }

        let mut bounding_box_width = glyph.bbox.x + glyph.bbox.width as isize;

        let byte_alignment = bounding_box_width % 8;
        let left = glyph.bbox.x % 8; // Add pixels to the left
                                     // let right = byte_alignment - left; // Add pixels to the right to fill up

        if byte_alignment != 0 {
            // Fill up on the left and on the right side!
            bounding_box_width = (bounding_box_width / 8) * 8 + 8;
        // Round up to multiple of a byte
        } else {
            bounding_box_width = bounding_box_width;
        }

        let x_start = ((glyph.bbox.x - left) / 8) * 8;
        let y_start = glyph.bbox.y;
        let y_height = glyph.bbox.height;

        let x_diff = glyph.bbox.x - x_start;
        assert!(x_diff >= 0);
        let bit_offset;
        if x_diff != 0 {
            // First byte filled up
            bit_offset = x_diff;
        } else {
            bit_offset = 0;
        }

        let line_number_bytes = bounding_box_width as usize / 8;

        let mut vec: Vec<u8> = Vec::with_capacity(line_number_bytes * y_height);
        match representation {
            ColorRepresentation::Normal => {
                for _ in 0..vec.capacity() {
                    vec.push(0);
                }
            }
            ColorRepresentation::Inverted => {
                for _ in 0..vec.capacity() {
                    vec.push(0xff);
                }
            }
        }

        assert_eq!(y_height, glyph.pixels.len());

        for (line_idx, line) in glyph.pixels.into_iter().enumerate() {
            for (index, pixel) in line.into_iter().enumerate() {
                let line_idx = match order {
                    Order::TopToBottom => line_idx,
                    Order::BottomToTop => y_height - 1 - line_idx,
                };
                // print!("{}", pixel / 255);
                let offset = line_idx as isize * line_number_bytes as isize * 8
                    + bit_offset
                    + index as isize;

                let byte_pos = offset / 8;
                let bit_pos = match bit_order {
                    BitOrder::LSB => 7 - (offset as usize % 8),
                    BitOrder::MSB => offset as usize % 8,
                };

                if pixel == 255 {
                    match representation {
                        ColorRepresentation::Normal => vec[byte_pos as usize] |= 1 << (7 - bit_pos),
                        ColorRepresentation::Inverted => {
                            vec[byte_pos as usize] &= !(1 << (7 - bit_pos))
                        }
                    }
                }
            }
            // println!("")
        }

        if debug {
            for (index, data) in vec.clone().into_iter().enumerate() {
                if index % line_number_bytes == 0 && index > 0 {
                    println!(""); // Starting new line
                }
                print!("{:08b}", data);
                //print!("{:02x}", data);
            }
            println!(""); // Starting new line
        }

        font.glyphs.push(GlyphFont {
            code,
            width: bounding_box_width as usize,
            x_start,
            y_start,
            line_bytes: line_number_bytes,
            data: vec,
        });
    }

    // Create rust file
    let mut file = fs::File::create(font_path.join(font_name.to_owned() + ".rs")).unwrap();

    write!(file, "{}", font).unwrap();
}
