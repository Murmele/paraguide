use native_dialog::{FileDialog, MessageDialog};
use std::{fs, io, path::PathBuf};
use std::io::BufRead;
use slint::{Model, StandardListViewItem};
use regex::{Regex, RegexBuilder};

slint::include_modules!();

pub fn logfile_analyzer() -> Result<(), slint::PlatformError> {
    let app = AppWindow::new().unwrap();

    {
        let app_weak = app.as_weak();
        app.on_add_pattern_clicked(move |text| {
            let app = app_weak.unwrap();
            let mut l: Vec<slint::SharedString> = app.get_pattern_list().iter().collect();
            l.push(text.into());
            let model = std::rc::Rc::new(slint::VecModel::from(l));
            app.set_pattern_list(model.into());
        });
    }

    {
        let app_weak = app.as_weak();
        app.on_save_config_clicked(move || {
            let app = app_weak.unwrap();
            let pattern_list = app.get_pattern_list();
            if let Ok(Some(path)) = FileDialog::new().show_save_single_file() {
                let mut contents = String::new();
                for pattern in pattern_list.iter() {
                    contents.push_str(pattern.to_string().as_str());
                    contents.push_str("\n");
                }
                if let Err(e) = fs::write(path, contents) {
                    MessageDialog::new().set_type(native_dialog::MessageType::Error)
                                        .set_title("Unable to save")
                                        .set_text(&format!("Unable to save config file: {:?}", e))
                                        .show_alert()
                                        .unwrap();
                }
            }
        });
    }

    {
        let app_weak = app.as_weak();
        app.on_import_config_clicked(move || {
            let app = app_weak.unwrap();
            let mut l: Vec<slint::SharedString> = app.get_pattern_list().iter().collect();
            l.clear();
            
            if let Some(path) = FileDialog::new().set_location("~").add_filter("Config file", &["logfile_analyzer"]).show_open_single_file().unwrap() {
                let contents = fs::read_to_string(path).unwrap_or("".to_string());
                for line in contents.split("\n") {
                    if line.starts_with("#") {
                        continue;
                    }
                    if line != "\n" && line != "" {
                        l.push(line.into());
                    }
                }
                let model = std::rc::Rc::new(slint::VecModel::from(l));
                app.set_pattern_list(model.into());
            }
        });
    }

    {
        let app_weak = app.as_weak();
        app.on_reimport_logfile_clicked(move || {
            let app = app_weak.unwrap();
            let path = app.get_import_file_path();
            if path.to_string().is_empty() {
                MessageDialog::new().set_type(native_dialog::MessageType::Error)
                                    .set_title("Unable Reimport Logfile")
                                    .set_text(&format!("Unable to reimport logfile, because it must be first imported one."))
                                    .show_alert()
                                    .unwrap(); 
                return;
            }
            let pattern_list = app.get_pattern_list();
            let model = load_log_file(path.to_string().into(), pattern_list);
            app.set_result_list(model.into());
        });
    }

    {
        let app_weak = app.as_weak();
        app.on_import_logfile_clicked(move || {
            let app = app_weak.unwrap();
            
            if let Some(path) = FileDialog::new().set_location("~").add_filter("Config file", &["log"]).show_open_single_file().unwrap() {

                let pattern_list = app.get_pattern_list();
                let model = std::rc::Rc::new(slint::ModelRc::from(load_log_file(path.clone(), pattern_list)));
                app.set_result_list(model.into());
                app.set_import_file_path(path.to_str().unwrap().to_string().into());
            }
        });
    }

    app.run()
}

fn load_log_file(path: PathBuf, pattern_list: slint::ModelRc<slint::SharedString>) -> std::rc::Rc<slint::VecModel<slint::ModelRc<StandardListViewItem>>> {
    let results: std::rc::Rc<slint::VecModel<slint::ModelRc<StandardListViewItem>>> = std::rc::Rc::new(slint::VecModel::default());

    let file = fs::File::open(path).unwrap();
    let reader = io::BufReader::new(file);

    let mut regexes: Vec<Regex> = vec![];
    for pattern in pattern_list.iter() {
        let p = r"^(?<time>[0-9]*(\.[0-9]*){0,1}) (?<content>".to_string() + &pattern.to_string() + ".*)";
        regexes.push(RegexBuilder::new(p.as_str()).build().unwrap());
    }

    let mut time = None;

    for line in reader.lines() {
        if let Ok(line) = &line {
            for re in &regexes {
                if let Some(captures) = &re.captures(line.as_str()) {
                    let new_time = &captures["time"];
                    let new_time = new_time.parse::<f64>().unwrap();

                    let diff;
                    if let Some(time) = time {
                        diff = new_time - time;
                    } else {
                        diff = 0.;
                    }
                    time = Some(new_time);

                    let item = std::rc::Rc::new(slint::VecModel::default());
                    item.push(slint::StandardListViewItem::from(new_time.to_string().as_str()));
                    item.push(slint::StandardListViewItem::from(diff.to_string().as_str()));
                    item.push(slint::StandardListViewItem::from(&captures["content"]));
                    results.push(item.into());
                }
            }
        }
    }
    results
}

#[cfg(test)]
mod tests {
    use super::*;
    use std::path::Path;

    const TEST_DATA: &str = "testlogfile.log";

    #[test]
    fn regex() {
        let pattern_list: Vec<slint::SharedString> = vec![slint::SharedString::from("State: ")];
        let pattern_model = std::rc::Rc::new(slint::VecModel::from(pattern_list));
        let manifest_path = Path::new(env!("CARGO_MANIFEST_DIR"));
        let test_data_path = manifest_path.join(TEST_DATA);
        let results = load_log_file(test_data_path, pattern_model.into());
        assert!(results.len() > 0);
    }
}
