fn main() {
    println!("cargo:rustc-env=SLINT_BACKEND=GL");
    slint_build::compile("ui/mainwindow.slint").expect("Slint build failed");
}
