use eg_font_converter::{FontConverter, Mapping};
use std::fs;
use std::path::Path;

fn convert_font(font_file_name: &str, font_const_name: &str) {
    let font_path = Path::new(env!("CARGO_MANIFEST_DIR"))
        .parent()
        .unwrap()
        .parent()
        .unwrap()
        .join("steigen3000")
        .join("ui")
        .join("fonts");

    let profont_bdf = font_path.join(format!("{font_file_name}.bdf"));
    let export_path = font_path.join(format!("{font_const_name}_export"));
    fs::create_dir_all(export_path.clone()).unwrap();

    // Convert to eg-bdf fonts.
    // let font = FontConverter::new(profont_bdf.clone(), font_name)
    //     //.file_stem("font_6x10")
    //     .glyphs(Mapping::Ascii)
    //     .missing_glyph_substitute('?')
    //     .convert_eg_bdf()
    //     .unwrap();

    //     font.save(&export_path).unwrap();

    // // Convert to Font fonts.
    let font = FontConverter::new(profont_bdf, format!("{font_const_name}_MONO").as_str())
        // .file_stem("font_6x10_mono")
        .glyphs(Mapping::Ascii)
        .missing_glyph_substitute(' ')
        .convert_mono_font()
        .unwrap();

    font.save(&export_path).unwrap();
}

fn main() {
    let font_file_name = "profont-24";
    let font_rust_const_name = "PROFONT_24";
    convert_font(font_file_name, font_rust_const_name);

    let font_file_name = "profont-60";
    let font_rust_const_name = "PROFONT_60";
    convert_font(font_file_name, font_rust_const_name);

    let font_file_name = "profont_ascii";
    let font_rust_const_name = "PROFONT";
    convert_font(font_file_name, font_rust_const_name);
}
