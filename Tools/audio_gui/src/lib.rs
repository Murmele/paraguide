use std::cell::RefCell;
use std::rc::Rc;
mod usb_cdc;
mod worker;

slint::include_modules!();

pub fn audio_gui() -> Result<(), slint::PlatformError> {
    // Start worker thread
    let worker = Rc::new(RefCell::new(worker::Worker::new()));

    let app = AppWindow::new().unwrap();

    {
        let w = worker.clone();
        app.on_frequency_changed(move |frequency| {
            w.borrow_mut().set_frequency(frequency as f64);
        });
    }

    {
        let w = worker.clone();
        app.on_duty_changed(move |duty| {
            w.borrow_mut().set_duty(duty as usize);
        });
    }

    {
        let w = worker.clone();
        app.on_on_time_changed(move |on_time| {
            w.borrow_mut().set_on_time(on_time as f64);
        });
    }

    {
        let w = worker.clone();
        app.on_off_time_changed(move |off_time| {
            w.borrow_mut().set_off_time(off_time as f64);
        });
    }

    {
        let w = worker.clone();
        app.on_mute_changed(move |mute| {
            w.borrow_mut().set_mute(mute);
        });
    }

    {
        let w = worker.clone();
        app.on_lowpass_changed(move |lowpass| {
            w.borrow_mut().set_lowpass(lowpass);
        });
    }

    {
        let w = worker.clone();
        let app_weak = app.as_weak();
        app.on_vertical_velocity_changed(move |v| {
            let s = w.borrow_mut().set_vertical_velocity(v as f64);
            let app = app_weak.unwrap();
            app.set_frequency(s.frequency.into());
            app.set_on_time(s.on_time_ms.into());
            app.set_off_time(s.off_time_ms.into());
            app.set_duty(s.duty.into());
        });
    }

    app.run()
}
