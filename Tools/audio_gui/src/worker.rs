use crate::usb_cdc::CdcDevice;
use std::thread;
use steigen3000::paraguidecore::paraguide::{VarioSound, VarioSoundSettings};
use tokio::sync::watch;

#[derive(Clone)]
enum Command {
    Vario(f32),
    Sound(VarioSoundSettings),
    Mute,
    LowPass(bool),
}

pub struct Worker {
    #[allow(dead_code)]
    worker_thread: Option<std::thread::JoinHandle<()>>,
    vario_signal_tx: watch::Sender<Command>,
    vario_sound_settings: VarioSoundSettings,
    muted: bool,
}

// Not required, because when vario_signal_tx drops, the thread detects it and stops automatically
// impl Drop for Worker {
//     fn drop(&mut self) {
//         self.worker_thread.take().unwrap().join().unwrap();
//     }
// }

impl Worker {
    pub fn new() -> Self {
        let (vario_signal_tx, vario_signal_rx) = watch::channel(Command::Vario(0.0));
        let worker_thread = thread::spawn({
            move || {
                tokio::runtime::Builder::new_multi_thread()
                    .enable_all()
                    .build()
                    .unwrap()
                    .block_on(Self::thread(vario_signal_rx))
            }
        });

        Self {
            worker_thread: Some(worker_thread),
            vario_signal_tx,
            vario_sound_settings: VarioSoundSettings {
                frequency: 100,
                on_time_ms: 100,
                off_time_ms: 100,
                duty: 500,
            },
            muted: false,
        }
    }

    pub fn set_vertical_velocity(&mut self, v: f64) -> VarioSoundSettings {
        if !self.muted {
            self.vario_signal_tx.send(Command::Vario(v as f32)).unwrap();
        }
        let mut s = VarioSound::new();
        self.vario_sound_settings = s.get(v);
        self.vario_sound_settings.clone()
    }

    pub fn set_frequency(&mut self, frequency: f64) {
        self.vario_sound_settings.frequency = frequency as u16;
        if !self.muted {
            self.vario_signal_tx
                .send(Command::Sound(self.vario_sound_settings.clone()))
                .unwrap();
        }
    }

    pub fn set_duty(&mut self, duty: usize) {
        self.vario_sound_settings.duty = duty as u16;
        if !self.muted {
            self.vario_signal_tx
                .send(Command::Sound(self.vario_sound_settings.clone()))
                .unwrap();
        }
    }

    pub fn set_on_time(&mut self, on_time: f64) {
        self.vario_sound_settings.on_time_ms = on_time as u16;
        if !self.muted {
            self.vario_signal_tx
                .send(Command::Sound(self.vario_sound_settings.clone()))
                .unwrap();
        }
    }

    pub fn set_off_time(&mut self, off_time: f64) {
        self.vario_sound_settings.off_time_ms = off_time as u16;
        if !self.muted {
            self.vario_signal_tx
                .send(Command::Sound(self.vario_sound_settings.clone()))
                .unwrap();
        }
    }

    pub fn set_mute(&mut self, mute: bool) {
        self.muted = mute;
        if mute {
            self.vario_signal_tx.send(Command::Mute).unwrap();
        } else {
            self.vario_signal_tx
                .send(Command::Sound(self.vario_sound_settings.clone()))
                .unwrap();
        }
    }

    pub fn set_lowpass(&mut self, lowpass: bool) {
        self.vario_signal_tx
            .send(Command::LowPass(lowpass))
            .unwrap();
    }

    fn lowpass_vario_value(
        mut vario_value_curr: f32,
        vario_value_should: f32,
        vario_step: f32,
    ) -> f32 {
        if vario_value_curr < vario_value_should {
            vario_value_curr += vario_step;
            if vario_value_curr > vario_value_should {
                vario_value_curr = vario_value_should;
            }
        } else if vario_value_curr > vario_value_should {
            vario_value_curr -= vario_step;
            if vario_value_curr < vario_value_should {
                vario_value_curr = vario_value_should;
            }
        }
        vario_value_curr
    }

    async fn thread(mut vario_signal_rx: watch::Receiver<Command>) {
        const VARIO_STEP: f32 = 0.2;
        let mut vario_value_curr = 0.;
        let mut vario_value_should = 0.;
        let mut s = VarioSound::new();
        #[cfg(not(feature = "usb"))]
        {
            let (vario_sound_signal_tx, vario_sound_signal_rx) =
                watch::channel(VarioSoundSettings {
                    frequency: 0,
                    on_time_ms: 0,
                    off_time_ms: 0,
                });

            tokio::spawn(async move {
                loop {
                    // Low pass filter
                    vario_value_should = *vario_signal_rx.borrow();
                    vario_value_curr = Self::calculate_new_vario_value(
                        vario_value_curr,
                        vario_value_should,
                        VARIO_STEP,
                    );
                    let vario_sound_settings = s.get(vario_value_curr as f64);
                    vario_sound_signal_tx.send(vario_sound_settings).unwrap();
                    tokio::time::sleep(std::time::Duration::from_millis(20)).await;
                }
            });

            steigen3000::platform::main::buzzer(vario_sound_signal_rx).await;
        }

        #[cfg(feature = "usb")]
        {
            const USB_VID: u16 = 0xc0de;
            const USB_PID: u16 = 0xcafe;
            let mut d = [0u8; 8];
            let mut lowpass = false;

            let mut device: Result<CdcDevice, ()> = Err(());

            loop {
                let vario_sound_settings;
                if !lowpass {
                    if let Err(_) = vario_signal_rx.changed().await {
                        // Finish, because sender is anymore available, so the other thread is finished
                        break;
                    }
                }
                let v = vario_signal_rx.borrow_and_update();
                let c = v.clone();
                drop(v);

                match c {
                    Command::Sound(v) => {
                        vario_sound_settings = v;
                    }
                    Command::Vario(v) => {
                        vario_value_should = v;
                        if lowpass {
                            vario_value_curr = Self::lowpass_vario_value(
                                vario_value_curr,
                                vario_value_should,
                                VARIO_STEP,
                            );
                        } else {
                            vario_value_curr = vario_value_should;
                        }

                        vario_sound_settings = s.get(vario_value_curr.into());
                    }
                    Command::Mute => {
                        vario_sound_settings = VarioSoundSettings {
                            frequency: 0,
                            on_time_ms: 0,
                            off_time_ms: 0,
                            duty: 50,
                        };
                    }
                    Command::LowPass(l) => {
                        lowpass = l;
                        continue;
                    }
                }

                let data = vario_sound_settings.frequency.to_le_bytes();
                d[0] = data[0];
                d[1] = data[1];
                let data = vario_sound_settings.duty.to_le_bytes();
                d[2] = data[0];
                d[3] = data[1];
                let data = vario_sound_settings.on_time_ms.to_le_bytes();
                d[4] = data[0];
                d[5] = data[1];
                let data = vario_sound_settings.off_time_ms.to_le_bytes();
                d[6] = data[0];
                d[7] = data[1];

                if device.is_err() {
                    device = CdcDevice::new(USB_VID, USB_PID).map_err(|_| ());
                }

                if let Ok(dv) = device.as_mut() {
                    if let Err(_) = dv.write(&d) {
                        device = Err(());
                    }
                }

                if lowpass {
                    tokio::time::sleep(std::time::Duration::from_millis(20)).await;
                }
            }
        }
    }
}
