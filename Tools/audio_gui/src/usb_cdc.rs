use rusb::{Context, Device, DeviceHandle, Error, UsbContext};
use std::time::Duration;

pub struct CdcDevice {
    handle: DeviceHandle<Context>,
    #[allow(unused)]
    in_endpoint: u8,
    out_endpoint: u8,
    interface: u8,
}

impl CdcDevice {
    pub fn new(vid: u16, pid: u16) -> Result<Self, Error> {
        let context = Context::new()?;

        // Find the USB device
        let device = find_device(&context, vid, pid)?;
        let config_desc = device.active_config_descriptor()?;

        // Get the first CDC interface
        let interface = config_desc
            .interfaces()
            .find(|interface| {
                interface.descriptors().any(|desc| {
                    desc.class_code() == 0x0A // CDC Data Interface Class
                })
            })
            .ok_or(Error::NotFound)?;

        let interface_number = interface.number();

        // Find the endpoints
        let interface_desc = interface.descriptors().next().ok_or(Error::NotFound)?;

        let mut in_endpoint = 0;
        let mut out_endpoint = 0;

        for endpoint in interface_desc.endpoint_descriptors() {
            if endpoint.direction() == rusb::Direction::In {
                in_endpoint = endpoint.address();
            } else {
                out_endpoint = endpoint.address();
            }
        }

        let handle = device.open()?;

        // Detach kernel driver if necessary
        if handle.kernel_driver_active(interface_number)? {
            handle.detach_kernel_driver(interface_number)?;
        }

        handle.claim_interface(interface_number)?;

        Ok(CdcDevice {
            handle,
            in_endpoint,
            out_endpoint,
            interface: interface_number,
        })
    }

    pub fn write(&self, data: &[u8]) -> Result<usize, Error> {
        self.handle
            .write_bulk(self.out_endpoint, data, Duration::from_secs(1))
    }

    #[allow(unused)]
    pub fn read(&self, buf: &mut [u8]) -> Result<usize, Error> {
        self.handle
            .read_bulk(self.in_endpoint, buf, Duration::from_secs(1))
    }
}

impl Drop for CdcDevice {
    fn drop(&mut self) {
        let _ = self.handle.release_interface(self.interface);
        if let Ok(true) = self.handle.kernel_driver_active(self.interface) {
            let _ = self.handle.attach_kernel_driver(self.interface);
        }
    }
}

fn find_device<T: UsbContext>(context: &T, vid: u16, pid: u16) -> Result<Device<T>, Error> {
    let devices = context.devices()?;

    for device in devices.iter() {
        let device_desc = device.device_descriptor()?;

        if device_desc.vendor_id() == vid && device_desc.product_id() == pid {
            return Ok(device);
        }
    }

    Err(Error::NotFound)
}
